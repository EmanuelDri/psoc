//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules


void main(void)
{
	// M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	static long indice=0;
	PWM1_Start();
	PWM2_Start();
	LCD_Start();
	LCD_Position(0,0);
	LCD_PrCString("Error de conf");
	LCD_Position(1,0);
	LCD_PrCString("Error 404");
	
	while(1){
		PRT0DR^=0x02;
		PRT0DR^=0x04;
		for(indice=0;indice<40000;indice++);
	}
		
}
