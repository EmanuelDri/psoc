//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules


void main(void)
{
	// M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	PWM8_1_Start();
	amplinoInversor_Start(amplinoInversor_HIGHPOWER);
//	ampliSC_Start(ampliSC_HIGHPOWER);
//	ampliInversorSC_Start(ampliInversorSC_HIGHPOWER);
//	integradorSC_Start(integradorSC_HIGHPOWER);
	comparadorSC_Start(comparadorSC_HIGHPOWER);
	bufferDigital_Start();
//	amplidelComparador_Start(amplidelComparador_HIGHPOWER);
	BPF2_1_Start(BPF2_1_HIGHPOWER);

}
