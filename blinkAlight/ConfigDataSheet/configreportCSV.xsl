<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
	xmlns:user="http://tempuri.net/cypressScriptFunctions" version="1.0">
<xsl:output method="text" indent="no"   />

<xsl:template match="/">"<xsl:value-of select="//CMX_PROJECT/CMX_MAKER_APPLICATION/@NAME"/>"<xsl:text>&#13;&#10;</xsl:text>
"Last Built:","<xsl:value-of select="//CMX_PROJECT/@BUILD_DATE"/>"<xsl:text>&#13;&#10;</xsl:text>
"Project Path:","<xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH"/>"<xsl:text>&#13;&#10;</xsl:text>
"Project Name:","<xsl:value-of select="//CMX_PROJECT/@NAME"/>"<xsl:text>&#13;&#10;</xsl:text>
<xsl:text>&#13;&#10;</xsl:text><xsl:text>&#13;&#10;</xsl:text>
	
<xsl:if test="count(//CMX_BOM_LIST/CMX_BOM) > 0" >
		<xsl:for-each select="//CMX_BOM_LIST/CMX_BOM"><xsl:if test="position()=1"><xsl:for-each select="CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN"><xsl:if test="position()!=1">,</xsl:if>"<xsl:value-of select="@NAME" />"</xsl:for-each><xsl:text>&#13;&#10;</xsl:text></xsl:if>"<xsl:value-of select="./@INSTANCE_NAME"/>_<xsl:value-of select="./CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME='Label']/@CONTENTS"/>"<xsl:for-each select="./CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME != 'Label']">,"<xsl:value-of select="./@CONTENTS"/>"</xsl:for-each><xsl:text>&#13;&#10;</xsl:text></xsl:for-each></xsl:if></xsl:template>

</xsl:stylesheet>
