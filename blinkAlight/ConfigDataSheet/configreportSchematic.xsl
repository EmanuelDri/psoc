<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="xml" version="1.0" indent="yes" encoding="utf-8" 
       doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" 
       doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />

  <xsl:variable name="g_maxLabelWidth">185</xsl:variable>

  <xsl:template match="/">

    <HTML id="main" xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>configreportSchematic</title>
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"></META>
        <META HTTP-EQUIV="Expires" CONTENT="-1"></META>

        <STYLE>

          .page
          {
          page-break-before: always;
          }

          body,
          td
          {
          font-family: verdana;
          font-size: 10px;
          }

          .brand,
          .brandSubPage
          {
          padding-top: 22px;
          padding-bottom: 4px;
          color:#ffffff;
          background-color:#09367A;
          border-width:1px;
          border-style:solid;
          border-color:#09367A;
          }

          .brandSubPage
          {
          margin-top:40px;
          }


          .copyright
          {
          text-align:right;
          font-size:7pt;
          margin-top:5px;
          padding-right: 5px;
          margin-bottom: 30px;
          }

          .trademark
          {
          vertical-align:super;
          font-size:6pt;
          }

          h1,
          .h1
          {
          font-size:22px;
          font-weight:bold;
          font-family:arial;
          }

          .reportTitle
          {
          margin-top:5px;
          }

          h2
          {
          font-family:arial;
          font-size:17px;
          font-weight:bold;
          }

          h1,
          h2
          {
          padding-left:16px;
          }

          .figureLabel,
          .label
          {
          font-family:arial;
          font-size: 11px;
          padding-left:16px;
          }

          .label
          {
          font-weight: bold;
          }

          .figureLabel,
          {
          padding-top:5px;
          }

          .columnHeading,
          {
          padding-left:16px;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          .cellForDeviceLabel,
          .cell,
          .pinoutHeading,
          th,
          .tableHeading,
          {
          padding-top:4px;
          padding-bottom:4px;
          /*
          border-bottom-width:1px;
          border-bottom-style:solid;
          border-bottom-color:#ffffff;
          */        }

          #tdRegisterProtocolDescription,
          #tdFlashUpdateProtocol
          {
          padding-left: 16px;
          padding-right:10px;
          padding-top:10px;
          padding-bottom:10px;
          background-color:#FFFFFF;
          color:#000000;
          font-weight: normal;
          }


          .cell,
          .cellForDeviceLabel,
          {
          color:black;
          text-align:center;
          background-color: e5e5e5;
          }

          .cellForDeviceLabel
          {
          color: black;
          font-weight: bold;
          text-align:left;
          padding-left:16px;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          .pinoutHeading,
          .dataTables,
          .tableHeading
          {
          color: white;
          font-family:arial;
          font-size: 11px;
          background-color:999999;
          font-weight: bold;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          {
          text-align: center;
          }

          .columnHeadingForDeviceLabel
          {
          text-align: left;
          padding-left:16px;
          }

          .notes
          {
          padding-top:16px;
          padding-left:16px;
          padding-bottom:10px;
          }

          .containerWidth,
          .brand,
          .brandSubPage,
          .copyright,
          .dataTables,
          .projectTable,
          .tableHeading,
          .descriptionTables
          {
          width:900px;
          }

          .projectTable,
          .descriptionTables
          {
          border-width:1px;
          border-style:solid;
          border-color:#999999;
          }

          .dataTables
          {
          border-collapse: collapse;
          border-width:1px;
          border-style:solid;
          border-color:#ffffff;
          }

          th,
          .pinoutHeading,
          .tableHeading
          {
          text-align:left;
          padding-left:16px;
          font-size:11pt;
          background-color:#6A6A6A;
          }

          .pdfDIV
          {
          margin-left: 16px;
          }

          .pdfLabel
          {
          font-size:9px;
          }

          a.pdfLabel
          {
          text-decoration: none;
          color:black;
          }

          a.pdfLabel:hover
          {
          text-decoration: underline;
          }

        </STYLE>

        <xsl:text disable-output-escaping="yes">
	<![CDATA[
	<script type="text/javascript" language="javascript">
		
		function GeneratePdf(filename)
		{
         window.location.href = "PDF/GeneratePDF.htm?filename=" + filename;
		}

		
	</script>
	]]>
	</xsl:text>
      </head>

      <BODY width="800px">

        <div class="brand">
          <h1>
            PSoC Designer<span class="trademark">TM</span>
          </h1>
        </div>
        <div class="copyright">Cypress Semiconductor Corporation Copyright 2007-2008</div>

        <H1>Schematic</H1>
        <div class="pdfDIV">
          <a id="PrintButon" onClick="return GeneratePdf('configreportSchematic');" href="#" 
                        class="pdfLabel">
            <img border="0" alt="Print to PDF" src="PDF/pdf_button.png" width="16" height="16" />  Print to PDF
          </a>
        </div>
        <br></br>
        <table summary="table1" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td id="outerCell">
              <table summary="table2">
                <tr>
                  <td class="label">Last Built: </td>
                  <td>
                    <xsl:value-of select="//CMX_PROJECT/@BUILD_DATE" />
                  </td>
                </tr>
                <tr>
                  <td class="label">Project Path: </td>
                  <td>
                    <xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH" />
                  </td>
                </tr>
                <tr>
                  <td class="label">Project Name: </td>
                  <td>
                    <xsl:value-of select="//CMX_PROJECT/@NAME" />
                  </td>
                </tr>
              </table>
              <br />

              <TABLE id="svg" BORDER="1" CELLSPACING="0" WIDTH="900" style=" ">
                <TR ALIGN="CENTER">
                  <TD>
                    <embed>
                      <xsl:attribute name="src">Pinout.svg</xsl:attribute>
                      <xsl:attribute name="name">Pinout</xsl:attribute>
                      <xsl:attribute name="id">Pinout</xsl:attribute>
                      <xsl:attribute name="width">400</xsl:attribute>
                      <xsl:attribute name="height">450</xsl:attribute>
                      <xsl:attribute name="type"><![CDATA[image/svg-xml]]> </xsl:attribute>
                    </embed>
                  </TD>
                  <TD>
                    <div style="padding-bottom: 50px; text-align: center">
                        <xsl:value-of select="//CMX_PROJECT/@NAME" />
                    </div>
                    <img>
                      <xsl:attribute name="src">
                        ProjectSchematic_<xsl:value-of select="//BASEPROJ_FILE_PATH/@BASEPROJ_PSOC_PART" />.gif
                      </xsl:attribute>

                    </img>
                    <div style="padding-top: 30px; text-align: center">
                        Recommended Support
                    </div>
                    <div style="text-align: center">
                        Components
                    </div>
                  </TD>
                </TR>

              </TABLE>

              <xsl:if test="count(//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT) > 0" >
                <br />
                <br />
                <br />
                <br />
                <br />
                <span id="Header">
                  <center>
                    <h2 class="pinoutHeading">
                      Driver Schematics
                    </h2>
                  </center>
                </span>
                <div id="table1" >
                  <xsl:call-template name="DrawTable">
                    <xsl:with-param name="Content">Images</xsl:with-param>
                  </xsl:call-template>
                </div>
              </xsl:if>

              <br />
              <br />
            </td>
          </tr>
        </table>

      </BODY>
    </HTML>
  </xsl:template>
  <xsl:template name="DrawTable" >




    <TABLE CELLSPACING="0" cellpadding="7" style="margin-top: -35px; border-collapse: collapse; border: 1px black solid;">
      <xsl:for-each select="//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='DRIVER_SCHEM_BOM']">
        <!-- if we're starting a new row -->
        <xsl:if test="((position() mod 3) = 1)">

          <!-- close out the old row (if there was a previous row) -->
          <xsl:if test="(position() != 1)">
            <xsl:text  disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
          </xsl:if>

          <!-- If we should close out the table + start a new one (2 rows/table) do that -->
          <xsl:if test="((position() mod 6) = 1)">
            <xsl:text  disable-output-escaping="yes">&lt;/table&gt;&lt;br /&gt;&lt;font style="font-size: 12px;"&gt;&lt;br /&gt;&lt;/font&gt;&lt;br /&gt;</xsl:text>
            <xsl:text  disable-output-escaping="yes">&lt;table CELLSPACING="0" cellpadding="7" style="border-collapse: collapse; border: 1px black solid;" &gt;</xsl:text>
          </xsl:if>

          <!-- Open the new row -->
          <xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
        </xsl:if>

        <xsl:variable name="uniqueId">
          <xsl:value-of select="generate-id(.)" />
        </xsl:variable>

        <xsl:variable name="INST_NAME">
          <xsl:value-of select="@INSTANCE_NAME" />
        </xsl:variable>
        <xsl:variable name="OUTPUT_TEXT">
          <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_OUTPUT']) > 0" >
            <xsl:value-of select="$INST_NAME" />
          </xsl:if>
        </xsl:variable>
        <xsl:variable name="INPUT_TEXT">
          <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_INPUT']) > 0" >
            <xsl:value-of select="$INST_NAME" />
          </xsl:if>
        </xsl:variable>

        <TD ALIGN="LEFT" VALIGN="TOP" class="ImageCell">
          <!--
          <xsl:choose>
            <xsl:when test="$Content ='Labels'">
              <xsl:attribute name="style">
                width: 289px; height: 330px;
              </xsl:attribute>
              <xsl:variable name="INST_NAME">
                <xsl:value-of select="@INSTANCE_NAME" />
              </xsl:variable>
              <xsl:variable name="OUTPUT_TEXT">
                <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_OUTPUT']) > 0" >
                  <xsl:value-of select="$INST_NAME" />
                </xsl:if>
              </xsl:variable>
              <xsl:variable name="INPUT_TEXT">
                <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_INPUT']) > 0" >
                  <xsl:value-of select="$INST_NAME" />
                </xsl:if>
              </xsl:variable>
              <center>
                <xsl:value-of select="$INST_NAME" />
              </center>
              <br	/>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <table border="0" width="100%">
                <tr>
                  <td>
                    <xsl:if test="string-length($OUTPUT_TEXT) != 0">
                      <xsl:value-of select="$OUTPUT_TEXT" />
                    </xsl:if>
                    <xsl:text  disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td>
                    <xsl:text  disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td align="right">
                    <xsl:if test="string-length($INPUT_TEXT) != 0">
                      <xsl:value-of select="$INPUT_TEXT" />
                    </xsl:if>
                  </td>
                </tr>
              </table>
              <br />
            </xsl:when>
            <xsl:otherwise>
              -->

          <!-- 
          <xsl:attribute name="style">
            width: 289px; height: 330px;
          </xsl:attribute>  -->

          <div style="text-align:center;width:289px;padding-bottom:3px">
            <xsl:value-of select="$INST_NAME" />
          </div>
          <div style="position: relative">
            <img hspace="0" vspace="0" border="0">
              <xsl:attribute name="src">
                <xsl:value-of select="@SCHEMATIC_PATH" />
              </xsl:attribute>
            </img>

            <xsl:choose>
              <xsl:when test="string-length(@NAME_LOCATION_SPEC) = 0">
                <xsl:if test="string-length($INPUT_TEXT) != 0">
                  <div>

                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: 130px; left: 100px; text-align: right; width:', $g_maxLabelWidth)"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$INPUT_TEXT" />
                  </div>
                </xsl:if>

                <xsl:if test="string-length($OUTPUT_TEXT) != 0">
                  <div>
                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: 130px; left: 1px; text-align: left; width: ', $g_maxLabelWidth)"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$OUTPUT_TEXT" />
                  </div>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <!-- Handle absolute placement of schematic labels.  Placement details are located in an external XML file loaded below by the document() function. -->
                <xsl:for-each select="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION">
                  <div>
                    <xsl:variable name="locationX">
                      <xsl:choose>
                        <xsl:when test="@JUSTIFY='RIGHT'">
                          <xsl:value-of select="number(@LOCATION_X) - $g_maxLabelWidth"></xsl:value-of>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="@LOCATION_X"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>
                    
                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: ', @LOCATION_Y, 'px; left: ', $locationX, 'px; text-align:', @JUSTIFY, '; width:', $g_maxLabelWidth, 'px')"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$INST_NAME" />
                    <xsl:value-of select="@APPEND_STRING" />
                  </div>
                </xsl:for-each>
              </xsl:otherwise>
            </xsl:choose>
          </div>


          <!--
            </xsl:otherwise>
          </xsl:choose>
          -->
        </TD>
      </xsl:for-each>
      <xsl:if test="(count(//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='DRIVER_SCHEM_BOM']) mod 3) != 1">
        <xsl:text  disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
      </xsl:if>
    </TABLE>
    <TABLE CELLSPACING="0" cellpadding="7" style="margin-top: -35px; border-collapse: collapse; border: 1px black solid;">
      <xsl:for-each select="//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='CHANNEL_SCHEM_BOM']">
        <!-- if we're starting a new row -->
        <xsl:if test="((position() mod 3) = 1)">

          <!-- close out the old row (if there was a previous row) -->
          <xsl:if test="(position() != 1)">
            <xsl:text  disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
          </xsl:if>

          <!-- If we should close out the table + start a new one (2 rows/table) do that -->
          <xsl:if test="((position() mod 6) = 1)">
            <xsl:text  disable-output-escaping="yes">&lt;/table&gt;&lt;br /&gt;&lt;font style="font-size: 12px;"&gt;&lt;br /&gt;&lt;/font&gt;&lt;br /&gt;</xsl:text>
            <xsl:text  disable-output-escaping="yes">&lt;table CELLSPACING="0" cellpadding="7" style="border-collapse: collapse; border: 1px black solid;" &gt;</xsl:text>
          </xsl:if>

          <!-- Open the new row -->
          <xsl:text disable-output-escaping="yes">&lt;tr&gt;</xsl:text>
        </xsl:if>

        <xsl:variable name="uniqueId">
          <xsl:value-of select="generate-id(.)" />
        </xsl:variable>

        <xsl:variable name="INST_NAME">
          <xsl:value-of select="@INSTANCE_NAME" />
        </xsl:variable>
        <xsl:variable name="OUTPUT_TEXT">
            <xsl:value-of select="$INST_NAME" />
        </xsl:variable>
        <xsl:variable name="INPUT_TEXT">
        </xsl:variable>

        <TD ALIGN="LEFT" VALIGN="TOP" class="ImageCell">
          <!--
          <xsl:choose>
            <xsl:when test="$Content ='Labels'">
              <xsl:attribute name="style">
                width: 289px; height: 330px;
              </xsl:attribute>
              <xsl:variable name="INST_NAME">
                <xsl:value-of select="@INSTANCE_NAME" />
              </xsl:variable>
              <xsl:variable name="OUTPUT_TEXT">
                <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_OUTPUT']) > 0" >
                  <xsl:value-of select="$INST_NAME" />
                </xsl:if>
              </xsl:variable>
              <xsl:variable name="INPUT_TEXT">
                <xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_INPUT']) > 0" >
                  <xsl:value-of select="$INST_NAME" />
                </xsl:if>
              </xsl:variable>
              <center>
                <xsl:value-of select="$INST_NAME" />
              </center>
              <br	/>
              <br />
              <br />
              <br />
              <br />
              <br />
              <br />
              <table border="0" width="100%">
                <tr>
                  <td>
                    <xsl:if test="string-length($OUTPUT_TEXT) != 0">
                      <xsl:value-of select="$OUTPUT_TEXT" />
                    </xsl:if>
                    <xsl:text  disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td>
                    <xsl:text  disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                  </td>
                  <td align="right">
                    <xsl:if test="string-length($INPUT_TEXT) != 0">
                      <xsl:value-of select="$INPUT_TEXT" />
                    </xsl:if>
                  </td>
                </tr>
              </table>
              <br />
            </xsl:when>
            <xsl:otherwise>
              -->

          <!-- 
          <xsl:attribute name="style">
            width: 289px; height: 330px;
          </xsl:attribute>  -->

          <div style="text-align:center;width:289px;padding-bottom:3px">
            <xsl:value-of select="$INST_NAME" />
          </div>
          <div style="position: relative">
            <img hspace="0" vspace="0" border="0">
              <xsl:attribute name="src">
                <xsl:value-of select="@SCHEMATIC_PATH" />
              </xsl:attribute>
            </img>

            <xsl:choose>
              <xsl:when test="string-length(@NAME_LOCATION_SPEC) = 0">
                <xsl:if test="string-length($INPUT_TEXT) != 0">
                  <div>

                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: 130px; left: 100px; text-align: right; width:', $g_maxLabelWidth)"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$INPUT_TEXT" />
                  </div>
                </xsl:if>

                <xsl:if test="string-length($OUTPUT_TEXT) != 0">
                  <div>
                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: 130px; left: 1px; text-align: left; width: ', $g_maxLabelWidth)"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$OUTPUT_TEXT" />
                  </div>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <!-- Handle absolute placement of schematic labels.  Placement details are located in an external XML file loaded below by the document() function. -->
                <xsl:for-each select="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION">
                  <div>
                    <xsl:variable name="locationX">
                      <xsl:choose>
                        <xsl:when test="@JUSTIFY='RIGHT'">
                          <xsl:value-of select="number(@LOCATION_X) - $g_maxLabelWidth"></xsl:value-of>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="@LOCATION_X"/>
                        </xsl:otherwise>
                      </xsl:choose>
                    </xsl:variable>

                    <xsl:attribute name="style">
                      <xsl:value-of select="concat('position: absolute; top: ', @LOCATION_Y, 'px; left: ', $locationX, 'px; text-align:', @JUSTIFY, '; width:', $g_maxLabelWidth, 'px')"></xsl:value-of>
                    </xsl:attribute>
                    <xsl:value-of select="$INST_NAME" />
                    <xsl:value-of select="@APPEND_STRING" />
                  </div>
                </xsl:for-each>
              </xsl:otherwise>
            </xsl:choose>
          </div>


          <!--
            </xsl:otherwise>
          </xsl:choose>
          -->
        </TD>
      </xsl:for-each>
      <xsl:if test="(count(//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='CHANNEL_SCHEM_BOM']) mod 3) != 1">
        <xsl:text  disable-output-escaping="yes">&lt;/tr&gt;</xsl:text>
      </xsl:if>
    </TABLE>

  </xsl:template>
</xsl:stylesheet>
