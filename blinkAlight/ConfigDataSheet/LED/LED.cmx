<!--
******************************************************************************
******************************************************************************
**  FILENAME: LED.cmx
**  Version: 1.0, Updated on 2009/10/29 at 15:6:51
**
**  DESCRIPTION: 
**    XML description of LED Driver.
**
** - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
**  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
******************************************************************************
******************************************************************************
-->
<CMX_DB>
<CMX_IO_DRIVER 
			 NAME             = "LED"
             DISPLAY_NAME     = "On/Off"
             VERSION          = "1.0"
             IO_CHANNEL_TYPE  = "CMX_DIGITAL_IO_CHAN"
             IMAGE            = "LED_icon.gif"
             WIDGET           = ""
			 DOC_FILE         = ""
			 HTML		      = "LED_datasheet.htm"
			 DRIVER_CATEGORY  = "Display\LED\Single Color"
			 DRIVER_TYPE	  = "CMX_OUTPUT"
             VALUE_TYPE       = "DISCRETE"
			 ORDINAL          = "10"

>

	<CMX_PROPERTY_LIST>
	<CMX_PROPERTY NAME="Current Mode" TYPE="ENUM" DEFAULT_VALUE="Sourcing" COMMENT="Select PSoC pin current mode" SCHEM_BOM_KEY="TRUE">
			<CMX_PROPERTY_VALUE_LIST>
				<CMX_PROPERTY_VALUE NAME="Sourcing" VALUE="1" ORDER="0"/>
				<CMX_PROPERTY_VALUE NAME="Sinking" VALUE="0"  ORDER="1"/>
			</CMX_PROPERTY_VALUE_LIST>
		</CMX_PROPERTY>
	</CMX_PROPERTY_LIST>
	
	<CMX_HIDDEN_PROPERTY_LIST>
		
		<CMX_PROPERTY NAME="InitialState" TYPE="INT" DEFAULT_VALUE = "1" MIN ="0" MAX = "1" COMMENT="Initial pin state"/>
		<CMX_PROPERTY NAME="DriveMode" TYPE="INT" DEFAULT_VALUE = "1" MIN ="0" MAX = "7" COMMENT="PSoC drive mode 0 - 7"/>
		<CMX_PROPERTY NAME="ReadPolarity" TYPE="INT" DEFAULT_VALUE = "0" MIN ="0" MAX = "1" COMMENT="Read Polarity  0 => Invert,  1 => Non-Invert"/>
	</CMX_HIDDEN_PROPERTY_LIST>
	
	<CMX_TEMPLATE_FILE_LIST>
		<CMX_TEMPLATE_FILE PATH="LEDFrags.tpl" PREFIX="" TARGET_FILE_NAME="" FILE_TYPE="SOURCE_FILE" />
		<CMX_TEMPLATE_FILE PATH="CMX_DRIVER_CATALOG\CMX\DIO\dio.h" PREFIX="" TARGET_FILE_NAME="dio.h" FILE_TYPE="" />
		<CMX_TEMPLATE_FILE PATH="CMX_DRIVER_CATALOG\CMX\DIO\dio.c" PREFIX="" TARGET_FILE_NAME="dio.c" FILE_TYPE="" />
	</CMX_TEMPLATE_FILE_LIST>
	
	<CMX_VALUE_LIST>
		<CMX_VALUE NAME="OFF" TYPE="DISCRETE" VALUE="0" ORDER="0"/>
		<CMX_VALUE NAME="ON" TYPE="DISCRETE" VALUE="1" ORDER="1"/>
	</CMX_VALUE_LIST>

	<CMX_SCHEM_BOM_LIST>
		<CMX_SCHEM_BOM NAME="LED" KEY_VALUE="Sinking" SCHEMATIC_PATH="LED\LED_sinking_schematic.gif" BOM_PATH="LED_BOM.xml"/>
		<CMX_SCHEM_BOM NAME="LED" KEY_VALUE="Sourcing" SCHEMATIC_PATH="LED\LED_sourcing_schematic.gif" BOM_PATH="LED_BOM.xml"/>
	</CMX_SCHEM_BOM_LIST>

	<CMX_ANNOTATION_STRING_LIST>
		<CMX_ANNOTATION_STRING STRING_TYPE="SUMMARY_DESCRIPTION" ANNOTATION_STRING="This LED output can drive any general purpose LED with up to 10mA of current." />
		<CMX_ANNOTATION_STRING STRING_TYPE="VARIABLE_DESCRIPTION" ANNOTATION_STRING="The LED state is controlled with an 8-bit unsigned byte. 0 = Off, 1 = On." />
		<CMX_ANNOTATION_STRING STRING_TYPE="PIN_DESCRIPTION" ANNOTATION_STRING="The PSoC pin sources current to a LED or sinks current from a LED through a current limiting resistor based on the property selected by user." />
	</CMX_ANNOTATION_STRING_LIST>
	
</CMX_IO_DRIVER>
</CMX_DB>
