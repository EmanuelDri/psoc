//
// Custom.h
//
// Prototypes for custom functions, including fixed functions
//
#ifndef CUSTOM_H
#define CUSTOM_H

#include "SystemVars.h"

extern void CustomInit( void);
extern void CustomPostInputUpdate( void);
extern void CustomPreOutputUpdate( void);

#endif
