//
// FunctionParamDecl.h
//

#ifndef FUNCTION_PARAMDECL_H
#define FUNCTION_PARAMDECL_H

#include <m8c.h>
#include "CMXSystemFunction.h"

// function parameter block ID's


// system variable value defines
// pse_OtroLED output state ID defines
#define ID_pse_OtroLED_OFF 0
#define ID_pse_OtroLED_ON 1
#define ID_pse_OtroLED_UNASSIGNED 255


// pse_OtroLED output state value defines
#define ID_pse_OtroLED_OFF_VALUE 0
#define ID_pse_OtroLED_ON_VALUE 1
#define ID_pse_OtroLED_UNASSIGNED_VALUE 0


// system variable value defines for expressions
// pse_OtroLED output state ID defines for expressions
#define pse_OtroLED__OFF ID_pse_OtroLED_OFF_VALUE
#define pse_OtroLED__ON ID_pse_OtroLED_ON_VALUE
#define pse_OtroLED__UNASSIGNED ID_pse_OtroLED_UNASSIGNED_VALUE


// function parameter block definitions


#endif
