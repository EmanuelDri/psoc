//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

const CMX_DIO_ParameterBlock pse_OtroLED = { ID_pse_OtroLED, 1, 1, 1, 0, ID_DIO_10};

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_OtroLED, // ID = 1
		// ID_MAX = 2
};
