//*****************************************************************************
//*****************************************************************************
//  FILENAME:     cmx_dio_lib.h
//   Version:     1.0.0
//
//  DESCRIPTION: 
//    Header file for  cmx_dio_lib.asm
//
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef DIO_LLD_include
#define DIO_LLD_include

#include <m8c.h>

#pragma fastcall16 GlobalSelectConnect
#pragma fastcall16 GlobalSelectDisconnect
#pragma fastcall16 DiscreteDriveMode 
#pragma fastcall16 SetDriveModePortPin 
#pragma fastcall16 SetPin
#pragma fastcall16 DISetPin
#pragma fastcall16 DiscreteReadPin

extern  void  GlobalSelectConnect(BYTE bpin, BYTE bport);
extern  void  GlobalSelectDisconnect(BYTE bpin, BYTE bport);
extern  void  DiscreteDriveMode(BYTE bpin, BYTE bport,BYTE bdrivemode);
extern  void  SetDriveModePortPin(BYTE bPortPin,BYTE bDriveMode);
extern  void  SetPin(BYTE bPortPin, BYTE bValue);
extern  void  DISetPin(BYTE bPortPin, BYTE bValue);
extern  BYTE  DiscreteReadPin(BYTE bpin, BYTE bport);


extern  const BYTE portlu[];
extern  const BYTE pinlu[];
 
#endif
