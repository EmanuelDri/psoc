//----------------------------------------------------------------------------
// DriverDecl.h
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "CMX_DIO.h"

// Driver ID's
#define ID_pse_OtroLED 1

// Channel ID's
#define ID_DIO_10 0
#define ID_SHADOWREGS_0 0
#define ID_SHADOWREGS_1 1
#define ID_SHADOWREGS_2 2

// Channel Type maximum count
#define CMX_DIGITAL_IO_CHAN_MAX 1
#define CMX_SHADOW_MAX 3

// Channel Type instantiation count
#define CMX_DIGITAL_IO_CHAN_COUNT 1
#define CMX_SHADOW_COUNT 0

// Driver Parameter Block externs
extern CMX_DIO_ParameterBlock const pse_OtroLED;
