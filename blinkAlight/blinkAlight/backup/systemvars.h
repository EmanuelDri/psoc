//
// SystemVars.h
//

#ifndef SYSTEM_VARS_H
#define SYSTEM_VARS_H

#include "CMXSystem.h"
#include "driverdecl.h"

void UpdateVariables( void);

typedef struct
{

	struct
	{
		BYTE pse_OtroLED;

	} ReadOnlyVars;
} SYSTEM_VARS_STRUC;

#define SYSTEM_VARIABLE_COUNT 1 			// Total size of all variables. ( 2 x ints + 1 x BYTEs )
#define SYSTEM_RW_VARIABLE_COUNT 0	// Total size of the read/write variables. ( 2 x ints + 1 x BYTEs )

#endif
