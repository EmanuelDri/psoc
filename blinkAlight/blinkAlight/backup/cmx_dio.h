//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_DIO.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for Discrete I/O driver 
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef dio_include
#define dio_include

#include <M8C.h>


////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE ID;                     // Instance ID
    BYTE ON_bDefine;             // ActiveState  1 => Active Hi,  0 => Active Low
    BYTE Initial_bCondition;     // Initial pin state Hi or Lo
    BYTE Initial_DriveMode;      // PSoC drive mode 0 - 7
    BYTE ReadOutputPolarity;     // Read Polarity  0 => Invert,  1 => Non-Invert
    BYTE ChannelID;              // Compact Pin/Port info  pin [3:0], port [7:4]
}CMX_DIO_ParameterBlock;
//-------------------------------------------------
// Prototypes of the DIO API.
//-------------------------------------------------

void CMX_DIO_Instantiate(const CMX_DIO_ParameterBlock * pPBlock);
void CMX_DIO_SetValue(const CMX_DIO_ParameterBlock * pPBlock, BYTE bValue);
BYTE CMX_DIO_GetValue(const CMX_DIO_ParameterBlock * pPBlock);

#endif
