//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_DIO.c
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This Driver calls the low level driver performs
//                individual pin input and drive.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "m8c.h"
#include "CMX_dio.h"
#include "cmx.h"

// channel type header file
#include "cmx_dio_chan.h"


//-----------------------------------------------------------------------------
//  FUNCTION NAME: DIO_Instantiate(const CMS_DIO_ParameterBlock * pPBlock)
//
//  DESCRIPTION: 
//    Initializes the DIO specified by the parameter block pointed to by pPBlock.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: pPBlock => Pointer to Parameter Block
//
//  RETURNS:  none.
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: Initialization for discrete I/O
//  Initial conditions and drive mode set based on parameters set in
//  DIO Parameter block
////////////////////////////////////////////////////////////////////////////////
void CMX_DIO_Instantiate(const CMX_DIO_ParameterBlock * pPBlock)
{
        BYTE port;
        BYTE pin;
        BYTE pinPort;

        CMX_DIO_ParameterBlock  thisBlock;
        thisBlock = *pPBlock;

        pinPort = DIOChannelPins[(thisBlock.ChannelID)];      // Get pin and port info

        pin  = pinlu[(BYTE)(pinPort & 0x0F)];           // Create pin mask
        port = portlu[(BYTE)((pinPort & 0xF0) >>4)];    // Extract port address

        GlobalSelectDisconnect(pin,port);               // Disconnect from Global bus
        CMX_DIO_SetValue(pPBlock,thisBlock.Initial_bCondition);  // Set initial condition	

        DiscreteDriveMode(pin,port,thisBlock.Initial_DriveMode);  // Set default drive mode
}
//-----------------------------------------------------------------------------
//  FUNCTION NAME: DIO_SetValue(const CMS_DIO_ParameterBlock * pPBlock, BYTE bValue)
//
//  DESCRIPTION: 
//     Sets the DIO to a 1 or 0 determined by bValue.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS:  
//              pPBlock => Pointer to parameter block
//              bValue  => Value to set
//
//  RETURNS:  none
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE:
///////////////////////////////////////////////////////////////////////////////
void CMX_DIO_SetValue(const CMX_DIO_ParameterBlock * pPBlock, BYTE bValue)
{
    BYTE pinport;

    pinport = DIOChannelPins[(pPBlock->ChannelID)];


	if ((pPBlock->ON_bDefine ^ bValue ) == 0) {   // Same mode
        DISetPin(pinport,1); 
    } else {
        DISetPin(pinport,0);
    }
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: DIO_GetValue(const CMS_DIO_ParameterBlock * pPBlock)
//
//  DESCRIPTION:
//     Return the value of the DIO pointed to by the parameter block
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//             pPBlock => Pointer to DIO Parameter block
//
//  RETURNS: 
//     Returns DIO status, 1 or 0
//
//  THEORY of OPERATION or PROCEDURE:
///////////////////////////////////////////////////////////////////////////////
BYTE CMX_DIO_GetValue(const CMX_DIO_ParameterBlock * pPBlock)
{

    BYTE port;
    BYTE pin;
    BYTE pinPort;
    BYTE pinValue;


    pinPort = DIOChannelPins[(pPBlock->ChannelID)] ;
    pin  = pinlu[(BYTE)(pinPort & 0x0F)];
    port = portlu[(BYTE)((pinPort & 0xF0) >> 4)];

    pinValue = DiscreteReadPin(pin,port);

    if (pPBlock->ReadOutputPolarity == 0) // if output is to be inverted
    {
       pinValue  = !pinValue;   // invert output
    }
    
    return ( pinValue);
}
