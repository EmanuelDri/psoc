//
// Custom.c
//
//   Custom functions for insertion into PSoC Express projects, including fixed functions
//

#include "custom.h"

//
//  CustomInit() is run after driver and channel instantiation functions, but before the main loop
//
void CustomInit( void )
{
}

//
// CustomPostInputUpdate() is run after input drivers have updated SystemVars, but before transfer 
//   functions are evaluated.
//
void CustomPostInputUpdate( void )
{
}

//
// CustomPreOutputUpdate() is run after transfer functions have been evaluated, but before the output 
//   driver are updated with values from SystemVars.
//
void CustomPreOutputUpdate( void )
{
}
