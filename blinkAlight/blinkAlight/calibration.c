//*****************************************************************************
//*****************************************************************************
//  FILENAME:  calibration.c              
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  This files contains the calibration constansts for the
//                ADC. Currently these values are default values that
//                are not calibrated.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#pragma abs_address:0x3FC0
const int CountsPerVolt = 25206;   // ADC gain for 0 to 2.6 volt range.  
#pragma end_abs_address

#pragma abs_address:0x3FC2
const int ADC_Offset    = 0;       // ADC offset in counts
#pragma end_abs_address

#pragma abs_address:0x3FC4
// This array of offsets allows for custom calibration
// of each input that uses the mVolts channel. The offset
// will be in the drivers native units.  For the mVolts
// driver it will be in mVolts.  For a temperature driver
// it will be in tenths of degrees, etc.
const int imVolts_Chan_Offset[8] = {0,0,0,0,0,0,0,0};
#pragma end_abs_address
