//
//  SimTransferFunction.js
//

// variable declarations
var g_OtroLED;

// variable ID's
var g_ID_OtroLED = 0;

// function parameter ID's


// function parameter defines


// function output states
// OtroLED output state ID 
var	g_ID_OtroLED_OFF = 0;
var	g_ID_OtroLED_ON = 1;
var	g_ID_OtroLED_UNASSIGNED = 255;


// variable output state declarations
// OtroLED output state declarations
var	g_OtroLED__OFF;
var	g_OtroLED__ON;
var	g_OtroLED__UNASSIGNED;


// function parameter declarations


var g_FunctionParams = new Array();

// current value array
var g_CurrentValues = new Array();

function TransferFunction()
{
	// load values from globals
	var ioInfo;
	ioInfo = GetIOInfoFromInstanceName("OtroLED");
	if (ioInfo != null)
	{
		g_OtroLED = parseInt(ioInfo.sValue);
	}

	// update current value array
	g_CurrentValues[g_ID_OtroLED] = g_OtroLED;

	// call transfer functions
	OtroLED_TransferFunction();

	// set values in globals
	SetIOInfoValueFromInstanceName("OtroLED", g_OtroLED);

}

function OtroLED_TransferFunction()
{
	


}


function InitTransferFunctionGlobals()
{


	// variable output state initializations
	// OtroLED output state initializations
	g_OtroLED__OFF = 0;
	g_OtroLED__ON = 1;
	g_OtroLED__UNASSIGNED = 0;


	// variable initialization
	// initialize OtroLED globals
    ioInfo = GetIOInfoFromInstanceName("OtroLED");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_OtroLED = parseInt(ioInfo.sValue);
		}
		else
		{
			g_OtroLED = 0;
		}
    }
	else
	{
		g_OtroLED = 0;
	}

}

InitTransferFunctionGlobals();




function UpdateSimValues()
{
	// set values in globals
	SetIOInfoValueFromInstanceName("OtroLED", g_OtroLED);
}

// update the input widgets
function UpdateInputWidgets()
{
//	alert("updating input widgets");
	for(var j  in g_IOInfos)
	{
        if (g_IOInfos[j].sType == "IN" || (g_IOInfos[j].sType == "VARIABLE" && g_IOInfos[j].sDriver == "Variable_IN"))
        {
			var sInstanceName = g_IOInfos[j].sInstanceName;
			var sID	= IDFromInstanceName(sInstanceName)
            var sValue	= g_IOInfos[j].sValue;
		    var sDriver	= g_IOInfos[j].sDriver;
//			alert("updating " + sInstanceName + " = " + sValue);
            sValue = RawToDisplayValue( sID, sValue);
		    WidgetSetValue(sDriver, sID, sValue);
		}
	}
}

function IOState( strID, strValue)
{
    this.strID = strID;
    this.strValue = strValue;
}
