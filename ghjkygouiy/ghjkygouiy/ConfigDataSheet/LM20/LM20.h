//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LM20.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header for LM20 temperature sensor driver.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include <m8c.h>

#ifndef `@LIB_NAME`_LM20_DRV_HEADER
#define `@LIB_NAME`_LM20_DRV_HEADER

typedef struct
{
   BYTE ID;      // Instance ID.  This ID value will be set upon code generation
                 // and will refer to the index in the array of pointers to 
                 // parameter blocks.

   BYTE InPort;  //  Value between 0 and 7 (Could be 0 to 11 if Port2 used)
                 //  For this value, it will relate directly to Port0 pins
                 //  0 through 7.

}`@LIB_NAME`_LM20_ParameterBlock;

// Function prototypes
void `@LIB_NAME`_LM20_Instantiate(const `@LIB_NAME`_LM20_ParameterBlock * thisBLK);
int  `@LIB_NAME`_LM20_GetValue(const `@LIB_NAME`_LM20_ParameterBlock * thisBLK);

#endif
