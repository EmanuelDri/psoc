<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
     version="1.0">

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='Expression']">

    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>

    <xsl:variable name="PriorityOrStatusEncoder">
    <xsl:choose>
      <xsl:when test="count(//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]//CMX_EXPRESSION_LIST/CMX_EXPRESSION[@EXPRESSION_TYPE='ELSEIF']) > 0">
        <xsl:text disable-output-escaping="yes">Priority Encoder</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">Status Encoder</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
    </xsl:variable>


    <div class="divTransferFunction">
      <TABLE BORDER="0" CELLSPACING="0" class="dataTables">

        <th>
          Transfer Function:  <xsl:value-of select="$CmxTransferInstanceName"/><xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text><xsl:value-of select="$PriorityOrStatusEncoder"/>
        </th>

        <TR ALIGN="CENTER">
          <TD>

            <TABLE BORDER="1" CELLSPACING="0" class="dataTables">
              <TR>
                <TD class="columnHeading" colspan="4">EXPRESSION</TD>
              </TR>

              <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]//CMX_EXPRESSION_LIST/CMX_EXPRESSION">
                <xsl:sort data-type="number" order="ascending" select="./@ORDINAL"/>
                <TR>
                  <TD class="cell">
                    <xsl:value-of select="./@EXPRESSION_TYPE"/>
                  </TD>

                  <TD class="cell">
                    <xsl:value-of select="./@LEFT_EXPRESSION"/>
                  </TD>

                  <TD class="cell">
                    THEN
                  </TD>
                  <TD class="cell">
                    <xsl:value-of select="./@RIGHT_EXPRESSION"/>
                  </TD>
                </TR>
              </xsl:for-each>
            </TABLE>
          </TD>
        </TR>
      </TABLE>
    </div>
  </xsl:template>
</xsl:stylesheet>