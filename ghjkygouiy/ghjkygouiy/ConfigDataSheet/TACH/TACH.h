//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_Tach.h
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  `@PSOC_VERSION`
//  DESCRIPTION:  Header file for Tach Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_tach_include
#define `@LIB_NAME`_tach_include

#include <M8C.h>

`@TACH_ORDINAL_DEFINE_BLOCK`
#define `@LIB_NAME`_TACH_COUNT `@TACH_COUNT`

#define ID_NoFan              0
#define TACH_EdgeGlitchLimit  20
#define ID_NoAssocFan         0xFF

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE Tach_ID; // Instance ID
    BYTE Tach_PulsesPerRevolution;  //# of pulses per revolution
    BYTE Tach_Fan_Source; /// Fan association
    BYTE Tach_ChannelID;
 
}`@LIB_NAME`_TACH_ParameterBlock;
/////////////////////////////

// Function prototypes
int  `@LIB_NAME`_TACH_GetValue( const `@LIB_NAME`_TACH_ParameterBlock * thisBLK );
void `@LIB_NAME`_TACH_Instantiate( const `@LIB_NAME`_TACH_ParameterBlock * thisBLK );


#endif
