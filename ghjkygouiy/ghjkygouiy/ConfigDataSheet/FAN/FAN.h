//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_Fan.h
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the Fan Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_fan_include
#define `@LIB_NAME`_fan_include

#include <M8C.h>
#define FAN_PRESENT 1

////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE FAN_ID;             // Instance ID
    BYTE FAN_KICKSTART;      // How long to kickstart fan
    BYTE FAN_KICKTACH;       //
    BYTE FAN_SpinUpTime;     // How much time it takes to spin up fan
    BYTE FAN_DrivePolarity;  // 1 non-invert, 0 invert
    BYTE FAN_ChannelID;      // Channel ID
 
}`@LIB_NAME`_FAN_ParameterBlock;

// Function prototypes
void `@LIB_NAME`_FAN_SetValue(const `@LIB_NAME`_FAN_ParameterBlock * thisBLK, BYTE bPercent_DutyCycle);
void `@LIB_NAME`_FAN_Instantiate(const `@LIB_NAME`_FAN_ParameterBlock * thisBLK);


#endif
