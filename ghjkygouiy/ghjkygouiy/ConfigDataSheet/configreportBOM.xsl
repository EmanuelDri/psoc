<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:template match="/">
    <HTML id="main" xmlns="http://www.w3.org/1999/xhtml">
    <HEAD>
      <META HTTP-EQUIV="Pragma" CONTENT="no-cache"></META>
      <META HTTP-EQUIV="Expires" CONTENT="-1"></META>
      <STYLE>

        .page
        {
        page-break-before: always;
        }

        body,
        td
        {
        font-family: verdana;
        font-size: 10px;
        }

        .brand,
        .brandSubPage
        {
        padding-top: 22px;
        padding-bottom: 4px;
        color:#ffffff;
        background-color:#09367A;
        border-width:1px;
        border-style:solid;
        border-color:#09367A;
        }

        .brandSubPage
        {
        margin-top:40px;
        }


        .copyright
        {
        text-align:right;
        font-size:7pt;
        margin-top:5px;
        padding-right: 5px;
        margin-bottom: 30px;
        }

        .trademark
        {
        vertical-align:super;
        font-size:6pt;
        }

        h1,
        .h1
        {
        font-size:22px;
        font-weight:bold;
        font-family:arial;
        }

        .reportTitle
        {
        margin-top:5px;
        }

        h2
        {
        font-family:arial;
        font-size:17px;
        font-weight:bold;
        }

        h1,
        h2
        {
        padding-left:16px;
        }

        .figureLabel,
        .label
        {
        font-family:arial;
        font-size: 11px;
        padding-left:16px;
        }

        .label
        {
        font-weight: bold;
        }

        .figureLabel,
        {
        padding-top:5px;
        }

        .columnHeading,
        {
        padding-left:16px;
        }

        .columnHeading,
        .columnHeadingForDeviceLabel,
        .cellForDeviceLabel,
        .cell,
        .pinoutHeading,
        th,
        .tableHeading,
        {
        padding-top:4px;
        padding-bottom:4px;
        /*
        border-bottom-width:1px;
        border-bottom-style:solid;
        border-bottom-color:#ffffff;
        */        }

        #tdRegisterProtocolDescription,
        #tdFlashUpdateProtocol
        {
        padding-left: 16px;
        padding-right:10px;
        padding-top:10px;
        padding-bottom:10px;
        background-color:#FFFFFF;
        color:#000000;
        font-weight: normal;
        }


        .cell,
        .cellForDeviceLabel,
        {
        color:black;
        text-align:center;
        background-color: e5e5e5;
        }

        .cellForDeviceLabel
        {
        color: black;
        font-weight: bold;
        text-align:left;
        padding-left:16px;
        }

        .columnHeading,
        .columnHeadingForDeviceLabel,
        .pinoutHeading,
        .dataTables,
        .tableHeading
        {
        color: white;
        font-family:arial;
        font-size: 11px;
        background-color:999999;
        font-weight: bold;
        }

        .columnHeading,
        .columnHeadingForDeviceLabel,
        {
        text-align: center;
        }

        .columnHeadingForDeviceLabel
        {
        text-align: left;
        padding-left:16px;
        }

        .notes
        {
        padding-top:16px;
        padding-left:16px;
        padding-bottom:10px;
        }

        .containerWidth,
        .brand,
        .brandSubPage,
        .copyright,
        .dataTables,
        .projectTable,
        .tableHeading,
        .descriptionTables
        {
        width:640px;
        }

        .projectTable,
        .descriptionTables
        {
        border-width:1px;
        border-style:solid;
        border-color:#999999;
        }

        .dataTables
        {
        border-collapse: collapse;
        border-width:1px;
        border-style:solid;
        border-color:#ffffff;
        }

        th,
        .pinoutHeading,
        .tableHeading
        {
        text-align:left;
        padding-left:16px;
        font-size:11pt;
        background-color:#6A6A6A;
        }

        .pdfDIV
        {
        margin-left: 16px;
        }

        .pdfLabel
        {
        font-size:9px;
        }

        a.pdfLabel
        {
        text-decoration: none;
        color:black;
        }

        a.pdfLabel:hover
        {
        text-decoration: underline;
        }

      </STYLE>

      <xsl:text disable-output-escaping="yes">
	        <![CDATA[
	        <script type="text/javascript" language="javascript">
                var g_pDlg = null;

                /////////////////////////////
                //
                // Called from outside
                //
                function setPSoC(pOtto, pDlg)
                {
                    g_pDlg = pDlg;
                }
                
                function CSV_onclick()
                {
                    try
                    {
                        var szText  = getCSVString();
                        var szPath  = "";
                        var szExtension = "csv";
                        var szFilter = "CSV Files (*.csv)|*.csv";
                        if (g_pDlg != null)
                        {
                            if (szText.indexOf(",") == -1)
                            {
                                alert("There is nothing to save.");
                            }
                            else
                            {
                                g_pDlg.SaveTextFile(szText, szPath, szExtension, szFilter);
                            }
                        }
                        else
                        {
                            // For testing...
                            alert(szText);
                        }
                     }
                     catch(e)
                     {
                     }
                }
                
                function getCSVString()
                {
                    var objCSVElement = document.getElementById("tableBOM");
                    return getCSVFromTable(objCSVElement);
                }
                
                // //////////////////////////
                //
                // Takes a table element and then returns a CSV string representing the data contained in the table.
                //
                function getCSVFromTable(table)
	            {
        	       
	                var len = 0;
	                var cells;
	                var str = "";
        	         
			        if (table.rows && table.rows.length > 1) 
			        {
			            len = table.rows.length;
				        for(var i=0; i<len; i++)
				        {
				            if (table.rows[i].className != "rowCSVIgnore")
				            {
					            cells = table.rows[i].cells;
            					 
					            for(var j=0; j<cells.length;j++)
					            {
					                str +="\"";  //add double quotes to each string
						            str += getInnerTxt(cells[j]);
						            str +="\"";  //add double quetest to each string
						            if(j<cells.length-1)
						            str +=',';
            						 
					            }
					            str += "\n";
					        }
				        }
        				 
			        }
                    
			        //document.write(str);
			        return str;
                }
                
                function getInnerTxt(cell)
                {
                    var str = "";
                     
			        if (typeof cell == "string") 
			        {
				        str = cell;
			        }
			        if(cell.innerText) 
			        {
				        str= cell.innerText;
			        }
			        else
			        {
			            str = "";
			        }	
        			
			        str = str.replace(/"/g, "'");
        			 
	                var cNodes = cell.childnodes
	                if(cNodes !=null)
	                {
				        for (var i = 0; i < cNodes.length; i++) 
				        {
					        switch (cNodes[i].nodeType) 
					        {
						        case 1:  
							        str += getInnerTxt(cs[i]);
							        break;
						        case 3:	 
							        str += cs[i].nodeValue;
							        break;
					        }
				        }
			        }

			        return str;
                }
                
		         function GeneratePdf(filename)
		         {
		            window.location.href = "PDF/GeneratePDF.htm?filename=" + filename;
		         }
                
            </script>
	        ]]>
	        </xsl:text>
           <script>
           function SetVendorAddOnPath()
           {
              document.getElementById("addonframe").src = 
                 "Virtual Vendor Tables/<xsl:value-of select="//CMX_PROJECT/@VENDOR"/>/addonBOM.htm";
           }
           </script>
    </HEAD>


    <BODY onload="SetVendorAddOnPath()">
    <div class="page">

      <div class="brand">
        <h1>
          PSoC Designer<span class="trademark">TM</span>
        </h1>
      </div>
      <div class="copyright">Cypress Semiconductor Corporation Copyright 2007-2008</div>

      <H1 class="reportTitle">BOM</H1>
     
      <div class="pdfDIV">
        <a id="PrintButon" onClick="return GeneratePdf('configreportBOM');" href="#" 
                        class="pdfLabel">
          <img border="0" alt="Print to PDF" src="PDF/pdf_button.png" width="16" height="16" />  Print to PDF
        </a>
      </div>
      <br></br>
    
      <table>
        <tr>
          <td class="label">Last Built: </td>
          <td>
            <xsl:value-of select="//CMX_PROJECT/@BUILD_DATE"/>
          </td>
        </tr>
        <tr>
          <td class="label">Project Path: </td>
          <td>
            <xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH"/>
          </td>
        </tr>
        <tr>
          <td class="label">Project Name: </td>
          <td>
            <xsl:value-of select="//CMX_PROJECT/@NAME"/>
          </td>
        </tr>
      </table>


      <div class="notes">
        <strong>Printing</strong><br />
        For best results, print in landscape format.
      </div>

      <table>
        <tr><td style="width:10"/>
          <td style="height:30;width:100"><input type="button" value="Save as CSV" onclick="CSV_onclick();" /></td>
          <td><iframe id="addonframe" frameborder="0" scrolling="no" style="height:30;width:110">
        </iframe></td></tr>
      </table>

      <P></P>
      <xsl:if test="count(//CMX_BOM_LIST/CMX_BOM) > 0" >

        <div id="excelContent">

          <xsl:variable name="columnNames" select="//CMX_BOM_COLUMN/@NAME[not(. = preceding::CMX_BOM_COLUMN/@NAME)]" />


          <TABLE BORDER="0" CELLSPACING="0" class="containerWidth">



            <TR ALIGN="CENTER">
              <TD>

                <TABLE id="tableBOM" BORDER="0" CELLSPACING="0" class="containerWidth">

                  <!-- Print Column Headings  -->
                  <tr>
                    <xsl:for-each select="//CMX_BOM_COLUMN/@NAME[not(. = preceding::CMX_BOM_COLUMN/@NAME)] ">
                      <td class="rowDivider" nowrap="true">
                        <xsl:choose>
                          <xsl:when test="position() = 1">
                            <xsl:attribute name="class">columnHeadingForDeviceLabel</xsl:attribute>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:attribute name="class">columnHeading</xsl:attribute>
                          </xsl:otherwise>
                        </xsl:choose>
                        <xsl:value-of select="." />
                      </td>
                    </xsl:for-each>
                  </tr>


                  <!--  Print Table Body -->

                  <xsl:for-each select="//CMX_BOM_LIST/CMX_BOM">
                    <tr class="rowCSVIgnore">
                      <td class="rowDivider" colspan="8">

                      </td>
                    </tr>
                    <TR>
                      <xsl:variable name="bomNode" select="." />
                      <xsl:variable name="deviceValues" select="./CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN" />
                      <xsl:for-each select="$columnNames">
                        <!--	<xsl:for-each select="./CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME != 'Label']">  -->
                        <xsl:variable name="columnName" select="." />
                        <TD>

                          <xsl:choose>
                            <xsl:when test="$columnName = 'Label'">
                              <xsl:attribute name="class">cellForDeviceLabel</xsl:attribute>
                              <xsl:choose>
                                <xsl:when test="$bomNode/@INSTANCE_NAME='BASE_PROJECT'">
                                  <xsl:value-of select="//CMX_PROJECT/@NAME"/>_<xsl:value-of select="$bomNode/CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME='Label']/@CONTENTS"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="$bomNode/@INSTANCE_NAME"/>_<xsl:value-of select="$bomNode/CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME='Label']/@CONTENTS"/>
                                </xsl:otherwise>
                              </xsl:choose>


                            </xsl:when>
                            <xsl:otherwise>

                              <xsl:attribute name="class">cell</xsl:attribute>

                              <xsl:value-of select="$deviceValues[@NAME = $columnName]/@CONTENTS"/>
                              <!-- <xsl:value-of select="./@CONTENTS"/> -->


                            </xsl:otherwise>
                          </xsl:choose>
                        </TD>


                      </xsl:for-each>
                    </TR>

                  </xsl:for-each>

                </TABLE>

              </TD>
            </TR>


          </TABLE>
        </div>
      </xsl:if>
    </div>
    </BODY>
    </HTML>
  </xsl:template>



</xsl:stylesheet>






