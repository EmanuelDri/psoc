//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_I2C_Drv.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for I2C_Drv.c                             
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef `@LIB_NAME`_I2C_include
#define `@LIB_NAME`_I2C_include

typedef struct
{
   BYTE ID;                 // Instance ID.  This ID value will be set upon code generation
                            // and will refer to the index in the array of pointers to 
                            // parameter blocks.

   BYTE bAddress;           //  I2C Address

   BYTE bRAMLen;            //  Length of RAM buffer used.

   BYTE bRWLen;             //  Length of RW memory locations starting from 0.

   BYTE * pbRAMRegs;        //  Pointer to RAM used for registers  (2 Byte pointer)
   
   BYTE bROMLen;            //  Length of ROM buffer used.

   const BYTE * pbROMRegs;  //  Pointer to ROM used for registers  (2 Byte pointer)  
   
}`@LIB_NAME`_I2C_ParameterBlock;

// Function prototypes
void `@LIB_NAME`_I2C_Instantiate(const `@LIB_NAME`_I2C_ParameterBlock * thisBLK );
void `@LIB_NAME`_I2C_SetValue(const `@LIB_NAME`_I2C_ParameterBlock * thisBLK , BYTE bAddrOffset);

#endif
