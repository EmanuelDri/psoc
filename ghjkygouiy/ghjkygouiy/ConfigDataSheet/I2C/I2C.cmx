<!--
******************************************************************************
******************************************************************************
**  FILENAME: I2C.cmx
**   Version: 1.2, Updated on 2009/10/29 at 15:6:51
**
**  DESCRIPTION: 
**    XML description for the I2C interface driver.
** - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
**  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
******************************************************************************
******************************************************************************
-->
<CMX_DB>
<CMX_IO_DRIVER 
             NAME             = "I2C"
             DISPLAY_NAME     = "Slave"
             VERSION          = "1.2"
             IO_CHANNEL_TYPE  = "CMX_I2C_IFC_CHAN"
             IMAGE            = "I2C.gif"
             WIDGET           = ""
             DOC_FILE         = ""
             HTML             = "datasheet.htm"
             DRIVER_CATEGORY  = "Communication\I2C"
             DRIVER_TYPE      = "CMX_INTERFACE"
>

        <CMX_PROPERTY_LIST>
           <CMX_PROPERTY NAME="I2C_Address" TYPE="INT" MIN="0" MAX="63" DEFAULT_VALUE="4" COMMENT="I2C base address, 0 to 63"/>
        </CMX_PROPERTY_LIST>

        <CMX_TEMPLATE_FILE_LIST>
           <CMX_TEMPLATE_FILE PATH="I2CFrags.tpl" PREFIX="" TARGET_FILE_NAME="" FILE_TYPE="" />
           <CMX_TEMPLATE_FILE PATH="I2C.h" PREFIX="" TARGET_FILE_NAME="I2C.h" FILE_TYPE="" />
           <CMX_TEMPLATE_FILE PATH="I2C.c" PREFIX="" TARGET_FILE_NAME="I2C.c" FILE_TYPE="" />
        </CMX_TEMPLATE_FILE_LIST>


	<CMX_SCHEM_BOM_LIST>
		<CMX_SCHEM_BOM NAME="I2C" KEY_VALUE="" SCHEMATIC_PATH="I2C\schematic.gif" BOM_PATH="BOM.xml"/>
	</CMX_SCHEM_BOM_LIST>



	<CMX_ANNOTATION_STRING_LIST>
		<CMX_ANNOTATION_STRING STRING_TYPE="SUMMARY_DESCRIPTION" ANNOTATION_STRING="This device includes an I2C register-based slave interface. The I2C bus is an industry standard, two-wire hardware interface developed by Philips. The master initiates all communication on the I2C bus and supplies the clock for all slave devices. The I2C interface supports standard mode speeds up to 400 kbits/s. This I2C interface is compatible with multiple devices on the same bus. The slave address defined for this device represents the 7 most significant bits of the address, with the least significant bit set to one or zero by the master to indicate a write or a read transaction. A second address is provided for ROM/Flash access at 0x40 plus the basic register device address." />
		<CMX_ANNOTATION_STRING STRING_TYPE="PIN_DESCRIPTION" ANNOTATION_STRING="The PSoC I2C bus pins are configured to connect to a standard I2C bus up to 400 kHz. " />
		<CMX_ANNOTATION_STRING STRING_TYPE="VARIABLE_DESCRIPTION" ANNOTATION_STRING="NA" />
		<CMX_ANNOTATION_STRING STRING_TYPE="PIN_PROTOCOL_DESCRIPTION" ANNOTATION_STRING="The PSoC I2C pins are configured to directly drive an I2C bus. Appropriate pull-ups must be supplied on both the clock and data lines at some point on the bus. For more information on I2C please see the Philips I2C specification."/>
		<CMX_ANNOTATION_STRING STRING_TYPE="REGISTER_PROTOCOL_HEADING" ANNOTATION_STRING="I2C Register Protocol Description (Requires an I2C Interface Object):" />
		<CMX_ANNOTATION_STRING STRING_TYPE="REGISTER_PROTOCOL_DESCRIPTION" ANNOTATION_STRING="When writing one or more bytes, the first data byte is always the data pointer. The byte after the data pointer will be written into the location pointed to by the data pointer byte. The third byte (second data byte) will be written to the data pointer plus 1 and so on. The data pointer will increment for each byte read or written, but will be reset at the beginning of each new read operation. \n\nA new read operation will begin to read data at the location pointed to by the last write operation's data pointer. For example, if the data pointer is set to 4, a read operation will begin to read data at location 4 and continue sequentially until the end of the data or the host completes the read operation.\n\nFollowing each read operation the data pointer will reset to 4 and the next read continues sequentially from that location. This is true whether a single or multiple read operations are performed. The data pointer will not be changed until a new write operation is initiated.\n\nIf the I2C master attempts to write data to a read-only location or an undefined location, the data will be discarded and will have no affect. Data cannot be read outside the defined range. Any read requests by the master, outside the allotted range will result in invalid data being returned."/>
		<CMX_ANNOTATION_STRING STRING_TYPE="CONSTANT_PROTOCOL_HEADING" ANNOTATION_STRING="I2C Flash Update Protocol Description (Requires an I2C Interface Object):" />
		<CMX_ANNOTATION_STRING STRING_TYPE="CONSTANT_PROTOCOL_DESCRIPTION" ANNOTATION_STRING="Accessing or changing constants is provided through a separate I2C address, determined by adding 0x40 to the defined register-based slave address. This is called Flash edit mode.\n\nThere are six commands that provide a method to enter the Flash edit mode and to read and write Flash memory. The commands and their sequence are: \n\nEnterFlashMode: 0x01, 0x21, 0xDE (key)\nShowBlock: 0x00, 0x22, 0 - 255\nReadBlock 0x00, 0xA3, 0 - 255\nWriteBlock: 0x28, 0xA5, 0 - 255,\nReset: 0x3A, 0xAC, 0xDA (key)\n\nWhen any of the listed commands are executed, the I2C clock line will be held low until the command is completed. A command status is always stored at the 65th location of the SRAM buffer for the primary address.  The status byte is cleared to zero at the beginning of the command and set to 1 if the command is completed properly.  If the command is invalid, this status byte will remain zero until the next command is completed properly.  This status byte is read only by the I2C master.\n\nIf an invalid parameter is given, such as a block out of range, or an invalid key, the command will not execute.\n\nEnterFlashMode:  This command causes the program flow to leave the normal operation mode and enter the flash edit mode.  Program execution will stay in this mode until a reset command is issued from the I2C host, a 11 (+/- 1) second timeout occurs, the reset line is driven high, or power is cycled.  This command is only valid in the normal mode.\n\nShowBlock:  This command changes the address pointer for the ROM location to point to the block passed to it in the parameter byte.  This block may be read from the secondary address interface.  Only valid blocks will be accepted as a parameter.  The base address to user block 0 and the block count are defined at compile time by code generation.  This command is valid in either the normal or flash edit mode.\n\nReadBlock:  This command reads the block referred to by the parameter block 
and writes the block into the 64 byte RAM buffer accessed by the primary address.  This command is only valid in the flash edit mode.\n\nWriteBlock:  This command writes the primary address 64 byte buffer into the specified user flash block.  This command is only valid in the flash edit mode.\n\nReset:  This command forces the program execution to jump to location 0x0000 so that the normal mode may be re-entered after flash operations are complete.  This command is only valid in the flash edit mode.\n\nAt this time the flash programming settings concerning die temperature are set to a value that will guarantee a minimum of 1000 flash write cycles."/>
      </CMX_ANNOTATION_STRING_LIST>

	<CMX_DRIVER_SUB_TREE_LIST>
		<CMX_DRIVER_SUB_TREE DISPLAY_NAME="Slave" DRIVER_CATEGORY="Communication\I2C" ORDINAL="0"/>
		<CMX_DRIVER_SUB_TREE DISPLAY_NAME="I2C Slave" DRIVER_CATEGORY="CY3270 PSoC FirstTouch Kit" ORDINAL="0"/>
	</CMX_DRIVER_SUB_TREE_LIST>

</CMX_IO_DRIVER>
</CMX_DB>
