//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_I2Cm_CHAN.h
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the I2C software master channel.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef I2Cm_HEADER
#define I2Cm_HEADER

#include "I2Cm.h"

extern void InitI2CmChan(void);   


#endif
