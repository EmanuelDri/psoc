//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMS_I2Cm_CHAN_c.h              
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  This file contains the initialization function for the 
//                software I2C master channel.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "CMX_I2Cm_CHAN.h"

// *********************** InitI2CmChan() *******************************
//
// Description:
//   This function starts the I2Cm user module.
//

void InitI2CmChan( void )
{
	I2Cm_Start();
}


