<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- Match the root node -->
<xsl:template match="/">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Select the nodes to apply the templates -->
<xsl:template match="@*|node()">
<xsl:copy>
	<xsl:apply-templates select="@*|node()"/>
</xsl:copy>
</xsl:template>

<!-- I2Cm Channel instance expansion -->
<xsl:template match="CMX_I2Cm_Shared_CHANNEL_INSTANCE">
<xsl:element name="CMX_CHANNEL_INSTANCE">
<xsl:attribute name="INSTANCE_NAME"><xsl:value-of select="@INSTANCE_NAME"/></xsl:attribute>
<xsl:attribute name="IO_CHANNEL_TYPE_ORDINAL"><xsl:value-of select="@ORDINAL"/></xsl:attribute>
	<xsl:element name="CMX_USER_MODULE_SPEC_LIST">
	<xsl:element name="CMX_USER_MODULE_SPEC">
	<xsl:attribute name="UM_INSTANCE_NAME"><xsl:value-of select="@UM_INSTANCE_NAME"/></xsl:attribute>
	<xsl:attribute name="UM_NAME">I2Cm</xsl:attribute>
		<xsl:element name="SHAPE_LOCATOR_LIST">
		</xsl:element>
		<xsl:element name="PARAMETER_LIST">
		<xsl:element name="PARAMETER">
		<xsl:attribute name="NAME">I2C_Port</xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:value-of select="@I2CPORT"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="PARAMETER">
		<xsl:attribute name="NAME">SDA_Pin</xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:value-of select="@SDAPIN"/></xsl:attribute>
		</xsl:element>
		<xsl:element name="PARAMETER">
		<xsl:attribute name="NAME">SCL_Pin</xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:value-of select="@SCLPIN"/></xsl:attribute>
		</xsl:element>
		</xsl:element>
	</xsl:element>
	</xsl:element>
	<xsl:element name="CMX_CHANNEL_RESOURCE_LIST">
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">SDAPin</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@SDAPIN"/></xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_UM_PARAMETER_NAME">SDA_Pin</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">SCLPin</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@SCLPIN"/></xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_UM_PARAMETER_NAME">SCL_Pin</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	</xsl:element>
	<xsl:element name="CMX_SUBSTITUTION_LIST">
		<xsl:element name="CMX_SUBSTITUTION">
			<xsl:attribute name="SOURCE">UMInstanceName</xsl:attribute>
			<xsl:attribute name="TARGET"><xsl:value-of select="@INSTANCE_NAME"/></xsl:attribute>
		</xsl:element>
	</xsl:element>
	</xsl:element>
	<xsl:element name="CMX_CHANNEL_RESOURCE_LIST">
	</xsl:element>
</xsl:template>

</xsl:stylesheet>
