//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LM75_Drv.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for LM75 driver                             
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_LM75_include
#define `@LIB_NAME`_LM75_include

#include <M8C.h>
`@LM75_ORDINAL_DEFINE_BLOCK`
#define `@LIB_NAME`_LM75_COUNT `@LM75_COUNT`

typedef struct
{
   BYTE bChannelID;         // Channel ID.
   BYTE bAddress;           // I2C Address
   BYTE bInstance;          // Instance of LM75, zero based count
}`@LIB_NAME`_LM75_ParameterBlock;

// Function prototypes
void `@LIB_NAME`_LM75_Instantiate(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK );
int  `@LIB_NAME`_LM75_GetValue(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK);

#endif
