//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LM75.c
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  This file contains the C code for the LM75 driver.  I uses
//                the I2C master channel to communicate to the remote LM75.
//                Up to 8 LM75s may be used in a single system.  The 8 device
//                limitation is because only 8 unique addresses may be selected
//                for the LM75.
//
//                Note:
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "PSoCAPI.h" 
#include "`@LIB_NAME`_LM75.h"
#include "cmx.h"
#include "SystemTimer.h"


int  `@LIB_NAME`_LM75_LastValue[`@LIB_NAME`_LM75_COUNT];
BYTE `@LIB_NAME`_LM75_DelayCount;
BYTE `@LIB_NAME`_LM75_NextInstance;
#define  `@LIB_NAME`_LM75_DELAY  32/`@LIB_NAME`_LM75_COUNT

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LM75_Instantiate(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function starts up the I2Cm interface.  Global interrupts should be 
//    enabled prior to this call. 
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void `@LIB_NAME`_LM75_Instantiate(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK)
{
   
   BYTE bInstance;
   bInstance = thisBLK->bInstance;
    
   `@LIB_NAME`_LM75_DelayCount = SystemTimer_bGetTickCntr() + `@LIB_NAME`_LM75_DELAY + 1;  // Force timeout          
   `@LIB_NAME`_LM75_NextInstance = bInstance;                                              // Force current instance
   `@LIB_NAME`_LM75_LastValue[bInstance] = `@LIB_NAME`_LM75_GetValue(thisBLK);             // Get value
   `@LIB_NAME`_LM75_DelayCount = SystemTimer_bGetTickCntr();                               // Reset for normal delay
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LM75_GetValue(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    Adds the address given by bAddrOffset with the default address.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS:  None
//     
//
//  RETURNS: 
//     Returns the Temperature in C times 10.
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
int `@LIB_NAME`_LM75_GetValue(const `@LIB_NAME`_LM75_ParameterBlock * thisBLK)
{
    // Variable declaration
    BYTE bAddress;                             // LM75 I2C address.
    BYTE bSubAddress;                          // Internal register offset.
    BYTE bStatus;                              // I2C comm status
    BYTE bInstance;                            // Current LM75 instance

    BYTE half_flag;
    union
    {
        struct
        {
            BYTE character0;
            BYTE character1;
        } string;                              // This struct used to eliminate array indexing code generation.
        int temperature;
    } sensor_data;                             // This union used to save code and stack space

	
    bAddress = thisBLK->bAddress + 0x48;       // Get address and add hardware addr offset.
    bInstance = thisBLK->bInstance;            // Get Instance of this LM75.

    if(`@LIB_NAME`_LM75_NextInstance == bInstance)         // Check if we are on the correct instance
    {  
        // Check to see if enough time has passed to get the next temperature reading
        if ((BYTE)(SystemTimer_bGetTickCntr() - `@LIB_NAME`_LM75_DelayCount) > `@LIB_NAME`_LM75_DELAY )
        {
            // Reset the delay counter     
            `@LIB_NAME`_LM75_DelayCount = SystemTimer_bGetTickCntr();

            // Sub address is always 0
            bSubAddress = 0;

            // Place the max reading and an invalid code in the sensor data (for error detection)
            sensor_data.string.character0 = 125;                    // Set to max reading
            sensor_data.string.character1 = 0x55;                   // Set for invalid code

            // Get temperature reading
            I2Cm_bWriteBytes(bAddress, &bSubAddress, 1, I2Cm_CompleteXfer);
            bStatus = I2Cm_fReadBytes(bAddress, &sensor_data.string.character0, 2, I2Cm_CompleteXfer);

            if((bStatus != 0) && (sensor_data.string.character1 != 0x55))
            {
                // Determine if there is a 0.5 degree offset
                half_flag = sensor_data.string.character1 & 0x80;


                // Shift the non-fractional portion of the temperature all the way to the right
                sensor_data.string.character1 = sensor_data.string.character0;

                // Sign extend the temperature
                if (sensor_data.string.character1 & 0x80)
                {
                    sensor_data.string.character0 = 0xFF;
                }
                else
                {
                    sensor_data.string.character0 = 0x00;
                }
                
                // Multiply by the scaling factor (10)
                sensor_data.temperature = sensor_data.temperature * 10;

                // Add the extra 0.5 degrees if needed
                if(half_flag)
                {          
                    sensor_data.temperature += 5;
                }
            }
            else
            {
                // Communication was bad, set the output value to the max temperature
                sensor_data.temperature = 1250;
            }

   
            // Update the temperature array
            `@LIB_NAME`_LM75_LastValue[bInstance] = sensor_data.temperature;


            // Advance to the next instance
            `@LIB_NAME`_LM75_NextInstance++;
            if(`@LIB_NAME`_LM75_NextInstance >= `@LIB_NAME`_LM75_COUNT)
            {
                // Reset to the first instance
                `@LIB_NAME`_LM75_NextInstance = 0;
            }
        } // End timeout
    } // End Instance match

    return `@LIB_NAME`_LM75_LastValue[bInstance];  
}
