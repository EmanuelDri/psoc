//
// SystemVars.h
//

#ifndef SYSTEM_VARS_H
#define SYSTEM_VARS_H

#include "CMXSystem.h"
#include "driverdecl.h"

void UpdateVariables( void);

typedef struct
{

	struct
	{
		int pse_AnalogTemp1;
		int pse_AnalogTemp2;
		int pse_AnalogTemp3;
		int pse_AnalogTemp4;
		int pse_AnalogTemp5;
		int pse_AnalogTemp6;
		int pse_AnalogTemp7;
		int pse_AnalogTemp8;
		int pse_DigitalTemp1;
		int pse_DigitalTemp2;
		int pse_DigitalTemp3;
		int pse_DigitalTemp4;
		int pse_DigitalTemp5;
		BYTE pse_Fan1;
		BYTE pse_Fan10;
		BYTE pse_Fan11;
		BYTE pse_Fan12;
		BYTE pse_Fan13;
		BYTE pse_Fan2;
		BYTE pse_Fan3;
		BYTE pse_Fan4;
		BYTE pse_Fan5;
		BYTE pse_Fan6;
		BYTE pse_Fan7;
		BYTE pse_Fan8;
		BYTE pse_Fan9;
		int pse_FanSpeed1;
		int pse_FanSpeed10;
		int pse_FanSpeed11;
		int pse_FanSpeed12;
		int pse_FanSpeed13;
		int pse_FanSpeed2;
		int pse_FanSpeed3;
		int pse_FanSpeed4;
		int pse_FanSpeed5;
		int pse_FanSpeed6;
		int pse_FanSpeed7;
		int pse_FanSpeed8;
		int pse_FanSpeed9;
		BYTE pse_RampFunction1;
		int pse_RampFunction10;
		int pse_RampFunction11;
		int pse_RampFunction12;
		int pse_RampFunction13;
		BYTE pse_RampFunction2;
		BYTE pse_RampFunction3;
		BYTE pse_RampFunction4;
		BYTE pse_RampFunction5;
		BYTE pse_RampFunction6;
		BYTE pse_RampFunction7;
		BYTE pse_RampFunction8;
		int pse_RampFunction9;

	} ReadOnlyVars;
} SYSTEM_VARS_STRUC;

#define SYSTEM_VARIABLE_COUNT 83 			// Total size of all variables. ( 2 x ints + 1 x BYTEs )
#define SYSTEM_RW_VARIABLE_COUNT 0	// Total size of the read/write variables. ( 2 x ints + 1 x BYTEs )

#endif
