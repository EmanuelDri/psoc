//
// SystemVars.c
//
//   Updates system variables
#include "CMXSystem.h"
#include "CMXSystemFunction.h"
#include "SystemConst.h"
#include "FunctionParamDecl.h"
#include "SystemVars.h"

// system variable structure definition
SYSTEM_VARS_STRUC SystemVars;

void pse_RampFunction1_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction1;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp1<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp1<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp1/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction1 = outputIndex;


}

void pse_RampFunction10_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction10;

	if (SystemVars.ReadOnlyVars.pse_DigitalTemp2 < 300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_DigitalTemp2 <= 1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_DigitalTemp2/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction10 = outputIndex;


}

void pse_RampFunction11_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction11;

	if (SystemVars.ReadOnlyVars.pse_DigitalTemp3 < 300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_DigitalTemp3 <= 1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_DigitalTemp3/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction11 = outputIndex;


}

void pse_RampFunction12_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction12;

	if (SystemVars.ReadOnlyVars.pse_DigitalTemp4 < 300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_DigitalTemp4 <= 1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_DigitalTemp4/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction12 = outputIndex;


}

void pse_RampFunction13_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction13;

	if (SystemVars.ReadOnlyVars.pse_DigitalTemp5 < 300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_DigitalTemp5 < 1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_DigitalTemp5/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction13 = outputIndex;


}

void pse_RampFunction2_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction2;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp2<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp2<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp2/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction2 = outputIndex;


}

void pse_RampFunction3_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction3;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp3<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp3<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp3/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction3 = outputIndex;


}

void pse_RampFunction4_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction4;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp4<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp4<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp4/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction4 = outputIndex;


}

void pse_RampFunction5_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction5;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp5<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp5<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp5/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction5 = outputIndex;


}

void pse_RampFunction6_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction6;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp6<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp6<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp6/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction6 = outputIndex;


}

void pse_RampFunction7_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction7;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp7<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp7<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp7/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction7 = outputIndex;


}

void pse_RampFunction8_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction8;

	if (SystemVars.ReadOnlyVars.pse_AnalogTemp8<300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_AnalogTemp8<=1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_AnalogTemp8/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction8 = outputIndex;


}

void pse_RampFunction9_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction9;

	if (SystemVars.ReadOnlyVars.pse_DigitalTemp1 < 300)
	{
		outputIndex = 0;
	}
	else if (SystemVars.ReadOnlyVars.pse_DigitalTemp1 <= 1000)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_DigitalTemp1/10;
	}
	else if (1)
	{
		outputIndex = 100;
	}

	SystemVars.ReadOnlyVars.pse_RampFunction9 = outputIndex;


}

void pse_Fan1_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan1;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction1;
	}

	SystemVars.ReadOnlyVars.pse_Fan1 = outputIndex;


}

void pse_Fan10_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan10;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction10;
	}

	SystemVars.ReadOnlyVars.pse_Fan10 = outputIndex;


}

void pse_Fan11_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan11;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction11;
	}

	SystemVars.ReadOnlyVars.pse_Fan11 = outputIndex;


}

void pse_Fan12_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan12;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction12;
	}

	SystemVars.ReadOnlyVars.pse_Fan12 = outputIndex;


}

void pse_Fan13_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan13;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction13;
	}

	SystemVars.ReadOnlyVars.pse_Fan13 = outputIndex;


}

void pse_Fan2_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan2;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction2;
	}

	SystemVars.ReadOnlyVars.pse_Fan2 = outputIndex;


}

void pse_Fan3_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan3;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction3;
	}

	SystemVars.ReadOnlyVars.pse_Fan3 = outputIndex;


}

void pse_Fan4_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan4;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction4;
	}

	SystemVars.ReadOnlyVars.pse_Fan4 = outputIndex;


}

void pse_Fan5_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan5;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction5;
	}

	SystemVars.ReadOnlyVars.pse_Fan5 = outputIndex;


}

void pse_Fan6_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan6;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction6;
	}

	SystemVars.ReadOnlyVars.pse_Fan6 = outputIndex;


}

void pse_Fan7_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan7;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction7;
	}

	SystemVars.ReadOnlyVars.pse_Fan7 = outputIndex;


}

void pse_Fan8_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan8;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction8;
	}

	SystemVars.ReadOnlyVars.pse_Fan8 = outputIndex;


}

void pse_Fan9_TransferFunction(void)
{
	BYTE outputIndex = SystemVars.ReadOnlyVars.pse_Fan9;

	if (1)
	{
		outputIndex = SystemVars.ReadOnlyVars.pse_RampFunction9;
	}

	SystemVars.ReadOnlyVars.pse_Fan9 = outputIndex;


}

