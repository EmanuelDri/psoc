//----------------------------------------------------------------------------
// DriverDecl.h
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "CMX_I2C.h"
#include "CMX_LM75.h"
#include "CMX_LM75.h"
#include "CMX_LM75.h"
#include "CMX_LM75.h"
#include "CMX_LM75.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_LM20.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_FAN.h"
#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"

#include "CMX_TACH.h"


// Driver ID's
#define ID_pse_Monitor 1
#define ID_pse_DigitalTemp1 2
#define ID_pse_DigitalTemp2 3
#define ID_pse_DigitalTemp3 4
#define ID_pse_DigitalTemp4 5
#define ID_pse_DigitalTemp5 6
#define ID_pse_AnalogTemp1 7
#define ID_pse_AnalogTemp2 8
#define ID_pse_AnalogTemp3 9
#define ID_pse_AnalogTemp4 10
#define ID_pse_AnalogTemp5 11
#define ID_pse_AnalogTemp6 12
#define ID_pse_AnalogTemp7 13
#define ID_pse_AnalogTemp8 14
#define ID_pse_Fan1 15
#define ID_pse_Fan10 16
#define ID_pse_Fan11 17
#define ID_pse_Fan12 18
#define ID_pse_Fan13 19
#define ID_pse_Fan2 20
#define ID_pse_Fan3 21
#define ID_pse_Fan4 22
#define ID_pse_Fan5 23
#define ID_pse_Fan6 24
#define ID_pse_Fan7 25
#define ID_pse_Fan8 26
#define ID_pse_Fan9 27
#define ID_pse_FanSpeed1 28
#define ID_pse_FanSpeed10 29
#define ID_pse_FanSpeed11 30
#define ID_pse_FanSpeed12 31
#define ID_pse_FanSpeed13 32
#define ID_pse_FanSpeed2 33
#define ID_pse_FanSpeed3 34
#define ID_pse_FanSpeed4 35
#define ID_pse_FanSpeed5 36
#define ID_pse_FanSpeed6 37
#define ID_pse_FanSpeed7 38
#define ID_pse_FanSpeed8 39
#define ID_pse_FanSpeed9 40

// Channel ID's
#define ID_ADC_00 0
#define ID_I2C_00 0
#define ID_I2Cm_00 0
#define ID_I2Cm_01 1
#define ID_I2Cm_02 2
#define ID_I2Cm_03 3
#define ID_I2Cm_04 4
#define ID_MVOLTS_00 0
#define ID_MVOLTS_01 1
#define ID_MVOLTS_02 2
#define ID_MVOLTS_03 3
#define ID_MVOLTS_04 4
#define ID_MVOLTS_05 5
#define ID_MVOLTS_06 6
#define ID_MVOLTS_07 7
#define ID_PWM_00 0
#define ID_PWM_04 1
#define ID_PWM_05 2
#define ID_PWM_06 3
#define ID_PWM_07 4
#define ID_PWM_08 5
#define ID_PWM_09 6
#define ID_PWM_10 7
#define ID_PWM_11 8
#define ID_PWM_12 9
#define ID_PWM_13 10
#define ID_PWM_14 11
#define ID_PWM_15 12
#define ID_SHADOWREGS_0 0
#define ID_SHADOWREGS_1 1
#define ID_SHADOWREGS_2 2
#define ID_SHADOWREGS_3 3
#define ID_SHADOWREGS_4 4
#define ID_ShI2Cm_22 22
#define ID_TACH_08 0
#define ID_TACH_09 1
#define ID_TACH_10 2
#define ID_TACH_11 3
#define ID_TACH_12 4
#define ID_TACH_14 5
#define ID_TACH_16 6
#define ID_TACH_17 7
#define ID_TACH_18 8
#define ID_TACH_19 9
#define ID_TACH_20 10
#define ID_TACH_21 11
#define ID_TACH_22 12
#define ID_TACH_Timer 1

// Channel Type maximum count
#define CMX_ADC_CHAN_MAX 1
#define CMX_I2C_IFC_CHAN_MAX 1
#define CMX_I2Cm_CHAN_MAX 5
#define CMX_MVOLTS_IN_CHAN_MAX 8
#define CMX_PWM_CHAN_MAX 13
#define CMX_SHADOW_MAX 5
#define CMX_I2Cm_Shared_CHAN_MAX 1
#define CMX_TACH_IN_CHAN_MAX 13
#define CMX_TACHTIMER_CHAN_MAX 1

// Channel Type instantiation count
#define CMX_ADC_CHAN_COUNT 0
#define CMX_I2C_IFC_CHAN_COUNT 1
#define CMX_I2Cm_CHAN_COUNT 5
#define CMX_MVOLTS_IN_CHAN_COUNT 8
#define CMX_PWM_CHAN_COUNT 13
#define CMX_SHADOW_COUNT 0
#define CMX_I2Cm_Shared_CHAN_COUNT 0
#define CMX_TACH_IN_CHAN_COUNT 13
#define CMX_TACHTIMER_CHAN_COUNT 0

// Driver Parameter Block externs
extern CMX_I2C_ParameterBlock const pse_Monitor;
extern CMX_LM75_ParameterBlock const pse_DigitalTemp1;
extern CMX_LM75_ParameterBlock const pse_DigitalTemp2;
extern CMX_LM75_ParameterBlock const pse_DigitalTemp3;
extern CMX_LM75_ParameterBlock const pse_DigitalTemp4;
extern CMX_LM75_ParameterBlock const pse_DigitalTemp5;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp1;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp2;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp3;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp4;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp5;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp6;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp7;
extern CMX_LM20_ParameterBlock const pse_AnalogTemp8;
extern CMX_FAN_ParameterBlock const pse_Fan1;
extern CMX_FAN_ParameterBlock const pse_Fan10;
extern CMX_FAN_ParameterBlock const pse_Fan11;
extern CMX_FAN_ParameterBlock const pse_Fan12;
extern CMX_FAN_ParameterBlock const pse_Fan13;
extern CMX_FAN_ParameterBlock const pse_Fan2;
extern CMX_FAN_ParameterBlock const pse_Fan3;
extern CMX_FAN_ParameterBlock const pse_Fan4;
extern CMX_FAN_ParameterBlock const pse_Fan5;
extern CMX_FAN_ParameterBlock const pse_Fan6;
extern CMX_FAN_ParameterBlock const pse_Fan7;
extern CMX_FAN_ParameterBlock const pse_Fan8;
extern CMX_FAN_ParameterBlock const pse_Fan9;
extern CMX_TACH_ParameterBlock const pse_FanSpeed1;
extern CMX_TACH_ParameterBlock const pse_FanSpeed10;
extern CMX_TACH_ParameterBlock const pse_FanSpeed11;
extern CMX_TACH_ParameterBlock const pse_FanSpeed12;
extern CMX_TACH_ParameterBlock const pse_FanSpeed13;
extern CMX_TACH_ParameterBlock const pse_FanSpeed2;
extern CMX_TACH_ParameterBlock const pse_FanSpeed3;
extern CMX_TACH_ParameterBlock const pse_FanSpeed4;
extern CMX_TACH_ParameterBlock const pse_FanSpeed5;
extern CMX_TACH_ParameterBlock const pse_FanSpeed6;
extern CMX_TACH_ParameterBlock const pse_FanSpeed7;
extern CMX_TACH_ParameterBlock const pse_FanSpeed8;
extern CMX_TACH_ParameterBlock const pse_FanSpeed9;
