//*****************************************************************************
//*****************************************************************************
//  FILENAME:  ADCMux8_LLD.h              
//  @Version@
//  
//
//  DESCRIPTION:  This files contains the conversion from ADC counts to
//                mVolts.  The driver will ALWAYS cal the " iGetChanMVolts()"
//                function to retrieve the mVolts result.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "calibration.h"
#include "CMX_ADC_CHAN.h"

extern void * const DriverParams[]; 

// ADC Parameters
//
#define ADC_RESOLUTION  12



// *********************** iGetChanMVolts() *******************************
//
// Description:
//   This function returns the reading of a given channel in mVolts (int).
//
//  Input:
//    BYTE bChan  <= Voltage channel number
//
//
//
int iGetChanMVolts(BYTE bChan)
{
   long lTemp;                                                    // Temp long variable 

   lTemp = (long)(iGetAinChanCounts(bChan) + ADC_Offset) * 1000;
   // The COUNTS_PER_VOLT is a default value at this time.  Later it
   // will be set by a calibration sequence.
   lTemp = lTemp/(CMX_ADC_CHAN_COUNTS16_PER_VOLT >> (16-CMX_ADC_CHAN_RESOLUTION));

   return ((int)lTemp);
}

// *********************** fGetChanVolts() *******************************
//
// Description:
//   This function returns the value in volts (float) for the given 
//   channel.
//
//  Input:
//    BYTE bChan    <= Voltage channel number
//    float fResult <= Pointer a float for the result
//
//
//
//float * fGetChanVolts(BYTE bChan, float * fResult)
//{
//
//   *fResult = (float)iGetAinChanCounts(bChan);
//   
   // The COUNTS_PER_VOLT is a default value at this time.  Later it
   // will be set by a calibration sequence.
//   *fResult = *fResult/(COUNTS_PER_VOLT >> (16-ADC_RESOLUTION));
//
//   return (fResult);
//}
