;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: CMX_TACHTIMER_CHAN.asm
;;   @Version@
;;  
;;
;;  DESCRIPTION: Low level driver for tach. Called function for tach Interrupt 
;;  Service Routine
;;
;;  Theory of Operation:
;;    The low level Tach measurement system used the Tach_Timer user module
;;    and the functions included in this file to measure fan speed from the
;;    fan Tach output.
;;
;;    The tachTimer16 user module is a modified version of the Timer16 user
;;    module.  In addition to the capture interrupt (LSB block), the terminal
;;    count interrupt (MSB block) is enabled as well.  The terminal count
;;    interrupt is used to detect a timer overflow (slow or stoped fan) and
;;    to control the amount of delay time between measurements.  The capture
;;    timer interrupt, measures the interval between two rising edges of the
;;    fan tach output. Below is a timing diagram of how the interrupts may 
;;    look during tach measurement.
;;
;;
;;                         1         2         3           N            1         2
;;        MSB IRQ ____|_________|_________|_____//____|__________|_____________|____
;;
;;        LSB IRQ ______|_|_|___________________//_________________|_|_|__________
;;
;;  Tach Mearsure     |---------|                                 |------------|
;;
;;  Measure Delay               |---------------//----------------|
;;
;;      Fan Ouput ||||----------||||||||||||||||//|||||||||||||||||-------------|||||
;;
;;
;;    During a tach measurement, the fan power must remain stable during the 
;;    entire measurement period.  This means that if the fan is driven with
;;    a PWM output, it must be disabled and driven steady.  Since during the 
;;    measurement cycle the fan is driven on 100%, if the tach measurement was
;;    taken too often, the fan would speed up instead of staying at a lower
;;    desired RPM.  Therefore, a tach measurement on a single fan is only taken
;;    once every N cycles of the MSB IRQ, N is about 24.
;;
;;    At the beginning of a tach measurement cycle "1", the LSB IRQ is enabled.
;;    The first LSB IRQ is skiped to allow for the system to settle, the second
;;    IRQ captures the time of the first rising edge of the tach output.  The
;;    3rd IRQ captures the second edge of the fan's tach output.   The difference
;;    in time between these two edges are stored in the aiTACH_Result array.  The
;;    values stored in this array are used to calculate the fan RPM. If a terminal
;;    count (MSB) IRQ occurs before the 3rd timer capture (LSB) IRQ, it is assumed
;;    that the fan is turning very slow or stoped.
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"
include "memory.inc"
include "TACH_Timer.inc"
include "Driverdecl.inc"

;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------

export   InitTachLLD
export  _InitTachLLD

export  _TACH_LLD_MSB_ISR
export  _TACH_LLD_LSB_ISR

export  _TachState
export   TachState

export  _aiTACH_Results

export  TACH_TABLE
export _TACH_TABLE

;------------------------
;  Constant Definitions
;------------------------

; Integer byte offsets
MSB:     equ   0         
LSB:     equ   1

; Tach state machine definitions
TACH_STATE1_FanOnHigh:    equ   0
TACH_STATE2_Settling:     equ   1
TACH_STATE3_FirstEdge:    equ   2
TACH_STATE4_SecondEdge:   equ   3
TACH_STATE5_Complete:     equ   4

; PWM Flags for FANFlags[] array
FAN_KickStart:            equ   0x01     ; Set if Fan must be kick started for low PWM values
FAN_TachSpin:             equ   0x02     ; Set if Fan must be on 100% for Tach reading
FAN_Polarity:             equ   0x04     ; Set if Fan is positive

; FAN association flag 
FAN_NotAssoc:             equ   0xFF     ; Set if Tach doesn't have associated fan

;------------------------
; Variable Allocation
;------------------------
AREA    bss(RAM)
_TachTickCounts:
 TachTickCounts:    Blk (2*CMX_TACH_IN_CHAN_COUNT)        ; Holds tick count for delay between Tach readings

_aiTACH_Results:                                          ; Tach result RAM allocation
 aiTACH_Results:    BLK (2*CMX_TACH_IN_CHAN_COUNT)        ; 2 Bytes per channel

AREA InterruptRAM (RAM, REL, CON)

_TachState:         
 TachState:         Blk 1                                 ; State variable for Tach Timer (TachTimer UM LSB IRQ)

 ChannelNumber:     Blk 1                                 ; Tach channel number

 TmpValue:          Blk 1                                 ; temporary holding byte used throughout ISR

 TachReading:       Blk 2                                 ; Used to calculate Tach period.
 
AREA UserModules (ROM, REL)

;-----------------------------------------------------------------------------
;  FUNCTION NAME:     void InitTachLLD(void)
;
;  DESCRIPTION: This function initializes the variable and state machine for the
;               Tach measurement process.
;
;-----------------------------------------------------------------------------
;

.LITERAL
 TACH_TABLE:
_TACH_TABLE:
	db 4h // TACH_08 pulses per revolution 4
	db 4h // TACH_09 pulses per revolution 4
	db 4h // TACH_10 pulses per revolution 4
	db 4h // TACH_11 pulses per revolution 4
	db 4h // TACH_12 pulses per revolution 4
	db 4h // TACH_14 pulses per revolution 4
	db 4h // TACH_16 pulses per revolution 4
	db 4h // TACH_17 pulses per revolution 4
	db 4h // TACH_18 pulses per revolution 4
	db 4h // TACH_19 pulses per revolution 4
	db 4h // TACH_20 pulses per revolution 4
	db 4h // TACH_21 pulses per revolution 4
	db 4h // TACH_22 pulses per revolution 4

TACHChannelBlocks:
	db 02h
 
_TACHChannelPins:
 TACHChannelPins:
	db 10h
	db 11h
	db 12h
	db 13h
	db 14h
	db 16h
	db 20h
	db 21h
	db 22h
	db 23h
	db 24h
	db 25h
	db 26h

_TACHChannelTickCounts:
 TACHChannelTickCounts:
	db 40h // TACH_08 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_09 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_10 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_11 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_12 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_14 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_16 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_17 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_18 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_19 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_20 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_21 Number of System Timer 64Hz ticks between updates
	db 40h // TACH_22 Number of System Timer 64Hz ticks between updates
.ENDLITERAL

 InitTachLLD:
_InitTachLLD:
   RAM_PROLOGUE RAM_USE_CLASS_4
   RAM_PROLOGUE RAM_USE_CLASS_3              ; Use IDX_PP for indexing in LMM
   
   RAM_SETPAGE_CUR >ChannelNumber            ; This only needs to be done in LMM
   RAM_SETPAGE_IDX >_TachTickCounts          ; This only needs to be done in LMM   

   mov   A, 0x00
   mov   [ChannelNumber],A
   mov   [TachState],TACH_STATE1_FanOnHigh
   mov   X,0x00

.tick_init_loop:
   index TACHChannelTickCounts
   cpl   A
   mov   [X+TachTickCounts],0xFF            ; TachTickCounts[i] MSB
   inc   X
   mov   [X+TachTickCounts], A              ; TachTickCounts[i] LSB
   inc   X
   mov   A,X
   cmp   A,(2*CMX_TACH_IN_CHAN_COUNT)
   jc    .tick_init_loop
   
   RAM_SETPAGE_IDX >_TachFanAssoc           ; This only needs to be done in LMM
   mov   X, 0x00
.tick_init_TachFanAssoc:
   mov   [X +_TachFanAssoc],FAN_NotAssoc    ;init TachFanAssoc by Not assoc Fan value 
   inc   X
   mov   A,X
   cmp   A,(CMX_TACH_IN_CHAN_COUNT)
   jc    .tick_init_TachFanAssoc   
   
   TACH_Timer_EnableTerminalInt_M           ;Turn on JUST the Terminal count IRQ
   TACH_Timer_EnableCaptureInt_M
   lcall TACH_Timer_Start
   
   RAM_EPILOGUE RAM_USE_CLASS_3
   RAM_EPILOGUE RAM_USE_CLASS_4   
   ret

;-----------------------------------------------------------------------------
;  FUNCTION NAME: NextChannel
;
;  DESCRIPTION: Increments to the next channel and compares for maximum channel.
;               Changes pin row connections for next channel Tach timer reading.
;               Resets ram variables to initial conditions.
;               Look up VC2 sampling rate from TACH_TABLE and write to VC2.
;  WARNING: 
;               Use CUR_PP=0 and PageMode=0x03 (use STK_PP for indexing) 
;               before function call 
;
;-----------------------------------------------------------------------------
;
IF (CMX_TACH_IN_CHAN_COUNT)
NextChannel:
   mov   A,0x00
   index TACHChannelBlocks
   mov   X,A                                ; Tach block_num in X

   mov   A, [ChannelNumber]                 ; Tach port_pin in A
   index TACHChannelPins

   lcall DisableInputToBlock                ; disconnects port_pin from global

   inc   [ChannelNumber]                    ; Increment to next channel
   cmp   [ChannelNumber],(CMX_TACH_IN_CHAN_COUNT)
   jc    .nxtchn                            ; If [ChannelNumber] >= CMX_TACH_IN_CHAN_COUNT
   mov   [ChannelNumber],0x00               ; Reset channel count to zero
.nxtchn:                                    ; Else, continue
   mov A, 0x00
   index TACHChannelBlocks
   mov X,A                                  ; Tach block_num in X
   mov   A, [ChannelNumber]                 ; Tach port_pin in A
   index TACHChannelPins

   lcall EnableInputToBlock                 ; routes port_pin to block_num

   mov   A,0xFF
   mov   X,0xFF
   lcall TACH_Timer_WritePeriod             ; load period
   or    reg[TACH_Timer_CONTROL_LSB_REG],02h   ; set LSB for capture Int
   ;  Set up VC2 divider for next tach reading
   M8C_SetBank1

   mov   A,[ChannelNumber]
   index TACH_TABLE                         ; Get the pulses per revolution value
   sub   A,01h
   mov   [TmpValue],A                       ; Preserve the  pulses per revolution value
   mov   A,reg[OSC_CR1]                     ; Get the OSC_CR1 register value
   and   A,0xF0                             ; modify the OSC_CR1 register value
   or    A,[TmpValue]
   mov   reg[OSC_CR1],A                     ; Set the OSC_CR1 register  

   M8C_SetBank0
   ret
ENDIF
;-----------------------------------------------------------------------------
;  FUNCTION NAME: _TACH_LLD_MSB_ISR
;
;  DESCRIPTION: 
;       The TACH MSB ISR controls how often tach measurements are taken.
;       During a TACH measurement, the Fan output is driven 100% so that the
;       TACH reading is not corrupted by the power switching on and off.
;       Because of this, tach measurements should only be taken too often.
;       This ISR limits this time to about 4% of each Fan drive.  The constant
;       TACH_CHANNEL_DELAY_TIME controls the delay between each tach measurement.
;       At the end of each delay period, the LSB ISR state machine is reset
;       and a measurement begins.
;
;-----------------------------------------------------------------------------
;
_TACH_LLD_MSB_ISR:
IF (CMX_TACH_IN_CHAN_COUNT)
   push  A
   push  X

IF (SYSTEM_LARGE_MEMORY_MODEL)           ; Only required for LMM (Large Memory Model)
   REG_PRESERVE IDX_PP                      ; Save IDX_PP register
   REG_PRESERVE CUR_PP
ENDIF    

   RAM_CHANGE_PAGE_MODE FLAG_PGMODE_10b     ; indexed instructions - by IDX_PP, direct - by CUR_PP
   RAM_SETPAGE_CUR >TachState

   ; End of measurement, reconnect the PWM if any
   TACH_Timer_DisableCaptureInt_M           ; All done with measurement, disable capture IRQ
   ; Check if on last state

   cmp   [TachState],TACH_STATE5_Complete   ; All done, change to Idle state and wait for MSB
   jz    .COMPLETE
.TACH_INCOMPLETE:
   ; Save the data
   mov   A,[ChannelNumber]                  ; Get current index
   asl   A                                  ; mult by two for correct index  (2 byte ints)
   mov   X,A

   RAM_SETPAGE_IDX >_aiTACH_Results
   ; Setup index register for result array.
   mov   [X+aiTACH_Results+MSB],FFh
   mov   [X+aiTACH_Results+LSB],FFh

   mov   [TachState],TACH_STATE5_Complete   ; All done, change to Idle state

   ;  Only required for LMM (Large Memory Model)

   call  Fan_ReConnect                      ; Reconnect if time-out
   ;  Only required for LMM (Large Memory Model)
.COMPLETE:
   ; Go on to the next channel    
   lcall TACH_Timer_Stop

   RAM_CHANGE_PAGE_MODE FLAG_PGMODE_11b     ; Function uses STK_PP for indexing
   lcall NextChannel                        ; Setup for next channel
   RAM_CHANGE_PAGE_MODE FLAG_PGMODE_10b     ; Continue operate on page specified by CUR_PP	    

   ; check if it's ready
   lcall SystemTimer_iGetTickCntr

   push  X ; MSB
   push  A ; LSB
   mov   A, [ChannelNumber]
   asl   A ; 2 Bytes per value
   mov   X, A
   pop   A ; LSB

   RAM_SETPAGE_IDX >TachTickCounts
   sub   A, [X+TachTickCounts+1]
   mov   [TmpValue], A
   pop   A ; MSB
   sbb   A, [X+TachTickCounts]
   jnz   .more_than_four_secs

   mov   A, [ChannelNumber]
   index TACHChannelTickCounts

   cmp   A, [TmpValue]
   jnc   .dont_measure_this_one             ; it is too soon to take a reading on this channel
   ; else we are ready to take a reading
.more_than_four_secs:

   lcall SystemTimer_iGetTickCntr

   push  X ; MSB
   push  A ; LSB

   mov   A, [ChannelNumber]
   asl   A ; 2 Bytes per value
   mov   X, A
   pop   A ; LSB	
   mov   [X+TachTickCounts+1], A
   pop   A ; MSB	
   mov   [X+TachTickCounts], A
   lcall TACH_Timer_Start
   TACH_Timer_EnableCaptureInt_M            ; Enable Capture Timer IRQ

   mov   [TachState],TACH_STATE1_FanOnHigh  ; Reset Tach fan state machine to start
   jmp   TACH_LLD_MSB_ISR_END
.dont_measure_this_one:
   ; just wait for another timer period to try the next one
   ; stay in TACH_STATE5_Complete with Capture int disabled
   lcall TACH_Timer_Start
   TACH_LLD_MSB_ISR_END:

IF (SYSTEM_LARGE_MEMORY_MODEL)           ; Restore IDX_PP register
   REG_RESTORE CUR_PP
   REG_RESTORE IDX_PP
ENDIF

   pop   X
   pop   A
ENDIF
   ret
;-----------------------------------------------------------------------------------------
;  FUNCTION NAME: _TACH_LLD_LSB_ISR
;
;  DESCRIPTION: Interrupt function for Tach LSB. Four states for each time measurement.
;               If 3 wire type the pwm is disconnected from the fan and driven at full speed
;               for a short period and the difference in two edges as the fan spins is used
;               to calculate fan speed. If 4 wire the pwm is not disconnected.
;               TachFanAssoc array contains which fan is associated with which tach.
;               PwmValues contains whether it is 3 wire or 4 wire tach and its drive polarity.
;               TachtoFan is a table lookup for the fan calculated by the index TachFanAssoc 
;               passes for the tach to fan association.
;
;               TACH_STATE1_FanOnHigh:  Disconnect pin associated with the fan and set pin 
;                                       to high or low dependent on polarity.
;               TACH_STATE2_Settling:   Wait for reading to settle, after swtiching input sigal mux.
;               TACH_STATE2_FirstEdge:  Measure and store first edge of tach timer.
;               TACH_STATE4_SecondEdge: Measure second edge and calculate difference store in Ram.
;                                       Reconnect pin. 
;               TACH_STATE5_Complete:       Wait Idle until MSB ISR resets to TACH_STATE1_FanOnHigh
;               
;------------------------------------------------------------------------------------------
;

_TACH_LLD_LSB_ISR:

IF (CMX_TACH_IN_CHAN_COUNT)
   push  X
   push  A

   ; If in large memory model, must make sure that the
   ; Index page registers are saved and restored
   ; since this function changes it.
IF (SYSTEM_LARGE_MEMORY_MODEL)           ;  Only required for LMM (Large Memory Model)
   REG_PRESERVE IDX_PP
   REG_PRESERVE CUR_PP
ENDIF 
   
   RAM_CHANGE_PAGE_MODE FLAG_PGMODE_10b     ; indexed instructions - by IDX_PP, direct - by CUR_PP

   RAM_SETPAGE_CUR >TachState               ; Set CUR_PP to 0 page in LMM
  
   cmp   [TachState],TACH_STATE1_FanOnHigh  ; state transition flag
   jz    .STATE1_FanOnHigh
   cmp   [TachState],TACH_STATE2_Settling
   jz    .STATE2_Settling
   cmp   [TachState],TACH_STATE3_FirstEdge
   jz    .STATE3_FirstEdge
   cmp   [TachState],TACH_STATE4_SecondEdge
   jz    .STATE4_SecondEdge 

   ; Other states just return
   jmp   TACH_LLD_LSB_ISR_End

.STATE4_SecondEdge:
   lcall TACH_Timer_Stop
   TACH_Timer_DisableCaptureInt_M           ; All done with measurement, disable capture IRQ
   lcall TACH_Timer_wReadCompareValue       ; get time of second rising edge on capture

   sub   [TachReading+LSB],A                ; Calculate LSB
   mov   A,X                                ; Move MSB into A
   sbb   [TachReading+MSB],A                ; Calculate MSB
                                            ; calculate Delta (first edge - second edge) and store in RAM

   call  Fan_ReConnect                      ; Reconnect if time-out

   mov   [TachState],TACH_STATE5_Complete   ; All done, change to Idle state and wait for MSB
   ; Save the data
   mov   A,[ChannelNumber]                  ; Get current index
   asl   A                                  ; mult by two for correct index  (2 byte ints)
   mov   X,A                                ; Setup index register for result array.
   mov   A,[TachReading+MSB]

   RAM_SETPAGE_IDX >aiTACH_Results          ; Set IDX_PP to 0 page in LMM    
   mov   [X+aiTACH_Results+MSB],A
   mov   A,[TachReading+LSB] 
   mov   [X+aiTACH_Results+LSB],A
   ; now we're going to wait for the MSB interrupt
   lcall TACH_Timer_Start                   ; ISR to reset to first state.
   jmp   TACH_LLD_LSB_ISR_End
   
.STATE1_FanOnHigh:

IF  CMX_PWM_CHAN_COUNT
   mov   X,[ChannelNumber]                  ; mov ChannelNumber to X

   RAM_SETPAGE_IDX >_TachFanAssoc           ; This only needs to be done if there are TACHs
   mov   A,[X +_TachFanAssoc]               ; get associated pwm channel
   inc   A
   jz    .driveend
   dec   A

   mov   X,A                                ; move pwm channel number to X

   RAM_SETPAGE_IDX >FANFlags
   mov   A,[X+FANFlags]                     ; get pwm info from Interrupt Ram
   and   A,FAN_TachSpin                     ; check to see if Tach control pin associated with Fan is set
   jz    .driveend                          ; if FAN_TachSpin not set go to driveend
   mov   A,[X+FANFlags]
   and   A,FAN_Polarity
   jz    .drivelo
.drivehi:       
   mov   A,0x01
.drivelo:
   swap  A,X
   lcall PWMSuspend                         ; drive low if negative polarity
.driveend:


ENDIF   ; IF CMX_PWM_CHAN_COUNT

   mov   [TachState], TACH_STATE2_Settling
   jmp   TACH_LLD_LSB_ISR_End

.STATE2_Settling:
   mov   [TachState], TACH_STATE3_FirstEdge
   jmp   TACH_LLD_LSB_ISR_End

.STATE3_FirstEdge: 
   lcall TACH_Timer_wReadCompareValue       ; get time of first rising edge on capture

   mov   [TachReading+LSB], A               ; store value
   mov   [TachReading+MSB], X
   mov   [TachState],TACH_STATE4_SecondEdge

TACH_LLD_LSB_ISR_End:

IF (SYSTEM_LARGE_MEMORY_MODEL)           ; Restore IDX_PP register
   REG_RESTORE CUR_PP
   REG_RESTORE IDX_PP
ENDIF

   pop   A
   pop   X
ENDIF
   ret


;-----------------------------------------------------------------------------------------
;  FUNCTION NAME: Fan_ReConnect
;
;  DESCRIPTION: 
;     This function reconnects the Fan to the PWM.
;               
;  WARNING: 
;      Use CUR_PP=0 and PageMode=0x02 (use IDX_PP for indexing) before function call 
;------------------------------------------------------------------------------------------
;
Fan_ReConnect:
IF (CMX_TACH_IN_CHAN_COUNT)
IF (CMX_PWM_CHAN_COUNT)

   RAM_SETPAGE_IDX >_TachFanAssoc           ; This only needs to be done in LMM 

   mov   X,[ChannelNumber]                  ; mov ChannelNumber to X                         
   mov   A,[X +_TachFanAssoc]               ; load fan channel based on tach channel  
   inc   A
   jz    .polarity_end
   dec   A    
   
   mov   X, A                               ; move pwm channel number into X
   RAM_SETPAGE_IDX >FANFlags                ; This only needs to be done in LMM    
   
;   mov   A,[X+FANFlags]                     ; get pwm info from Interrupt Ram
;   and   A,FAN_TachSpin                     ; check to see if Tach control pin associated with Fan is set
;   jz    .polarity_end                      ; if FAN_TachSpin not set go to polarity_end

   mov   A,[X+FANFlags]                     ; get pwm info from Interrupt Ram
   and   A,FAN_Polarity                     ; check to see if Tach control pin associated with Fan is set
   jz   .neg_polarity                               
.pos_polarity:       
   mov   A,0x01  
.neg_polarity:
   swap  A,X
   lcall PWMSetPolarity
.polarity_end:
	
ENDIF ;IF CMX_PWM_CHAN_COUNT
ENDIF ;CMX_TACH_IN_CHAN_COUNT
	ret

; end of file TACH_LLD.asm
