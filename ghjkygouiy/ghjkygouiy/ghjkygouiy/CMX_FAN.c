//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_Fan.c
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  
//
//  DESCRIPTION:  This Driver controls the fan parameters and returns
//                the percent duty cycle for the selected channel.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "CMX_fan.h"
#include "cmx.h"
// Channel type header file
#include "CMX_PWM_CHAN.h"
#include "SystemTimer.h"
#include "projectproperties.h"
  
//-----------------------------------------------------------------------------
//  FUNCTION NAME: FAN_Instantiate(const CMX_FAN_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//     Initialixation for FAN.  Initial conditions are set based on parameters set
//     in Fan Parameter block.      
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void CMX_FAN_Instantiate(const CMX_FAN_ParameterBlock * thisBLK)
{
   CMX_FAN_ParameterBlock thisPB;    // Create RAM parameter block.
   thisPB = *thisBLK;                        // Copy data to parameter block
        
   if (thisPB.FAN_KICKSTART == 1)
   {
      FANFlags[thisPB.FAN_ChannelID] |= 0x01;
   }
   if (thisPB.FAN_KICKTACH == 1)
   {
      FANFlags[thisPB.FAN_ChannelID] |= 0x02;
   }
   if (thisPB.FAN_DrivePolarity == 1)
   {
      FANFlags[thisPB.FAN_ChannelID] |= 0x04;
   }

   PWMSetPolarity(thisPB.FAN_ChannelID, thisPB.FAN_DrivePolarity);
   PWMSetDutyCycle(thisPB.FAN_ChannelID, 0);
   PWMStart(thisPB.FAN_ChannelID);
   FANStates[thisPB.FAN_ChannelID] = 0;
   FANSpinStartTimes[thisPB.FAN_ChannelID] = SystemTimer_bGetTickCntr();   
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: FAN_SetValue(const CMX_FAN_ParameterBlock * thisBLK, BYTE bPercent_DutyCycle)
//
//  DESCRIPTION:  
//    Set Fan speed, byt setting PWM value from 0 to 100 percent.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK            => Pointer to ParameterBlock for this instance.
//    bPercent_DutyCycle => New Duty cycle to set fan
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void  CMX_FAN_SetValue(const CMX_FAN_ParameterBlock * thisBLK, BYTE bPercent_DutyCycle)
{
   CMX_FAN_ParameterBlock  thisPB;    // Create RAM parameter block.
   thisPB = *thisBLK;                         // Copy data to parameter block
        
   if (thisPB.FAN_KICKSTART == 0)
   {
      FANStates[thisPB.FAN_ChannelID] = 2;
   }
   else
   {
      if (bPercent_DutyCycle == 0)            // MinimumSpin
      {
         FANStates[thisPB.FAN_ChannelID] = 0;  // go to off state
         bPercent_DutyCycle = 0;              // turn fan off
         if (thisPB.FAN_KICKTACH == 1)
         {                
            FANFlags[thisPB.FAN_ChannelID] &= 0xfd;
         }
      }
           
      if (FANStates[thisPB.FAN_ChannelID] == 1)   // presently in spin up state
      {
         if ((BYTE)(SystemTimer_bGetTickCntr() - FANSpinStartTimes[thisPB.FAN_ChannelID]) > (thisPB.FAN_SpinUpTime))     
         {
            FANStates[thisPB.FAN_ChannelID] = 2;  // go to on state
            if (thisPB.FAN_KICKTACH == 1)
            {
               FANFlags[thisPB.FAN_ChannelID] |= 0x02;
            }  
            FANSpinStartTimes[thisPB.FAN_ChannelID] = SystemTimer_bGetTickCntr(); // save the time            
         }
      }

      if (FANStates[thisPB.FAN_ChannelID] == 0)  // presently in off state
      {
         if (bPercent_DutyCycle >= 1)           // MinimumSpin
         {
            FANStates[thisPB.FAN_ChannelID] = 1;  // go from off to spin up state
            FANSpinStartTimes[thisPB.FAN_ChannelID] = 0;
            bPercent_DutyCycle = 100;            // set to 100 percent
         }
      }
   }

   if (FANStates[thisPB.FAN_ChannelID] == 1)
   {
      bPercent_DutyCycle = 100;
   }

   PWMSetDutyCycle(thisPB.FAN_ChannelID, bPercent_DutyCycle);
}

