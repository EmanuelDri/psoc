//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_Tach.h
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  
//  DESCRIPTION:  Header file for Tach Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_tach_include
#define CMX_tach_include

#include <M8C.h>

#define TACH_pse_FanSpeed1_ORDINAL  0
#define TACH_pse_FanSpeed10_ORDINAL  1
#define TACH_pse_FanSpeed11_ORDINAL  2
#define TACH_pse_FanSpeed12_ORDINAL  3
#define TACH_pse_FanSpeed13_ORDINAL  4
#define TACH_pse_FanSpeed2_ORDINAL  5
#define TACH_pse_FanSpeed3_ORDINAL  6
#define TACH_pse_FanSpeed4_ORDINAL  7
#define TACH_pse_FanSpeed5_ORDINAL  8
#define TACH_pse_FanSpeed6_ORDINAL  9
#define TACH_pse_FanSpeed7_ORDINAL 10
#define TACH_pse_FanSpeed8_ORDINAL 11
#define TACH_pse_FanSpeed9_ORDINAL 12
#define CMX_TACH_COUNT 13

#define ID_NoFan              0
#define TACH_EdgeGlitchLimit  20
#define ID_NoAssocFan         0xFF

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE Tach_ID; // Instance ID
    BYTE Tach_PulsesPerRevolution;  //# of pulses per revolution
    BYTE Tach_Fan_Source; /// Fan association
    BYTE Tach_ChannelID;
 
}CMX_TACH_ParameterBlock;
/////////////////////////////

// Function prototypes
int  CMX_TACH_GetValue( const CMX_TACH_ParameterBlock * thisBLK );
void CMX_TACH_Instantiate( const CMX_TACH_ParameterBlock * thisBLK );


#endif
