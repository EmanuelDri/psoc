//*****************************************************************************
//*****************************************************************************
//  FILENAME: CMX_tach.c
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  
//
//  DESCRIPTION: This Driver returns the tachometer reading
//               for the  selected channel
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "m8c.h"
#include "CMX_tach.h"
#include "cmx.h"
#include "driverdecl.h"
#include "CMX_TACH_IN_CHAN.h"

unsigned int LastReading[CMX_TACH_IN_CHAN_COUNT];

#ifndef FAN_PRESENT
typedef struct
{
    BYTE FAN_ChannelID; // Instance ID

}CMX_FAN_ParameterBlock;
const CMX_FAN_ParameterBlock NoFan = {ID_NoAssocFan};
#else
const CMX_FAN_ParameterBlock NoFan = {0, 0, 0, 0, 0, ID_NoAssocFan};
#endif

BYTE TachFanAssoc[CMX_TACH_IN_CHAN_COUNT];


const BYTE * TachFanLookup[] =
{
	&(NoFan.FAN_ChannelID),
	&(pse_Fan10.FAN_ChannelID),
	&(pse_Fan11.FAN_ChannelID),
	&(pse_Fan12.FAN_ChannelID),
	&(pse_Fan13.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(NoFan.FAN_ChannelID),
	&(pse_Fan9.FAN_ChannelID),
};


//-----------------------------------------------------------------------------
//  FUNCTION NAME: TACH_Instantiate(const CMX_TACH_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function loads the TachFanAssoc[] array if there are PWMs present
//    in the project.  If not, this functions does not perform a function.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void CMX_TACH_Instantiate(const CMX_TACH_ParameterBlock * thisBLK)
{

#if CMX_PWM_CHAN_COUNT
        BYTE Instance;
        
        Instance = thisBLK->Tach_ChannelID;
        TachFanAssoc[Instance] = * TachFanLookup[Instance];
#endif
}

// *********************** TACH_GetValue() *******************************
//
//  Description:
//    This function returns tach value for the given Tach_ID.
//
//    input:
//      ID_Index => Index of instance in the pointer to PB table.
//
//   Return Value:
//      RPM of Fan.
//
//  Theory:
//
//-----------------------------------------------------------------------------
//  FUNCTION NAME: TACH_GetValue(const CMX_TACH_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function returns tach value for the given Tach channel.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: 
//    Integervalue representing Fan RPM
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
int  CMX_TACH_GetValue(const CMX_TACH_ParameterBlock * thisBLK)
{
        BYTE         Instance;
        unsigned int newReading;
        long         RPM;
        BYTE         NumOfPulsesPerRev;
        BYTE         VC2Divider;

        Instance = thisBLK->Tach_ChannelID;                     // Get instance of tach
        NumOfPulsesPerRev = thisBLK->Tach_PulsesPerRevolution;
        VC2Divider = TACH_TABLE[Instance];

        if ((aiTACH_Results[Instance] > TACH_EdgeGlitchLimit))
        {
            newReading = (aiTACH_Results[Instance]);
            LastReading[Instance] = newReading;
        }
      
        if ((aiTACH_Results[Instance] <= TACH_EdgeGlitchLimit))
        {
            newReading = LastReading[Instance];
        }

        // Convert to RPM
        RPM = (120000000)/(newReading);
        RPM = RPM/(NumOfPulsesPerRev*VC2Divider);

        // Limit maximum reading to 25000 rpm
        if (RPM > 25000)
        {
            RPM = 25000;
        }
        // Limit minimum reading to 300 rpm
        if (RPM < 300)
        {
            RPM = 0;
        }
        // if no reading was made because MSB of timer timed out return 0 rpm
        if ((aiTACH_Results[Instance] == 0xFFFF))
        {
            RPM = 0;
        }

     
return ( (int) RPM);
}	

