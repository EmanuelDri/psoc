//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_Fan.h
//  Version: 1.1, Updated on 2009/10/29 at 15:6:51 
//  
//
//  DESCRIPTION:  Header file for the Fan Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_fan_include
#define CMX_fan_include

#include <M8C.h>
#define FAN_PRESENT 1

////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE FAN_ID;             // Instance ID
    BYTE FAN_KICKSTART;      // How long to kickstart fan
    BYTE FAN_KICKTACH;       //
    BYTE FAN_SpinUpTime;     // How much time it takes to spin up fan
    BYTE FAN_DrivePolarity;  // 1 non-invert, 0 invert
    BYTE FAN_ChannelID;      // Channel ID
 
}CMX_FAN_ParameterBlock;

// Function prototypes
void CMX_FAN_SetValue(const CMX_FAN_ParameterBlock * thisBLK, BYTE bPercent_DutyCycle);
void CMX_FAN_Instantiate(const CMX_FAN_ParameterBlock * thisBLK);


#endif
