//
// FunctionParamDecl.h
//

#ifndef FUNCTION_PARAMDECL_H
#define FUNCTION_PARAMDECL_H

#include <m8c.h>
#include "CMXSystemFunction.h"

// function parameter block ID's


// system variable value defines
// pse_RampFunction1 output state ID defines
#define ID_pse_RampFunction1_UNASSIGNED 255


// pse_RampFunction1 output state value defines
#define ID_pse_RampFunction1_UNASSIGNED_VALUE 0

// pse_RampFunction10 output state ID defines
#define ID_pse_RampFunction10_UNASSIGNED 255


// pse_RampFunction10 output state value defines
#define ID_pse_RampFunction10_UNASSIGNED_VALUE 0

// pse_RampFunction11 output state ID defines
#define ID_pse_RampFunction11_UNASSIGNED 255


// pse_RampFunction11 output state value defines
#define ID_pse_RampFunction11_UNASSIGNED_VALUE 0

// pse_RampFunction12 output state ID defines
#define ID_pse_RampFunction12_UNASSIGNED 255


// pse_RampFunction12 output state value defines
#define ID_pse_RampFunction12_UNASSIGNED_VALUE 0

// pse_RampFunction13 output state ID defines
#define ID_pse_RampFunction13_UNASSIGNED 255


// pse_RampFunction13 output state value defines
#define ID_pse_RampFunction13_UNASSIGNED_VALUE 0

// pse_RampFunction2 output state ID defines
#define ID_pse_RampFunction2_UNASSIGNED 255


// pse_RampFunction2 output state value defines
#define ID_pse_RampFunction2_UNASSIGNED_VALUE 0

// pse_RampFunction3 output state ID defines
#define ID_pse_RampFunction3_UNASSIGNED 255


// pse_RampFunction3 output state value defines
#define ID_pse_RampFunction3_UNASSIGNED_VALUE 0

// pse_RampFunction4 output state ID defines
#define ID_pse_RampFunction4_UNASSIGNED 255


// pse_RampFunction4 output state value defines
#define ID_pse_RampFunction4_UNASSIGNED_VALUE 0

// pse_RampFunction5 output state ID defines
#define ID_pse_RampFunction5_UNASSIGNED 255


// pse_RampFunction5 output state value defines
#define ID_pse_RampFunction5_UNASSIGNED_VALUE 0

// pse_RampFunction6 output state ID defines
#define ID_pse_RampFunction6_UNASSIGNED 255


// pse_RampFunction6 output state value defines
#define ID_pse_RampFunction6_UNASSIGNED_VALUE 0

// pse_RampFunction7 output state ID defines
#define ID_pse_RampFunction7_UNASSIGNED 255


// pse_RampFunction7 output state value defines
#define ID_pse_RampFunction7_UNASSIGNED_VALUE 0

// pse_RampFunction8 output state ID defines
#define ID_pse_RampFunction8_UNASSIGNED 255


// pse_RampFunction8 output state value defines
#define ID_pse_RampFunction8_UNASSIGNED_VALUE 0

// pse_RampFunction9 output state ID defines
#define ID_pse_RampFunction9_UNASSIGNED 255


// pse_RampFunction9 output state value defines
#define ID_pse_RampFunction9_UNASSIGNED_VALUE 0

// pse_AnalogTemp1 output state ID defines
#define ID_pse_AnalogTemp1_UNASSIGNED 255


// pse_AnalogTemp1 output state value defines
#define ID_pse_AnalogTemp1_UNASSIGNED_VALUE 0

// pse_AnalogTemp2 output state ID defines
#define ID_pse_AnalogTemp2_UNASSIGNED 255


// pse_AnalogTemp2 output state value defines
#define ID_pse_AnalogTemp2_UNASSIGNED_VALUE 0

// pse_AnalogTemp3 output state ID defines
#define ID_pse_AnalogTemp3_UNASSIGNED 255


// pse_AnalogTemp3 output state value defines
#define ID_pse_AnalogTemp3_UNASSIGNED_VALUE 0

// pse_AnalogTemp4 output state ID defines
#define ID_pse_AnalogTemp4_UNASSIGNED 255


// pse_AnalogTemp4 output state value defines
#define ID_pse_AnalogTemp4_UNASSIGNED_VALUE 0

// pse_AnalogTemp5 output state ID defines
#define ID_pse_AnalogTemp5_UNASSIGNED 255


// pse_AnalogTemp5 output state value defines
#define ID_pse_AnalogTemp5_UNASSIGNED_VALUE 0

// pse_AnalogTemp6 output state ID defines
#define ID_pse_AnalogTemp6_UNASSIGNED 255


// pse_AnalogTemp6 output state value defines
#define ID_pse_AnalogTemp6_UNASSIGNED_VALUE 0

// pse_AnalogTemp7 output state ID defines
#define ID_pse_AnalogTemp7_UNASSIGNED 255


// pse_AnalogTemp7 output state value defines
#define ID_pse_AnalogTemp7_UNASSIGNED_VALUE 0

// pse_AnalogTemp8 output state ID defines
#define ID_pse_AnalogTemp8_UNASSIGNED 255


// pse_AnalogTemp8 output state value defines
#define ID_pse_AnalogTemp8_UNASSIGNED_VALUE 0

// pse_DigitalTemp1 output state ID defines
#define ID_pse_DigitalTemp1_UNASSIGNED 255


// pse_DigitalTemp1 output state value defines
#define ID_pse_DigitalTemp1_UNASSIGNED_VALUE 0

// pse_DigitalTemp2 output state ID defines
#define ID_pse_DigitalTemp2_UNASSIGNED 255


// pse_DigitalTemp2 output state value defines
#define ID_pse_DigitalTemp2_UNASSIGNED_VALUE 0

// pse_DigitalTemp3 output state ID defines
#define ID_pse_DigitalTemp3_UNASSIGNED 255


// pse_DigitalTemp3 output state value defines
#define ID_pse_DigitalTemp3_UNASSIGNED_VALUE 0

// pse_DigitalTemp4 output state ID defines
#define ID_pse_DigitalTemp4_UNASSIGNED 255


// pse_DigitalTemp4 output state value defines
#define ID_pse_DigitalTemp4_UNASSIGNED_VALUE 0

// pse_DigitalTemp5 output state ID defines
#define ID_pse_DigitalTemp5_UNASSIGNED 255


// pse_DigitalTemp5 output state value defines
#define ID_pse_DigitalTemp5_UNASSIGNED_VALUE 0

// pse_Fan1 output state ID defines
#define ID_pse_Fan1_Off 0
#define ID_pse_Fan1_Low 1
#define ID_pse_Fan1_Medium 2
#define ID_pse_Fan1_High 3
#define ID_pse_Fan1_UNASSIGNED 255


// pse_Fan1 output state value defines
#define ID_pse_Fan1_Off_VALUE 0
#define ID_pse_Fan1_Low_VALUE 33
#define ID_pse_Fan1_Medium_VALUE 67
#define ID_pse_Fan1_High_VALUE 100
#define ID_pse_Fan1_UNASSIGNED_VALUE 0

// pse_Fan10 output state ID defines
#define ID_pse_Fan10_Off 0
#define ID_pse_Fan10_Low 1
#define ID_pse_Fan10_Medium 2
#define ID_pse_Fan10_High 3
#define ID_pse_Fan10_UNASSIGNED 255


// pse_Fan10 output state value defines
#define ID_pse_Fan10_Off_VALUE 0
#define ID_pse_Fan10_Low_VALUE 33
#define ID_pse_Fan10_Medium_VALUE 67
#define ID_pse_Fan10_High_VALUE 100
#define ID_pse_Fan10_UNASSIGNED_VALUE 0

// pse_Fan11 output state ID defines
#define ID_pse_Fan11_Off 0
#define ID_pse_Fan11_Low 1
#define ID_pse_Fan11_Medium 2
#define ID_pse_Fan11_High 3
#define ID_pse_Fan11_UNASSIGNED 255


// pse_Fan11 output state value defines
#define ID_pse_Fan11_Off_VALUE 0
#define ID_pse_Fan11_Low_VALUE 33
#define ID_pse_Fan11_Medium_VALUE 67
#define ID_pse_Fan11_High_VALUE 100
#define ID_pse_Fan11_UNASSIGNED_VALUE 0

// pse_Fan12 output state ID defines
#define ID_pse_Fan12_Off 0
#define ID_pse_Fan12_Low 1
#define ID_pse_Fan12_Medium 2
#define ID_pse_Fan12_High 3
#define ID_pse_Fan12_UNASSIGNED 255


// pse_Fan12 output state value defines
#define ID_pse_Fan12_Off_VALUE 0
#define ID_pse_Fan12_Low_VALUE 33
#define ID_pse_Fan12_Medium_VALUE 67
#define ID_pse_Fan12_High_VALUE 100
#define ID_pse_Fan12_UNASSIGNED_VALUE 0

// pse_Fan13 output state ID defines
#define ID_pse_Fan13_Off 0
#define ID_pse_Fan13_Low 1
#define ID_pse_Fan13_Medium 2
#define ID_pse_Fan13_High 3
#define ID_pse_Fan13_UNASSIGNED 255


// pse_Fan13 output state value defines
#define ID_pse_Fan13_Off_VALUE 0
#define ID_pse_Fan13_Low_VALUE 33
#define ID_pse_Fan13_Medium_VALUE 67
#define ID_pse_Fan13_High_VALUE 100
#define ID_pse_Fan13_UNASSIGNED_VALUE 0

// pse_Fan2 output state ID defines
#define ID_pse_Fan2_Off 0
#define ID_pse_Fan2_Low 1
#define ID_pse_Fan2_Medium 2
#define ID_pse_Fan2_High 3
#define ID_pse_Fan2_UNASSIGNED 255


// pse_Fan2 output state value defines
#define ID_pse_Fan2_Off_VALUE 0
#define ID_pse_Fan2_Low_VALUE 33
#define ID_pse_Fan2_Medium_VALUE 67
#define ID_pse_Fan2_High_VALUE 100
#define ID_pse_Fan2_UNASSIGNED_VALUE 0

// pse_Fan3 output state ID defines
#define ID_pse_Fan3_Off 0
#define ID_pse_Fan3_Low 1
#define ID_pse_Fan3_Medium 2
#define ID_pse_Fan3_High 3
#define ID_pse_Fan3_UNASSIGNED 255


// pse_Fan3 output state value defines
#define ID_pse_Fan3_Off_VALUE 0
#define ID_pse_Fan3_Low_VALUE 33
#define ID_pse_Fan3_Medium_VALUE 67
#define ID_pse_Fan3_High_VALUE 100
#define ID_pse_Fan3_UNASSIGNED_VALUE 0

// pse_Fan4 output state ID defines
#define ID_pse_Fan4_Off 0
#define ID_pse_Fan4_Low 1
#define ID_pse_Fan4_Medium 2
#define ID_pse_Fan4_High 3
#define ID_pse_Fan4_UNASSIGNED 255


// pse_Fan4 output state value defines
#define ID_pse_Fan4_Off_VALUE 0
#define ID_pse_Fan4_Low_VALUE 33
#define ID_pse_Fan4_Medium_VALUE 67
#define ID_pse_Fan4_High_VALUE 100
#define ID_pse_Fan4_UNASSIGNED_VALUE 0

// pse_Fan5 output state ID defines
#define ID_pse_Fan5_Off 0
#define ID_pse_Fan5_Low 1
#define ID_pse_Fan5_Medium 2
#define ID_pse_Fan5_High 3
#define ID_pse_Fan5_UNASSIGNED 255


// pse_Fan5 output state value defines
#define ID_pse_Fan5_Off_VALUE 0
#define ID_pse_Fan5_Low_VALUE 33
#define ID_pse_Fan5_Medium_VALUE 67
#define ID_pse_Fan5_High_VALUE 100
#define ID_pse_Fan5_UNASSIGNED_VALUE 0

// pse_Fan6 output state ID defines
#define ID_pse_Fan6_Off 0
#define ID_pse_Fan6_Low 1
#define ID_pse_Fan6_Medium 2
#define ID_pse_Fan6_High 3
#define ID_pse_Fan6_UNASSIGNED 255


// pse_Fan6 output state value defines
#define ID_pse_Fan6_Off_VALUE 0
#define ID_pse_Fan6_Low_VALUE 33
#define ID_pse_Fan6_Medium_VALUE 67
#define ID_pse_Fan6_High_VALUE 100
#define ID_pse_Fan6_UNASSIGNED_VALUE 0

// pse_Fan7 output state ID defines
#define ID_pse_Fan7_Off 0
#define ID_pse_Fan7_Low 1
#define ID_pse_Fan7_Medium 2
#define ID_pse_Fan7_High 3
#define ID_pse_Fan7_UNASSIGNED 255


// pse_Fan7 output state value defines
#define ID_pse_Fan7_Off_VALUE 0
#define ID_pse_Fan7_Low_VALUE 33
#define ID_pse_Fan7_Medium_VALUE 67
#define ID_pse_Fan7_High_VALUE 100
#define ID_pse_Fan7_UNASSIGNED_VALUE 0

// pse_Fan8 output state ID defines
#define ID_pse_Fan8_Off 0
#define ID_pse_Fan8_Low 1
#define ID_pse_Fan8_Medium 2
#define ID_pse_Fan8_High 3
#define ID_pse_Fan8_UNASSIGNED 255


// pse_Fan8 output state value defines
#define ID_pse_Fan8_Off_VALUE 0
#define ID_pse_Fan8_Low_VALUE 33
#define ID_pse_Fan8_Medium_VALUE 67
#define ID_pse_Fan8_High_VALUE 100
#define ID_pse_Fan8_UNASSIGNED_VALUE 0

// pse_Fan9 output state ID defines
#define ID_pse_Fan9_Off 0
#define ID_pse_Fan9_Low 1
#define ID_pse_Fan9_Medium 2
#define ID_pse_Fan9_High 3
#define ID_pse_Fan9_UNASSIGNED 255


// pse_Fan9 output state value defines
#define ID_pse_Fan9_Off_VALUE 0
#define ID_pse_Fan9_Low_VALUE 33
#define ID_pse_Fan9_Medium_VALUE 67
#define ID_pse_Fan9_High_VALUE 100
#define ID_pse_Fan9_UNASSIGNED_VALUE 0

// pse_FanSpeed1 output state ID defines
#define ID_pse_FanSpeed1_UNASSIGNED 255


// pse_FanSpeed1 output state value defines
#define ID_pse_FanSpeed1_UNASSIGNED_VALUE 0

// pse_FanSpeed10 output state ID defines
#define ID_pse_FanSpeed10_UNASSIGNED 255


// pse_FanSpeed10 output state value defines
#define ID_pse_FanSpeed10_UNASSIGNED_VALUE 0

// pse_FanSpeed11 output state ID defines
#define ID_pse_FanSpeed11_UNASSIGNED 255


// pse_FanSpeed11 output state value defines
#define ID_pse_FanSpeed11_UNASSIGNED_VALUE 0

// pse_FanSpeed12 output state ID defines
#define ID_pse_FanSpeed12_UNASSIGNED 255


// pse_FanSpeed12 output state value defines
#define ID_pse_FanSpeed12_UNASSIGNED_VALUE 0

// pse_FanSpeed13 output state ID defines
#define ID_pse_FanSpeed13_UNASSIGNED 255


// pse_FanSpeed13 output state value defines
#define ID_pse_FanSpeed13_UNASSIGNED_VALUE 0

// pse_FanSpeed2 output state ID defines
#define ID_pse_FanSpeed2_UNASSIGNED 255


// pse_FanSpeed2 output state value defines
#define ID_pse_FanSpeed2_UNASSIGNED_VALUE 0

// pse_FanSpeed3 output state ID defines
#define ID_pse_FanSpeed3_UNASSIGNED 255


// pse_FanSpeed3 output state value defines
#define ID_pse_FanSpeed3_UNASSIGNED_VALUE 0

// pse_FanSpeed4 output state ID defines
#define ID_pse_FanSpeed4_UNASSIGNED 255


// pse_FanSpeed4 output state value defines
#define ID_pse_FanSpeed4_UNASSIGNED_VALUE 0

// pse_FanSpeed5 output state ID defines
#define ID_pse_FanSpeed5_UNASSIGNED 255


// pse_FanSpeed5 output state value defines
#define ID_pse_FanSpeed5_UNASSIGNED_VALUE 0

// pse_FanSpeed6 output state ID defines
#define ID_pse_FanSpeed6_UNASSIGNED 255


// pse_FanSpeed6 output state value defines
#define ID_pse_FanSpeed6_UNASSIGNED_VALUE 0

// pse_FanSpeed7 output state ID defines
#define ID_pse_FanSpeed7_UNASSIGNED 255


// pse_FanSpeed7 output state value defines
#define ID_pse_FanSpeed7_UNASSIGNED_VALUE 0

// pse_FanSpeed8 output state ID defines
#define ID_pse_FanSpeed8_UNASSIGNED 255


// pse_FanSpeed8 output state value defines
#define ID_pse_FanSpeed8_UNASSIGNED_VALUE 0

// pse_FanSpeed9 output state ID defines
#define ID_pse_FanSpeed9_UNASSIGNED 255


// pse_FanSpeed9 output state value defines
#define ID_pse_FanSpeed9_UNASSIGNED_VALUE 0


// system variable value defines for expressions
// pse_RampFunction1 output state ID defines for expressions
#define pse_RampFunction1__UNASSIGNED ID_pse_RampFunction1_UNASSIGNED_VALUE

// pse_RampFunction10 output state ID defines for expressions
#define pse_RampFunction10__UNASSIGNED ID_pse_RampFunction10_UNASSIGNED_VALUE

// pse_RampFunction11 output state ID defines for expressions
#define pse_RampFunction11__UNASSIGNED ID_pse_RampFunction11_UNASSIGNED_VALUE

// pse_RampFunction12 output state ID defines for expressions
#define pse_RampFunction12__UNASSIGNED ID_pse_RampFunction12_UNASSIGNED_VALUE

// pse_RampFunction13 output state ID defines for expressions
#define pse_RampFunction13__UNASSIGNED ID_pse_RampFunction13_UNASSIGNED_VALUE

// pse_RampFunction2 output state ID defines for expressions
#define pse_RampFunction2__UNASSIGNED ID_pse_RampFunction2_UNASSIGNED_VALUE

// pse_RampFunction3 output state ID defines for expressions
#define pse_RampFunction3__UNASSIGNED ID_pse_RampFunction3_UNASSIGNED_VALUE

// pse_RampFunction4 output state ID defines for expressions
#define pse_RampFunction4__UNASSIGNED ID_pse_RampFunction4_UNASSIGNED_VALUE

// pse_RampFunction5 output state ID defines for expressions
#define pse_RampFunction5__UNASSIGNED ID_pse_RampFunction5_UNASSIGNED_VALUE

// pse_RampFunction6 output state ID defines for expressions
#define pse_RampFunction6__UNASSIGNED ID_pse_RampFunction6_UNASSIGNED_VALUE

// pse_RampFunction7 output state ID defines for expressions
#define pse_RampFunction7__UNASSIGNED ID_pse_RampFunction7_UNASSIGNED_VALUE

// pse_RampFunction8 output state ID defines for expressions
#define pse_RampFunction8__UNASSIGNED ID_pse_RampFunction8_UNASSIGNED_VALUE

// pse_RampFunction9 output state ID defines for expressions
#define pse_RampFunction9__UNASSIGNED ID_pse_RampFunction9_UNASSIGNED_VALUE

// pse_AnalogTemp1 output state ID defines for expressions
#define pse_AnalogTemp1__UNASSIGNED ID_pse_AnalogTemp1_UNASSIGNED_VALUE

// pse_AnalogTemp2 output state ID defines for expressions
#define pse_AnalogTemp2__UNASSIGNED ID_pse_AnalogTemp2_UNASSIGNED_VALUE

// pse_AnalogTemp3 output state ID defines for expressions
#define pse_AnalogTemp3__UNASSIGNED ID_pse_AnalogTemp3_UNASSIGNED_VALUE

// pse_AnalogTemp4 output state ID defines for expressions
#define pse_AnalogTemp4__UNASSIGNED ID_pse_AnalogTemp4_UNASSIGNED_VALUE

// pse_AnalogTemp5 output state ID defines for expressions
#define pse_AnalogTemp5__UNASSIGNED ID_pse_AnalogTemp5_UNASSIGNED_VALUE

// pse_AnalogTemp6 output state ID defines for expressions
#define pse_AnalogTemp6__UNASSIGNED ID_pse_AnalogTemp6_UNASSIGNED_VALUE

// pse_AnalogTemp7 output state ID defines for expressions
#define pse_AnalogTemp7__UNASSIGNED ID_pse_AnalogTemp7_UNASSIGNED_VALUE

// pse_AnalogTemp8 output state ID defines for expressions
#define pse_AnalogTemp8__UNASSIGNED ID_pse_AnalogTemp8_UNASSIGNED_VALUE

// pse_DigitalTemp1 output state ID defines for expressions
#define pse_DigitalTemp1__UNASSIGNED ID_pse_DigitalTemp1_UNASSIGNED_VALUE

// pse_DigitalTemp2 output state ID defines for expressions
#define pse_DigitalTemp2__UNASSIGNED ID_pse_DigitalTemp2_UNASSIGNED_VALUE

// pse_DigitalTemp3 output state ID defines for expressions
#define pse_DigitalTemp3__UNASSIGNED ID_pse_DigitalTemp3_UNASSIGNED_VALUE

// pse_DigitalTemp4 output state ID defines for expressions
#define pse_DigitalTemp4__UNASSIGNED ID_pse_DigitalTemp4_UNASSIGNED_VALUE

// pse_DigitalTemp5 output state ID defines for expressions
#define pse_DigitalTemp5__UNASSIGNED ID_pse_DigitalTemp5_UNASSIGNED_VALUE

// pse_Fan1 output state ID defines for expressions
#define pse_Fan1__Off ID_pse_Fan1_Off_VALUE
#define pse_Fan1__Low ID_pse_Fan1_Low_VALUE
#define pse_Fan1__Medium ID_pse_Fan1_Medium_VALUE
#define pse_Fan1__High ID_pse_Fan1_High_VALUE
#define pse_Fan1__UNASSIGNED ID_pse_Fan1_UNASSIGNED_VALUE

// pse_Fan10 output state ID defines for expressions
#define pse_Fan10__Off ID_pse_Fan10_Off_VALUE
#define pse_Fan10__Low ID_pse_Fan10_Low_VALUE
#define pse_Fan10__Medium ID_pse_Fan10_Medium_VALUE
#define pse_Fan10__High ID_pse_Fan10_High_VALUE
#define pse_Fan10__UNASSIGNED ID_pse_Fan10_UNASSIGNED_VALUE

// pse_Fan11 output state ID defines for expressions
#define pse_Fan11__Off ID_pse_Fan11_Off_VALUE
#define pse_Fan11__Low ID_pse_Fan11_Low_VALUE
#define pse_Fan11__Medium ID_pse_Fan11_Medium_VALUE
#define pse_Fan11__High ID_pse_Fan11_High_VALUE
#define pse_Fan11__UNASSIGNED ID_pse_Fan11_UNASSIGNED_VALUE

// pse_Fan12 output state ID defines for expressions
#define pse_Fan12__Off ID_pse_Fan12_Off_VALUE
#define pse_Fan12__Low ID_pse_Fan12_Low_VALUE
#define pse_Fan12__Medium ID_pse_Fan12_Medium_VALUE
#define pse_Fan12__High ID_pse_Fan12_High_VALUE
#define pse_Fan12__UNASSIGNED ID_pse_Fan12_UNASSIGNED_VALUE

// pse_Fan13 output state ID defines for expressions
#define pse_Fan13__Off ID_pse_Fan13_Off_VALUE
#define pse_Fan13__Low ID_pse_Fan13_Low_VALUE
#define pse_Fan13__Medium ID_pse_Fan13_Medium_VALUE
#define pse_Fan13__High ID_pse_Fan13_High_VALUE
#define pse_Fan13__UNASSIGNED ID_pse_Fan13_UNASSIGNED_VALUE

// pse_Fan2 output state ID defines for expressions
#define pse_Fan2__Off ID_pse_Fan2_Off_VALUE
#define pse_Fan2__Low ID_pse_Fan2_Low_VALUE
#define pse_Fan2__Medium ID_pse_Fan2_Medium_VALUE
#define pse_Fan2__High ID_pse_Fan2_High_VALUE
#define pse_Fan2__UNASSIGNED ID_pse_Fan2_UNASSIGNED_VALUE

// pse_Fan3 output state ID defines for expressions
#define pse_Fan3__Off ID_pse_Fan3_Off_VALUE
#define pse_Fan3__Low ID_pse_Fan3_Low_VALUE
#define pse_Fan3__Medium ID_pse_Fan3_Medium_VALUE
#define pse_Fan3__High ID_pse_Fan3_High_VALUE
#define pse_Fan3__UNASSIGNED ID_pse_Fan3_UNASSIGNED_VALUE

// pse_Fan4 output state ID defines for expressions
#define pse_Fan4__Off ID_pse_Fan4_Off_VALUE
#define pse_Fan4__Low ID_pse_Fan4_Low_VALUE
#define pse_Fan4__Medium ID_pse_Fan4_Medium_VALUE
#define pse_Fan4__High ID_pse_Fan4_High_VALUE
#define pse_Fan4__UNASSIGNED ID_pse_Fan4_UNASSIGNED_VALUE

// pse_Fan5 output state ID defines for expressions
#define pse_Fan5__Off ID_pse_Fan5_Off_VALUE
#define pse_Fan5__Low ID_pse_Fan5_Low_VALUE
#define pse_Fan5__Medium ID_pse_Fan5_Medium_VALUE
#define pse_Fan5__High ID_pse_Fan5_High_VALUE
#define pse_Fan5__UNASSIGNED ID_pse_Fan5_UNASSIGNED_VALUE

// pse_Fan6 output state ID defines for expressions
#define pse_Fan6__Off ID_pse_Fan6_Off_VALUE
#define pse_Fan6__Low ID_pse_Fan6_Low_VALUE
#define pse_Fan6__Medium ID_pse_Fan6_Medium_VALUE
#define pse_Fan6__High ID_pse_Fan6_High_VALUE
#define pse_Fan6__UNASSIGNED ID_pse_Fan6_UNASSIGNED_VALUE

// pse_Fan7 output state ID defines for expressions
#define pse_Fan7__Off ID_pse_Fan7_Off_VALUE
#define pse_Fan7__Low ID_pse_Fan7_Low_VALUE
#define pse_Fan7__Medium ID_pse_Fan7_Medium_VALUE
#define pse_Fan7__High ID_pse_Fan7_High_VALUE
#define pse_Fan7__UNASSIGNED ID_pse_Fan7_UNASSIGNED_VALUE

// pse_Fan8 output state ID defines for expressions
#define pse_Fan8__Off ID_pse_Fan8_Off_VALUE
#define pse_Fan8__Low ID_pse_Fan8_Low_VALUE
#define pse_Fan8__Medium ID_pse_Fan8_Medium_VALUE
#define pse_Fan8__High ID_pse_Fan8_High_VALUE
#define pse_Fan8__UNASSIGNED ID_pse_Fan8_UNASSIGNED_VALUE

// pse_Fan9 output state ID defines for expressions
#define pse_Fan9__Off ID_pse_Fan9_Off_VALUE
#define pse_Fan9__Low ID_pse_Fan9_Low_VALUE
#define pse_Fan9__Medium ID_pse_Fan9_Medium_VALUE
#define pse_Fan9__High ID_pse_Fan9_High_VALUE
#define pse_Fan9__UNASSIGNED ID_pse_Fan9_UNASSIGNED_VALUE

// pse_FanSpeed1 output state ID defines for expressions
#define pse_FanSpeed1__UNASSIGNED ID_pse_FanSpeed1_UNASSIGNED_VALUE

// pse_FanSpeed10 output state ID defines for expressions
#define pse_FanSpeed10__UNASSIGNED ID_pse_FanSpeed10_UNASSIGNED_VALUE

// pse_FanSpeed11 output state ID defines for expressions
#define pse_FanSpeed11__UNASSIGNED ID_pse_FanSpeed11_UNASSIGNED_VALUE

// pse_FanSpeed12 output state ID defines for expressions
#define pse_FanSpeed12__UNASSIGNED ID_pse_FanSpeed12_UNASSIGNED_VALUE

// pse_FanSpeed13 output state ID defines for expressions
#define pse_FanSpeed13__UNASSIGNED ID_pse_FanSpeed13_UNASSIGNED_VALUE

// pse_FanSpeed2 output state ID defines for expressions
#define pse_FanSpeed2__UNASSIGNED ID_pse_FanSpeed2_UNASSIGNED_VALUE

// pse_FanSpeed3 output state ID defines for expressions
#define pse_FanSpeed3__UNASSIGNED ID_pse_FanSpeed3_UNASSIGNED_VALUE

// pse_FanSpeed4 output state ID defines for expressions
#define pse_FanSpeed4__UNASSIGNED ID_pse_FanSpeed4_UNASSIGNED_VALUE

// pse_FanSpeed5 output state ID defines for expressions
#define pse_FanSpeed5__UNASSIGNED ID_pse_FanSpeed5_UNASSIGNED_VALUE

// pse_FanSpeed6 output state ID defines for expressions
#define pse_FanSpeed6__UNASSIGNED ID_pse_FanSpeed6_UNASSIGNED_VALUE

// pse_FanSpeed7 output state ID defines for expressions
#define pse_FanSpeed7__UNASSIGNED ID_pse_FanSpeed7_UNASSIGNED_VALUE

// pse_FanSpeed8 output state ID defines for expressions
#define pse_FanSpeed8__UNASSIGNED ID_pse_FanSpeed8_UNASSIGNED_VALUE

// pse_FanSpeed9 output state ID defines for expressions
#define pse_FanSpeed9__UNASSIGNED ID_pse_FanSpeed9_UNASSIGNED_VALUE


// function parameter block definitions


#endif
