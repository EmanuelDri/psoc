include "memory.inc"
include "m8c.inc"

export _EnableInputToBlock
export  EnableInputToBlock
export _EnableClockToBlock
export  EnableClockToBlock
export _EnableAnyInputToBlock
export  EnableAnyInputToBlock
export _DisableInputToBlock
export  DisableInputToBlock

export _EnableOutputFromBlock
export  EnableOutputFromBlock
export _DisableOutputFromBlock
export  DisableOutputFromBlock
export _ControlOutputFromBlock
export  ControlOutputFromBlock

export _SetDriveMode
export  SetDriveMode

AREA text(ROM,REL)



; Global DB Registers offset constants
DB_IN_REG_OFFSET:   equ  0x21
RDI_RI_REG_OFFSET:  equ  0xB0
PRT_GS_REG_OFFSET:  equ  0x02
DB_OUT_REG_OFFSET:  equ  0x22
RDI_RO_REG_OFFSET:  equ  0xB5


; Inputs Identifiers for the EnableAnyInputToBlock function
ROUTING_DATA:       equ  0x0F         ; upper nibble
ROUTING_CLK:        equ  0xF0         ; lower nibble

; Enable/disable DB otput Identifiers for the ControlOutputFromBlock function
ENABLE_OUT:         equ  0xFF         ; upper nibble
DISABLE_OUT:        equ  0x00         ; lower nibble


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: EnableInputToBlock
;
;  DESCRIPTION: Makes the routing connections from a pin to a digital block input
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A, block_num passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the input routing from
;  a pin to block: DxBxxIN controls the input mux at the block, RDIxRI connects
;  the row input to the global bus, and PRTxGS connects a pin to the global bus
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------
_EnableInputToBlock:
EnableInputToBlock:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                         ; pass block_num
    push  A                         ; pass port_pin
    mov   A, ROUTING_DATA
    push  A                         ; pass ROUTING_DATA identificator
    call  EnableAnyInputToBlock
    add   SP, -3
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: EnableClockToBlock
;
;  DESCRIPTION: Makes the routing clock connections from a pin to a digital block input
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A, block_num passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the input routing from
;  a pin to block: DxBxxIN controls the input mux at the block, RDIxRI connects
;  the row input to the global bus, and PRTxGS connects a pin to the global bus
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------
_EnableClockToBlock:
EnableClockToBlock:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                         ; pass block_num
    push  A                         ; pass port_pin
    mov   A, ROUTING_CLK
    push  A                         ; pass ROUTING_CLK identificator
    call  EnableAnyInputToBlock
    add   SP, -3
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: EnableAnyInputToBlock
;
;  DESCRIPTION: Makes the routing connections from a pin or clock source to a digital block input
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: passed to stack accordingly to the fastcall16
;   [X-3] - ROUTING_DATA/ROUTING_CLK  -> 0x0F/0xF0
;   [X-4] - port_pin
;   [X-5] - block_num
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the input routing from
;  a pin to block: DxBxxIN controls the input mux at the block, RDIxRI connects
;  the row input to the global bus, and PRTxGS connects a pin to the global bus
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------

; Stack offset constants

set_value_ei:       equ  -1
clear_value_ei:     equ  -2

input_select:       equ  -5
port_pin_ei:        equ  -6
block_num_ei:       equ  -7

_EnableAnyInputToBlock:
EnableAnyInputToBlock:
    RAM_PROLOGUE RAM_USE_CLASS_2
    add   SP,2                    ; space for set_value_ei and clear_value_ei
    mov   X,SP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The DxBxxIN setting value depends on the pin
; pin 0,4 -> 0x0C
; pin 1,5 -> 0x0D
; pin 2,6 to Row input 2  -> 0x0E  (0xE0  - to Capture input)   
; pin 3,7 -> 0x0F
; DxBxxIN setting value 
;    for ROUTING_DATA mode is ((pin&0x03)+0x0C << 4) => MS Nibble 
;    for ROUTING_CLK mode  is ((pin&0x03)+0x0C)      => LS Nibble 
; DxBxxIN register number is 0x21+(block_num*4)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; DxBxxIN setting value forming 
    mov   A, [X+port_pin_ei]        ; A has port_pin 
    and   A, 0x03                   ; A has pin&0x03
    add   A, 0x0C                   ; (pin&0x03)+0x0C
    tst   [X+input_select], ROUTING_CLK     ; check  whether the clock input is requested
    jnz   .ROUTING_CLK_1            ; yes, clock input is requested, skip shifts code:
    asl   A                         ; shift left by 4
    asl   A
    asl   A
    asl   A                         ; upper nibble is data input, lower is clock input 
.ROUTING_CLK_1:    
    mov   [X+set_value_ei], A        ; use to or in bits

;;;; DxBxxIN register number forming  
    mov   A, [X+block_num_ei]       ; A has block_num
    asl   A                         ; A has block_num*2
    asl   A                         ; A has have block_num*4
    add   A, DB_IN_REG_OFFSET       ; A has 0x21+(block_num*4)   
    push  A                         ; store the register address on the stack

;;;; DxBxxIN register update   	   
    M8C_SetBank1                    ; DxBxxIN is in Bank 1
    mov   X, A                      ; move the register address to X
    mov   A, reg[X]                 ; get current value of register
    mov   X, SP                     ; get frame pointer
    and   A, [X+input_select-1]     ; clear nibble that should be updated:
    or    A, [X+set_value_ei-1]     ; set the appropriate bits (nibble)
    pop   X                         ; get back the register address   
    mov   reg[X], A                 ; move new value into DxBxxIN
    M8C_SetBank0
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The RDIxRI setting value depends on the port and pin
; RDIxRI setting value is found by:
; if (port is odd) then or with 0x02
; if (pin > 3) then or with 0x01
; shift this by pin&0x03*2
; RDIxRI register number is 0xB0+(row_num*8)
; where row_num is block_num/4
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   X,SP                      ; get frame pointer    
    
 ;;;; RDIxRI setting value forming    
    mov   [X+set_value_ei], 0x00    ; initialize setting value
    tst   [X+port_pin_ei], 0x10     ; check if port is odd
    jz    .notoddport               ; port is even
    or    [X+set_value_ei], 0x02    ; set upper bit for odd port
.notoddport:
    tst   [X+port_pin_ei], 0x04     ; if pin < 4, ZF is reset (NZ)
    jz   .pinlessthanfour
    or    [X+set_value_ei], 0x01    ; set lower bit for pin > 3
.pinlessthanfour:
    mov   [X+clear_value_ei], 0x03  ; mask to clear appropriate bits   
    mov   A,[X+port_pin_ei]     
    and   A, 0x03                   ; isolate lowest 3 pin bits
    jz    .done_shift_ri
.shift_ri:
    asl   [X+clear_value_ei]        
    asl   [X+clear_value_ei]        ; move to next bit position
    asl   [X+set_value_ei]
    asl   [X+set_value_ei]          ; move to next bit position
    dec   A
    jnz   .shift_ri
.done_shift_ri:
    xor    [X+clear_value_ei], 0xFF  ; clear_value_ei has the mask for anding

;;;; RDIxRI register number forming 
    mov   A, [X+block_num_ei]       ; A has block_num  
    and   A, 0xFC                   ; A has (block/4)*4, implicit shifting
    asl   A                         ; A has (block/4)*8
    add   A, RDI_RI_REG_OFFSET      ; A has 0xB0+(row_num*8)                        
    push  A                         ; store the register address on the stack

;;;; RDIxRI register update 
    mov   X, A                      ; move the register address to X
    mov   A, reg[X]                 ; get register's current value
    mov   X, SP                     ; get the frame pointer + 1
    and   A, [X+clear_value_ei-1]    ; clear bits with the mask, -1 to fixup the frame pointer
    or    A, [X+set_value_ei-1]      ; set appropriate bits, -1 to fixup the frame pointer
    pop   X                         ; get back the register address   
    mov   reg[X], A                 ; move new value into RDIxRI
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The PRTxGS setting value depends on the pin
; PRTxGS setting value is found by:
; setting = 1 << pin
; PRTxGS register number is 0x02+4*port
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   X,SP                      ; get frame pointer

 ;;;; PRTxGS setting value forming    
    mov   A, [X+port_pin_ei]        ; A has port_pin
    mov   [X+set_value_ei], 0x01    ; bit to or
    and   A, 0x0F                   ; isolate the pin
    jz    .done_shift_gs
.shift_gs:
    asl   [X+set_value_ei]          ; shift the bit by one
    dec   A                        
    jnz   .shift_gs
.done_shift_gs:
    
;;;; PRTxGS register number forming 
    mov   A, [X+port_pin_ei]        ; A has port_pin
    and   A, 0x70                   ; A has port*16
    asr   A                         ; A has port*8
    asr   A                         ; A has port*4
    add   A, PRT_GS_REG_OFFSET      ; A has 0x02+4*port
    push  A                         ; store the register address on the stack

;;;; PRTxGS register update 
    mov   X, A                      ; move the register address to X
    mov   A, reg[X]                 ; get register's current value
    mov   X, SP                     ; get frame pointer
    or    A, [X+set_value_ei-1]     ; set appropriate bit
    pop   X                         ; get back the register address  
    mov   reg[X], A                 ; move new value into PRTxGS
    add   SP, -2                    ; clean up the stack
    RAM_EPILOGUE RAM_USE_CLASS_2     
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: DisableInputToBlock
;
;  DESCRIPTION: Disconnects a pin from a global bus
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: To disable an input connection to a block
;  it is only necessary to disconnect the pin from the global. All other
;  connections are don't cares if the pin has been disconnected
;  This function uses 3 bytes on the stack: 2 as locals, 1 push/pop.
;
;------------------------------------------------------------------------------

; Stack offset constants
clear_value_di:		equ -1

_DisableInputToBlock:
 DisableInputToBlock:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  A                        ; clear_value_di variable declaration
    mov   X,SP                     ; get the frame pointer
    push  A                        ; store port_pin to stack 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The PRTxGS clear value depends on the pin
; PRTxGS clear value is found by:
; value = 1 << pin
; PRTxGS register number is 0x02+4*port
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

 ;;;; PRTxGS setting value forming
    mov   [X+clear_value_di], 0x01 ; bit to clear
    and   A, 0x0F                  ; isolate the pin
    jz    .done_shift_gs
.shift_gs:
    asl   [X+clear_value_di]       ; shift the bit by one
    dec   A
    jnz   .shift_gs
.done_shift_gs:
    xor   [X+clear_value_di], 0xFF   ; write bit mask for clearing

;;;; PRTxGS register number forming
    pop   A
    and   A, 0x70                  ; A has port*16
    asr   A                        ; A has port*8
    asr   A                        ; A has port*4
    add   A, PRT_GS_REG_OFFSET     ; A has 0x02+4*port
    push  A                        ; push the register address on the stack

;;;; PRTxGS register update 
    mov   X, A
    mov   A, reg[X]                ; get register's current value
    mov   X, SP                    ; get the frame pointer + 1
    and   A, [X+clear_value_di-1]  ; clear appropriate bit, -1 to fixup frame pointer
    pop   X                        ; get back the register address
    mov   reg[X], A                ; move new value into PRTxGS
    pop   A
    RAM_EPILOGUE RAM_USE_CLASS_2 	
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: EnableOutputFromBlock
;
;  DESCRIPTION: Makes the routing connections from a pin to a digital block input
;  Also, enables the block's primary function output driver
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A, block_num passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the output routing from
;  a block to pin: DxBxxOU controls the output mux at the block, RDIxRO0/RDIxRO1 connects
;  the row output to the global bus, and PRTxGS connects the global bus to the pin
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------

_EnableOutputFromBlock:
EnableOutputFromBlock:
	RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                         ; pass block_num
    push  A                         ; pass port_pin
    mov   A, ENABLE_OUT
    push  A                         ; pass ROUTING_DATA identificator
    call  ControlOutputFromBlock
    add   SP, -3
	RAM_EPILOGUE RAM_USE_CLASS_2 	
	ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: DisableOutputFromBlock
;
;  DESCRIPTION: Disconnects a pin from a global bus, disconnects the row
;  output from driving the global, and disables the block output to the row.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A, block_num passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: To disable an output connection from a block
;  it is necessary to disconnect the row output pin from the global, and disconnect
;  the pin from the global bus. The block output is also disconnected from the row output.
;  This function uses 4 bytes on the stack: 3 as locals, 1 push/pop.
;
;------------------------------------------------------------------------------

_DisableOutputFromBlock:	
DisableOutputFromBlock:
	RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                         ; pass block_num
    push  A                         ; pass port_pin
    mov   A, DISABLE_OUT
    push  A                         ; pass ROUTING_DATA identificator
    call  ControlOutputFromBlock
    add   SP, -3
	RAM_EPILOGUE RAM_USE_CLASS_2 	
	ret
.ENDSECTION	
	
	
.SECTION	
;-----------------------------------------------------------------------------
;  FUNCTION NAME: ControlOutputFromBlock
;
;  DESCRIPTION: Makes the routing connections from a pin to a digital block input
;  Also, enables the block's primary function output driver
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: passed to stack accordingly to the fastcall16   //port_pin passed in A, block_num passed in X by fastcall16
;   [X-3] - ctr_flag disable/enable output -> 0x00/0xFF
;   [X-4] - port_pin
;   [X-5] - block_num
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the output routing from
;  a block to pin: DxBxxOU controls the output mux at the block, RDIxRO0/RDIxRO1 connects
;  the row output to the global bus, and PRTxGS connects the global bus to the pin
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------

; Stack offset constants
set_value_co:       equ  -1
ctr_flag_co:        equ  -4
port_pin_co:        equ  -5
block_num_co:       equ  -6

_ControlOutputFromBlock:
 ControlOutputFromBlock:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  A                         ; set_value_�o variable declaration
    mov   X,SP                      ; get the frame pointer
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The DxBxxOU setting value depends on the pin
; pin 0,4 -> 0x00
; pin 1,5 -> 0x01
; pin 2,6 -> 0x02
; pin 3,7 -> 0x03
; DxBxxOU setting value is pin&0x03
; DxBxxOU register number is 0x22+(block_num*4)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; DxBxxOU setting value forming
    mov   A, [X+port_pin_co]        ; A has port_pin 
    and   A, 0x03                   ; A has pin&0x03
    or    A, 0x04                   ; set OUTEN to turn on the block output
    and   A, [X+ctr_flag_co]        ; clear for disable output operation   
    mov   [X+set_value_co], A       ; use to or in bits        

;;;; DxBxxOU register number forming
    mov   A, [X+block_num_co]       ; A has block_num
    asl   A                         ; A has block_num*2
    asl   A                         ; A has have block_num*4
    add   A, DB_OUT_REG_OFFSET      ; A has 0x22+(block_num*4)   
    push  A                         ; push the register address on the stack

;;;; DxBxxOU register update
    M8C_SetBank1                    ; DxBxxOU is in Bank 1
    mov   X, A                      ; register address => X 
    mov   A, reg[X]                 ; get current value of register
    and   A, 0xF8                   ; the lowest two bits are the output select
    mov   X, SP                     ; get the frame pointer + 1
    or    A, [X+set_value_co-1]     ; set the appropriate bits for the output select, -1 to fixup the frame pointer
    pop   X                         ; get the register address back 
    mov   reg[X], A                 ; move new value into DxBxxOU
    M8C_SetBank0
    
    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The RDIxRO0/RDIxRO1 setting value depends on the port and pin
; RDIxROx setting value is found by:
; x = 1
; if (port is odd) then x <<= 2
; if (pin > 3) then x <<= 1
; if (pin is odd) then x <<= 4
; RDIxROx register number is ((pin&0x02)>>1)+0xB5+8*row_num
; where row_num is block_num/4
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   X, SP                     ; get the frame pointer    
    
;;;; RDIxRO0/RDIxRO1 setting value forming    
    mov   A,0x01                    ; initialize setting value  
    tst   [X+port_pin_co], 0x10     ; check if port is odd
    jz    .notoddport                
    asl   A                         ; port is even
    asl   A                         ; shifted left by 2  
.notoddport:
    tst   [X+port_pin_co], 0x04     ; if pin < 4, ZF is set
    jz    .pinlessthanfour                 
    asl   A
.pinlessthanfour:
    tst   [X+port_pin_co], 0x01     ; check if pin is odd
    jz    .notoddpin
    asl   A
    asl   A
    asl   A
    asl   A                          ; shifted left by 4
.notoddpin:    
    cpl   A
    xor   A, [X+ctr_flag_co] 
    mov   [X+set_value_co], A        ; store A to set_value_co   

;;;; RDIxRO0/RDIxRO1 register number forming   
    mov   A, [X+block_num_co]       ; A has block_num  
    and   A, 0xFC                   ; A has (block/4)*4, implicit shifting
    asl   A                         ; A has (block/4)*8
    add   A, RDI_RO_REG_OFFSET      ; A has 0xB5+(row_num*8)                        
    tst   [X+port_pin_co], 0x02     ; check if bit1 of pin is set
    jz   .reg_rdixro0
    inc   A                         ; register is RDIxRO1    
.reg_rdixro0:                    
    push  A                         ; store register address into stack
    
;;;; RDIxRO0/RDIxRO1 register update   
    mov   X, A 
    mov   A, reg[X]                 ; get register's current value
    mov   X, SP                     ; get the frame pointer + 1
    cmp   [X+ctr_flag_co-1], ENABLE_OUT
    jnz   .and_set_value
    or    A, [X+set_value_co-1]     ; set appropriate bits, -1 to fixup the frame pointer
    jmp   .included_set_value   
.and_set_value:    
    and   A, [X+set_value_co-1]     ; set appropriate bits, -1 to fixup the frame pointer    
.included_set_value:    
    pop   X                         ; get the register address 
    mov   reg[X], A                 ; move new value into RDIxROx

    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The PRTxGS setting value depends on the pin
; PRTxGS setting value is found by:
; setting = 1 << pin
; PRTxGS register number is 0x02+4*port
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   X, SP                     ; get the frame pointer
    
;;;; PRTxGS setting value forming  
    mov   [X+set_value_co], 0x01    ; bit to or
    mov   A, [X+port_pin_co]        ; A has port_pin
    and   A, 0x0F                   ; isolate the pin
    jz    .done_shift_gs    
.shift_gs:
    asl   [X+set_value_co]          ; shift the bit by one
    dec   A                        
    jnz   .shift_gs
.done_shift_gs:
    mov   A, [X+ctr_flag_co] 
    cpl   A
    xor   [X+set_value_co], A        ; store A to set_value_co  

;;;; PRTxGS register number forming 
    mov   A, [X+port_pin_co]        ; A has port_pin
    and   A, 0x70                   ; A has port*16
    asr   A                         ; A has port*8
    asr   A                         ; A has port*4
    add   A, PRT_GS_REG_OFFSET      ; A has 0x02+4*port
    push  A
    
;;;; PRTxGS register update    
    mov   X, A                      ; X has register number
    mov   A, reg[X]                 ; get register's current value
    mov   X, SP                     ; get frame pointer + 1
    cmp   [X+ctr_flag_co-1], ENABLE_OUT
    jnz   .and_set_value2
    or    A, [X+set_value_co-1]     ; set appropriate bits, -1 to fixup the frame pointer
    jmp   .included_set_value2
.and_set_value2:
    and   A, [X+set_value_co-1]     ; set appropriate bits, -1 to fixup the frame pointer
.included_set_value2:
    pop   X                         ; get register address back
    mov   reg[X], A                 ; move new value into PRTxGS
    pop   A
    RAM_EPILOGUE RAM_USE_CLASS_2     
    ret
.ENDSECTION   


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SetDriveMode
;
;  DESCRIPTION: Sets the drive mode of a port pin to the setting passed in X
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: port_pin passed in A, drive_mode passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the drive mode:
;  PRTxDM0, PRTxDM1, and PRTxDM2. These registers are set by the setting passed in.
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;
;------------------------------------------------------------------------------

; Stack offset constants
port_pin_dm:         equ -1
set_value_dm:        equ -2
clear_value_dm:      equ -3
drive_mode_dm:       equ -4
; Register offset constants
dm1_offset:          equ 1        ; offset from dm0
dm2_offset:          equ 3        ; offset from dm0    

_SetDriveMode:
SetDriveMode:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                         ; effectively moves X into drive_mode_dm
    add   SP,3                      ; SP is at +4
    mov   X,SP        
    mov   [X+port_pin_dm], A        ; A has port_pin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; PRTxDM0 register number is 0x00+4*port
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    and   A, 0x70                   ; A has port*16
    asr   A                         ; A has port*8
    asr   A                         ; A has port*4 which is the register address
    push  A                         ; put the register address on the stack
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get the bit mask
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   [X+set_value_dm], 0x01    ; bit to set
    mov   A, [X+port_pin_dm]        ; A has port_pin
    and   A, 0x0F                   ; isolate the pin
    jz    .done_shift_dm
.shift_dm:
    asl   [X+set_value_dm]          ; shift the bit by one
    dec   A                        
    jnz   .shift_dm
.done_shift_dm:
    mov   A, [X+set_value_dm]       ; get the bit mask
    cpl   A                         ; prepare to clear bits with 'and'
    mov   [X+clear_value_dm], A     ; write bit mask for clearing    
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM0, Bank1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
    M8C_SetBank1
    pop   X                         ; get the register address
    push  X                         ; save the register address                        
    mov   A, reg[X]                 ; get register's current value
    mov   X, SP                     ; get frame pointer + 1
    tst   [X+drive_mode_dm-1], 0x01 ; test bit0, -1 to fixup the frame pointer
    jz    .clear_dm0
    or    A, [X+set_value_dm-1]     ; or in the appropriate bit, -1 to fixup the frame pointer
    jmp   .done_set_dm0
.clear_dm0:
    and   A, [X+clear_value_dm-1]   ; and out the appropriate bit, -1 to fixup the frame pointer
.done_set_dm0:
    pop   X                         ; get the register address back
    mov   reg[X], A                 ; move new value into PRTxDM0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM1, already in Bank1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
    mov   A, reg[X+dm1_offset]      ; get register's current value
    push  X                         ; save the register address
    mov   X,SP                      ; get the frame pointer + 1
    tst   [X+drive_mode_dm-1], 0x02 ; test bit1, -1 to fixup the frame pointer
    jz    .clear_dm1
    or    A, [X+set_value_dm-1]     ; or in the appropriate bit, -1 to fixup the frame pointer
    jmp   .done_set_dm1
.clear_dm1:
    and   A, [X+clear_value_dm-1]   ; and out the appropriate bit, -1 to fixup the frame pointer
.done_set_dm1:
    pop   X                         ; get the register address
    mov   reg[X+dm1_offset], A      ; move new value into PRTxDM1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM2, Bank0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    M8C_SetBank0    
    mov   A, reg[X+dm2_offset]      ; get register's current value
    push  X                         ; save the register address
    mov   X,SP                      ; get the frame pointer + 1
    tst   [X+drive_mode_dm-1], 0x04 ; test bit2, -1 to fixup the frame pointer
    jz    .clear_dm2
    or    A, [X+set_value_dm-1]     ; or in the appropriate bit, -1 to fixup the frame pointer
    jmp   .done_set_dm2
.clear_dm2:
    and   A, [X+clear_value_dm-1]   ; and out the appropriate bit, -1 to fixup the frame pointer
.done_set_dm2:
    pop   X                         ; get the register address
    mov   reg[X+dm2_offset], A      ; move new value into PRTxDM2
    add   SP, -4                    ; clean up stack
    RAM_EPILOGUE RAM_USE_CLASS_2        
    ret
.ENDSECTION    
