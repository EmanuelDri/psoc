//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>          // part specific constants and macros
#include "PSoCAPI.h"      // PSoC API definitions for all User Modules

#include "driverdecl.h"
#include "CMXSystem.h"
#include "CMXSystemExtern.h"
#include "TransferFunction.h"

#include "cmx.h"
#include "ProjectProperties.h"
#include "Custom.h"

// Channel includes
// ADC_00 Include
#include "CMX_ADC_CHAN.h"
// ShI2Cm_22 Include
#include "CMX_I2Cm_CHAN.h"
// TACH_Timer Include
#include "CMX_TACH_IN_CHAN.h"

void main( void )
{
    // Initialize Project
    M8C_EnableGInt;                            // Turn on interrupts 
    SystemTimer_Start();
    SystemTimer_SetInterval(SystemTimer_64_HZ);
    SystemTimer_EnableInt();

    // Initialize Channels
    // ADC_00 Initialization  
    ADCBUF_Start(3); // Power up ADC Buffer PGA
    ADC_Start(3); // Power up ADC
    AdcScanReset(); // Initialize ADC scanner
    ADC_GetSamples(0); // Turn on GetSamples
    // set I2C block pins to high
    PRT1DR |= 1 << 5;
    PRT1DR |= 1 << 7;
    // ShI2Cm_22 Initialization
    InitI2CmChan();
    // TACH_Timer Initialization
    InitTachLLD();
    I2C_CFG &= 0xFC;                           // Disable I2C in case it's not used.
    
    // Initialize Variables
	SystemVars.ReadOnlyVars.pse_AnalogTemp1 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp2 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp3 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp4 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp5 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp6 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp7 = 0;
	SystemVars.ReadOnlyVars.pse_AnalogTemp8 = 0;
	SystemVars.ReadOnlyVars.pse_DigitalTemp1 = 0;
	SystemVars.ReadOnlyVars.pse_DigitalTemp2 = 0;
	SystemVars.ReadOnlyVars.pse_DigitalTemp3 = 0;
	SystemVars.ReadOnlyVars.pse_DigitalTemp4 = 0;
	SystemVars.ReadOnlyVars.pse_DigitalTemp5 = 0;
	SystemVars.ReadOnlyVars.pse_Fan1 = 0;
	SystemVars.ReadOnlyVars.pse_Fan10 = 0;
	SystemVars.ReadOnlyVars.pse_Fan11 = 0;
	SystemVars.ReadOnlyVars.pse_Fan12 = 0;
	SystemVars.ReadOnlyVars.pse_Fan13 = 0;
	SystemVars.ReadOnlyVars.pse_Fan2 = 0;
	SystemVars.ReadOnlyVars.pse_Fan3 = 0;
	SystemVars.ReadOnlyVars.pse_Fan4 = 0;
	SystemVars.ReadOnlyVars.pse_Fan5 = 0;
	SystemVars.ReadOnlyVars.pse_Fan6 = 0;
	SystemVars.ReadOnlyVars.pse_Fan7 = 0;
	SystemVars.ReadOnlyVars.pse_Fan8 = 0;
	SystemVars.ReadOnlyVars.pse_Fan9 = 0;
	SystemVars.ReadOnlyVars.pse_FanSpeed1 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed10 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed11 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed12 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed13 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed2 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed3 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed4 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed5 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed6 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed7 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed8 = 300;
	SystemVars.ReadOnlyVars.pse_FanSpeed9 = 300;
	SystemVars.ReadOnlyVars.pse_RampFunction1 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction10 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction11 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction12 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction13 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction2 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction3 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction4 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction5 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction6 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction7 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction8 = 0;
	SystemVars.ReadOnlyVars.pse_RampFunction9 = 0;

    // Driver instantiations
CMX_I2C_Instantiate(&pse_Monitor);
CMX_LM75_Instantiate(&pse_DigitalTemp1);
CMX_LM75_Instantiate(&pse_DigitalTemp2);
CMX_LM75_Instantiate(&pse_DigitalTemp3);
CMX_LM75_Instantiate(&pse_DigitalTemp4);
CMX_LM75_Instantiate(&pse_DigitalTemp5);
CMX_LM20_Instantiate(&pse_AnalogTemp1);
CMX_LM20_Instantiate(&pse_AnalogTemp2);
CMX_LM20_Instantiate(&pse_AnalogTemp3);
CMX_LM20_Instantiate(&pse_AnalogTemp4);
CMX_LM20_Instantiate(&pse_AnalogTemp5);
CMX_LM20_Instantiate(&pse_AnalogTemp6);
CMX_LM20_Instantiate(&pse_AnalogTemp7);
CMX_LM20_Instantiate(&pse_AnalogTemp8);
CMX_FAN_Instantiate(&pse_Fan1);
CMX_FAN_Instantiate(&pse_Fan10);
CMX_FAN_Instantiate(&pse_Fan11);
CMX_FAN_Instantiate(&pse_Fan12);
CMX_FAN_Instantiate(&pse_Fan13);
CMX_FAN_Instantiate(&pse_Fan2);
CMX_FAN_Instantiate(&pse_Fan3);
CMX_FAN_Instantiate(&pse_Fan4);
CMX_FAN_Instantiate(&pse_Fan5);
CMX_FAN_Instantiate(&pse_Fan6);
CMX_FAN_Instantiate(&pse_Fan7);
CMX_FAN_Instantiate(&pse_Fan8);
CMX_FAN_Instantiate(&pse_Fan9);
CMX_TACH_Instantiate(&pse_FanSpeed1);
CMX_TACH_Instantiate(&pse_FanSpeed10);
CMX_TACH_Instantiate(&pse_FanSpeed11);
CMX_TACH_Instantiate(&pse_FanSpeed12);
CMX_TACH_Instantiate(&pse_FanSpeed13);
CMX_TACH_Instantiate(&pse_FanSpeed2);
CMX_TACH_Instantiate(&pse_FanSpeed3);
CMX_TACH_Instantiate(&pse_FanSpeed4);
CMX_TACH_Instantiate(&pse_FanSpeed5);
CMX_TACH_Instantiate(&pse_FanSpeed6);
CMX_TACH_Instantiate(&pse_FanSpeed7);
CMX_TACH_Instantiate(&pse_FanSpeed8);
CMX_TACH_Instantiate(&pse_FanSpeed9);

    // Custom initization code.
    CustomInit();
    // End Initialize Project

	while(1)
	{
        // Sync loop sample rate
#if ( SAMPLE_DIVIDER  )
        SystemTimer_SyncWait(SAMPLE_DIVIDER, SystemTimer_WAIT_RELOAD);
#endif
		// update input variables
		SystemVars.ReadOnlyVars.pse_AnalogTemp1 = CMX_LM20_GetValue(&pse_AnalogTemp1);
		SystemVars.ReadOnlyVars.pse_AnalogTemp2 = CMX_LM20_GetValue(&pse_AnalogTemp2);
		SystemVars.ReadOnlyVars.pse_AnalogTemp3 = CMX_LM20_GetValue(&pse_AnalogTemp3);
		SystemVars.ReadOnlyVars.pse_AnalogTemp4 = CMX_LM20_GetValue(&pse_AnalogTemp4);
		SystemVars.ReadOnlyVars.pse_AnalogTemp5 = CMX_LM20_GetValue(&pse_AnalogTemp5);
		SystemVars.ReadOnlyVars.pse_AnalogTemp6 = CMX_LM20_GetValue(&pse_AnalogTemp6);
		SystemVars.ReadOnlyVars.pse_AnalogTemp7 = CMX_LM20_GetValue(&pse_AnalogTemp7);
		SystemVars.ReadOnlyVars.pse_AnalogTemp8 = CMX_LM20_GetValue(&pse_AnalogTemp8);
		SystemVars.ReadOnlyVars.pse_DigitalTemp1 = CMX_LM75_GetValue(&pse_DigitalTemp1);
		SystemVars.ReadOnlyVars.pse_DigitalTemp2 = CMX_LM75_GetValue(&pse_DigitalTemp2);
		SystemVars.ReadOnlyVars.pse_DigitalTemp3 = CMX_LM75_GetValue(&pse_DigitalTemp3);
		SystemVars.ReadOnlyVars.pse_DigitalTemp4 = CMX_LM75_GetValue(&pse_DigitalTemp4);
		SystemVars.ReadOnlyVars.pse_DigitalTemp5 = CMX_LM75_GetValue(&pse_DigitalTemp5);
		SystemVars.ReadOnlyVars.pse_FanSpeed1 = CMX_TACH_GetValue(&pse_FanSpeed1);
		SystemVars.ReadOnlyVars.pse_FanSpeed10 = CMX_TACH_GetValue(&pse_FanSpeed10);
		SystemVars.ReadOnlyVars.pse_FanSpeed11 = CMX_TACH_GetValue(&pse_FanSpeed11);
		SystemVars.ReadOnlyVars.pse_FanSpeed12 = CMX_TACH_GetValue(&pse_FanSpeed12);
		SystemVars.ReadOnlyVars.pse_FanSpeed13 = CMX_TACH_GetValue(&pse_FanSpeed13);
		SystemVars.ReadOnlyVars.pse_FanSpeed2 = CMX_TACH_GetValue(&pse_FanSpeed2);
		SystemVars.ReadOnlyVars.pse_FanSpeed3 = CMX_TACH_GetValue(&pse_FanSpeed3);
		SystemVars.ReadOnlyVars.pse_FanSpeed4 = CMX_TACH_GetValue(&pse_FanSpeed4);
		SystemVars.ReadOnlyVars.pse_FanSpeed5 = CMX_TACH_GetValue(&pse_FanSpeed5);
		SystemVars.ReadOnlyVars.pse_FanSpeed6 = CMX_TACH_GetValue(&pse_FanSpeed6);
		SystemVars.ReadOnlyVars.pse_FanSpeed7 = CMX_TACH_GetValue(&pse_FanSpeed7);
		SystemVars.ReadOnlyVars.pse_FanSpeed8 = CMX_TACH_GetValue(&pse_FanSpeed8);
		SystemVars.ReadOnlyVars.pse_FanSpeed9 = CMX_TACH_GetValue(&pse_FanSpeed9);

        // Custom Post Input function
        CustomPostInputUpdate();

		// run transfer function and update output variables
		TransferFunction();

        // CustomPreOutputUpdate();
        CustomPreOutputUpdate();

		// set outputs
		CMX_FAN_SetValue(&pse_Fan1, (BYTE)SystemVars.ReadOnlyVars.pse_Fan1);
		CMX_FAN_SetValue(&pse_Fan10, (BYTE)SystemVars.ReadOnlyVars.pse_Fan10);
		CMX_FAN_SetValue(&pse_Fan11, (BYTE)SystemVars.ReadOnlyVars.pse_Fan11);
		CMX_FAN_SetValue(&pse_Fan12, (BYTE)SystemVars.ReadOnlyVars.pse_Fan12);
		CMX_FAN_SetValue(&pse_Fan13, (BYTE)SystemVars.ReadOnlyVars.pse_Fan13);
		CMX_FAN_SetValue(&pse_Fan2, (BYTE)SystemVars.ReadOnlyVars.pse_Fan2);
		CMX_FAN_SetValue(&pse_Fan3, (BYTE)SystemVars.ReadOnlyVars.pse_Fan3);
		CMX_FAN_SetValue(&pse_Fan4, (BYTE)SystemVars.ReadOnlyVars.pse_Fan4);
		CMX_FAN_SetValue(&pse_Fan5, (BYTE)SystemVars.ReadOnlyVars.pse_Fan5);
		CMX_FAN_SetValue(&pse_Fan6, (BYTE)SystemVars.ReadOnlyVars.pse_Fan6);
		CMX_FAN_SetValue(&pse_Fan7, (BYTE)SystemVars.ReadOnlyVars.pse_Fan7);
		CMX_FAN_SetValue(&pse_Fan8, (BYTE)SystemVars.ReadOnlyVars.pse_Fan8);
		CMX_FAN_SetValue(&pse_Fan9, (BYTE)SystemVars.ReadOnlyVars.pse_Fan9);

	}
}
