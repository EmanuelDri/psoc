//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LM75_Drv.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for LM75 driver                             
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_LM75_include
#define CMX_LM75_include

#include <M8C.h>
#define LM75_pse_DigitalTemp1_ORDINAL  0
#define LM75_pse_DigitalTemp2_ORDINAL  1
#define LM75_pse_DigitalTemp3_ORDINAL  2
#define LM75_pse_DigitalTemp4_ORDINAL  3
#define LM75_pse_DigitalTemp5_ORDINAL  4
#define CMX_LM75_COUNT 5

typedef struct
{
   BYTE bChannelID;         // Channel ID.
   BYTE bAddress;           // I2C Address
   BYTE bInstance;          // Instance of LM75, zero based count
}CMX_LM75_ParameterBlock;

// Function prototypes
void CMX_LM75_Instantiate(const CMX_LM75_ParameterBlock * thisBLK );
int  CMX_LM75_GetValue(const CMX_LM75_ParameterBlock * thisBLK);

#endif
