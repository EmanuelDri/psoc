//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LM20.c
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This Driver converts volts to temperature per the LM20
//                specification.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "CMX_LM20.h"
#include "cmx.h"
#include "calibration.h"

// Channel type header file
#include "CMX_MVOLTS_IN_CHAN.h"

#define MIN -550
#define MAX 1300

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LM20_Instantiate(const CMX_LM20_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function doesn't do anything at this time, but is placed here
//    for forward compatibility.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void CMX_LM20_Instantiate(const CMX_LM20_ParameterBlock * thisBLK)
{
   // Blank function
   
}


//-----------------------------------------------------------------------------
//  FUNCTION NAME: LM20_GetValue(const CMX_LM20_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function gets the reading from the ADC in mVolts, converts
//    it to degrees C and returns the result.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: 
//    Interger value of degrees C.
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//  LM20 Transfer function
//
//  Vout = -11.69mV/C * T  + 1.8663 Volts
//
//  Solving for T
//
//       Vout - 1.8663      -Vout          1.8663 V
//  T = --------------- = -----------  +  ------------
//       -11.69 mV/C      0.01169 V/C      0.01169 V/C
//       
// In order to not require floating point math, the numbers were normalized
// to use integer math.  Vout is now in mVolts so we must use the conversion
// of 1/1000 V/mV
//
//       -Vout (mV)      1 V        1.8663 V
//  T = -----------  * -------  +  ------------
//      0.01169 V/C    1000 mV     0.01169 V/C
//       
//                                  1 V       
//  T =- Vout (mV) * 85.543 C/V * -------  +  159.6 C          
//                               1000 mV     
//       
//  Now multiply  constant (C/V) by a thousand to maintain accuracy
//
//                   85543          1 V       
//  T =- Vout (mV) * ------ C/V * -------  +  159.6 C          
//                   1000        1000 mV     
//       
// Simplify and cancel units
//
//                           1     
//  T =- Vout * 85543 C * --------  +  159.6 C          
//                        1000,000      
//       
// Now to convert to 10ths of a degree
//
//  T                            1     
//  -- =- Vout * 85543 C/10 * --------  +  1596 C/10          
//  10                        100,000      
//       
//
//  The constants for the equation were then modified to provide more accuracy
//  betweet 0 and 100 C.
//
//-----------------------------------------------------------------------------
int  CMX_LM20_GetValue(const CMX_LM20_ParameterBlock * thisBLK)
{
   long lTemp;
   BYTE bChan;

   bChan = thisBLK->InPort;

   lTemp = (long)iGetChanMVolts(bChan);            // Get mVolts

   lTemp = lTemp  * (long)84111;                   // Convert mVolts to degrees C
   lTemp = lTemp / (long)(-100000);
   lTemp = lTemp + (long)1572;

   lTemp += imVolts_Chan_Offset[bChan];           // Add user calibration offset
   
   if(lTemp < MIN) lTemp = MIN;
   if(lTemp > MAX) lTemp = MAX;
   
   return ((int)lTemp);
}

