//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_I2C.c
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This file contains the wrapper for the I2C interface.  When
//                the I2C interface is instantiated, the buffer will be set
//                and the interface will be started.
//
//                Note:
//                Global interrupts MUST be enable prior to calling
//                I2C_Instantiate.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "PSoCAPI.h" 
#include "CMX_I2C.h"
#include "cmx.h"

//-----------------------------------------------------------------------------
//  FUNCTION NAME: I2C_Instantiate(const CMX_I2C_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function starts up the I2C interface.  Global interrupts should be 
//    enabled prior to this call. 
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void CMX_I2C_Instantiate( const CMX_I2C_ParameterBlock * thisBLK )
{
    CMX_I2C_ParameterBlock  thisPB;      // Create copy of parameter block
    thisPB = *thisBLK;                           // Make copy, smaller than all the individual
                                                 // ROMX calls.
   
    I2C_CFG |= 0x01;                             // Make sure I2C slave is enabled.

    EzI2Cs_SetRamBuffer(thisPB.bRAMLen, thisPB.bRWLen, (BYTE *)thisPB.pbRAMRegs); // Set up RAM buffer for I2C

#ifndef DISABLE_I2C_ROM
    if(thisPB.bROMLen > 0 ) {                                          // Only setup ROM buffer if it is present.
        EzI2Cs_SetRomBuffer(thisPB.bROMLen, thisPB.pbROMRegs); // Set up ROM buffer for I2C
    }
#endif

    EzI2Cs_Start(); // Turn on I2C
     EzI2Cs_SetAddr(thisPB.bAddress); // Set I2C address

}
//-----------------------------------------------------------------------------
//  FUNCTION NAME: I2C_SetValue(const CMX_I2C_ParameterBlock * thisBLK, BYTE bAddrOffset)
//
//  DESCRIPTION:  
//    Adds the address given by bAddrOffset with the default address.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK     => Pointer to ParameterBlock for this instance.
//    bAddrOffset => Add this value with default address.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void CMX_I2C_SetValue(const CMX_I2C_ParameterBlock * thisBLK, BYTE bAddrOffset )
{
    EzI2Cs_SetAddr(thisBLK->bAddress + (BYTE)(bAddrOffset)); // Set I2C address
}
