//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_TACHTIMER_CHAN.h
//  @Version@
//  
//
//  DESCRIPTION:  Header file for the tach lld.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef tach_HEADER
#define tach_HEADER

extern unsigned int aiTACH_Results[];
extern BYTE         PwmValues[];
extern const BYTE   tachChannelPorts[];
extern const BYTE   TACH_TABLE[];

void   InitTachLLD(void);
#endif
