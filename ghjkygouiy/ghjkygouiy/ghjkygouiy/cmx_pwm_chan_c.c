//*****************************************************************************
//*****************************************************************************
//  FILENAME:  cmx_pwm_chan.c
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  C file for the PWM channel.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "cmx_pwm_chan.h"

//-----------------------------------------------------------------------------
//  FUNCTION NAME: PWMSetDutyCycle(BYTE bID, BYTE bDutyCycle)
//
//  DESCRIPTION:  
//    Sets the duty cycle for the given channel.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    bID        => Pointer to ParameterBlock for this instance.
//    bDutyCycle =>
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//
//-----------------------------------------------------------------------------
void PWMSetDutyCycle(BYTE bID, BYTE bDutyCycle)
{
	BYTE Period;
	BYTE CompareValue;
	if (bDutyCycle > 100)
	{
		bDutyCycle = 100;
	}
	Period = PWMGetPeriod(bID);
	CompareValue = ((int)(Period + 1) * (int)bDutyCycle)/100;
    PWMSetPulseWidth(bID, CompareValue);
}
