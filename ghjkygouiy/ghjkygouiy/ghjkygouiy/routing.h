#pragma fastcall16 EnableInputToBlock
#pragma fastcall16 EnableClockToBlock
#pragma fastcall16 EnableAnyInputToBlock
#pragma fastcall16 DisableInputToBlock

#pragma fastcall16 EnableOutputFromBlock
#pragma fastcall16 DisableOutputFromBlock
#pragma fastcall16 ControlOutputFromBlock

#pragma fastcall16 SetDriveMode

// Inputs Identifiers for the EnableAnyInputToBlock function
#define		ROUTING_DATA	0x0F         	// data  input
#define		ROUTING_CLK		0xF0         	// clock input

// Enable/disable DB otput Identifiers for the ControlOutputFromBlock function
#define		ENABLE_OUT		0xFF         	// enable output of DB block	
#define		DISABLE_OUT		0x00         	// disable output of DB block


extern void EnableInputToBlock(BYTE port_pin, BYTE block_num);
extern void EnableClockToBlock(BYTE port_pin, BYTE block_num);
extern void EnableAnyInputToBlock(BYTE input_select, BYTE port_pin, BYTE block_num);
extern void DisableInputToBlock(BYTE port_pin);

extern void EnableOutputFromBlock(BYTE port_pin, BYTE block_num);
extern void DisableOutputFromBlock(BYTE port_pin, BYTE block_num);
extern void ControlOutputFromBlock(BYTE ctr_flag, BYTE port_pin, BYTE block_num);

extern void SetDriveMode(BYTE port_pin, BYTE drive_mode);
