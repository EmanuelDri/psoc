//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

#if (SYSTEM_CONST_COUNT & 0xFF00)
const CMX_I2C_ParameterBlock pse_Monitor = { ID_pse_Monitor, 4, (BYTE)SYSTEM_VARIABLE_COUNT, (BYTE)SYSTEM_RW_VARIABLE_COUNT, (BYTE *) &SystemVars, 0xFF, (BYTE const *)&SystemConst };
#else
const CMX_I2C_ParameterBlock pse_Monitor = { ID_pse_Monitor, 4, (BYTE)SYSTEM_VARIABLE_COUNT, (BYTE)SYSTEM_RW_VARIABLE_COUNT, (BYTE *) &SystemVars, (BYTE)SYSTEM_CONST_COUNT, (BYTE const *)&SystemConst };
#endif
const CMX_LM75_ParameterBlock pse_DigitalTemp1 = { ID_I2Cm_00, 0, LM75_pse_DigitalTemp1_ORDINAL };
const CMX_LM75_ParameterBlock pse_DigitalTemp2 = { ID_I2Cm_01, 1, LM75_pse_DigitalTemp2_ORDINAL };
const CMX_LM75_ParameterBlock pse_DigitalTemp3 = { ID_I2Cm_02, 2, LM75_pse_DigitalTemp3_ORDINAL };
const CMX_LM75_ParameterBlock pse_DigitalTemp4 = { ID_I2Cm_03, 3, LM75_pse_DigitalTemp4_ORDINAL };
const CMX_LM75_ParameterBlock pse_DigitalTemp5 = { ID_I2Cm_04, 4, LM75_pse_DigitalTemp5_ORDINAL };
const CMX_LM20_ParameterBlock pse_AnalogTemp1 = { ID_pse_AnalogTemp1, ID_MVOLTS_00 };
const CMX_LM20_ParameterBlock pse_AnalogTemp2 = { ID_pse_AnalogTemp2, ID_MVOLTS_01 };
const CMX_LM20_ParameterBlock pse_AnalogTemp3 = { ID_pse_AnalogTemp3, ID_MVOLTS_02 };
const CMX_LM20_ParameterBlock pse_AnalogTemp4 = { ID_pse_AnalogTemp4, ID_MVOLTS_03 };
const CMX_LM20_ParameterBlock pse_AnalogTemp5 = { ID_pse_AnalogTemp5, ID_MVOLTS_04 };
const CMX_LM20_ParameterBlock pse_AnalogTemp6 = { ID_pse_AnalogTemp6, ID_MVOLTS_05 };
const CMX_LM20_ParameterBlock pse_AnalogTemp7 = { ID_pse_AnalogTemp7, ID_MVOLTS_06 };
const CMX_LM20_ParameterBlock pse_AnalogTemp8 = { ID_pse_AnalogTemp8, ID_MVOLTS_07 };
const CMX_FAN_ParameterBlock pse_Fan1 = { ID_pse_Fan1,  1, 1, 16,  1, ID_PWM_00};
const CMX_FAN_ParameterBlock pse_Fan10 = { ID_pse_Fan10,  1, 1, 16,  1, ID_PWM_04};
const CMX_FAN_ParameterBlock pse_Fan11 = { ID_pse_Fan11,  1, 1, 16,  1, ID_PWM_05};
const CMX_FAN_ParameterBlock pse_Fan12 = { ID_pse_Fan12,  1, 1, 16,  1, ID_PWM_06};
const CMX_FAN_ParameterBlock pse_Fan13 = { ID_pse_Fan13,  1, 1, 16,  1, ID_PWM_07};
const CMX_FAN_ParameterBlock pse_Fan2 = { ID_pse_Fan2,  1, 1, 16,  1, ID_PWM_08};
const CMX_FAN_ParameterBlock pse_Fan3 = { ID_pse_Fan3,  1, 1, 16,  1, ID_PWM_09};
const CMX_FAN_ParameterBlock pse_Fan4 = { ID_pse_Fan4,  1, 1, 16,  1, ID_PWM_10};
const CMX_FAN_ParameterBlock pse_Fan5 = { ID_pse_Fan5,  1, 1, 16,  1, ID_PWM_11};
const CMX_FAN_ParameterBlock pse_Fan6 = { ID_pse_Fan6,  1, 1, 16,  1, ID_PWM_12};
const CMX_FAN_ParameterBlock pse_Fan7 = { ID_pse_Fan7,  1, 1, 16,  1, ID_PWM_13};
const CMX_FAN_ParameterBlock pse_Fan8 = { ID_pse_Fan8,  1, 1, 16,  1, ID_PWM_14};
const CMX_FAN_ParameterBlock pse_Fan9 = { ID_pse_Fan9,  1, 1, 16,  1, ID_PWM_15};
const CMX_TACH_ParameterBlock pse_FanSpeed1 = { ID_pse_FanSpeed1, 4, ID_NoFan, ID_TACH_08};
const CMX_TACH_ParameterBlock pse_FanSpeed10 = { ID_pse_FanSpeed10, 4, ID_pse_Fan10, ID_TACH_09};
const CMX_TACH_ParameterBlock pse_FanSpeed11 = { ID_pse_FanSpeed11, 4, ID_pse_Fan11, ID_TACH_10};
const CMX_TACH_ParameterBlock pse_FanSpeed12 = { ID_pse_FanSpeed12, 4, ID_pse_Fan12, ID_TACH_11};
const CMX_TACH_ParameterBlock pse_FanSpeed13 = { ID_pse_FanSpeed13, 4, ID_pse_Fan13, ID_TACH_12};
const CMX_TACH_ParameterBlock pse_FanSpeed2 = { ID_pse_FanSpeed2, 4, ID_NoFan, ID_TACH_14};
const CMX_TACH_ParameterBlock pse_FanSpeed3 = { ID_pse_FanSpeed3, 4, ID_NoFan, ID_TACH_16};
const CMX_TACH_ParameterBlock pse_FanSpeed4 = { ID_pse_FanSpeed4, 4, ID_NoFan, ID_TACH_17};
const CMX_TACH_ParameterBlock pse_FanSpeed5 = { ID_pse_FanSpeed5, 4, ID_NoFan, ID_TACH_18};
const CMX_TACH_ParameterBlock pse_FanSpeed6 = { ID_pse_FanSpeed6, 4, ID_NoFan, ID_TACH_19};
const CMX_TACH_ParameterBlock pse_FanSpeed7 = { ID_pse_FanSpeed7, 4, ID_NoFan, ID_TACH_20};
const CMX_TACH_ParameterBlock pse_FanSpeed8 = { ID_pse_FanSpeed8, 4, ID_NoFan, ID_TACH_21};
const CMX_TACH_ParameterBlock pse_FanSpeed9 = { ID_pse_FanSpeed9, 4, ID_pse_Fan9, ID_TACH_22};

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_Monitor, // ID = 1
	(void *)&pse_DigitalTemp1, // ID = 2
	(void *)&pse_DigitalTemp2, // ID = 3
	(void *)&pse_DigitalTemp3, // ID = 4
	(void *)&pse_DigitalTemp4, // ID = 5
	(void *)&pse_DigitalTemp5, // ID = 6
	(void *)&pse_AnalogTemp1, // ID = 7
	(void *)&pse_AnalogTemp2, // ID = 8
	(void *)&pse_AnalogTemp3, // ID = 9
	(void *)&pse_AnalogTemp4, // ID = 10
	(void *)&pse_AnalogTemp5, // ID = 11
	(void *)&pse_AnalogTemp6, // ID = 12
	(void *)&pse_AnalogTemp7, // ID = 13
	(void *)&pse_AnalogTemp8, // ID = 14
	(void *)&pse_Fan1, // ID = 15
	(void *)&pse_Fan10, // ID = 16
	(void *)&pse_Fan11, // ID = 17
	(void *)&pse_Fan12, // ID = 18
	(void *)&pse_Fan13, // ID = 19
	(void *)&pse_Fan2, // ID = 20
	(void *)&pse_Fan3, // ID = 21
	(void *)&pse_Fan4, // ID = 22
	(void *)&pse_Fan5, // ID = 23
	(void *)&pse_Fan6, // ID = 24
	(void *)&pse_Fan7, // ID = 25
	(void *)&pse_Fan8, // ID = 26
	(void *)&pse_Fan9, // ID = 27
	(void *)&pse_FanSpeed1, // ID = 28
	(void *)&pse_FanSpeed10, // ID = 29
	(void *)&pse_FanSpeed11, // ID = 30
	(void *)&pse_FanSpeed12, // ID = 31
	(void *)&pse_FanSpeed13, // ID = 32
	(void *)&pse_FanSpeed2, // ID = 33
	(void *)&pse_FanSpeed3, // ID = 34
	(void *)&pse_FanSpeed4, // ID = 35
	(void *)&pse_FanSpeed5, // ID = 36
	(void *)&pse_FanSpeed6, // ID = 37
	(void *)&pse_FanSpeed7, // ID = 38
	(void *)&pse_FanSpeed8, // ID = 39
	(void *)&pse_FanSpeed9, // ID = 40
		// ID_MAX = 41
};
