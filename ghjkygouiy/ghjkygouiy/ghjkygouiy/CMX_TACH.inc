; tach.inc
;

TACH_pse_FanSpeed1_ORDINAL: equ  0
TACH_pse_FanSpeed10_ORDINAL: equ  1
TACH_pse_FanSpeed11_ORDINAL: equ  2
TACH_pse_FanSpeed12_ORDINAL: equ  3
TACH_pse_FanSpeed13_ORDINAL: equ  4
TACH_pse_FanSpeed2_ORDINAL: equ  5
TACH_pse_FanSpeed3_ORDINAL: equ  6
TACH_pse_FanSpeed4_ORDINAL: equ  7
TACH_pse_FanSpeed5_ORDINAL: equ  8
TACH_pse_FanSpeed6_ORDINAL: equ  9
TACH_pse_FanSpeed7_ORDINAL: equ 10
TACH_pse_FanSpeed8_ORDINAL: equ 11
TACH_pse_FanSpeed9_ORDINAL: equ 12
