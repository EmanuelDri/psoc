//
//  TransferFunction.c
//

#include <m8c.h>
#include "DriverDecl.h"
#include "CMXSystem.h"
#include "CMXSystemFunction.h"
#include "CMXSystemExtern.h"
#include "FunctionParamDecl.h"

extern void pse_RampFunction1_TransferFunction(void);
extern void pse_RampFunction10_TransferFunction(void);
extern void pse_RampFunction11_TransferFunction(void);
extern void pse_RampFunction12_TransferFunction(void);
extern void pse_RampFunction13_TransferFunction(void);
extern void pse_RampFunction2_TransferFunction(void);
extern void pse_RampFunction3_TransferFunction(void);
extern void pse_RampFunction4_TransferFunction(void);
extern void pse_RampFunction5_TransferFunction(void);
extern void pse_RampFunction6_TransferFunction(void);
extern void pse_RampFunction7_TransferFunction(void);
extern void pse_RampFunction8_TransferFunction(void);
extern void pse_RampFunction9_TransferFunction(void);
extern void pse_Fan1_TransferFunction(void);
extern void pse_Fan10_TransferFunction(void);
extern void pse_Fan11_TransferFunction(void);
extern void pse_Fan12_TransferFunction(void);
extern void pse_Fan13_TransferFunction(void);
extern void pse_Fan2_TransferFunction(void);
extern void pse_Fan3_TransferFunction(void);
extern void pse_Fan4_TransferFunction(void);
extern void pse_Fan5_TransferFunction(void);
extern void pse_Fan6_TransferFunction(void);
extern void pse_Fan7_TransferFunction(void);
extern void pse_Fan8_TransferFunction(void);
extern void pse_Fan9_TransferFunction(void);

void TransferFunction( void)
{
	pse_RampFunction9_TransferFunction();
	pse_RampFunction8_TransferFunction();
	pse_RampFunction7_TransferFunction();
	pse_RampFunction6_TransferFunction();
	pse_RampFunction5_TransferFunction();
	pse_RampFunction4_TransferFunction();
	pse_RampFunction3_TransferFunction();
	pse_RampFunction2_TransferFunction();
	pse_RampFunction13_TransferFunction();
	pse_RampFunction12_TransferFunction();
	pse_RampFunction11_TransferFunction();
	pse_RampFunction10_TransferFunction();
	pse_RampFunction1_TransferFunction();
	pse_Fan9_TransferFunction();
	pse_Fan8_TransferFunction();
	pse_Fan7_TransferFunction();
	pse_Fan6_TransferFunction();
	pse_Fan5_TransferFunction();
	pse_Fan4_TransferFunction();
	pse_Fan3_TransferFunction();
	pse_Fan2_TransferFunction();
	pse_Fan13_TransferFunction();
	pse_Fan12_TransferFunction();
	pse_Fan11_TransferFunction();
	pse_Fan10_TransferFunction();
	pse_Fan1_TransferFunction();
}
