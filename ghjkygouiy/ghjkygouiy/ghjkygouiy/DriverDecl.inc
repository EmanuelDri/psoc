//----------------------------------------------------------------------------
// DriverDecl.inc
//----------------------------------------------------------------------------

include "m8c.inc"
include "memory.inc"

; default case, will be redefined below if PWM channels are used
IF(TOOLCHAIN & HITECH) 
	; in HI-TECH, symbols can't be re-equated, so set must be used.
	CMX_PWM_CHAN_COUNT: set 0
ELSE
	CMX_PWM_CHAN_COUNT: equ 0 
ENDIF

; Driver ID's
ID_pse_Monitor:	equ	1
ID_pse_DigitalTemp1:	equ	2
ID_pse_DigitalTemp2:	equ	3
ID_pse_DigitalTemp3:	equ	4
ID_pse_DigitalTemp4:	equ	5
ID_pse_DigitalTemp5:	equ	6
ID_pse_AnalogTemp1:	equ	7
ID_pse_AnalogTemp2:	equ	8
ID_pse_AnalogTemp3:	equ	9
ID_pse_AnalogTemp4:	equ	10
ID_pse_AnalogTemp5:	equ	11
ID_pse_AnalogTemp6:	equ	12
ID_pse_AnalogTemp7:	equ	13
ID_pse_AnalogTemp8:	equ	14
ID_pse_Fan1:	equ	15
ID_pse_Fan10:	equ	16
ID_pse_Fan11:	equ	17
ID_pse_Fan12:	equ	18
ID_pse_Fan13:	equ	19
ID_pse_Fan2:	equ	20
ID_pse_Fan3:	equ	21
ID_pse_Fan4:	equ	22
ID_pse_Fan5:	equ	23
ID_pse_Fan6:	equ	24
ID_pse_Fan7:	equ	25
ID_pse_Fan8:	equ	26
ID_pse_Fan9:	equ	27
ID_pse_FanSpeed1:	equ	28
ID_pse_FanSpeed10:	equ	29
ID_pse_FanSpeed11:	equ	30
ID_pse_FanSpeed12:	equ	31
ID_pse_FanSpeed13:	equ	32
ID_pse_FanSpeed2:	equ	33
ID_pse_FanSpeed3:	equ	34
ID_pse_FanSpeed4:	equ	35
ID_pse_FanSpeed5:	equ	36
ID_pse_FanSpeed6:	equ	37
ID_pse_FanSpeed7:	equ	38
ID_pse_FanSpeed8:	equ	39
ID_pse_FanSpeed9:	equ	40

; Channel ID's
ID_ADC_00:		equ 0
ID_I2C_00:		equ 0
ID_I2Cm_00:		equ 0
ID_I2Cm_01:		equ 1
ID_I2Cm_02:		equ 2
ID_I2Cm_03:		equ 3
ID_I2Cm_04:		equ 4
ID_MVOLTS_00:		equ 0
ID_MVOLTS_01:		equ 1
ID_MVOLTS_02:		equ 2
ID_MVOLTS_03:		equ 3
ID_MVOLTS_04:		equ 4
ID_MVOLTS_05:		equ 5
ID_MVOLTS_06:		equ 6
ID_MVOLTS_07:		equ 7
ID_PWM_00:		equ 0
ID_PWM_04:		equ 1
ID_PWM_05:		equ 2
ID_PWM_06:		equ 3
ID_PWM_07:		equ 4
ID_PWM_08:		equ 5
ID_PWM_09:		equ 6
ID_PWM_10:		equ 7
ID_PWM_11:		equ 8
ID_PWM_12:		equ 9
ID_PWM_13:		equ 10
ID_PWM_14:		equ 11
ID_PWM_15:		equ 12
ID_SHADOWREGS_0:		equ 0
ID_SHADOWREGS_1:		equ 1
ID_SHADOWREGS_2:		equ 2
ID_SHADOWREGS_3:		equ 3
ID_SHADOWREGS_4:		equ 4
ID_ShI2Cm_22:		equ 22
ID_TACH_08:		equ 0
ID_TACH_09:		equ 1
ID_TACH_10:		equ 2
ID_TACH_11:		equ 3
ID_TACH_12:		equ 4
ID_TACH_14:		equ 5
ID_TACH_16:		equ 6
ID_TACH_17:		equ 7
ID_TACH_18:		equ 8
ID_TACH_19:		equ 9
ID_TACH_20:		equ 10
ID_TACH_21:		equ 11
ID_TACH_22:		equ 12
ID_TACH_Timer:		equ 1

; Channel Type maximum count
CMX_ADC_CHAN_MAX:	equ	1
CMX_I2C_IFC_CHAN_MAX:	equ	1
CMX_I2Cm_CHAN_MAX:	equ	5
CMX_MVOLTS_IN_CHAN_MAX:	equ	8
CMX_PWM_CHAN_MAX:	equ	13
CMX_SHADOW_MAX:	equ	5
CMX_I2Cm_Shared_CHAN_MAX:	equ	1
CMX_TACH_IN_CHAN_MAX:	equ	13
CMX_TACHTIMER_CHAN_MAX:	equ	1

; Channel Type instantiation count
CMX_ADC_CHAN_COUNT:	equ	0
CMX_I2C_IFC_CHAN_COUNT:	equ	1
CMX_I2Cm_CHAN_COUNT:	equ	5
CMX_MVOLTS_IN_CHAN_COUNT:	equ	8
CMX_PWM_CHAN_COUNT:	equ	13
CMX_SHADOW_COUNT:	equ	0
CMX_I2Cm_Shared_CHAN_COUNT:	equ	0
CMX_TACH_IN_CHAN_COUNT:	equ	13
CMX_TACHTIMER_CHAN_COUNT:	equ	0
