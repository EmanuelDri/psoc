;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: PWM_05.inc
;;   Version: 2.5, Updated on 2009/10/15 at 17:11:37
;;  Generated by PSoC Designer 
;;
;;  DESCRIPTION: Assembler declarations for the PWM8 user module interface
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"


;--------------------------------------------------
; Constants for PWM_05 API's.
;--------------------------------------------------

PWM_05_CONTROL_REG_START_BIT:              equ 0x01    ; Control register start bit
PWM_05_INT_REG:                            equ 0x0e1
PWM_05_INT_MASK:                           equ 0x20


;--------------------------------------------------
; Constants for PWM_05 user defined values
;--------------------------------------------------

PWM_05_PERIOD:                             equ 0xf9
PWM_05_PULSE_WIDTH:                        equ 0x64


;--------------------------------------------------
; Register Address Constants for PWM_05
;--------------------------------------------------

PWM_05_COUNTER_REG:                    equ 0x34   ; DR0 Count register
PWM_05_PERIOD_REG:                     equ 0x35   ; DR1 Period register
PWM_05_COMPARE_REG:                    equ 0x36   ; DR2 Compare register
PWM_05_CONTROL_REG:                    equ 0x37   ; Control register
PWM_05_FUNC_REG:                       equ 0x34   ; Function register
PWM_05_INPUT_REG:                      equ 0x35   ; Input register
PWM_05_OUTPUT_REG:                     equ 0x36   ; Output register


;--------------------------------------------------
; PWM_05 Macro 'Functions'
;--------------------------------------------------

   macro PWM_05_Start_M
   or    reg[PWM_05_CONTROL_REG],  PWM_05_CONTROL_REG_START_BIT
   endm

   macro PWM_05_Stop_M
   and   reg[PWM_05_CONTROL_REG], ~PWM_05_CONTROL_REG_START_BIT
   endm

   macro PWM_05_EnableInt_M
   M8C_EnableIntMask PWM_05_INT_REG, PWM_05_INT_MASK
   endm

   macro PWM_05_DisableInt_M
   M8C_DisableIntMask PWM_05_INT_REG, PWM_05_INT_MASK
   endm


; end of file PWM_05.inc
