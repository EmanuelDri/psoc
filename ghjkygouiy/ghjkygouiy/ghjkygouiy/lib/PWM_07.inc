;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: PWM_07.inc
;;   Version: 2.5, Updated on 2009/10/15 at 17:11:37
;;  Generated by PSoC Designer 
;;
;;  DESCRIPTION: Assembler declarations for the PWM8 user module interface
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "m8c.inc"


;--------------------------------------------------
; Constants for PWM_07 API's.
;--------------------------------------------------

PWM_07_CONTROL_REG_START_BIT:              equ 0x01    ; Control register start bit
PWM_07_INT_REG:                            equ 0x0e1
PWM_07_INT_MASK:                           equ 0x80


;--------------------------------------------------
; Constants for PWM_07 user defined values
;--------------------------------------------------

PWM_07_PERIOD:                             equ 0xf9
PWM_07_PULSE_WIDTH:                        equ 0x64


;--------------------------------------------------
; Register Address Constants for PWM_07
;--------------------------------------------------

PWM_07_COUNTER_REG:                    equ 0x3c   ; DR0 Count register
PWM_07_PERIOD_REG:                     equ 0x3d   ; DR1 Period register
PWM_07_COMPARE_REG:                    equ 0x3e   ; DR2 Compare register
PWM_07_CONTROL_REG:                    equ 0x3f   ; Control register
PWM_07_FUNC_REG:                       equ 0x3c   ; Function register
PWM_07_INPUT_REG:                      equ 0x3d   ; Input register
PWM_07_OUTPUT_REG:                     equ 0x3e   ; Output register


;--------------------------------------------------
; PWM_07 Macro 'Functions'
;--------------------------------------------------

   macro PWM_07_Start_M
   or    reg[PWM_07_CONTROL_REG],  PWM_07_CONTROL_REG_START_BIT
   endm

   macro PWM_07_Stop_M
   and   reg[PWM_07_CONTROL_REG], ~PWM_07_CONTROL_REG_START_BIT
   endm

   macro PWM_07_EnableInt_M
   M8C_EnableIntMask PWM_07_INT_REG, PWM_07_INT_MASK
   endm

   macro PWM_07_DisableInt_M
   M8C_DisableIntMask PWM_07_INT_REG, PWM_07_INT_MASK
   endm


; end of file PWM_07.inc
