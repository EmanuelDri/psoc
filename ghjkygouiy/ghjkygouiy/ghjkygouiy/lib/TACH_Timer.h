//*****************************************************************************
//*****************************************************************************
//  FILENAME: TACH_Timer.h
//   Version: 1.1, Updated on 2009/10/15 at 17:11:37
//  Generated by PSoC Designer 
//
//  DESCRIPTION: Timer16 User Module C Language interface file
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include <m8c.h>

#pragma fastcall16 TACH_Timer_EnableInt
#pragma fastcall16 TACH_Timer_EnableTerminalInt
#pragma fastcall16 TACH_Timer_EnableCaptureInt
#pragma fastcall16 TACH_Timer_DisableInt
#pragma fastcall16 TACH_Timer_DisableTerminalInt
#pragma fastcall16 TACH_Timer_DisableCaptureInt
#pragma fastcall16 TACH_Timer_Start
#pragma fastcall16 TACH_Timer_Stop
#pragma fastcall16 TACH_Timer_wReadTimer                // Read  DR0
#pragma fastcall16 TACH_Timer_wReadTimerSaveCV          // Read  DR0
#pragma fastcall16 TACH_Timer_WritePeriod               // Write DR1
#pragma fastcall16 TACH_Timer_wReadCompareValue         // Read  DR2
#pragma fastcall16 TACH_Timer_WriteCompareValue         // Write DR2



//-------------------------------------------------
// Prototypes of the TACH_Timer API.
//-------------------------------------------------

extern void TACH_Timer_EnableInt(void);
extern void TACH_Timer_EnableTerminalInt(void);
extern void TACH_Timer_EnableCaptureInt(void);
extern void TACH_Timer_DisableInt(void);
extern void TACH_Timer_DisableTerminalInt(void);
extern void TACH_Timer_DisableCaptureInt(void);
extern void TACH_Timer_Start(void);
extern void TACH_Timer_Stop(void);
extern WORD TACH_Timer_wReadTimer(void);
extern WORD TACH_Timer_wReadTimerSaveCV(void);
extern void TACH_Timer_WritePeriod(WORD wPeriod);
extern WORD TACH_Timer_wReadCompareValue(void);
extern void TACH_Timer_WriteCompareValue(WORD wCompareValue);




//--------------------------------------------------
// Constants for TACH_Timer API's.
//--------------------------------------------------

#define TACH_Timer_CONTROL_REG_START_BIT       ( 0x01 )
#define TACH_Timer_INT_REG_ADDR                ( 0x@TIMER16_MSB__ISR_ADDR )
#define TACH_Timer_INT_MASK                    ( 0x@TIMER16_MSB__ISR_MASK )


//--------------------------------------------------
// Constants for TACH_Timer user defined values
//--------------------------------------------------

#define TACH_Timer_PERIOD                      ( 0xffff )
#define TACH_Timer_COMPARE_VALUE               ( 0x0 )


//-------------------------------------------------
// Register Addresses for TACH_Timer
//-------------------------------------------------

#pragma ioport  TACH_Timer_COUNTER_LSB_REG: 0x028          //Count register LSB
BYTE            TACH_Timer_COUNTER_LSB_REG;
#pragma ioport  TACH_Timer_COUNTER_MSB_REG: 0x02c          //Count register MSB
BYTE            TACH_Timer_COUNTER_MSB_REG;
#pragma ioport  TACH_Timer_PERIOD_LSB_REG:  0x029          //Period register LSB
BYTE            TACH_Timer_PERIOD_LSB_REG;
#pragma ioport  TACH_Timer_PERIOD_MSB_REG:  0x02d          //Period register MSB
BYTE            TACH_Timer_PERIOD_MSB_REG;
#pragma ioport  TACH_Timer_COMPARE_LSB_REG: 0x02a          //Compare register LSB
BYTE            TACH_Timer_COMPARE_LSB_REG;
#pragma ioport  TACH_Timer_COMPARE_MSB_REG: 0x02e          //Compare register MSB
BYTE            TACH_Timer_COMPARE_MSB_REG;
#pragma ioport  TACH_Timer_CONTROL_LSB_REG: 0x02b          //Control register LSB
BYTE            TACH_Timer_CONTROL_LSB_REG;
#pragma ioport  TACH_Timer_CONTROL_MSB_REG: 0x02f          //Control register MSB
BYTE            TACH_Timer_CONTROL_MSB_REG;
#pragma ioport  TACH_Timer_FUNC_LSB_REG:    0x128          //Function register LSB
BYTE            TACH_Timer_FUNC_LSB_REG;
#pragma ioport  TACH_Timer_FUNC_MSB_REG:    0x12c          //Function register MSB
BYTE            TACH_Timer_FUNC_MSB_REG;
#pragma ioport  TACH_Timer_INPUT_LSB_REG:   0x129          //Input register LSB
BYTE            TACH_Timer_INPUT_LSB_REG;
#pragma ioport  TACH_Timer_INPUT_MSB_REG:   0x12d          //Input register MSB
BYTE            TACH_Timer_INPUT_MSB_REG;
#pragma ioport  TACH_Timer_OUTPUT_LSB_REG:  0x12a          //Output register LSB
BYTE            TACH_Timer_OUTPUT_LSB_REG;
#pragma ioport  TACH_Timer_OUTPUT_MSB_REG:  0x12e          //Output register MSB
BYTE            TACH_Timer_OUTPUT_MSB_REG;
#pragma ioport  TACH_Timer_MSB_INT_REG:       0x0e1        //Interrupt Mask Register
BYTE            TACH_Timer_MSB_INT_REG;


//-------------------------------------------------
// TACH_Timer Macro 'Functions'
//-------------------------------------------------

#define TACH_Timer_Start_M \
   ( TACH_Timer_CONTROL_LSB_REG |=  TACH_Timer_CONTROL_REG_START_BIT )

#define TACH_Timer_Stop_M  \
   ( TACH_Timer_CONTROL_LSB_REG &= ~TACH_Timer_CONTROL_REG_START_BIT )

#define TACH_Timer_EnableInt_M   \
   M8C_EnableIntMask(  TACH_Timer_MSB_INT_REG, TACH_Timer_MSB_INT_MASK ) \
   M8C_EnableIntMask(  TACH_Timer_LSB_INT_REG, TACH_Timer_LSB_INT_MASK )

#define TACH_Timer_DisableInt_M  \
   M8C_DisableIntMask( TACH_Timer_MSB_INT_REG, TACH_Timer_MSB_INT_MASK ) \
   M8C_DisableIntMask( TACH_Timer_LSB_INT_REG, TACH_Timer_LSB_INT_MASK )


// end of file TACH_Timer.h


