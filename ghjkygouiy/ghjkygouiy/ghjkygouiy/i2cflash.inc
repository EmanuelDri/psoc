;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: I2CFLASH.inc
;;  @Version@
;;  
;;
;;  DESCRIPTION: Assembler declarations for the I2CFLASH low level driver.
;;               29/27/24/22xxx PSoC family of devices
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************
include "systemconst.inc"

;--------------------------------------------------
; Constants for EzI2Cs API's.
;--------------------------------------------------


;--------------------------------------------------
;   EzI2Cs API Constants
;--------------------------------------------------

I2C_ENTER_FLASH_MODE_CMD:                  equ  0x21
I2C_ENTER_FLASH_MODE_KEY1:                 equ  0xDE
I2C_ENTER_FLASH_MODE_KEY0:                 equ  0x01

I2C_FLASH_SHOW_BLOCK_CMD:                  equ  0x22
I2C_FLASH_SHOW_BLOCK_KEY0:                 equ  0x00

I2C_FLASH_READ_BLOCK_CMD:                  equ  0xA3
I2C_FLASH_READ_BLOCK_KEY0:                 equ  0x00

I2C_FLASH_WRITE_BLOCK_CMD:                 equ  0xA5
I2C_FLASH_WRITE_BLOCK_KEY0:                equ  0x28

I2C_FLASH_RESET_CMD:                       equ  0xAC
I2C_FLASH_RESET_KEY1:                      equ  0xDA
I2C_FLASH_RESET_KEY0:                      equ  0x3A


I2C_FLASH_DEFAULT_TEMP:                    equ  0                  ; Use defaul programming temp of 0C


IF (Enable_Flash_Interface & 0x02)
I2C_FLASH_AUTO_TIMEOUT:                    equ  0                  ; Set to 0 to disable flash mode timeout
ELSE
I2C_FLASH_AUTO_TIMEOUT:                    equ  1                  ; Set to 1 to enable flash mode
                                                                   ; timeout after 10 Sec of inactivity.
ENDIF


; end of file I2CFLASH.inc


