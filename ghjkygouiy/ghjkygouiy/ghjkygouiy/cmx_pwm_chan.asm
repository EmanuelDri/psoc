;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: cmx_pwm_chan.asm
;;   @Version@
;;
;;  DESCRIPTION: Implements pwm channel interface
;; 
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;  HISTORY:
;;
;;*****************************************************************************
include "m8c.inc"
include "memory.inc"
include "psocapi.inc"
include "DriverDecl.inc"

;-----------------------------------------------
;  Interface Functions
;-----------------------------------------------
 
export  _PWMStart
export   PWMStart

export  _PWMSetPulseWidth
export   PWMSetPulseWidth

export  _PWMGetPeriod
export   PWMGetPeriod

export  _PWMSetPolarity
export   PWMSetPolarity

export  _PWMSuspend
export   PWMSuspend

export  _FANStates
export   FANStates

export  _FANFlags
export   FANFlags

export  _FANSpinStartTimes
export   FANSpinStartTimes

export  _PWMChannelPins
export   PWMChannelPins 

export  _PWMChannelBlocks
export   PWMChannelBlocks

;-----------------------------------------------
;  Constant Definitions
;-----------------------------------------------

area text(ROM,REL)
.literal
_PWMChannelPins:
 PWMChannelPins:
    db 27h
    db 32h
    db 33h
    db 34h
    db 35h
    db 36h
    db 37h
    db 40h
    db 41h
    db 42h
    db 43h
    db 44h
    db 45h

_PWMChannelBlocks:
 PWMChannelBlocks:
	db 00h
	db 04h
	db 05h
	db 06h
	db 07h
	db 08h
	db 09h
	db 10h
	db 11h
	db 12h
	db 13h
	db 14h
	db 15h
.endliteral
;-----------------------------------------------
 
;-----------------------------------------------
; Variable Allocation
;-----------------------------------------------
area    bss(RAM)
_FANStates:
 FANStates: BLK (CMX_PWM_CHAN_COUNT)

_FANFlags:
 FANFlags: BLK (CMX_PWM_CHAN_COUNT)

_FANSpinStartTimes:
 FANSpinStartTimes: BLK (CMX_PWM_CHAN_COUNT)
 
area    text(ROM,REL)
;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMStart
;
;  DESCRIPTION: 
;    This function starts the selected PWM.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: Channel ID of selected PWM in A
;
;  RETURNS: Nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
	
 PWMStart:
_PWMStart:

    asl   A
    asl   A
    jacc  PWMStrt_JTable ; @STEP_SIZE=4 @NUM_STEPS=13

PWMStrt_JTable:
	; Start PWM_00
	nop
    ljmp  PWM_00_Start ; call UM API
	; Start PWM_04
	nop
    ljmp  PWM_04_Start ; call UM API
	; Start PWM_05
	nop
    ljmp  PWM_05_Start ; call UM API
	; Start PWM_06
	nop
    ljmp  PWM_06_Start ; call UM API
	; Start PWM_07
	nop
    ljmp  PWM_07_Start ; call UM API
	; Start PWM_08
	nop
    ljmp  PWM_08_Start ; call UM API
	; Start PWM_09
	nop
    ljmp  PWM_09_Start ; call UM API
	; Start PWM_10
	nop
    ljmp  PWM_10_Start ; call UM API
	; Start PWM_11
	nop
    ljmp  PWM_11_Start ; call UM API
	; Start PWM_12
	nop
    ljmp  PWM_12_Start ; call UM API
	; Start PWM_13
	nop
    ljmp  PWM_13_Start ; call UM API
	; Start PWM_14
	nop
    ljmp  PWM_14_Start ; call UM API
	; Start PWM_15
	nop
    ljmp  PWM_15_Start ; call UM API

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSetPulseWidth
;
;  DESCRIPTION: 
;    This function writes the compare value of the selected pwm.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: Channel ID of selected pwm in A and pulse width in X
;
;  RETURNS: Nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
 PWMSetPulseWidth:
_PWMSetPulseWidth:
 
    asl   A
    asl   A
    jacc  SetPW_JTable ; @STEP_SIZE=4 @NUM_STEPS=13

SetPW_JTable:
	; Set PulseWidth for PWM_00
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_00_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_04
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_04_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_05
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_05_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_06
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_06_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_07
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_07_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_08
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_08_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_09
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_09_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_10
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_10_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_11
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_11_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_12
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_12_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_13
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_13_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_14
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_14_WritePulseWidth ; call UM API
	; Set PulseWidth for PWM_15
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_15_WritePulseWidth ; call UM API

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMGetPeriod
;
;  DESCRIPTION: 
;    This function returns the period value of the selected pwm.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => Channel ID
;
;  RETURNS: 
;    Period value in A
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
 PWMGetPeriod:
_PWMGetPeriod:
 
    asl   A
    asl   A
    jacc  GetPer_JTable ; @STEP_SIZE=4 @NUM_STEPS=13

GetPer_JTable:
	; Get Period for PWM_00
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_04
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_05
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_06
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_07
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_08
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_09
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_10
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_11
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_12
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_13
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_14
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	; Get Period for PWM_15
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	 
;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSetPolarity
;
;  DESCRIPTION: 
;    Programs positive polarity or negative polarity through reg
;    RDIxLTx.
; 
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => Channel ID 
;    X => polarity
;
;  RETURNS: nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;

 PWMSetPolarity:
_PWMSetPolarity:
	push A
	mov  A,X ; zero if neg polarity, nonzero if positive           
    jnz positive
negative:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMPol_Neg_JTable ; @STEP_SIZE=8 @NUM_STEPS=13
PWMPol_Neg_JTable:
	; Set LUT polarity for PWM_00  
	and  reg[B4h], ~F0h ; 3 byte instruction
	or   reg[B4h], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_04  
	and  reg[BCh], ~0Fh ; 3 byte instruction
	or   reg[BCh], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_05  
	and  reg[BCh], ~F0h ; 3 byte instruction
	or   reg[BCh], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_06  
	and  reg[BBh], ~0Fh ; 3 byte instruction
	or   reg[BBh], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_07  
	and  reg[BBh], ~F0h ; 3 byte instruction
	or   reg[BBh], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_08  
	and  reg[C4h], ~0Fh ; 3 byte instruction
	or   reg[C4h], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_09  
	and  reg[C4h], ~F0h ; 3 byte instruction
	or   reg[C4h], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_10  
	and  reg[C3h], ~0Fh ; 3 byte instruction
	or   reg[C3h], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_11  
	and  reg[C3h], ~F0h ; 3 byte instruction
	or   reg[C3h], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_12  
	and  reg[CCh], ~0Fh ; 3 byte instruction
	or   reg[CCh], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_13  
	and  reg[CCh], ~F0h ; 3 byte instruction
	or   reg[CCh], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_14  
	and  reg[CBh], ~0Fh ; 3 byte instruction
	or   reg[CBh], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_15  
	and  reg[CBh], ~F0h ; 3 byte instruction
	or   reg[CBh], (CCh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

positive:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMPol_Pos_JTable ; @STEP_SIZE=8 @NUM_STEPS=13
PWMPol_Pos_JTable:  
	; Set LUT polarity for PWM_00  
	and  reg[B4h], ~F0h ; 3 byte instruction
	or   reg[B4h], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_04  
	and  reg[BCh], ~0Fh ; 3 byte instruction
	or   reg[BCh], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_05  
	and  reg[BCh], ~F0h ; 3 byte instruction
	or   reg[BCh], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_06  
	and  reg[BBh], ~0Fh ; 3 byte instruction
	or   reg[BBh], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_07  
	and  reg[BBh], ~F0h ; 3 byte instruction
	or   reg[BBh], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_08  
	and  reg[C4h], ~0Fh ; 3 byte instruction
	or   reg[C4h], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_09  
	and  reg[C4h], ~F0h ; 3 byte instruction
	or   reg[C4h], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_10  
	and  reg[C3h], ~0Fh ; 3 byte instruction
	or   reg[C3h], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_11  
	and  reg[C3h], ~F0h ; 3 byte instruction
	or   reg[C3h], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_12  
	and  reg[CCh], ~0Fh ; 3 byte instruction
	or   reg[CCh], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_13  
	and  reg[CCh], ~F0h ; 3 byte instruction
	or   reg[CCh], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_14  
	and  reg[CBh], ~0Fh ; 3 byte instruction
	or   reg[CBh], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT polarity for PWM_15  
	and  reg[CBh], ~F0h ; 3 byte instruction
	or   reg[CBh], (33h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSuspend
;
;  DESCRIPTION: 
;    Forces high or low state at the row output lut through reg
;    RDIxLTx.
; 
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => channel ID 
;    X => polarity of channel, 1 for high or 0 for low
;
;  RETURNS: none
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;

 PWMSuspend:
_PWMSuspend:
	push A
	mov  A,X ; zero if low, nonzero if high           
    	jnz pwm_high
pwm_low:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMSuspend_Neg_JTable ; @STEP_SIZE=8 @NUM_STEPS=13
PWMSuspend_Neg_JTable:
	; Set LUT to 0 for PWM_00  
	and  reg[B4h], ~F0h ; 3 byte instruction
	or   reg[B4h], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_04  
	and  reg[BCh], ~0Fh ; 3 byte instruction
	or   reg[BCh], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_05  
	and  reg[BCh], ~F0h ; 3 byte instruction
	or   reg[BCh], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_06  
	and  reg[BBh], ~0Fh ; 3 byte instruction
	or   reg[BBh], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_07  
	and  reg[BBh], ~F0h ; 3 byte instruction
	or   reg[BBh], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_08  
	and  reg[C4h], ~0Fh ; 3 byte instruction
	or   reg[C4h], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_09  
	and  reg[C4h], ~F0h ; 3 byte instruction
	or   reg[C4h], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_10  
	and  reg[C3h], ~0Fh ; 3 byte instruction
	or   reg[C3h], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_11  
	and  reg[C3h], ~F0h ; 3 byte instruction
	or   reg[C3h], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_12  
	and  reg[CCh], ~0Fh ; 3 byte instruction
	or   reg[CCh], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_13  
	and  reg[CCh], ~F0h ; 3 byte instruction
	or   reg[CCh], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_14  
	and  reg[CBh], ~0Fh ; 3 byte instruction
	or   reg[CBh], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 0 for PWM_15  
	and  reg[CBh], ~F0h ; 3 byte instruction
	or   reg[CBh], (00h & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

pwm_high:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMSuspend_Pos_JTable ; @STEP_SIZE=8 @NUM_STEPS=13
PWMSuspend_Pos_JTable:  
	; Set LUT to 1 for PWM_00  
	and  reg[B4h], ~F0h ; 3 byte instruction
	or   reg[B4h], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_04  
	and  reg[BCh], ~0Fh ; 3 byte instruction
	or   reg[BCh], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_05  
	and  reg[BCh], ~F0h ; 3 byte instruction
	or   reg[BCh], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_06  
	and  reg[BBh], ~0Fh ; 3 byte instruction
	or   reg[BBh], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_07  
	and  reg[BBh], ~F0h ; 3 byte instruction
	or   reg[BBh], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_08  
	and  reg[C4h], ~0Fh ; 3 byte instruction
	or   reg[C4h], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_09  
	and  reg[C4h], ~F0h ; 3 byte instruction
	or   reg[C4h], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_10  
	and  reg[C3h], ~0Fh ; 3 byte instruction
	or   reg[C3h], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_11  
	and  reg[C3h], ~F0h ; 3 byte instruction
	or   reg[C3h], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_12  
	and  reg[CCh], ~0Fh ; 3 byte instruction
	or   reg[CCh], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_13  
	and  reg[CCh], ~F0h ; 3 byte instruction
	or   reg[CCh], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_14  
	and  reg[CBh], ~0Fh ; 3 byte instruction
	or   reg[CBh], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction
	; Set LUT to 1 for PWM_15  
	and  reg[CBh], ~F0h ; 3 byte instruction
	or   reg[CBh], (FFh & F0h) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction


 



