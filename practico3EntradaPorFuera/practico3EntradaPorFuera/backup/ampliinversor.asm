;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME:   ampliInversor.asm
;;  Version: 1.10, Updated on 2015/3/4 at 22:27:44
;;  Generated by PSoC Designer 5.4.3191
;;
;;  DESCRIPTION: SwitchCapConfig User Module software implementation file.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "ampliInversor.inc"
include "m8c.inc"
include "memory.inc"

;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------
export  ampliInversor_Start
export _ampliInversor_Start
export  ampliInversor_SetPower
export _ampliInversor_SetPower
export  ampliInversor_Stop
export _ampliInversor_Stop
export  ampliInversor_SetGain
export _ampliInversor_SetGain

AREA UserModules (ROM, REL)

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: ampliInversor_Start(BYTE bPowerSetting)
;  FUNCTION NAME: ampliInversor_SetPower(BYTE bPowerSetting)
;
;  DESCRIPTION:
;    Applies power setting to the module's analog PSoC block.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  contains the power setting 0-3
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 ampliInversor_Start:
_ampliInversor_Start:
 ampliInversor_SetPower:
_ampliInversor_SetPower:
   RAM_PROLOGUE RAM_USE_CLASS_2
   and   A, ampliInversor_POWERMASK
   push  X
   mov   X, SP
   push  A
   mov   A, reg[ampliInversor_CR3]
   and   A, ~ampliInversor_POWERMASK
   or    A, [X]
   mov   reg[ampliInversor_CR3], A
   pop   A
   pop   X
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: ampliInversor_Stop(void)
;
;  DESCRIPTION:
;    Removes power from the module's analog PSoC block
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: None
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 ampliInversor_Stop:
_ampliInversor_Stop:
   RAM_PROLOGUE RAM_USE_CLASS_1
   and   reg[ampliInversor_CR3], ~ampliInversor_POWERMASK
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: ampliInversor_SetGain(BYTE bGainSetting)
;
;  DESCRIPTION:
;    This function sets the Gain/Atten of the amplifier.  Valid gain settings
;    are defined in the .inc file.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  Contains gain settings.
;
;    Gain values shown are for example. (See .inc file for gain equates)
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 ampliInversor_SetGain:
_ampliInversor_SetGain:
   RAM_PROLOGUE RAM_USE_CLASS_2
   and   A, ampliInversor_GAINMASK                         ; mask A to protect unchanged bits
   mov   X, SP                                             ; define temp store location
   push  A                                                 ; put gain value in temp store
   mov   A, reg[ampliInversor_CR0]                         ; read reg settings
   and   A, ~ampliInversor_GAINMASK                        ; clear gain bits in A
   or    A, [X]                                            ; combine gain value with balance of reg.
   mov   reg[ampliInversor_CR0], A                         ; move complete value back to register
   pop   A
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

; End of File ampliInversor.asm
