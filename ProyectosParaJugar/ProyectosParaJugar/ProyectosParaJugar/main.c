//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules


void main(void)
{
	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	LCD_Start();
	LCD_Position(0,0);
	LCD_PrCString("Apreta el boton");
	while (1){
		if (PRT0DR&0x01){
			LCD_Position(0,0);
			LCD_PrCString("Lo apretaste");
		}
		else {
			LCD_Position(0,0);
	LCD_PrCString("Apreta el boton");
	}
	}
}
