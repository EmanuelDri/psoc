//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules


void main(void)
{
	   unsigned char bADCData; /*Define the LCD display variable*/
     int bADCDataPWM; /*Define the PWM output variable*/
      /* Enable Interrupt for ADC */
      M8C_EnableGInt ;

      /* Initialize all User Modules */
      LCD_Start();
      PGA_Start(PGA_HIGHPOWER);
      ADCINC_Start(ADCINC_HIGHPOWER);

       /* Run ADC continuously */
       ADCINC_GetSamples(0);

       while(1) /* Loop forever */
       {
              /* If data is valid display on LCD */
              if(ADCINC_fIsDataAvailable() != 0)
              {
                       /* Read ADC Result and assign to PWM output */
                       bADCDataPWM = ADCINC_bGetData();
                       /* Assign PWM output to LCD Display variable */
                       bADCData = bADCDataPWM;

                       /* Display on LCD */
                       LCD_Position(0,0);
                       /* Write LCD display value to LCD screen */
                       LCD_PrHexByte(bADCData);
                       /* Write PWM output variable to control PWM pulse width */
                       ADCINC_WritePulseWidth(bADCDataPWM);

                       /* Clear ADC flag for next reading */
                       ADCINC_fClearFlag();
              }
       }
}
