//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "string.h"
#include "stdlib.h"

void main(void)
{
	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	ADC_Start(ADC_HIGHPOWER);
	LCD_Start();
	PGA_Start(PGA_HIGHPOWER);
	LCD_Position(0,0);
	LCD_PrCString("Lectura del ADC");
	ADC_GetSamples(0);
	while (1){
		if(ADC_fIsDataAvailable!=0){
			unsigned int valorLectura;
			valorLectura=ADC_bGetData();
			LCD_Position(1,0);
			LCD_PrHexInt(valorLectura);
			ADC_fClearFlag();
			ADC_WritePulseWidth(valorLectura);
		}
	}
}
