;  Generated by PSoC Designer 5.4.3191
;
; =============================================================================
; FILENAME: PSoCConfigTBL.asm
;  
; Copyright (c) Cypress Semiconductor 2013. All Rights Reserved.
;  
; NOTES:
; Do not modify this file. It is generated by PSoC Designer each time the
; generate application function is run. The values of the parameters in this
; file can be modified by changing the values of the global parameters in the
; device editor.
;  
; =============================================================================
 
include "m8c.inc"
;  Personalization tables 
export LoadConfigTBL_detectordeflancos2_Bank1
export LoadConfigTBL_detectordeflancos2_Bank0
export LoadConfigTBL_detectordeflancos2_Ordered
export UnloadConfigTBL_detectordeflancos2_Bank1
export UnloadConfigTBL_detectordeflancos2_Bank0
export ReloadConfigTBL_detectordeflancos2_Bank1
export ReloadConfigTBL_detectordeflancos2_Bank0
export LoadConfigTBL_laOtraConfig_Bank1
export LoadConfigTBL_laOtraConfig_Bank0
export UnloadConfigTBL_laOtraConfig_Bank1
export UnloadConfigTBL_laOtraConfig_Bank0
export UnloadConfigTBL_Total_Bank1
export UnloadConfigTBL_Total_Bank0
AREA lit(rom, rel)
LoadConfigTBL_detectordeflancos2_Bank0:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		23h, 00h		;PWM1_CONTROL_REG(DBB00CR0)
	db		21h, ffh		;PWM1_PERIOD_REG(DBB00DR1)
	db		22h, 80h		;PWM1_COMPARE_REG(DBB00DR2)
;  Global Register values Bank 0
	db		60h, 28h		; AnalogColumnInputSelect register (AMX_IN)
	db		66h, 00h		; AnalogComparatorControl1 register (CMP_CR1)
	db		63h, 05h		; AnalogReferenceControl register (ARF_CR)
	db		65h, 00h		; AnalogSyncControl register (ASY_CR)
	db		e6h, 00h		; DecimatorControl_0 register (DEC_CR0)
	db		e7h, 00h		; DecimatorControl_1 register (DEC_CR1)
	db		d6h, 00h		; I2CConfig register (I2CCFG)
	db		b0h, 00h		; Row_0_InputMux register (RDI0RI)
	db		b1h, 00h		; Row_0_InputSync register (RDI0SYN)
	db		b2h, 00h		; Row_0_LogicInputAMux register (RDI0IS)
	db		b3h, 33h		; Row_0_LogicSelect_0 register (RDI0LT0)
	db		b4h, 33h		; Row_0_LogicSelect_1 register (RDI0LT1)
	db		b5h, 04h		; Row_0_OutputDrive_0 register (RDI0SRO0)
	db		b6h, 00h		; Row_0_OutputDrive_1 register (RDI0SRO1)
	db		b8h, 55h		; Row_1_InputMux register (RDI1RI)
	db		b9h, 00h		; Row_1_InputSync register (RDI1SYN)
	db		bah, 10h		; Row_1_LogicInputAMux register (RDI1IS)
	db		bbh, 33h		; Row_1_LogicSelect_0 register (RDI1LT0)
	db		bch, 33h		; Row_1_LogicSelect_1 register (RDI1LT1)
	db		bdh, 00h		; Row_1_OutputDrive_0 register (RDI1SRO0)
	db		beh, 00h		; Row_1_OutputDrive_1 register (RDI1SRO1)
	db		ffh
LoadConfigTBL_detectordeflancos2_Bank1:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		20h, 21h		;PWM1_FUNC_REG(DBB00FN)
	db		21h, 11h		;PWM1_INPUT_REG(DBB00IN)
	db		22h, 44h		;PWM1_OUTPUT_REG(DBB00OU)
;  Global Register values Bank 1
	db		61h, 00h		; AnalogClockSelect1 register (CLK_CR1)
	db		69h, 00h		; AnalogClockSelect2 register (CLK_CR2)
	db		60h, 00h		; AnalogColumnClockSelect register (CLK_CR0)
	db		62h, 00h		; AnalogIOControl_0 register (ABF_CR0)
	db		67h, 33h		; AnalogLUTControl0 register (ALT_CR0)
	db		68h, 33h		; AnalogLUTControl1 register (ALT_CR1)
	db		63h, 00h		; AnalogModulatorControl_0 register (AMD_CR0)
	db		66h, 00h		; AnalogModulatorControl_1 register (AMD_CR1)
	db		d1h, 00h		; GlobalDigitalInterconnect_Drive_Even_Input register (GDI_E_IN)
	db		d3h, 00h		; GlobalDigitalInterconnect_Drive_Even_Output register (GDI_E_OU)
	db		d0h, 00h		; GlobalDigitalInterconnect_Drive_Odd_Input register (GDI_O_IN)
	db		d2h, 00h		; GlobalDigitalInterconnect_Drive_Odd_Output register (GDI_O_OU)
	db		e1h, ffh		; OscillatorControl_1 register (OSC_CR1)
	db		e2h, 00h		; OscillatorControl_2 register (OSC_CR2)
	db		dfh, ffh		; OscillatorControl_3 register (OSC_CR3)
	db		deh, 02h		; OscillatorControl_4 register (OSC_CR4)
	db		ddh, 00h		; OscillatorGlobalBusEnableControl register (OSC_GO_EN)
	db		ffh
AREA psoc_config(rom, rel)
LoadConfigTBL_detectordeflancos2_Ordered:
;  Ordered Global Register values
	M8C_SetBank0
	mov	reg[00h], 00h		; Port_0_Data register (PRT0DR)
	M8C_SetBank1
	mov	reg[00h], 02h		; Port_0_DriveMode_0 register (PRT0DM0)
	mov	reg[01h], fch		; Port_0_DriveMode_1 register (PRT0DM1)
	M8C_SetBank0
	mov	reg[03h], fch		; Port_0_DriveMode_2 register (PRT0DM2)
	mov	reg[02h], 00h		; Port_0_GlobalSelect register (PRT0GS)
	M8C_SetBank1
	mov	reg[02h], 00h		; Port_0_IntCtrl_0 register (PRT0IC0)
	mov	reg[03h], 01h		; Port_0_IntCtrl_1 register (PRT0IC1)
	M8C_SetBank0
	mov	reg[01h], 01h		; Port_0_IntEn register (PRT0IE)
	mov	reg[04h], 00h		; Port_1_Data register (PRT1DR)
	M8C_SetBank1
	mov	reg[04h], 1fh		; Port_1_DriveMode_0 register (PRT1DM0)
	mov	reg[05h], e0h		; Port_1_DriveMode_1 register (PRT1DM1)
	M8C_SetBank0
	mov	reg[07h], e0h		; Port_1_DriveMode_2 register (PRT1DM2)
	mov	reg[06h], 01h		; Port_1_GlobalSelect register (PRT1GS)
	M8C_SetBank1
	mov	reg[06h], 00h		; Port_1_IntCtrl_0 register (PRT1IC0)
	mov	reg[07h], 00h		; Port_1_IntCtrl_1 register (PRT1IC1)
	M8C_SetBank0
	mov	reg[05h], 00h		; Port_1_IntEn register (PRT1IE)
	mov	reg[08h], 00h		; Port_2_Data register (PRT2DR)
	M8C_SetBank1
	mov	reg[08h], 7fh		; Port_2_DriveMode_0 register (PRT2DM0)
	mov	reg[09h], 80h		; Port_2_DriveMode_1 register (PRT2DM1)
	M8C_SetBank0
	mov	reg[0bh], 80h		; Port_2_DriveMode_2 register (PRT2DM2)
	mov	reg[0ah], 00h		; Port_2_GlobalSelect register (PRT2GS)
	M8C_SetBank1
	mov	reg[0ah], 00h		; Port_2_IntCtrl_0 register (PRT2IC0)
	mov	reg[0bh], 00h		; Port_2_IntCtrl_1 register (PRT2IC1)
	M8C_SetBank0
	mov	reg[09h], 00h		; Port_2_IntEn register (PRT2IE)
	mov	reg[0ch], 00h		; Port_3_Data register (PRT3DR)
	M8C_SetBank1
	mov	reg[0ch], 00h		; Port_3_DriveMode_0 register (PRT3DM0)
	mov	reg[0dh], 00h		; Port_3_DriveMode_1 register (PRT3DM1)
	M8C_SetBank0
	mov	reg[0fh], 00h		; Port_3_DriveMode_2 register (PRT3DM2)
	mov	reg[0eh], 00h		; Port_3_GlobalSelect register (PRT3GS)
	M8C_SetBank1
	mov	reg[0eh], 00h		; Port_3_IntCtrl_0 register (PRT3IC0)
	mov	reg[0fh], 00h		; Port_3_IntCtrl_1 register (PRT3IC1)
	M8C_SetBank0
	mov	reg[0dh], 00h		; Port_3_IntEn register (PRT3IE)
	mov	reg[10h], 00h		; Port_4_Data register (PRT4DR)
	M8C_SetBank1
	mov	reg[10h], 00h		; Port_4_DriveMode_0 register (PRT4DM0)
	mov	reg[11h], 00h		; Port_4_DriveMode_1 register (PRT4DM1)
	M8C_SetBank0
	mov	reg[13h], 00h		; Port_4_DriveMode_2 register (PRT4DM2)
	mov	reg[12h], 00h		; Port_4_GlobalSelect register (PRT4GS)
	M8C_SetBank1
	mov	reg[12h], 00h		; Port_4_IntCtrl_0 register (PRT4IC0)
	mov	reg[13h], 00h		; Port_4_IntCtrl_1 register (PRT4IC1)
	M8C_SetBank0
	mov	reg[11h], 00h		; Port_4_IntEn register (PRT4IE)
	mov	reg[14h], 00h		; Port_5_Data register (PRT5DR)
	M8C_SetBank1
	mov	reg[14h], 00h		; Port_5_DriveMode_0 register (PRT5DM0)
	mov	reg[15h], 00h		; Port_5_DriveMode_1 register (PRT5DM1)
	M8C_SetBank0
	mov	reg[17h], 00h		; Port_5_DriveMode_2 register (PRT5DM2)
	mov	reg[16h], 00h		; Port_5_GlobalSelect register (PRT5GS)
	M8C_SetBank1
	mov	reg[16h], 00h		; Port_5_IntCtrl_0 register (PRT5IC0)
	mov	reg[17h], 00h		; Port_5_IntCtrl_1 register (PRT5IC1)
	M8C_SetBank0
	mov	reg[15h], 00h		; Port_5_IntEn register (PRT5IE)
	M8C_SetBank0
	ret
AREA lit(rom, rel)
ReloadConfigTBL_detectordeflancos2_Bank0:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		23h, 00h		;PWM1_CONTROL_REG(DBB00CR0)
	db		21h, ffh		;PWM1_PERIOD_REG(DBB00DR1)
	db		22h, 80h		;PWM1_COMPARE_REG(DBB00DR2)
	db		ffh
ReloadConfigTBL_detectordeflancos2_Bank1:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		20h, 21h		;PWM1_FUNC_REG(DBB00FN)
	db		21h, 11h		;PWM1_INPUT_REG(DBB00IN)
	db		22h, 44h		;PWM1_OUTPUT_REG(DBB00OU)
	db		ffh
UnloadConfigTBL_detectordeflancos2_Bank0:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		23h, 00h		;PWM1_CONTROL_0 (DBB00CR0)
	db		ffh
UnloadConfigTBL_detectordeflancos2_Bank1:
;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		20h, 00h		;PWM1_DIG_BasicFunction (DBB00FN)
	db		21h, 00h		;PWM1_DIG_Input (DBB00IN)
	db		22h, 00h		;PWM1_DIG_Output (DBB00OU)
	db		ffh

;  Instance name LCD, User Module LCD
;  Instance name PWM1, User Module PWM8
;       Instance name PWM1, Block Name PWM8(DBB00)
	db		ffh
LoadConfigTBL_laOtraConfig_Bank0:
;  Instance name PWM2, User Module PWM8
;       Instance name PWM2, Block Name PWM8(DBB11)
	db		37h, 00h		;PWM2_CONTROL_REG(DBB11CR0)
	db		35h, 80h		;PWM2_PERIOD_REG(DBB11DR1)
	db		36h, 40h		;PWM2_COMPARE_REG(DBB11DR2)
	db		ffh
LoadConfigTBL_laOtraConfig_Bank1:
;  Instance name PWM2, User Module PWM8
;       Instance name PWM2, Block Name PWM8(DBB11)
	db		34h, 21h		;PWM2_FUNC_REG(DBB11FN)
	db		35h, 11h		;PWM2_INPUT_REG(DBB11IN)
	db		36h, 46h		;PWM2_OUTPUT_REG(DBB11OU)
	db		ffh
UnloadConfigTBL_laOtraConfig_Bank0:
;  Instance name PWM2, User Module PWM8
;       Instance name PWM2, Block Name PWM8(DBB11)
	db		37h, 00h		;PWM2_CONTROL_0 (DBB11CR0)
	db		ffh
UnloadConfigTBL_laOtraConfig_Bank1:
;  Instance name PWM2, User Module PWM8
;       Instance name PWM2, Block Name PWM8(DBB11)
	db		34h, 00h		;PWM2_DIG_BasicFunction (DBB11FN)
	db		35h, 00h		;PWM2_DIG_Input (DBB11IN)
	db		36h, 00h		;PWM2_DIG_Output (DBB11OU)
	db		ffh

;  Instance name PWM2, User Module PWM8
;       Instance name PWM2, Block Name PWM8(DBB11)
	db		ffh
UnloadConfigTBL_Total_Bank0:
;  Block DBB00
	db		23h, 00h		; CONTROL_0 register (DBB00CR0)
;  Block DBB01
	db		27h, 00h		; CONTROL_0 register (DBB01CR0)
;  Block DCB02
	db		2bh, 00h		; CONTROL_0 register (DCB02CR0)
;  Block DCB03
	db		2fh, 00h		; CONTROL_0 register (DCB03CR0)
;  Block DBB10
	db		33h, 00h		; CONTROL_0 register (DBB10CR0)
;  Block DBB11
	db		37h, 00h		; CONTROL_0 register (DBB11CR0)
;  Block DCB12
	db		3bh, 00h		; CONTROL_0 register (DCB12CR0)
;  Block DCB13
	db		3fh, 00h		; CONTROL_0 register (DCB13CR0)
;  Block ACB00
	db		71h, 05h		; CR0 register (ACB00CR0)
	db		72h, 00h		; CR1 register (ACB00CR1)
	db		73h, 00h		; CR2 register (ACB00CR2)
	db		70h, 00h		; CR3 register (ACB00CR3)
;  Block ASC10
	db		80h, 00h		; CR0 register (ASC10CR0)
	db		81h, 00h		; CR1 register (ASC10CR1)
	db		82h, 00h		; CR2 register (ASC10CR2)
	db		83h, 00h		; CR3 register (ASC10CR3)
;  Block ASD20
	db		90h, 00h		; CR0 register (ASD20CR0)
	db		91h, 00h		; CR1 register (ASD20CR1)
	db		92h, 00h		; CR2 register (ASD20CR2)
	db		93h, 00h		; CR3 register (ASD20CR3)
;  Block ACB01
	db		75h, 05h		; CR0 register (ACB01CR0)
	db		76h, 00h		; CR1 register (ACB01CR1)
	db		77h, 00h		; CR2 register (ACB01CR2)
	db		74h, 00h		; CR3 register (ACB01CR3)
;  Block ASD11
	db		84h, 00h		; CR0 register (ASD11CR0)
	db		85h, 00h		; CR1 register (ASD11CR1)
	db		86h, 00h		; CR2 register (ASD11CR2)
	db		87h, 00h		; CR3 register (ASD11CR3)
;  Block ASC21
	db		94h, 00h		; CR0 register (ASC21CR0)
	db		95h, 00h		; CR1 register (ASC21CR1)
	db		96h, 00h		; CR2 register (ASC21CR2)
	db		97h, 00h		; CR3 register (ASC21CR3)
;  Block ACB02
	db		79h, 05h		; CR0 register (ACB02CR0)
	db		7ah, 00h		; CR1 register (ACB02CR1)
	db		7bh, 00h		; CR2 register (ACB02CR2)
	db		78h, 00h		; CR3 register (ACB02CR3)
;  Block ASC12
	db		88h, 00h		; CR0 register (ASC12CR0)
	db		89h, 00h		; CR1 register (ASC12CR1)
	db		8ah, 00h		; CR2 register (ASC12CR2)
	db		8bh, 00h		; CR3 register (ASC12CR3)
;  Block ASD22
	db		98h, 00h		; CR0 register (ASD22CR0)
	db		99h, 00h		; CR1 register (ASD22CR1)
	db		9ah, 00h		; CR2 register (ASD22CR2)
	db		9bh, 00h		; CR3 register (ASD22CR3)
;  Block ACB03
	db		7dh, 05h		; CR0 register (ACB03CR0)
	db		7eh, 00h		; CR1 register (ACB03CR1)
	db		7fh, 00h		; CR2 register (ACB03CR2)
	db		7ch, 00h		; CR3 register (ACB03CR3)
;  Block ASD13
	db		8ch, 00h		; CR0 register (ASD13CR0)
	db		8dh, 00h		; CR1 register (ASD13CR1)
	db		8eh, 00h		; CR2 register (ASD13CR2)
	db		8fh, 00h		; CR3 register (ASD13CR3)
;  Block ASC23
	db		9ch, 00h		; CR0 register (ASC23CR0)
	db		9dh, 00h		; CR1 register (ASC23CR1)
	db		9eh, 00h		; CR2 register (ASC23CR2)
	db		9fh, 00h		; CR3 register (ASC23CR3)
	db		ffh
UnloadConfigTBL_Total_Bank1:
;  Block DBB00
	db		20h, 00h		; DIG_BasicFunction register (DBB00FN)
	db		21h, 00h		; DIG_Input register (DBB00IN)
	db		22h, 00h		; DIG_Output register (DBB00OU)
;  Block DBB01
	db		24h, 00h		; DIG_BasicFunction register (DBB01FN)
	db		25h, 00h		; DIG_Input register (DBB01IN)
	db		26h, 00h		; DIG_Output register (DBB01OU)
;  Block DCB02
	db		28h, 00h		; DIG_BasicFunction register (DCB02FN)
	db		29h, 00h		; DIG_Input register (DCB02IN)
	db		2ah, 00h		; DIG_Output register (DCB02OU)
;  Block DCB03
	db		2ch, 00h		; DIG_BasicFunction register (DCB03FN)
	db		2dh, 00h		; DIG_Input register (DCB03IN)
	db		2eh, 00h		; DIG_Output register (DCB03OU)
;  Block DBB10
	db		30h, 00h		; DIG_BasicFunction register (DBB10FN)
	db		31h, 00h		; DIG_Input register (DBB10IN)
	db		32h, 00h		; DIG_Output register (DBB10OU)
;  Block DBB11
	db		34h, 00h		; DIG_BasicFunction register (DBB11FN)
	db		35h, 00h		; DIG_Input register (DBB11IN)
	db		36h, 00h		; DIG_Output register (DBB11OU)
;  Block DCB12
	db		38h, 00h		; DIG_BasicFunction register (DCB12FN)
	db		39h, 00h		; DIG_Input register (DCB12IN)
	db		3ah, 00h		; DIG_Output register (DCB12OU)
;  Block DCB13
	db		3ch, 00h		; DIG_BasicFunction register (DCB13FN)
	db		3dh, 00h		; DIG_Input register (DCB13IN)
	db		3eh, 00h		; DIG_Output register (DCB13OU)
;  Block ACB00
;  Block ASC10
;  Block ASD20
;  Block ACB01
;  Block ASD11
;  Block ASC21
;  Block ACB02
;  Block ASC12
;  Block ASD22
;  Block ACB03
;  Block ASD13
;  Block ASC23
	db		ffh


; PSoC Configuration file trailer PsocConfig.asm
