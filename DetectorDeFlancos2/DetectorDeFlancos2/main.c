//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "psocdynamic.h"

//#pragma interrupt_handler interrupcion

void main(void)
{
//	const char carga;
//	int indice;
//	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	
	PWM1_Stop();
	PWM1_DisableInt();
	LoadConfig_laOtraConfig();
	LCD_Start();
	LCD_Position(0,0);
	LCD_PrCString("Interrupcion no tratada");

	LCD_Position(1,0);
		LCD_PrCString("la otra config");
	PWM1_Stop();
	PWM1_Start();
	LCD_Position(1,0);
	PWM2_Start();
	LCD_PrHexByte(IslaOtraConfigLoaded());
	PRT1DR=0x07;
//	M8C_EnableIntMask(INT_MSK0, INT_MSK0_GPIO);
//	while (1){
//		M8C_EnableIntMask(INT_MSK0, INT_MSK0_GPIO);
//		for (indice=0;indice<4000;indice++);
//		
//	}
//	while (1){
//		UnloadConfig_laOtraConfig();
//		for (indice=0;indice<=100000;indice++);
//		LoadConfig_laOtraConfig();
//		LCD_Position(1,0);
//		LCD_PrCString("la otra config");
//		PWM2_Start();
//		for (indice=0;indice<=100000;indice++);
//		PWM2_Stop();
//		LCD_Position(1,0);
//		LCD_PrCString("la config base");
//	}
	
}

//void interrupcion(void ){
////	M8C_DisableIntMask(INT_MSK0, INT_MSK0_GPIO);
//	LCD_Position(0,0);
//	LCD_PrCString("interrupcion tratada");
//	if (IslaOtraConfigLoaded!=0){
//		PWM1_Stop();
//		LoadConfig_laOtraConfig();
//		PWM2_Start();
//		LCD_Position(1,0);
//		LCD_PrCString("la otra config");
//	}
//	else {
//		PWM1_Start();
//		PWM2_Stop();
//		UnloadConfig_laOtraConfig();
//		LCD_Position(1,0);
//		LCD_PrCString("la config base");
//	}
//}