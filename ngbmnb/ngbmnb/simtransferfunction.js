//
//  SimTransferFunction.js
//

// variable declarations
var g_CycleTime;
var g_CyclingLED;
var g_PulseGenerator;
var g_Timer;

// variable ID's
var g_ID_CycleTime = 0;
var g_ID_CyclingLED = 1;
var g_ID_PulseGenerator = 2;
var g_ID_Timer = 3;

// function parameter ID's


// function parameter defines


// function output states
// CycleTime output state ID 
var	g_ID_CycleTime_UNASSIGNED = 255;

// CyclingLED output state ID 
var	g_ID_CyclingLED_Off = 0;
var	g_ID_CyclingLED_Low = 1;
var	g_ID_CyclingLED_Medium = 2;
var	g_ID_CyclingLED_High = 3;
var	g_ID_CyclingLED_UNASSIGNED = 255;

// PulseGenerator output state ID 
var	g_ID_PulseGenerator_Not_Triggered = 0;
var	g_ID_PulseGenerator_Triggered = 1;
var	g_ID_PulseGenerator_UNASSIGNED = 255;

// Timer output state ID 
var	g_ID_Timer_UNASSIGNED = 255;


// variable output state declarations
// CycleTime output state declarations
var	g_CycleTime__UNASSIGNED;

// CyclingLED output state declarations
var	g_CyclingLED__Off;
var	g_CyclingLED__Low;
var	g_CyclingLED__Medium;
var	g_CyclingLED__High;
var	g_CyclingLED__UNASSIGNED;

// PulseGenerator output state declarations
var	g_PulseGenerator__Not_Triggered;
var	g_PulseGenerator__Triggered;
var	g_PulseGenerator__UNASSIGNED;

// Timer output state declarations
var	g_Timer__UNASSIGNED;


// function parameter declarations


var g_FunctionParams = new Array();

// current value array
var g_CurrentValues = new Array();

function TransferFunction()
{
	// load values from globals
	var ioInfo;
	ioInfo = GetIOInfoFromInstanceName("CycleTime");
	if (ioInfo != null)
	{
		g_CycleTime = parseInt(ioInfo.sValue);
	}
	ioInfo = GetIOInfoFromInstanceName("CyclingLED");
	if (ioInfo != null)
	{
		g_CyclingLED = parseInt(ioInfo.sValue);
	}
	ioInfo = GetIOInfoFromInstanceName("PulseGenerator");
	if (ioInfo != null)
	{
		g_PulseGenerator = parseInt(ioInfo.sValue);
	}
	ioInfo = GetIOInfoFromInstanceName("Timer");
	if (ioInfo != null)
	{
		g_Timer = parseInt(ioInfo.sValue);
	}

	// update current value array
	g_CurrentValues[g_ID_CycleTime] = g_CycleTime;
	g_CurrentValues[g_ID_CyclingLED] = g_CyclingLED;
	g_CurrentValues[g_ID_PulseGenerator] = g_PulseGenerator;
	g_CurrentValues[g_ID_Timer] = g_Timer;

	// call transfer functions
	CycleTime_TransferFunction();
	Timer_TransferFunction();
	CyclingLED_TransferFunction();

	// set values in globals
	SetIOInfoValueFromInstanceName("CycleTime", g_CycleTime);
	SetIOInfoValueFromInstanceName("CyclingLED", g_CyclingLED);
	SetIOInfoValueFromInstanceName("PulseGenerator", g_PulseGenerator);
	SetIOInfoValueFromInstanceName("Timer", g_Timer);

}

function CycleTime_TransferFunction()
{
	


}

function CyclingLED_TransferFunction()
{
		if (1)
	{
		g_CyclingLED = (100*g_Timer)/g_CycleTime;
	}


	g_CurrentValues[g_ID_CyclingLED] = g_CyclingLED; 

}

function PulseGenerator_TransferFunction()
{
	


}

function Timer_TransferFunction()
{
		if ((g_PulseGenerator==g_PulseGenerator__Triggered) && (g_Timer<g_CycleTime))
	{
		g_Timer = g_Timer+1;
	}
	else if (g_PulseGenerator==g_PulseGenerator__Triggered)
	{
		g_Timer = 0;
	}


	g_CurrentValues[g_ID_Timer] = g_Timer; 

}


function InitTransferFunctionGlobals()
{


	// variable output state initializations
	// CycleTime output state initializations
	g_CycleTime__UNASSIGNED = 0;

	// CyclingLED output state initializations
	g_CyclingLED__Off = 0;
	g_CyclingLED__Low = 33;
	g_CyclingLED__Medium = 67;
	g_CyclingLED__High = 100;
	g_CyclingLED__UNASSIGNED = 0;

	// PulseGenerator output state initializations
	g_PulseGenerator__Not_Triggered = 0;
	g_PulseGenerator__Triggered = 1;
	g_PulseGenerator__UNASSIGNED = 0;

	// Timer output state initializations
	g_Timer__UNASSIGNED = 0;


	// variable initialization
	// initialize CycleTime globals
    ioInfo = GetIOInfoFromInstanceName("CycleTime");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_CycleTime = parseInt(ioInfo.sValue);
		}
		else
		{
			g_CycleTime = 40;
		}
    }
	else
	{
		g_CycleTime = 40;
	}
	// initialize CyclingLED globals
    ioInfo = GetIOInfoFromInstanceName("CyclingLED");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_CyclingLED = parseInt(ioInfo.sValue);
		}
		else
		{
			g_CyclingLED = 0;
		}
    }
	else
	{
		g_CyclingLED = 0;
	}
	// initialize PulseGenerator globals
    ioInfo = GetIOInfoFromInstanceName("PulseGenerator");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_PulseGenerator = parseInt(ioInfo.sValue);
		}
		else
		{
			g_PulseGenerator = 0;
		}
    }
	else
	{
		g_PulseGenerator = 0;
	}
	// initialize Timer globals
    ioInfo = GetIOInfoFromInstanceName("Timer");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_Timer = parseInt(ioInfo.sValue);
		}
		else
		{
			g_Timer = 0;
		}
    }
	else
	{
		g_Timer = 0;
	}

}

InitTransferFunctionGlobals();




function UpdateSimValues()
{
	// set values in globals
	SetIOInfoValueFromInstanceName("CycleTime", g_CycleTime);
	SetIOInfoValueFromInstanceName("CyclingLED", g_CyclingLED);
	SetIOInfoValueFromInstanceName("PulseGenerator", g_PulseGenerator);
	SetIOInfoValueFromInstanceName("Timer", g_Timer);
}

// update the input widgets
function UpdateInputWidgets()
{
//	alert("updating input widgets");
	for(var j  in g_IOInfos)
	{
        if (g_IOInfos[j].sType == "IN" || (g_IOInfos[j].sType == "VARIABLE" && g_IOInfos[j].sDriver == "Variable_IN"))
        {
			var sInstanceName = g_IOInfos[j].sInstanceName;
			var sID	= IDFromInstanceName(sInstanceName)
            var sValue	= g_IOInfos[j].sValue;
		    var sDriver	= g_IOInfos[j].sDriver;
//			alert("updating " + sInstanceName + " = " + sValue);
            sValue = RawToDisplayValue( sID, sValue);
		    WidgetSetValue(sDriver, sID, sValue);
		}
	}
}

function IOState( strID, strValue)
{
    this.strID = strID;
    this.strValue = strValue;
}
