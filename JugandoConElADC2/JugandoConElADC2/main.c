//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules

#pragma interrupt_handler interrupcionComparador
#pragma interrupt_handler interrupcionBuffer

void main(void)
{
	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	LCD_Start();
	PGA_Start(PGA_HIGHPOWER);
	comparadorSC_Start(comparadorSC_HIGHPOWER);
	ADC_Start(ADC_HIGHPOWER);
	bufferDigital_Start();
	ADC_GetSamples(0);
	LCD_Position(0,0);
	LCD_PrCString("hola");
//		M8C_DisableIntMask(INT_MSK0,INT_MSK0_ACOLUMN_0);
	M8C_DisableIntMask(INT_MSK1,INT_MSK1_DBB01);
	M8C_EnableIntMask(INT_MSK0,INT_MSK0_ACOLUMN_0);
//	M8C_EnableIntMask(INT_MSK1,INT_MSK1_DBB01);
//	bufferDigital_EnableInt();
	comparadorSC_EnableInt();
	while (1){
		if (ADC_fIsDataAvailable()!=0){
			unsigned char adcData;
			adcData=ADC_wGetData();
//			ADC_bGetData();
			ADC_fClearFlag();
			LCD_Position(0,7);
			LCD_PrHexInt(adcData);
//			LCD_PrCString("data");
		}
	}
}

void interrupcionComparador(void ){
	LCD_Position(1,0);
	LCD_PrCString("Int comparador");
	PRT1DR^=0x04;
}

void interrupcionBuffer(void ){
	LCD_Position(1,0);
	LCD_PrCString("Int buffer");
	PRT1DR^=0x08;
}