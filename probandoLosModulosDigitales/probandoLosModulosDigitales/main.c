//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "math.h"
#include "string.h"
#include "stdlib.h"

#pragma interrupt_handler cargarELPWM

void main(void)
{
	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	PWM_Start();
	PWM_WritePeriod(255);
	PWM_WritePulseWidth(128);
	LCD_Start();
	LCD_Position(0,0);
	LCD_PrCString("Periodo");
	LCD_Position(1,0);
	LCD_PrHexByte((255));
	
	M8C_EnableIntMask(INT_MSK1,INT_MSK1_DBB00);
	
}

void cargarELPWM(void){
	int nuevoValorDePeriodo;
	float auxiliarFloat;
	
	LCD_Position(0,9);
	LCD_PrCString("int");
	
	nuevoValorDePeriodo=rand();
	LCD_Position(1,5);
	LCD_PrHexInt(RAND_MAX);
	auxiliarFloat=rand()/RAND_MAX;
	LCD_Position(1,10);
	LCD_PrHexInt(auxiliarFloat);
	auxiliarFloat*=255;
//	nuevoValorDePeriodo=ceil(auxiliarFloat);
	nuevoValorDePeriodo=rand();
	
	
	PWM_WritePeriod(nuevoValorDePeriodo);
	PWM_WritePulseWidth(ceil(nuevoValorDePeriodo/2));
	LCD_Position(1,0);
	LCD_PrHexInt((nuevoValorDePeriodo));
	
}