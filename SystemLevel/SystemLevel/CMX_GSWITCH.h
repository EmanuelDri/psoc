//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_GSWITCH.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for Generic Switch driver 
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef GSWITCH_include
#define GSWITCH_include

#include <M8C.h>
#define GSWITCH_pse_Input1_ORDINAL  0
#define CMX_GSWITCH_COUNT 1

////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE ID;                // Instance ID ( All drivers )
    BYTE Instance;          // DIO channel instance
    BYTE InputActiveState;  // 1 active Hi,0 active lo
    BYTE DriveMode;         // Pin IO Drive Mode, PU, PD, etc
    BYTE ChannelID;         // Pin and port, pin [3:0], port [7:4]
}CMX_GSWITCH_ParameterBlock;

//-------------------------------------------------
// Prototypes of the GSWITCH API.
//-------------------------------------------------
extern void CMX_GSWITCH_Instantiate(const CMX_GSWITCH_ParameterBlock * pPBlock);
extern BYTE CMX_GSWITCH_GetValue(const CMX_GSWITCH_ParameterBlock * pPBlock);

#endif
