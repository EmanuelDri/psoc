//----------------------------------------------------------------------------
// DriverDecl.h
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "CMX_GSWITCH.h"

#include "CMX_LCDVALUE.h"
#include "CMX_GENERICPWM.h"

// Driver ID's
#define ID_pse_Input1 1
#define ID_pse_LCD1 2
#define ID_pse_Output3 3

// Channel ID's
#define ID_DIO_11 0
#define ID_LCD_00 0
#define ID_LCD_SHARED_00 0
#define ID_PWM_00 0
#define ID_SHADOWREGS_0 0
#define ID_SHADOWREGS_1 1
#define ID_SHADOWREGS_2 2

// Channel Type maximum count
#define CMX_DIGITAL_IO_CHAN_MAX 1
#define CMX_LCD_CHAN_MAX 1
#define CMX_LCD_SHARED_CHAN_MAX 1
#define CMX_PWM_CHAN_MAX 1
#define CMX_SHADOW_MAX 3

// Channel Type instantiation count
#define CMX_DIGITAL_IO_CHAN_COUNT 1
#define CMX_LCD_CHAN_COUNT 1
#define CMX_LCD_SHARED_CHAN_COUNT 0
#define CMX_PWM_CHAN_COUNT 1
#define CMX_SHADOW_COUNT 0

// Driver Parameter Block externs
extern CMX_GSWITCH_ParameterBlock const pse_Input1;
extern CMX_LCDVALUE_ParameterBlock const pse_LCD1;
extern CMX_GENERICPWM_ParameterBlock const pse_Output3;
