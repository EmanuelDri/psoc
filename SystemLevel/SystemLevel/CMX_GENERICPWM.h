//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_GENERICPWM.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51 
//  
//
//  DESCRIPTION:  Header file for the GENERICPWM Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_GENERICPWM_include
#define CMX_GENERICPWM_include

#include <M8C.h>


////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE GENERICPWM_ID; // Instance ID
    BYTE GENERICPWM_ChannelID;
    BYTE GENERICPWM_Polarity;
}CMX_GENERICPWM_ParameterBlock;
/////////////////////////////

// Function prototypes
void CMX_GENERICPWM_SetValue(const CMX_GENERICPWM_ParameterBlock * thisBLK , BYTE bPercent_DutyCycle);
void CMX_GENERICPWM_Instantiate(const CMX_GENERICPWM_ParameterBlock * thisBLK );


#endif
