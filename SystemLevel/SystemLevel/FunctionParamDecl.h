//
// FunctionParamDecl.h
//

#ifndef FUNCTION_PARAMDECL_H
#define FUNCTION_PARAMDECL_H

#include <m8c.h>
#include "CMXSystemFunction.h"

// function parameter block ID's


// system variable value defines
// pse_Input1 output state ID defines
#define ID_pse_Input1_Off 0
#define ID_pse_Input1_On 1
#define ID_pse_Input1_UNASSIGNED 255


// pse_Input1 output state value defines
#define ID_pse_Input1_Off_VALUE 0
#define ID_pse_Input1_On_VALUE 1
#define ID_pse_Input1_UNASSIGNED_VALUE 0

// pse_LCD1 output state ID defines
#define ID_pse_LCD1_UNASSIGNED 255
#define ID_pse_LCD1_UNASSIGNED 255


// pse_LCD1 output state value defines
#define ID_pse_LCD1_UNASSIGNED_VALUE 0
#define ID_pse_LCD1_UNASSIGNED_VALUE 0

// pse_Output3 output state ID defines
#define ID_pse_Output3_Off 0
#define ID_pse_Output3_Low 1
#define ID_pse_Output3_Medium 2
#define ID_pse_Output3_High 3
#define ID_pse_Output3_UNASSIGNED 255


// pse_Output3 output state value defines
#define ID_pse_Output3_Off_VALUE 0
#define ID_pse_Output3_Low_VALUE 33
#define ID_pse_Output3_Medium_VALUE 67
#define ID_pse_Output3_High_VALUE 100
#define ID_pse_Output3_UNASSIGNED_VALUE 0

// pse_LCD1 output state ID defines
#define ID_pse_LCD1_UNASSIGNED 255
#define ID_pse_LCD1_UNASSIGNED 255


// pse_LCD1 output state value defines
#define ID_pse_LCD1_UNASSIGNED_VALUE 0
#define ID_pse_LCD1_UNASSIGNED_VALUE 0


// system variable value defines for expressions
// pse_Input1 output state ID defines for expressions
#define pse_Input1__Off ID_pse_Input1_Off_VALUE
#define pse_Input1__On ID_pse_Input1_On_VALUE
#define pse_Input1__UNASSIGNED ID_pse_Input1_UNASSIGNED_VALUE

// pse_LCD1 output state ID defines for expressions
#define pse_LCD1__UNASSIGNED ID_pse_LCD1_UNASSIGNED_VALUE
#define pse_LCD1__UNASSIGNED ID_pse_LCD1_UNASSIGNED_VALUE

// pse_Output3 output state ID defines for expressions
#define pse_Output3__Off ID_pse_Output3_Off_VALUE
#define pse_Output3__Low ID_pse_Output3_Low_VALUE
#define pse_Output3__Medium ID_pse_Output3_Medium_VALUE
#define pse_Output3__High ID_pse_Output3_High_VALUE
#define pse_Output3__UNASSIGNED ID_pse_Output3_UNASSIGNED_VALUE

// pse_LCD1 output state ID defines for expressions
#define pse_LCD1__UNASSIGNED ID_pse_LCD1_UNASSIGNED_VALUE
#define pse_LCD1__UNASSIGNED ID_pse_LCD1_UNASSIGNED_VALUE


// function parameter block definitions


#endif
