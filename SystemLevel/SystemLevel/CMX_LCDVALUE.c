//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LCDVALUE.c
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This file contains the C code for LCDVALUE Driver.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "CMX_LCDVALUE.h"
#include "cmx.h"
#include "SystemTimer.h"
// Channel type header file
#include "CMX_LCD_CHAN.h"


//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDVALUE_Instantiate(const CMX_LCDVALUE_ParameterBlock * thisBLK)
//
//  DESCRIPTION: LCDVALUE Initialization
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to device parameter block
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//  	Initialization for  LCDVALUE 
////////////////////////////////////////////////////////////////////////////////

void CMX_LCDVALUE_Instantiate(const CMX_LCDVALUE_ParameterBlock * thisBLK)
{
	
}
//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDVALUE_SetValue(const CMX_LCDVALUE_ParameterBlock * thisBLK, int iValue)
//
//  DESCRIPTION: LCDVALUE Display  iValue
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK       => Pointer to parameter block
//    iValue          => integer displaying value
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE:  
//  This driver displays on LCD current integer value from
//   input driver in hexdecimal or decimal code.

////////////////////////////////////////////////////////////////////////////////
void CMX_LCDVALUE_SetValue(const CMX_LCDVALUE_ParameterBlock * thisBLK, int iValue)
{
	BYTE bRow,bColumn;
	BYTE bCurrentPosition;
	
	bRow=0;
	bColumn=0;
	bCurrentPosition=thisBLK->bPosition;
	//specify the current position
	if ((bCurrentPosition == 2)||(bCurrentPosition == 3))
	{
		bRow=1;
	}
	
	if ((bCurrentPosition == 4)||(bCurrentPosition == 5))
	{
		bRow=2;
	}
	
	if ((bCurrentPosition == 6)||(bCurrentPosition == 7))
	{
		bRow=3;
	}
	
	if (bCurrentPosition & 1)
	{
		bColumn=9;
	}
	
	LCD_Position(bRow,bColumn);  //set position (bRow , bColumn )
	//select the scale of notation
	if (thisBLK->bScaleNotation)
	    LCDPrDecInt(iValue); //display integer value in decimal
	else
		LCD_PrHexInt(iValue); //display integer value in hexdecimal
	
}
