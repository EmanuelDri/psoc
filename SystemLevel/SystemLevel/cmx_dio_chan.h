//*****************************************************************************
//*****************************************************************************
//  FILENAME:     cmx_dio_chan.h
//   Version:     1.0.0
//
//  DESCRIPTION: 
//    Header file for cmx_dio_chan.asm
//
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef DIO_CHAN_include
#define DIO_CHAN_include

#include <m8c.h>
#include "cmx_dio_lib.h"

//extern  void  DiscreteDriveMode(BYTE bpin, BYTE bport);
extern  const BYTE DIOChannelPins[];

#endif
