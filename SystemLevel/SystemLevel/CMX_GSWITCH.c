//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_GSWITCH.c
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This Driver calls the low level driver performs
//                individual pin input and drive.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "m8c.h"
#include "CMX_GSWITCH.h"
#include "cmx.h"
#include "systemtimer.h"
// channel type header file
#include "CMX_DIO_CHAN.h"
#include "SystemTimer.h"

// Button return values
#define PRESSED       1
#define NOT_PRESSED   0

// Drive Modes
#define MODE_RES_PULL_DOWN  0
#define MODE_HIZ            2
#define MODE_RES_PULL_HI    3

// Button States
#define BUTTON_OFF          0    // Button off, output off.  If button pressed advance state.
#define BUTTON_ON_WAIT      1    // Ouput High, waiting for min delay, stay in this state until delay expires
#define BUTTON_DELAY_MET    2    // Min delay met, next state depend on current state of button


BYTE PushButtonState[CMX_GSWITCH_COUNT];
BYTE PushButtonDelayCount[CMX_GSWITCH_COUNT];



//-----------------------------------------------------------------------------
//  FUNCTION NAME: GSWITCH_Instantiate(const CMX_GSWITCH_ParameterBlock * pPBlock)
//
//  DESCRIPTION: GSWITCH Initialization
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: Channel ID 
//
//  RETURNS:
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: Initialization for discrete I/O
//  Initial conditions and drive mode set based on parameters set in
//  GSWITCH Parameter block
////////////////////////////////////////////////////////////////////////////////
void CMX_GSWITCH_Instantiate(const CMX_GSWITCH_ParameterBlock * pPBlock)
{
   BYTE HI_State;
   BYTE port;
   BYTE pin;
   BYTE pinPort;
   BYTE DM;
   BYTE Instance;

   DM = pPBlock->DriveMode;	
   
   HI_State=0x01; 									//for Hi-Z input and Resistive pullUp DM 
   
   pinPort = DIOChannelPins[(pPBlock->ChannelID)]; 	// Get pin and port info
   pin  = pinlu[(BYTE)(pinPort & 0x0F)];           	// Create pin mask
   port = portlu[(BYTE)((pinPort & 0xF0) >>4)];    	// Extract port address

   DiscreteDriveMode(pin,port,DM);	

   if (DM == MODE_RES_PULL_DOWN)                   	
   {
      HI_State=0x00;								// for Resistive pullDown DM
   }

   DISetPin(pinPort, HI_State);
   Instance = pPBlock->Instance;
   PushButtonState[Instance] = BUTTON_OFF;
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: GSWITCH_GetValue(const CMX_GSWITCH_ParameterBlock * pPBlock)
//
//  DESCRIPTION:
//    Get current value of pushbutton.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: Channel ID
//
//  RETURNS: BYTE
//
//  THEORY of OPERATION or PROCEDURE:
//    The pushbutton driver is more than just a simple get state of a button.
//    It guarentes that a minimum button on time will be met.  The button
//    state will go active as soon as the button state is detected on.  If
//    the button goes low, the driver output will stay high for at least the
//    minimum delay time.  If the button goes low and the minimum high time
//    has already been met, the output will go low immediatly.
//
//  Button States:
//       BUTTON_OFF       0    // Button not on and output was not on.
//       BUTTON_TRAN_HI   1    // Button just pressed, but has not met min time
//       BUTTON_DELAY_MET 2    // Button on and has min on time, may be turned off at any time.
//
///////////////////////////////////////////////////////////////////////////////
BYTE CMX_GSWITCH_GetValue(const CMX_GSWITCH_ParameterBlock * pPBlock)
{

   BYTE port;
   BYTE pin;
   BYTE pinPort;
   BYTE bInstance;
   BYTE returnValue;

   pinPort = DIOChannelPins[(pPBlock->ChannelID)];   // Get pin and port info
   pin     = pinlu[(BYTE)(pinPort & 0x0F)];          // Create pin mask
   port    = portlu[(BYTE)((pinPort & 0xF0) >>4)];   // Extract port address

   bInstance  = pPBlock->Instance;

   returnValue = NOT_PRESSED;                                          // By default button not pressed
   
   if(!(DiscreteReadPin(pin,port) ^ pPBlock->InputActiveState)) {      // Check if Button is pressed
                                                                       // Button is pressed
      if (PushButtonState[bInstance] == BUTTON_OFF) {
         PushButtonDelayCount[bInstance] = SystemTimer_bGetTickCntr();  // Start debouce timer
         PushButtonState[bInstance] = BUTTON_ON_WAIT;

      } else if (PushButtonState[bInstance] == BUTTON_ON_WAIT) {       // Waiting for debouce timeout?

         if ((BYTE)(SystemTimer_bGetTickCntr() - PushButtonDelayCount[bInstance]) > 1 ) { // Check if debounce?
            PushButtonState[bInstance] = BUTTON_DELAY_MET;
            returnValue = PRESSED;
         }

      } else {                                                         // Button still pressed
          returnValue = PRESSED;
      }

   } else {                                                            // Button released
      PushButtonState[bInstance] = BUTTON_OFF;                         // Reset state
   }

   return ( returnValue);
}
