//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>          // part specific constants and macros
#include "PSoCAPI.h"      // PSoC API definitions for all User Modules

#include "driverdecl.h"
#include "CMXSystem.h"
#include "CMXSystemExtern.h"
#include "TransferFunction.h"

#include "cmx.h"
#include "ProjectProperties.h"
#include "Custom.h"

// Channel includes
// LCD_SHARED_00 Include
#include "cmx_lcd_chan.h"

void main( void )
{
    // Initialize Project
    M8C_EnableGInt;                            // Turn on interrupts 
    SystemTimer_Start();
    SystemTimer_SetInterval(SystemTimer_64_HZ);
    SystemTimer_EnableInt();

    // Initialize Channels
    // LCD_SHARED_00 Initialization
    LCDStartSharedChan();
    I2C_CFG &= 0xFC;                           // Disable I2C in case it's not used.
    
    // Initialize Variables
	SystemVars.ReadOnlyVars.pse_Input1 = 0;
	SystemVars.ReadOnlyVars.pse_LCD1 = -32768;
	SystemVars.ReadOnlyVars.pse_Output3 = 100;

    // Driver instantiations
CMX_GSWITCH_Instantiate(&pse_Input1);
CMX_LCDVALUE_Instantiate(&pse_LCD1);
CMX_GENERICPWM_Instantiate(&pse_Output3);

    // Custom initization code.
    CustomInit();
    // End Initialize Project

	while(1)
	{
        // Sync loop sample rate
#if ( SAMPLE_DIVIDER  )
        SystemTimer_SyncWait(SAMPLE_DIVIDER, SystemTimer_WAIT_RELOAD);
#endif
		// update input variables
		SystemVars.ReadOnlyVars.pse_Input1 = CMX_GSWITCH_GetValue(&pse_Input1);

        // Custom Post Input function
        CustomPostInputUpdate();

		// run transfer function and update output variables
		TransferFunction();

        // CustomPreOutputUpdate();
        CustomPreOutputUpdate();

		// set outputs
		CMX_LCDVALUE_SetValue(&pse_LCD1, (int)SystemVars.ReadOnlyVars.pse_LCD1);
		CMX_GENERICPWM_SetValue(&pse_Output3, (BYTE)SystemVars.ReadOnlyVars.pse_Output3);

	}
}
