//
//  SystemConst.h
//
//     Defines system constant structure

#ifndef SYSTEMCONST_H
#define SYSTEMCONST_H

#include "m8c.h"
#include "CMXSystemFunction.h"


#define RESERVED_BLOCK_SIZE 2 // This is the number of bytes of flash reserved

typedef struct
{


	struct
	{
		char title[RESERVED_BLOCK_SIZE];
	} ReserveBlock;
} SYSTEM_CONSTANT_STRUC;

// overall count oF integer values contained in the constant structure
#define SYSTEM_CONST_COUNT 2

#endif
