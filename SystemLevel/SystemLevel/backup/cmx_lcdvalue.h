//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LCDVALUE.h
//   Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the LCDVALUE Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_LCDVALUE_include
#define CMX_LCDVALUE_include

#include <M8C.h>
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
	BYTE bPosition;            //displaying position
	BYTE bScaleNotation;       //scale of notation
}CMX_LCDVALUE_ParameterBlock;

// Function Prototypes
void CMX_LCDVALUE_SetValue(const CMX_LCDVALUE_ParameterBlock * thisBLK, int iValue);
void CMX_LCDVALUE_Instantiate(const CMX_LCDVALUE_ParameterBlock * thisBLK);


#endif
