//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

const CMX_LCDVALUE_ParameterBlock pse_LCD1 = {0,1};

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_LCD1, // ID = 1
		// ID_MAX = 2
};
