;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: cmx_dio_chan.asm
;;   Version: 1.0.0
;;
;;  DESCRIPTION:
;;    Digital Input Output algorithms.
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;  HISTORY:
;;
;;*****************************************************************************
include "m8c.inc"
include "memory.inc"

;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------
export  _DIOChannelPins
export   DIOChannelPins

;-----------------------------------------------
;  Function Prototypes
;-----------------------------------------------

;-----------------------------------------------
;  Constant Definitions
;-----------------------------------------------
area text(ROM,REL)
.literal

_DIOChannelPins:
 DIOChannelPins:
    db 11h
.endliteral

;-----------------------------------------------
; Variable Allocation
;-----------------------------------------------
AREA InterruptRAM (RAM, REL, CON)

 
area    text(ROM,REL)
; End of File DIO_LLD.asm
