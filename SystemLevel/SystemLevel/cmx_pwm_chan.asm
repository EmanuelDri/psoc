;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: cmx_pwm_chan.asm
;;   @Version@
;;
;;  DESCRIPTION: Implements pwm channel interface
;; 
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;  HISTORY:
;;
;;*****************************************************************************
include "m8c.inc"
include "memory.inc"
include "psocapi.inc"
include "DriverDecl.inc"

;-----------------------------------------------
;  Interface Functions
;-----------------------------------------------
 
export  _PWMStart
export   PWMStart

export  _PWMSetPulseWidth
export   PWMSetPulseWidth

export  _PWMGetPeriod
export   PWMGetPeriod

export  _PWMSetPolarity
export   PWMSetPolarity

export  _PWMSuspend
export   PWMSuspend

export  _FANStates
export   FANStates

export  _FANFlags
export   FANFlags

export  _FANSpinStartTimes
export   FANSpinStartTimes

export  _PWMChannelPins
export   PWMChannelPins 

export  _PWMChannelBlocks
export   PWMChannelBlocks

;-----------------------------------------------
;  Constant Definitions
;-----------------------------------------------

area text(ROM,REL)
.literal
_PWMChannelPins:
 PWMChannelPins:
    db 10h

_PWMChannelBlocks:
 PWMChannelBlocks:
	db 00h
.endliteral
;-----------------------------------------------
 
;-----------------------------------------------
; Variable Allocation
;-----------------------------------------------
area    bss(RAM)
_FANStates:
 FANStates: BLK (CMX_PWM_CHAN_COUNT)

_FANFlags:
 FANFlags: BLK (CMX_PWM_CHAN_COUNT)

_FANSpinStartTimes:
 FANSpinStartTimes: BLK (CMX_PWM_CHAN_COUNT)
 
area    text(ROM,REL)
;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMStart
;
;  DESCRIPTION: 
;    This function starts the selected PWM.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: Channel ID of selected PWM in A
;
;  RETURNS: Nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
	
 PWMStart:
_PWMStart:

    asl   A
    asl   A
    jacc  PWMStrt_JTable ; @STEP_SIZE=4 @NUM_STEPS=1

PWMStrt_JTable:
	; Start PWM_00
	nop
    ljmp  PWM_00_Start ; call UM API

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSetPulseWidth
;
;  DESCRIPTION: 
;    This function writes the compare value of the selected pwm.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: Channel ID of selected pwm in A and pulse width in X
;
;  RETURNS: Nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
 PWMSetPulseWidth:
_PWMSetPulseWidth:
 
    asl   A
    asl   A
    jacc  SetPW_JTable ; @STEP_SIZE=4 @NUM_STEPS=1

SetPW_JTable:
	; Set PulseWidth for PWM_00
    mov   A,X ; move pulsewidth byte to A
    ljmp  PWM_00_WritePulseWidth ; call UM API

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMGetPeriod
;
;  DESCRIPTION: 
;    This function returns the period value of the selected pwm.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => Channel ID
;
;  RETURNS: 
;    Period value in A
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;
 PWMGetPeriod:
_PWMGetPeriod:
 
    asl   A
    asl   A
    jacc  GetPer_JTable ; @STEP_SIZE=4 @NUM_STEPS=1

GetPer_JTable:
	; Get Period for PWM_00
    mov   A,249 ; 2 bytes, move period byte to A								
    ret ; 1 byte
    nop ; 1 byte, 4 total for section
	 
;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSetPolarity
;
;  DESCRIPTION: 
;    Programs positive polarity or negative polarity through reg
;    RDIxLTx.
; 
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => Channel ID 
;    X => polarity
;
;  RETURNS: nothing
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;

 PWMSetPolarity:
_PWMSetPolarity:
	push A
	mov  A,X ; zero if neg polarity, nonzero if positive           
    jnz positive
negative:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMPol_Neg_JTable ; @STEP_SIZE=8 @NUM_STEPS=1
PWMPol_Neg_JTable:
	; Set LUT polarity for PWM_00  
	and  reg[B3h], ~0Fh ; 3 byte instruction
	or   reg[B3h], (CCh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

positive:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMPol_Pos_JTable ; @STEP_SIZE=8 @NUM_STEPS=1
PWMPol_Pos_JTable:  
	; Set LUT polarity for PWM_00  
	and  reg[B3h], ~0Fh ; 3 byte instruction
	or   reg[B3h], (33h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

;-----------------------------------------------------------------------------
;  FUNCTION NAME: PWMSuspend
;
;  DESCRIPTION: 
;    Forces high or low state at the row output lut through reg
;    RDIxLTx.
; 
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;    A => channel ID 
;    X => polarity of channel, 1 for high or 0 for low
;
;  RETURNS: none
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE:
;

 PWMSuspend:
_PWMSuspend:
	push A
	mov  A,X ; zero if low, nonzero if high           
    	jnz pwm_high
pwm_low:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMSuspend_Neg_JTable ; @STEP_SIZE=8 @NUM_STEPS=1
PWMSuspend_Neg_JTable:
	; Set LUT to 0 for PWM_00  
	and  reg[B3h], ~0Fh ; 3 byte instruction
	or   reg[B3h], (00h & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction

pwm_high:
	pop  A
	asl  A ; x2
	asl  A ; x4
	asl  A ; x8
	jacc PWMSuspend_Pos_JTable ; @STEP_SIZE=8 @NUM_STEPS=1
PWMSuspend_Pos_JTable:  
	; Set LUT to 1 for PWM_00  
	and  reg[B3h], ~0Fh ; 3 byte instruction
	or   reg[B3h], (FFh & 0Fh) ; 3 byte instruction
	nop	; 1 byte instruction
	ret	; 1 byte instruction


 



