//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

const CMX_GSWITCH_ParameterBlock pse_Input1 = { ID_pse_Input1, GSWITCH_pse_Input1_ORDINAL, 0x00, 0x03, ID_DIO_11};
const CMX_LCDVALUE_ParameterBlock pse_LCD1 = {0,1};
const CMX_GENERICPWM_ParameterBlock pse_Output3 = { ID_pse_Output3, ID_PWM_00, 1 };

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_Input1, // ID = 1
	(void *)&pse_LCD1, // ID = 2
	(void *)&pse_Output3, // ID = 3
		// ID_MAX = 4
};
