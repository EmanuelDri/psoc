//*****************************************************************************
//*****************************************************************************
//  FILENAME:  PWM_LLD.h
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the pwm lld.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"

#pragma fastcall16 PWMStart
#pragma fastcall16 PWMSetFrequency
#pragma fastcall16 PWMSetPolarity
#pragma fastcall16 PWMSetPulseWidth
#pragma fastcall16 PWMGetPeriod
#pragma fastcall16 PWMSuspend

// asm functions
extern void PWMStart(BYTE bID);
extern void PWMSetFrequency(BYTE bID);
extern void PWMSetPolarity(BYTE bID, BYTE bPolarity); // also used to Resume after Suspend  
extern void PWMSetPulseWidth(BYTE bID, BYTE bPulseWidth);
extern BYTE PWMGetPeriod(BYTE bID);
extern void PWMSuspend(BYTE bID, BYTE bPinState);
// c funcitons
extern void PWMSetDutyCycle(BYTE bID, BYTE bDutyCycle);

extern BYTE FANFlags[];
extern BYTE FANStates[];
extern BYTE FANSpinStartTimes[];

