//
//  CMXSystemExterns.h
//
#include "cmx.h"
#include "SystemVars.h"
#include "SystemConst.h"

extern SYSTEM_CONSTANT_STRUC const SystemConst;
extern void * const FunctionParams[];
extern SYSTEM_VARS_STRUC SystemVars;

// variable transfer function call externs

