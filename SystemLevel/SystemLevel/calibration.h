//*****************************************************************************
//*****************************************************************************
//  FILENAME:  calibration.h              
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for calibration.c.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

extern const int CountsPerVolt;
extern const int ADC_Offset;
extern const int imVolts_Chan_Offset[8];
