//
//  SimTransferFunction.js
//

// variable declarations
var g_Input1;
var g_LCD1;
var g_Output3;

// variable ID's
var g_ID_Input1 = 0;
var g_ID_LCD1 = 1;
var g_ID_Output3 = 2;

// function parameter ID's


// function parameter defines


// function output states
// Input1 output state ID 
var	g_ID_Input1_Off = 0;
var	g_ID_Input1_On = 1;
var	g_ID_Input1_UNASSIGNED = 255;

// LCD1 output state ID 
var	g_ID_LCD1_UNASSIGNED = 255;
var	g_ID_LCD1_UNASSIGNED = 255;

// Output3 output state ID 
var	g_ID_Output3_Off = 0;
var	g_ID_Output3_Low = 1;
var	g_ID_Output3_Medium = 2;
var	g_ID_Output3_High = 3;
var	g_ID_Output3_UNASSIGNED = 255;

// LCD1 output state ID 
var	g_ID_LCD1_UNASSIGNED = 255;
var	g_ID_LCD1_UNASSIGNED = 255;


// variable output state declarations
// Input1 output state declarations
var	g_Input1__Off;
var	g_Input1__On;
var	g_Input1__UNASSIGNED;

// LCD1 output state declarations
var	g_LCD1__UNASSIGNED;
var	g_LCD1__UNASSIGNED;

// Output3 output state declarations
var	g_Output3__Off;
var	g_Output3__Low;
var	g_Output3__Medium;
var	g_Output3__High;
var	g_Output3__UNASSIGNED;

// LCD1 output state declarations
var	g_LCD1__UNASSIGNED;
var	g_LCD1__UNASSIGNED;


// function parameter declarations


var g_FunctionParams = new Array();

// current value array
var g_CurrentValues = new Array();

function TransferFunction()
{
	// load values from globals
	var ioInfo;
	ioInfo = GetIOInfoFromInstanceName("Input1");
	if (ioInfo != null)
	{
		g_Input1 = parseInt(ioInfo.sValue);
	}
	ioInfo = GetIOInfoFromInstanceName("LCD1");
	if (ioInfo != null)
	{
		g_LCD1 = parseInt(ioInfo.sValue);
	}
	ioInfo = GetIOInfoFromInstanceName("Output3");
	if (ioInfo != null)
	{
		g_Output3 = parseInt(ioInfo.sValue);
	}

	// update current value array
	g_CurrentValues[g_ID_Input1] = g_Input1;
	g_CurrentValues[g_ID_LCD1] = g_LCD1;
	g_CurrentValues[g_ID_Output3] = g_Output3;

	// call transfer functions
	Output3_TransferFunction();
	LCD1_TransferFunction();

	// set values in globals
	SetIOInfoValueFromInstanceName("Input1", g_Input1);
	SetIOInfoValueFromInstanceName("LCD1", g_LCD1);
	SetIOInfoValueFromInstanceName("Output3", g_Output3);

}

function Input1_TransferFunction()
{
	


}

function LCD1_TransferFunction()
{
	


}

function Output3_TransferFunction()
{
	


}


function InitTransferFunctionGlobals()
{


	// variable output state initializations
	// Input1 output state initializations
	g_Input1__Off = 0;
	g_Input1__On = 1;
	g_Input1__UNASSIGNED = 0;

	// LCD1 output state initializations
	g_LCD1__UNASSIGNED = 0;
	g_LCD1__UNASSIGNED = 0;

	// Output3 output state initializations
	g_Output3__Off = 0;
	g_Output3__Low = 33;
	g_Output3__Medium = 67;
	g_Output3__High = 100;
	g_Output3__UNASSIGNED = 0;

	// LCD1 output state initializations
	g_LCD1__UNASSIGNED = 0;
	g_LCD1__UNASSIGNED = 0;


	// variable initialization
	// initialize Input1 globals
    ioInfo = GetIOInfoFromInstanceName("Input1");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_Input1 = parseInt(ioInfo.sValue);
		}
		else
		{
			g_Input1 = 0;
		}
    }
	else
	{
		g_Input1 = 0;
	}
	// initialize LCD1 globals
    ioInfo = GetIOInfoFromInstanceName("LCD1");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_LCD1 = parseInt(ioInfo.sValue);
		}
		else
		{
			g_LCD1 = -32768;
		}
    }
	else
	{
		g_LCD1 = -32768;
	}
	// initialize Output3 globals
    ioInfo = GetIOInfoFromInstanceName("Output3");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_Output3 = parseInt(ioInfo.sValue);
		}
		else
		{
			g_Output3 = 100;
		}
    }
	else
	{
		g_Output3 = 100;
	}

}

InitTransferFunctionGlobals();




function UpdateSimValues()
{
	// set values in globals
	SetIOInfoValueFromInstanceName("Input1", g_Input1);
	SetIOInfoValueFromInstanceName("LCD1", g_LCD1);
	SetIOInfoValueFromInstanceName("Output3", g_Output3);
}

// update the input widgets
function UpdateInputWidgets()
{
//	alert("updating input widgets");
	for(var j  in g_IOInfos)
	{
        if (g_IOInfos[j].sType == "IN" || (g_IOInfos[j].sType == "VARIABLE" && g_IOInfos[j].sDriver == "Variable_IN"))
        {
			var sInstanceName = g_IOInfos[j].sInstanceName;
			var sID	= IDFromInstanceName(sInstanceName)
            var sValue	= g_IOInfos[j].sValue;
		    var sDriver	= g_IOInfos[j].sDriver;
//			alert("updating " + sInstanceName + " = " + sValue);
            sValue = RawToDisplayValue( sID, sValue);
		    WidgetSetValue(sDriver, sID, sValue);
		}
	}
}

function IOState( strID, strValue)
{
    this.strID = strID;
    this.strValue = strValue;
}
