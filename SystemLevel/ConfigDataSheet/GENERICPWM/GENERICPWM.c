//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_GENERICPWM.c
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51 
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  This Driver controls the Generic PWM parameters and returns
//                the percent duty cycle for the selected channel.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "`@LIB_NAME`_GENERICPWM.h"
#include "cmx.h"

// Channel type header file
#include "cmx_pwm_chan.h"

 
//-----------------------------------------------------------------------------
//  FUNCTION NAME: GENERICPWM_Instantiate(const `@LIB_NAME`_GERNERICPWM_ParameterBlock * thisBLK)
//
//  DESCRIPTION: 
//     GENERICPWM Initialization
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//     thisBLK => Pointer to parameter Block for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: Initialization for GENERICPWM
//  Initial conditions set based on parameters set in
//  GENERICPWM Parameter block
//-----------------------------------------------------------------------------
void `@LIB_NAME`_GENERICPWM_Instantiate(const `@LIB_NAME`_GENERICPWM_ParameterBlock * thisBLK)
{
    BYTE Instance;
    BYTE Polarity;

    Instance = thisBLK->GENERICPWM_ChannelID;
    Polarity = thisBLK->GENERICPWM_Polarity;

    PWMSetPolarity(Instance,Polarity);
    PWMSetDutyCycle(Instance, 0);
    PWMStart(Instance);
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: GENERICPWM_SetValue(const `@LIB_NAME`_GENERICPWM_ParameterBlock * thisBLK, BYTE bPercent_DutyCycle)
//
//  DESCRIPTION: 
//     Sets Percent Duty Cycle Value for given instance.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS:  
//     thisBLK            => Pointer to parameter Block for this instance.
//     bPercent_DutyCycle => Percent duty cycle in which to set PWM.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: Initialization for GENERICPWM
//  Parameters set in GENERICPWM Parameter block
//-----------------------------------------------------------------------------

void `@LIB_NAME`_GENERICPWM_SetValue(const `@LIB_NAME`_GENERICPWM_ParameterBlock * thisBLK, BYTE bDutyCycle)
{
    BYTE Instance;
		        
    Instance = thisBLK->GENERICPWM_ChannelID;
    PWMSetDutyCycle(Instance, bDutyCycle);
}
