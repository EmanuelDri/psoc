// GenericPWM GPM Script

function GenericPWMSetDriverProperties( sDriverInstanceName)
{
	var FrequencyRequest = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_DRIVER_INSTANCE=\""+sDriverInstanceName+"\"]//CMX_REQUEST_METADATA_LIST/CMX_REQUEST_METADATA");
	var FrequencyDriverProperty = g_ReqDoc.selectSingleNode("//CMX_IO_DRIVER[@DRIVER_INSTANCE=\""+sDriverInstanceName+"\"]/CMX_PROPERTY_LIST/CMX_PROPERTY[@NAME=\"DriveFrequency\"]");
	FrequencyDriverProperty.setAttribute("CURRENT_VALUE", FrequencyRequest.getAttribute("ACTUAL_FREQUENCY"));
	FrequencyRequest.setAttribute("DRIVER_PROPERTY_VALUE_CALCULATED", FrequencyRequest.getAttribute("ACTUAL_FREQUENCY"));
}

function GenericPWMSetChannelProperties( sDriverInstanceName)
{
	// Get Channel Instance Name
	var SelfGPMRequest = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_DRIVER_INSTANCE=\""+sDriverInstanceName+"\"]");
	var ChannelInstanceName = SelfGPMRequest.getAttribute("SOURCE_CHANNEL");

	var RequestFrequency;
	var FrequencyRequest = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+ChannelInstanceName+"\"]//CMX_REQUEST_METADATA_LIST//CMX_REQUEST_METADATA");
	if (FrequencyRequest != null)
	{
		// Get Request Frequency from GUI
		RequestFrequency = FrequencyRequest.getAttribute("DRIVER_PROPERTY_VALUE_REQUESTED");
	}
	else
	{
		// Get Request Frequency from driver
		var FrequencyProperty = g_ReqDoc.selectSingleNode("//CMX_IO_DRIVER[@DRIVER_INSTANCE=\""+sDriverInstanceName+"\"]/CMX_PROPERTY_LIST/CMX_PROPERTY[@NAME=\"DriveFrequency\"]");
		var RequestFrequency = FrequencyProperty.getAttribute("CURRENT_VALUE");
		var RequestFrequencyMin = FrequencyProperty.getAttribute("MIN");
		var RequestFrequencyMax = FrequencyProperty.getAttribute("MAX");
		
		// Create Request Metadata
		var RequestMetadataList = g_ReqDoc.createElement("CMX_REQUEST_METADATA_LIST");
		FrequencyRequest = g_ReqDoc.createElement("CMX_REQUEST_METADATA");
		RequestMetadataList.appendChild(FrequencyRequest);
		var SelfGPMRequest = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+ChannelInstanceName+"\"]");
		SelfGPMRequest.appendChild(RequestMetadataList);

		FrequencyRequest.setAttribute("DRIVER_PROPERTY_MIN", RequestFrequencyMin);
		FrequencyRequest.setAttribute("DRIVER_PROPERTY_MAX", RequestFrequencyMax);
	}
	
	FrequencyRequest.setAttribute("DRIVER_PROPERTY_NAME", "DriveFreqeuncy");
	FrequencyRequest.setAttribute("DRIVER_PROPERTY_UNITS", "Hz");
	FrequencyRequest.setAttribute("DRIVER_PROPERTY_VALUE_REQUESTED", RequestFrequency);
	FrequencyRequest.setAttribute("DESIRED_FREQUENCY", RequestFrequency);
	FrequencyRequest.setAttribute("CHANNEL_INSTANCE", ChannelInstanceName);
	FrequencyRequest.setAttribute("DRIVER_INSTANCE", sDriverInstanceName);	  
}

