//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_GENERICPWM.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51 
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the GENERICPWM Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_GENERICPWM_include
#define `@LIB_NAME`_GENERICPWM_include

#include <M8C.h>


////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE GENERICPWM_ID; // Instance ID
    BYTE GENERICPWM_ChannelID;
    BYTE GENERICPWM_Polarity;
}`@LIB_NAME`_GENERICPWM_ParameterBlock;
/////////////////////////////

// Function prototypes
void `@LIB_NAME`_GENERICPWM_SetValue(const `@LIB_NAME`_GENERICPWM_ParameterBlock * thisBLK , BYTE bPercent_DutyCycle);
void `@LIB_NAME`_GENERICPWM_Instantiate(const `@LIB_NAME`_GENERICPWM_ParameterBlock * thisBLK );


#endif
