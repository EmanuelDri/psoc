//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LCDVALUE.h
//   Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the LCDVALUE Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_LCDVALUE_include
#define `@LIB_NAME`_LCDVALUE_include

#include <M8C.h>
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
	BYTE bPosition;            //displaying position
	BYTE bScaleNotation;       //scale of notation
}`@LIB_NAME`_LCDVALUE_ParameterBlock;

// Function Prototypes
void `@LIB_NAME`_LCDVALUE_SetValue(const `@LIB_NAME`_LCDVALUE_ParameterBlock * thisBLK, int iValue);
void `@LIB_NAME`_LCDVALUE_Instantiate(const `@LIB_NAME`_LCDVALUE_ParameterBlock * thisBLK);


#endif
