<!--
******************************************************************************
******************************************************************************
**  FILENAME: LCDVALUE.cmx
**   Version: 1.2, Updated on 2009/10/29 at 15:6:51
**
**  DESCRIPTION: 
**    XML description for the LCDVALUE driver.
** - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
**  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
******************************************************************************
******************************************************************************
-->
<CMX_DB>
<CMX_IO_DRIVER 
            NAME             		= "LCDVALUE"
            DISPLAY_NAME     		= "LCD Value"
            VERSION                 = "1.2"
            IO_CHANNEL_TYPE  		= "CMX_LCD_CHAN"
            IMAGE            		= "LCDVALUE.gif"
            WIDGET           		= ""
            DOC_FILE         		= ""
            HTML             		= "LCDVALUE_datasheet.htm"
            DRIVER_CATEGORY  		= "Display\LCD"
            DRIVER_TYPE      		= "CMX_OUTPUT"
			VALUE_TYPE	  	       	= "DISCRETE"
			VARIABLE_TYPE 			= "int"
			VARIABLE_SIZE			= "2"
			WIDGMIN		  			= "-32768"
			WIDGMAX		  			= "32767"
			WIDGSCALE		  		= "1"
			WIDGUNITS		  		= ""

>

	<CMX_PROPERTY_LIST>
	
		<CMX_PROPERTY NAME="Position" TYPE="ENUM"  DEFAULT_VALUE="Row 0,Left Side" COMMENT="Select the display position.">
			<CMX_PROPERTY_VALUE_LIST>
				<CMX_PROPERTY_VALUE NAME="Row 0,Left Side"  VALUE="0" ORDER="0"/>
				<CMX_PROPERTY_VALUE NAME="Row 0,Right Side" VALUE="1" ORDER="1"/>
				<CMX_PROPERTY_VALUE NAME="Row 1,Left Side"  VALUE="2" ORDER="2"/>
				<CMX_PROPERTY_VALUE NAME="Row 1,Right Side" VALUE="3" ORDER="3"/>
				<CMX_PROPERTY_VALUE NAME="Row 2,Left Side" VALUE="4" ORDER="4"/>
				<CMX_PROPERTY_VALUE NAME="Row 2,Right Side" VALUE="5" ORDER="5"/>
				<CMX_PROPERTY_VALUE NAME="Row 3,Left Side" VALUE="6" ORDER="6"/>
				<CMX_PROPERTY_VALUE NAME="Row 3,Right Side" VALUE="7" ORDER="7"/>
				
			</CMX_PROPERTY_VALUE_LIST>
		</CMX_PROPERTY>
		
		<CMX_PROPERTY NAME="Base" TYPE="ENUM"  DEFAULT_VALUE="Decimal" COMMENT="Select numerical base for display.">
			<CMX_PROPERTY_VALUE_LIST>
				<CMX_PROPERTY_VALUE NAME="Hexadecimal"  VALUE="0" ORDER="0"/>
				<CMX_PROPERTY_VALUE NAME="Decimal"      VALUE="1" ORDER="1"/>
			</CMX_PROPERTY_VALUE_LIST>
		</CMX_PROPERTY>

	</CMX_PROPERTY_LIST>


	<CMX_HIDDEN_PROPERTY_LIST>
	<CMX_PROPERTY NAME="BarGraph" TYPE="ENUM" DEFAULT_VALUE="Disable">
				<CMX_PROPERTY_VALUE_LIST>
					<CMX_PROPERTY_VALUE NAME="Disable" VALUE="0" ORDER="0">
						<CMX_PROJECT_SETTING_LIST>
							<CMX_PROJECT_SETTING SETTING_TYPE="CHANNEL" PARAMETER_NAME="BarGraph" PARAMETER_VALUE="Disable"/>
						</CMX_PROJECT_SETTING_LIST>
					</CMX_PROPERTY_VALUE>
				</CMX_PROPERTY_VALUE_LIST>
		   </CMX_PROPERTY>
	</CMX_HIDDEN_PROPERTY_LIST>

	<CMX_TEMPLATE_FILE_LIST>
		<CMX_TEMPLATE_FILE PATH="LCDVALUE.h" PREFIX="" TARGET_FILE_NAME="LCDVALUE.h" FILE_TYPE="" />
		<CMX_TEMPLATE_FILE PATH="LCDVALUE.c" PREFIX="" TARGET_FILE_NAME="LCDVALUE.c" FILE_TYPE="" />
		<CMX_TEMPLATE_FILE PATH="LCDVALUEFrags.tpl" PREFIX="" TARGET_FILE_NAME="" FILE_TYPE="" />
	</CMX_TEMPLATE_FILE_LIST>

	
	<CMX_SCHEM_BOM_LIST>
		<CMX_SCHEM_BOM NAME="LCDVALUE" KEY_VALUE="" SCHEMATIC_PATH="LCDVALUE\LCDVALUE_schematic.gif" BOM_PATH="" NAME_LOCATION_SPEC="LCDVALUE\LCDVALUE_seglocation.xml"/>
	</CMX_SCHEM_BOM_LIST>

	<CMX_ANNOTATION_STRING_LIST>
		<CMX_ANNOTATION_STRING STRING_TYPE="SUMMARY_DESCRIPTION" ANNOTATION_STRING="This LCD driver supports a two or four line by 16 character Hitachi HD44780 based LCD (or functional equivalent). The driver displays a signed 16 bit value. The value is a decimal or hexadecimal number on the left or right half of an LCD line. 
Four instances of this driver may be used on a two line display. A four line LCD supports eight instances of this driver. When using multiple LCD drivers within a project, select position properties so that driver display regions do not conflict." />
		<CMX_ANNOTATION_STRING STRING_TYPE="PIN_DESCRIPTION" ANNOTATION_STRING="Control of the LCD driver chip uses seven IO pins. These pins must be on the same PSoC port. The driver uses a 4-bit communications interface."/>
		<CMX_ANNOTATION_STRING STRING_TYPE="VARIABLE_DESCRIPTION" ANNOTATION_STRING="The driver display value is a 16 bit signed value." />
	</CMX_ANNOTATION_STRING_LIST>

</CMX_IO_DRIVER>
</CMX_DB>
