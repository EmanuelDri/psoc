<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:user="http://www.tempuri.org" xmlns:msxsl="urn:schemas-microsoft-com:xslt" version="1.0">
  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
                function ReplaceFxn(str, pattern, replacement)
				{
					return str.replace(pattern, replacement)
				}
	]]>
  </msxsl:script>

  <xsl:template match="/">
    <HTML>
      <HEAD>
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache"></META>
        <META HTTP-EQUIV="Expires" CONTENT="-1"></META>
        <STYLE>

          .page
          {
          page-break-before: always;
          }

          body,
          td
          {
          font-family: verdana;
          font-size: 10px;
          }

          .brand,
          .brandSubPage
          {
          padding-top: 22px;
          padding-bottom: 4px;
          color:#ffffff;
          background-color:#09367A;
          border-width:1px;
          border-style:solid;
          border-color:#09367A;
          }

          .brandSubPage
          {
          margin-top:40px;
          }


          .copyright
          {
          text-align:right;
          font-size:7pt;
          margin-top:5px;
          padding-right: 5px;
          margin-bottom: 30px;
          }

          .trademark
          {
          vertical-align:super;
          font-size:6pt;
          }

          h1,
          .h1
          {
          font-size:22px;
          font-weight:bold;
          font-family:arial;
          }

          .reportTitle
          {
          margin-top:5px;
          }

          h2
          {
          font-family:arial;
          font-size:17px;
          font-weight:bold;
          }

          h1,
          h2
          {
          padding-left:16px;
          }

          .figureLabel,
          .label
          {
          font-family:arial;
          font-size: 11px;
          padding-left:16px;
          }

          .label
          {
          font-weight: bold;
          }

          .figureLabel,
          {
          padding-top:5px;
          }

          .columnHeading,
          {
          padding-left:16px;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          .cellForDeviceLabel,
          .cell,
          .pinoutHeading,
          th,
          .tableHeading,
          {
          padding-top:4px;
          padding-bottom:4px;
          /*
          border-bottom-width:1px;
          border-bottom-style:solid;
          border-bottom-color:#ffffff;
          */        }

          #tdRegisterProtocolDescription,
          #tdFlashUpdateProtocol
          {
          padding-left: 16px;
          padding-right:10px;
          padding-top:10px;
          padding-bottom:10px;
          background-color:#FFFFFF;
          color:#000000;
          font-weight: normal;
          }


          .cell,
          .cellForDeviceLabel,
          {
          color:black;
          text-align:center;
          background-color: e5e5e5;
          }

          .cellForDeviceLabel
          {
          color: black;
          font-weight: bold;
          text-align:left;
          padding-left:16px;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          .pinoutHeading,
          .dataTables,
          .tableHeading
          {
          color: white;
          font-family:arial;
          font-size: 11px;
          background-color:999999;
          font-weight: bold;
          }

          .columnHeading,
          .columnHeadingForDeviceLabel,
          {
          text-align: center;
          }

          .columnHeadingForDeviceLabel
          {
          text-align: left;
          padding-left:16px;
          }

          .notes
          {
          padding-top:16px;
          padding-left:16px;
          padding-bottom:10px;
          }

          .containerWidth,
          .brand,
          .brandSubPage,
          .copyright,
          .dataTables,
          .projectTable,
          .tableHeading,
          .descriptionTables
          {
          width:640px;
          }

          .projectTable,
          .descriptionTables
          {
          border-width:1px;
          border-style:solid;
          border-color:#999999;
          }

          .dataTables
          {
          border-collapse: collapse;
          border-width:1px;
          border-style:solid;
          border-color:#ffffff;
          }

          th,
          .pinoutHeading,
          .tableHeading
          {
          text-align:left;
          padding-left:16px;
          font-size:11pt;
          background-color:#6A6A6A;
          }

          .pdfDIV
          {
          margin-left: 16px;
          }

          .pdfLabel
          {
          font-size:9px;
          }

          a.pdfLabel
          {
          text-decoration: none;
          color:black;
          }

          a.pdfLabel:hover
          {
          text-decoration: underline;
          }

          .divTransferFunction
          {
            padding-bottom:20px;
          }
        </STYLE>

        <script language="javascript">
          <![CDATA[
	
	function bodyOnLoad()
	{
	   var obj = document.getElementById("tdRegisterProtocolDescription");
	   
	   if (obj != null)
	   {
	      obj.innerHTML = obj.innerHTML.replace(/\\n/g, "<br />");
	   }
	   
	   obj = document.getElementById("tdFlashUpdateProtocol");
	  	if (obj != null)
	   {
	      obj.innerHTML = obj.innerHTML.replace(/\\n/g, "<br />");
	   } 
	}
                
   function GeneratePdf(filename)
   {
      window.location.href = "PDF/GeneratePDF.htm?filename=" + filename;
   }
	]]>
        </script>

      </HEAD>
      <BODY onload="bodyOnLoad();">

        <div class="brand">
          <h1>
            PSoC Designer<span class="trademark">TM</span>
          </h1>
        </div>
        <div class="copyright">Cypress Semiconductor Corporation Copyright 2007-2008</div>

        <H1>DataSheet</H1>

        <div class="pdfDIV">
          <a id="PrintButon" onClick="return GeneratePdf('configreportDataSheet');" href="#" 
                      class="pdfLabel">
            <img border="0" alt="Print to PDF" src="PDF/pdf_button.png" width="16" height="16" />  Print to PDF
          </a>
        </div>
        <br></br>
        <!--
		
		  <h2>
			<xsl:value-of select="//CMX_PROJECT/CMX_MAKER_APPLICATION/@NAME"/>
		  </h2>
		
		-->
        <table>
          <tr>
            <td class="label">Last Built: </td>
            <td>
              <xsl:value-of select="//CMX_PROJECT/@BUILD_DATE"/>
            </td>
          </tr>
          <tr>
            <td class="label">Project Path: </td>
            <td>
              <xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH"/>
            </td>
          </tr>
          <tr>
            <td class="label">Project Name: </td>
            <td>
              <xsl:value-of select="//CMX_PROJECT/@NAME"/>
            </td>
          </tr>
        </table>

        <div class="notes">
          <strong>Printing</strong><br />
          For best results, print in portrait format.
        </div>
        <p></p>
        <TABLE BORDER="0" CELLSPACING="0" cellpadding="0" class="projectTable">
          <TR>
            <TD class="pinoutHeading">Project Pinout</TD>
          </TR>
          <TR>
            <TD ALIGN="CENTER">
              <embed>
                <xsl:attribute name="src">Pinout.svg</xsl:attribute>
                <xsl:attribute name="name">Pinout</xsl:attribute>
                <xsl:attribute name="id">Pinout</xsl:attribute>
                <xsl:attribute name="width">500</xsl:attribute>
                <xsl:attribute name="height">510</xsl:attribute>
                <xsl:attribute name="type"><![CDATA[image/svg-xml]]> </xsl:attribute>
              </embed>
            </TD>
          </TR>

        </TABLE>

        <div class="figureLabel"  style="page-break-after: always;">
          Figure 1: <xsl:value-of select="//CMX_PROJECT/@NAME" /> Pin Layout Diagram
        </div>

        <!--

	Page

-->

        <div class="brandSubPage">
          <h1>
            PSoC Designer<span class="trademark">TM</span>
          </h1>
        </div>
        <div class="copyright">Cypress Semiconductor Corporation Copyright 2006-2008</div>


        <h2>
          PSoC Base Device Description:
          <xsl:value-of select="//CMX_ANNOTATION_STRING[@STRING_TYPE='BASE_PROJECT_DESCRIPTION']/@ANNOTATION_STRING" />
        </h2>


        <!-- Table1a: Pin Details -->
        <xsl:if test="count(//PIN_DESC_LIST/PIN_DESC) > 0" >

          <TABLE BORDER="0" CELLSPACING="0" class="dataTables">
            <tr>
              <th>
                Table 1: <xsl:value-of select="//CMX_PROJECT/@NAME"/> Pin Descriptions
              </th>
            </tr>


            <TR ALIGN="CENTER">
              <TD>

                <TABLE BORDER="1" CELLSPACING="0" class="dataTables">

                  <xsl:for-each select="//PIN_DESC_LIST/PIN_DESC">
                    <xsl:if test="position() = 1">
                      <tr>
                        <xsl:for-each select="@*">
                          <xsl:if test="name() != 'PIN_SELECT' and name() != 'PIN_DRIVE' and name() != 'PIN_INTERRUPT'">
                            <td class="columnHeading">
                              <xsl:value-of select="name()" />
                            </td>
                          </xsl:if>
                        </xsl:for-each>
                        <td class="columnHeading">DRIVER_TYPE</td>
                      </tr>
                    </xsl:if>
                    <tr>
                      <xsl:for-each select="@*">
                        <xsl:if test="name() != 'PIN_SELECT' and name() != 'PIN_DRIVE' and name() != 'PIN_INTERRUPT'">
                          <td class="cell">
                            <xsl:choose>
                              <xsl:when test="string-length(.) > 0">
                                <xsl:value-of select="." />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </xsl:if>
                      </xsl:for-each>
                      <td class="cell">
                        <xsl:variable name="CustomName">
                          <xsl:value-of select="@PIN_CUSTOM_NAME" />
                        </xsl:variable>
                        <xsl:variable name="OutputField">
                          <xsl:value-of select="//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER[@DRIVER_INSTANCE=$CustomName]/@DISPLAY_NAME" />
                        </xsl:variable>

                        <xsl:choose>
                          <xsl:when test="string-length($OutputField) > 0">
                            <xsl:value-of select="$OutputField" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>
                    </tr>
                  </xsl:for-each>

                </TABLE>
              </TD>
            </TR>
          </TABLE>
        </xsl:if>


        <br />
        <br />


        <!-- Table2: I2C Register Map -->
        <xsl:if test="count(//CMX_VARIABLE_OFFSET_LIST/CMX_VARIABLE_OFFSET) > 0" >
          <BR/>
          <BR/>
          <TABLE BORDER="0" CELLSPACING="0" class="dataTables">
            <tr>
              <th>
                Table 2: <xsl:value-of select="//CMX_PROJECT/@NAME"/> Interface Register Map
              </th>
            </tr>

            <TR ALIGN="CENTER">
              <TD>

                <TABLE BORDER="1" CELLSPACING="0" class="dataTables">
                  <TR>
                    <td class="columnHeading">OFFSET</td>
                    <td class="columnHeading">NAME</td>
                    <td class="columnHeading">SIZE</td>
                    <td class="columnHeading">READ/WRITE ACCESS</td>
                  </TR>
                  <xsl:for-each select="//CMX_VARIABLE_OFFSET_LIST/CMX_VARIABLE_OFFSET">
                    <xsl:sort data-type="number" order="ascending" select="./@VARIABLE_OFFSET"/>
                    <TR>
                      <TD class="cell">
                        <xsl:value-of select="./@VARIABLE_OFFSET"/>
                      </TD>

                      <TD class="cell">
                        <xsl:value-of select="@VARIABLE_NAME"/>
                      </TD>

                      <TD class="cell">
                        <xsl:value-of select="./@VARIABLE_SIZE"/>
                      </TD>


                      <TD class="cell">
                        <xsl:value-of select="@VARIABLE_ACCESS"/>
                      </TD>
                    </TR>
                    <TR>
                      <TD colspan="4">
                        <xsl:for-each select="./CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']">
                          <xsl:value-of select="./@ANNOTATION_STRING"/>
                        </xsl:for-each>
                      </TD>


                    </TR>
                  </xsl:for-each>

                </TABLE>
              </TD>
            </TR>
          </TABLE>
        </xsl:if>
        <P></P>



        <!-- 
	  <br /><br />
	<b>I2C Register Protocol Description (Requires an I2C Interface Object): </b><br /><br /> -->
        <xsl:choose>
          <xsl:when test="count(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/@ANNOTATION_STRING) > 0">
            <br></br>
            <DIV class="tableHeading">
              <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/@ANNOTATION_STRING"/>
            </DIV>
            <TABLE BORDER="0" CELLSPACING="0" class="descriptionTables">
              <tr>
                <td id="tdRegisterProtocolDescription">
                  <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/../CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_DESCRIPTION']/@ANNOTATION_STRING" />
                </td>
              </tr>
            </TABLE>
          </xsl:when>
        </xsl:choose>

        <xsl:if test="count(//CMX_CONSTANT_BLOCK_OFFSET_LIST/CMX_CONSTANT_BLOCK_OFFSET) > 0" >
          <BR/>
          <BR/>
          <TABLE BORDER="0" CELLSPACING="0" class="dataTables">

            <th>
              Table 3: <xsl:value-of select="//CMX_PROJECT/@NAME"/> Flash Constant Map
            </th>

            <TR ALIGN="CENTER">
              <TD>

                <TABLE BORDER="1" CELLSPACING="0" class="dataTables">
                  <TR>
                    <TD class="columnHeading">OFFSET</TD>
                    <TD class="columnHeading">BLOCK</TD>
                    <TD class="columnHeading">SIZE</TD>
                    <TD class="columnHeading">NAME</TD>
                  </TR>
                  <xsl:for-each select="//CMX_CONSTANT_BLOCK_OFFSET_LIST/CMX_CONSTANT_BLOCK_OFFSET">
                    <xsl:sort data-type="number" order="ascending" select="./@CONSTANT_BLOCK_OFFSET"/>

                    <TR>
                      <TD class="cell">
                        <xsl:value-of select="./@CONSTANT_BLOCK_OFFSET"/>
                      </TD>

                      <TD class="cell">
                        <xsl:value-of select="@CONSTANT_BLOCK_ID"/>
                      </TD>


                      <TD class="cell">
                        <xsl:value-of select="@CONSTANT_BLOCK_SIZE"/>
                      </TD>

                      <TD class="cell">
                        <xsl:value-of select="user:ReplaceFxn(string(@CONSTANT_BLOCK_NAME), '_SetPointRegion_SetPointArray0_SetPointParam0', '_Region')"/>
                      </TD>
                    </TR>
                    <TR>
                      <TD class="cell">
                        <xsl:for-each select="./CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']">
                          <xsl:value-of select="./@ANNOTATION_STRING"/>
                        </xsl:for-each>
                      </TD>


                    </TR>
                  </xsl:for-each>

                </TABLE>
              </TD>
            </TR>
          </TABLE>
        </xsl:if>
        <P></P>




        <xsl:choose>
          <xsl:when test="count(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/@ANNOTATION_STRING) > 0">
            <br></br>
            <DIV class="tableHeading">
              <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/@ANNOTATION_STRING"/>
            </DIV>
            <TABLE BORDER="0" CELLSPACING="0" class="descriptionTables">
              <tr>
                <td id="tdFlashUpdateProtocol">
                  <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/../CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_DESCRIPTION']/@ANNOTATION_STRING" />
                </td>
              </tr>
            </TABLE>
          </xsl:when>
        </xsl:choose>

        <div class="page"></div>

        <div class="brandSubPage">
          <h1>
            PSoC Designer<span class="trademark">TM</span>
          </h1>
        </div>
        <div class="copyright">Cypress Semiconductor Corporation Copyright 2006-2008</div>

        <xsl:if test="count(//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER) > 0" >

          <h2>
            Driver Summary Descriptions:  <xsl:value-of select="//CMX_PROJECT/@NAME" />
          </h2>
          <br></br>
          <xsl:for-each select="//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER">
            <div style="padding:10px; width:605px; border:1px solid #000000">
              <table border="0" cellpadding="4px" style="border-collapse: collapse; width: 604px;">
                <tr>
                  <td valign="top" style="padding-right: 10px">
                    <b>Name:</b>
                  </td>
                  <td valign="top">
                    <xsl:value-of select="@DRIVER_INSTANCE"/>
                  </td>
                </tr>
                <tr>
                  <td valign="top" style="padding-right: 10px">
                    <b>Type:</b>
                  </td>
                  <td valign="top">
                    <xsl:value-of select="@DISPLAY_NAME"/>
                  </td>
                </tr>
                <tr>
                  <td valign="top" style="padding-right: 10px">
                    <b>Summary:</b>
                  </td>
                  <td valign="top">
                    <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='SUMMARY_DESCRIPTION']/@ANNOTATION_STRING"/>
                  </td>
                </tr>
                <tr>
                  <td nowrap="true" valign="top" style="padding-right: 10px">
                    <b>Hardware Interface:</b>
                  </td>
                  <td valign="top">
                    <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='PIN_DESCRIPTION']/@ANNOTATION_STRING"/>
                  </td>
                </tr>
                <tr>
                  <td nowrap="true" valign="top" style="padding-right: 10px">
                    <b>Software Interface:</b>
                  </td>
                  <td valign="top">
                    <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']/@ANNOTATION_STRING"/>
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <br />
                    <xsl:choose>
                      <xsl:when test="count(CMX_PROPERTY_LIST/CMX_PROPERTY) = 0">
                        <i>(No Configurable Properties Defined)</i>
                      </xsl:when>
                      <xsl:otherwise>
                        <b>Configurable Properties:</b>
                        <table border="1" cellpadding="5" style="margin-top:3px;" cellspacing="0" width="100%">
                          <tr>
                            <td class="columnHeading">Name</td>
                            <td class="columnHeading">Value</td>
                            <td class="columnHeading">Comment</td>
                          </tr>

                          <xsl:for-each select="CMX_PROPERTY_LIST/CMX_PROPERTY">
                            <tr>
                              <td class="cell">
                                <xsl:value-of select="@NAME"/>
                              </td>
                              <td class="cell">
                                <xsl:value-of select="@CURRENT_VALUE"/>
                              </td>
                              <td class="cell">
                                <xsl:value-of select="@COMMENT"/>
                              </td>
                            </tr>
                          </xsl:for-each>
                        </table>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>

                <tr>
                  <td colspan="3">
                    <br />
                    <xsl:if test="count(CMX_HIDDEN_PROPERTY_LIST/CMX_PROPERTY) != 0">
                      <b>Non-Configurable Properties:</b>
                      <table border="1" cellpadding="5" style="margin-top:3px;" cellspacing="0" width="100%">
                        <tr>
                          <td class="columnHeading">Name</td>
                          <td class="columnHeading">Value</td>
                          <td class="columnHeading">Comment</td>
                        </tr>

                        <xsl:for-each select="CMX_HIDDEN_PROPERTY_LIST/CMX_PROPERTY">
                          <tr>
                            <td class="cell">
                              <xsl:value-of select="@NAME"/>
                            </td>
                            <td class="cell">
                              <xsl:value-of select="@CURRENT_VALUE"/>
                            </td>
                            <td class="cell">
                              <xsl:value-of select="@COMMENT"/>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </table>
                    </xsl:if>
                  </td>
                </tr>

              </table>
            </div>
            <br />
            <br />
          </xsl:for-each>

        </xsl:if>
        <P></P>


        
        
        
        
        
        
        
        
        
        
        
        <xsl:if test="count(//CMX_TRANSFER_FUNCTION_SUMMARY_LIST/CMX_TRANSFER_FUNCTION_SUMMARY) > 0" >

          <div class="page"></div>

          <div class="brandSubPage">
            <h1>
              PSoC Designer<span class="trademark">TM</span>
            </h1>
          </div>
          <div class="copyright">Cypress Semiconductor Corporation Copyright 2006-2008</div>


          <h2>
            Transfer Functions:  <xsl:value-of select="//CMX_PROJECT/@NAME" />
          </h2>
          <br></br>
          
          <xsl:apply-templates select="//CMX_TRANSFER_FUNCTION_SUMMARY_LIST"/>
          
   
            <br />
            <br />
   
        </xsl:if>




      </BODY>
    </HTML>
  </xsl:template>

  
  <xsl:include href="configreportDataSheet_includes.xsl"/>



</xsl:stylesheet>






