//*****************************************************************************
//*****************************************************************************
//  FILENAME:  cmx_LCD_chan.h
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the LCD.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef LCD_SHARED_CHAN_INCLUDE
#define LCD_SHARED_CHAN_INCLUDE

#include "lcd.h"

//definition for bargraph
#define LCD_SOLID_BG	0x00
#define LCD_LINE_BG		0x01

//define empty string
#define LCD_EMPTY7_STRING	"       "
#define LCD_EMPTY8_STRING	"        "
#define LCD_EMPTY16_STRING	"                "
//define constant string
#define THRESHOLD		"T:"
#define HYSTERESIS		"H:"
#define ON				"ON "
#define OFF				"OFF"
#define SEPARATOR		':'
#define NONSEPARATOR	' '


//c function
extern void  LCDPrDecInt(int iValue);
extern void LCDStartSharedChan(void);

#endif