// LCD GPM Script
var g_BargraphValue = 0;

//*******************call from unshared channel part*********************//
function LCDPropertySet( sChannelInstanceName)
{
	//otto.GetData("ENABLE_JAVASCRIPT_LOGGING", "1", "", "");	
 	var LCDReqNode = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+sChannelInstanceName+ "\"]//");
	
    if (LCDReqNode != null) 
	{
		var LCDDriverProperty= g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+sChannelInstanceName+"\"]/CMX_IO_DRIVER/CMX_HIDDEN_PROPERTY_LIST/CMX_PROPERTY[@NAME=\"BarGraph\"]");
		
		if(LCDDriverProperty!=null)
			var BargraphValue = LCDDriverProperty.getAttribute("CURRENT_VALUE");
			//otto.LogToFile(LCDDriverProperty.xml, "C:\\LCDDriverProperty"+sChannelInstanceName+".xml");	
		if (BargraphValue=="Enable")
			g_BargraphValue = g_BargraphValue || 0x01;
		
		//otto.LogToFile(g_BargraphValue, "C:\\g_BargraphValue"+sChannelInstanceName+".xml");
	}
}

//*******************call from shared channel part*********************//
function LCDUMPropertyUpdate (sChannelInstanceName)
{
	//otto.GetData("ENABLE_JAVASCRIPT_LOGGING", "1", "", "");	
	var LCDSharedReqNode = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+sChannelInstanceName+ "\"]//");
	
    if (LCDSharedReqNode != null) 
	{
		var LCDChannelProperty = g_ReqDoc.selectSingleNode("//CMX_GLOBAL_PARAMETER_REQUEST[@SOURCE_CHANNEL=\""+sChannelInstanceName+"\"]//CMX_CHANNEL/CMX_PROPERTY_LIST/CMX_PROPERTY[@NAME=\"BarGraph\"]");
			
		if (g_BargraphValue)
			LCDChannelProperty.setAttribute("CURRENT_VALUE", "Enable");
		else
			LCDChannelProperty.setAttribute("CURRENT_VALUE", "Disable");
				
			//otto.LogToFile(g_BargraphValue, "C:\\g_BargraphValue"+sChannelInstanceName+".xml");
			//otto.LogToFile(LCDChannelProperty.xml, "C:\\LCDChannelProperty.xml");
	}
	
}
