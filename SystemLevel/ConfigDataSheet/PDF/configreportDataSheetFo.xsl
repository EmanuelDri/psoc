<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="user">

  <msxsl:script language="JScript" implements-prefix="user">
    <![CDATA[
            function ReplaceFxn(str, pattern, replacement)
				{
					return str.replace(pattern, replacement);
				}
            
            function ReplaceLineBreak(str)
				{
               var re = /\\n/g;
               var ret = str.replace(re, '</fo:block><fo:block>');

               re = /<fo:block><\/fo:block>/g;
               return ret.replace(re, '<fo:block padding-before="10px"></fo:block>');
				}
   ]]>
  </msxsl:script>


  <xsl:output method="xml"
              version="1.0"
              encoding="UTF-8"
              indent="yes"/>

  <xsl:template match="/">

    <fo:root writing-mode="lr-tb" hyphenate="false" text-align="start" role="html:html" id="main" 
         xmlns:fo="http://www.w3.org/1999/XSL/Format" 
         xmlns:html="http://www.w3.org/1999/xhtml">

      <!-- Define the layouts for the report -->
      <fo:layout-master-set>

        <fo:simple-page-master page-width="8.5in" page-height="11in" master-name="all-pages">
          <fo:region-body margin-top="1.25in" margin-right="0.5in" margin-bottom="1in" margin-left="0.5in"/>
          <fo:region-before region-name="page-header" extent="2in" display-align="before"/>
          <fo:region-after region-name="page-footer" extent="0.5in" display-align="after"/>
        </fo:simple-page-master>

      </fo:layout-master-set>

      <!-- The 1st page sequence shows the pinout. -->
      <fo:page-sequence master-reference="all-pages">

        <!-- Define the header -->
        <fo:static-content flow-name="page-header">
          <!-- Output the brand -->
          <fo:block role="brand" span="all" color="#ffffff" font-weight="bold"
                 font-family="Helvetica" font-size="22px" background-color="#09367A"
                 padding-top="13px" padding-bottom="13px" padding-left="10px"
                 margin-left="0.5in" margin-right="0.5in" border-style="solid"
                 border-width="1px" border-color="#09367A" space-before="0.25in">
            PSoC Designer
            <fo:inline role="trademark" vertical-align="super" font-size="6pt">TM</fo:inline>
          </fo:block>

          <!-- Output the copyright -->
          <fo:block role="copyright" text-align="right" font-size="7pt"
              padding-top="5px" margin-right="0.5in">
            Cypress Semiconductor Corporation Copyright 2006-2008
          </fo:block>
        </fo:static-content>

        <!-- Define the footer -->
        <fo:static-content flow-name="page-footer">
          <fo:block font-size="small" text-align="center" space-after.conditionality="retain" space-after="0.5in">
            - <fo:page-number/> -
          </fo:block>
        </fo:static-content>

        <fo:flow flow-name="xsl-region-body">

          <!-- Output the report title -->
          <fo:block role="reportTitle" font-size="22px" font-weight="bold"
              font-family="Helvetica">
            DataSheet
          </fo:block>

          <!-- Output the report build status -->
          <fo:table role="statusTable" padding-top="22px" table-layout="fixed" width="500px" border-collapse="separate"
                  border-spacing="0px" border="0px" border-style="hidden">

            <fo:table-column column-width="85px"/>
            <fo:table-column column-width="415px"/>

            <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
              <fo:table-row>
                <fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
                  <fo:block>Last Built:</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="0px" font-size="9pt">
                  <fo:block>
                    <xsl:value-of select="//CMX_PROJECT/@BUILD_DATE" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
                  <fo:block>Project Path:</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="0px" font-size="9pt">
                  <fo:block>
                    <xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              <fo:table-row>
                <fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
                  <fo:block>Project Name:</fo:block>
                </fo:table-cell>
                <fo:table-cell padding="0px" font-size="9pt">
                  <fo:block>
                    <xsl:value-of select="//CMX_PROJECT/@NAME" />
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
            </fo:table-body>
          </fo:table>

          <!-- Display pinout and schematic -->
          <fo:table role="pinout" table-layout="fixed" width="540px" border-collapse="separate"
                    border="0px" border-style="outset" padding-top="10px">

            <fo:table-column column-width="540px"/>

            <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">

              <fo:table-row background-color="#6A6A6A">
                <fo:table-cell text-align="left" padding-left="16px" color="white" font-weight="bold"
                             font-size="11pt" padding="0px" border-style="inset"
                             border="0px" padding-top="4px" padding-bottom="4px">
                  <fo:block>Project Pinout</fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell border="0.5px" border-style="solid">
                  <fo:block>
                    <fo:external-graphic content-width="540px"
                               content-height="510px" width="540px" height="510px" src="url('../Pinout.svg')"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row>
                <fo:table-cell color="black" font-size="11px" padding-left="16px">
                  <fo:block>
                    Figure 1: <xsl:value-of select="//CMX_PROJECT/@NAME" /> Pin Layout Diagram
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>

            </fo:table-body>

          </fo:table>

          <fo:block break-before="page"></fo:block>

          <!-- Output the report title -->
          <fo:block role="deviceDescription" font-size="17px" font-weight="bold"
              font-family="Helvetica">
            PSoC Base Device Description:
            <xsl:value-of select="//CMX_ANNOTATION_STRING[@STRING_TYPE='BASE_PROJECT_DESCRIPTION']/@ANNOTATION_STRING" />
          </fo:block>

          <!-- Table1a: Pin Details -->
          <xsl:if test="count(//PIN_DESC_LIST/PIN_DESC) > 0" >

            <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate"
                border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="540px"/>

              <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
                <fo:table-row>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
                           font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
                           font-weight="bold" padding-left="16px">
                    <fo:block>
                      Table 1: <xsl:value-of select="//CMX_PROJECT/@NAME"/> Pin Descriptions
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>

              </fo:table-body>
            </fo:table>

            <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
                    border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="97px"/>
              <fo:table-column column-width="96px"/>
              <fo:table-column column-width="97px"/>
              <fo:table-column column-width="130px"/>
              <fo:table-column column-width="120px"/>

              <!-- Define the table header -->
              <fo:table-header>

                <xsl:for-each select="//PIN_DESC_LIST/PIN_DESC">
                  <xsl:if test="position() = 1">
                    <fo:table-row>
                      <xsl:for-each select="@*">
                        <xsl:if test="name() != 'PIN_SELECT' and name() != 'PIN_DRIVE' and name() != 'PIN_INTERRUPT'">
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                              font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                              font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                              border-color="white">
                            <fo:block>
                              <xsl:value-of select="name()"/>
                            </fo:block>
                          </fo:table-cell>
                        </xsl:if>
                      </xsl:for-each>

                      <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                          font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                          font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                          border-color="white">
                        <fo:block>DRIVER_TYPE</fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  </xsl:if>
                </xsl:for-each>

              </fo:table-header>

              <fo:table-body>

                <xsl:for-each select="//PIN_DESC_LIST/PIN_DESC">
                  <fo:table-row>

                    <xsl:for-each select="@*">
                      <xsl:if test="name() != 'PIN_SELECT' and name() != 'PIN_DRIVE' and name() != 'PIN_INTERRUPT'">
                        <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                            font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                            text-align="center" border="0.5px" border-style="solid"
                            border-color="white">
                          <fo:block>
                            <xsl:choose>
                              <xsl:when test="string-length(.) > 0">
                                <xsl:value-of select="." />
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:text disable-output-escaping="yes"></xsl:text>
                              </xsl:otherwise>
                            </xsl:choose>
                          </fo:block>
                        </fo:table-cell>
                      </xsl:if>
                    </xsl:for-each>

                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                        font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block>
                        <xsl:variable name="CustomName">
                          <xsl:value-of select="@PIN_CUSTOM_NAME" />
                        </xsl:variable>
                        <xsl:variable name="OutputField">
                          <xsl:value-of select="//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER[@DRIVER_INSTANCE=$CustomName]/@DISPLAY_NAME" />
                        </xsl:variable>

                        <xsl:choose>
                          <xsl:when test="string-length($OutputField) > 0">
                            <xsl:value-of select="$OutputField" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text disable-output-escaping="yes"></xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                      </fo:block>
                    </fo:table-cell>

                  </fo:table-row>

                </xsl:for-each>

              </fo:table-body>

            </fo:table>

          </xsl:if>

          <!-- Table2: I2C Register Map -->
          <xsl:if test="count(//CMX_VARIABLE_OFFSET_LIST/CMX_VARIABLE_OFFSET) > 0" >
            <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate"
                border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="540px"/>

              <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
                <fo:table-row>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
                           font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
                           font-weight="bold" padding-left="16px">
                    <fo:block>
                      Table 2: <xsl:value-of select="//CMX_PROJECT/@NAME"/> I2C Register Map
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>

              </fo:table-body>
            </fo:table>

            <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
                    border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="60px"/>
              <fo:table-column column-width="240px"/>
              <fo:table-column column-width="60px"/>
              <fo:table-column column-width="180px"/>

              <!-- Define the table header -->
              <fo:table-header>

                <fo:table-row>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>OFFSET</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>NAME</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
    font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
    font-weight="bold" text-align="center" border="0.5px" border-style="solid"
    border-color="white">
                    <fo:block>SIZE</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>READ/WRITE ACCESS</fo:block>
                  </fo:table-cell>
                </fo:table-row>

              </fo:table-header>

              <fo:table-body>

                <xsl:for-each select="//CMX_VARIABLE_OFFSET_LIST/CMX_VARIABLE_OFFSET">
                  <xsl:sort data-type="number" order="ascending" select="./@VARIABLE_OFFSET"/>
                  <fo:table-row>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                      font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                      text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                      <fo:block  wrap-option="wrap">
                        <xsl:value-of select="./@VARIABLE_OFFSET"/>
                      </fo:block>
                    </fo:table-cell>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block  wrap-option="wrap">
                        <xsl:value-of select="@VARIABLE_NAME"/>
                      </fo:block>
                    </fo:table-cell>

                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                      font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                      text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                      <fo:block  wrap-option="wrap">
                        <xsl:value-of select="./@VARIABLE_SIZE"/>
                      </fo:block>
                    </fo:table-cell>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block  wrap-option="wrap">
                        <xsl:value-of select="@VARIABLE_ACCESS"/>
                      </fo:block>
                    </fo:table-cell>

                  </fo:table-row>

                  <!-- Determine the variable description, if present. -->
                  <xsl:variable name="VariableDescription">
                    <xsl:for-each select="./CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']">
                      <xsl:value-of select="./@ANNOTATION_STRING"/>
                    </xsl:for-each>
                  </xsl:variable>

                  <!-- Only show the variable description if it has a length greater than zero. -->
                  <xsl:if test="string-length($VariableDescription) > 0" >

                    <fo:table-row>

                      <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                          text-align="center" border="0.5px" border-style="solid"
                          border-color="white" number-columns-spanned="3">
                        <fo:block>
                          <xsl:value-of select="$VariableDescription"/>
                        </fo:block>
                      </fo:table-cell>

                    </fo:table-row>
                  </xsl:if>

                </xsl:for-each>

              </fo:table-body>

            </fo:table>

          </xsl:if>

          <!-- Register Protocol Description -->
          <xsl:choose>
            <xsl:when test="count(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/@ANNOTATION_STRING) > 0">

              <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate"
                  border-spacing="0px" border="0px" border-style="hidden">

                <fo:table-column column-width="540px"/>

                <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
                  <fo:table-row>
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
                             font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
                             font-weight="bold" padding-left="16px">
                      <fo:block>
                        <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/@ANNOTATION_STRING"/>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                  <fo:table-row keep-with-previous="always">
                    <fo:table-cell border-width="0.5px" border-style="solid" font-size="8px" border-color="#999999"
                             padding="4px">
                      <fo:block>
                        <xsl:value-of disable-output-escaping="yes"
                        select="user:ReplaceLineBreak(string(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_HEADING']/../CMX_ANNOTATION_STRING[@STRING_TYPE='REGISTER_PROTOCOL_DESCRIPTION']/@ANNOTATION_STRING))" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                </fo:table-body>
              </fo:table>

            </xsl:when>
          </xsl:choose>

          <!-- Constant Map -->
          <xsl:if test="count(//CMX_CONSTANT_BLOCK_OFFSET_LIST/CMX_CONSTANT_BLOCK_OFFSET) > 0" >

            <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate"
                border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="540px"/>

              <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
                <fo:table-row>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
                           font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
                           font-weight="bold" padding-left="16px">
                    <fo:block>
                      Table 3: <xsl:value-of select="//CMX_PROJECT/@NAME"/> Flash Constant Map
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>

              </fo:table-body>
            </fo:table>

            <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
                    border-spacing="0px" border="0px" border-style="hidden">

              <fo:table-column column-width="90px"/>
              <fo:table-column column-width="90px"/>
              <fo:table-column column-width="80px"/>
              <fo:table-column column-width="280px"/>

              <!-- Define the table header -->
              <fo:table-header>

                <fo:table-row>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>OFFSET</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>BLOCK</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>SIZE</fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                      font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                      font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                      border-color="white">
                    <fo:block>NAME</fo:block>
                  </fo:table-cell>
                </fo:table-row>

              </fo:table-header>

              <fo:table-body>
                <xsl:for-each select="//CMX_CONSTANT_BLOCK_OFFSET_LIST/CMX_CONSTANT_BLOCK_OFFSET">
                  <xsl:sort data-type="number" order="ascending" select="./@CONSTANT_BLOCK_OFFSET"/>

                  <fo:table-row>
                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block  wrap-option="wrap">
                        <xsl:value-of select="./@CONSTANT_BLOCK_OFFSET"/>
                      </fo:block>
                    </fo:table-cell>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block wrap-option="wrap">
                        <xsl:value-of select="@CONSTANT_BLOCK_ID"/>
                      </fo:block>
                    </fo:table-cell>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block wrap-option="wrap">
                        <xsl:value-of select="@CONSTANT_BLOCK_SIZE"/>
                      </fo:block>
                    </fo:table-cell>

                    <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                        font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                        text-align="center" border="0.5px" border-style="solid"
                        border-color="white">
                      <fo:block wrap-option="wrap">
                        <xsl:value-of select="user:ReplaceFxn(string(@CONSTANT_BLOCK_NAME), '_SetPointRegion_SetPointArray0_SetPointParam0', '_Region')"/>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                  <!-- Determine the variable description, if present. -->
                  <xsl:variable name="VariableDescription">
                    <xsl:for-each select="./CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']">
                      <xsl:value-of select="./@ANNOTATION_STRING"/>
                    </xsl:for-each>
                  </xsl:variable>

                  <!-- Only show the variable description if it has a length greater than zero. -->
                  <xsl:if test="string-length($VariableDescription) > 0" >

                    <fo:table-row>
                      <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                          text-align="center" border="0.5px" border-style="solid"
                          border-color="white" number-columns-spanned="4">
                        <fo:block>
                          <xsl:value-of select="$VariableDescription"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                  </xsl:if>

                </xsl:for-each>

              </fo:table-body>

            </fo:table>

          </xsl:if>

          <!-- Register Protocol Description -->
          <xsl:choose>
            <xsl:when test="count(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/@ANNOTATION_STRING) > 0">

              <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate"
                  border-spacing="0px" border="0px" border-style="hidden">

                <fo:table-column column-width="540px"/>

                <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
                  <fo:table-row>
                    <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
                             font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
                             font-weight="bold" padding-left="16px">
                      <fo:block>
                        <xsl:value-of select="//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/@ANNOTATION_STRING"/>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                  <fo:table-row keep-with-previous="always">
                    <fo:table-cell border-width="0.5px" border-style="solid" font-size="8px" border-color="#999999"
                             padding="4px">
                      <fo:block>
                        <xsl:value-of disable-output-escaping="yes"
                        select="user:ReplaceLineBreak(string(//CMX_IO_DRIVER/CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_HEADING']/../CMX_ANNOTATION_STRING[@STRING_TYPE='CONSTANT_PROTOCOL_DESCRIPTION']/@ANNOTATION_STRING))" />
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>

                </fo:table-body>
              </fo:table>

            </xsl:when>
          </xsl:choose>

          <!-- Driver Summary Descriptions -->
          <xsl:if test="count(//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER) > 0" >
            <!-- Output the report title -->
            <fo:block role="deviceDescription" font-size="17px" font-weight="bold"
                font-family="Helvetica" break-before="page">
              Driver Summary Descriptions:  <xsl:value-of select="//CMX_PROJECT/@NAME" />
            </fo:block>

            <xsl:for-each select="//CMX_IO_DRIVER_LIST/CMX_IO_DRIVER">

              <fo:block space-before="10px" border="0.5px" border-style="solid" border-color="black">

                <fo:table padding-top="5px" table-layout="fixed" width="540px" border-collapse="separate"
                    border-spacing="0px" border="0px" border-style="hidden">

                  <fo:table-column column-width="115px"/>
                  <fo:table-column column-width="425px"/>

                  <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">

                    <fo:table-row keep-together="always">
                      <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                        <fo:block>Name:</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding-top="4px" padding-bottom="4px" padding-left="5px" font-size="10px">
                        <fo:block>
                          <xsl:value-of select="@DRIVER_INSTANCE"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row keep-together="always">
                      <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                        <fo:block>Type:</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding-top="4px" padding-bottom="4px" padding-left="5px" font-size="10px">
                        <fo:block>
                          <xsl:value-of select="@DISPLAY_NAME"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row keep-together="always">
                      <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                        <fo:block>Summary:</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding-top="4px" padding-bottom="4px" padding-left="5px" font-size="10px">
                        <fo:block>
                          <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='SUMMARY_DESCRIPTION']/@ANNOTATION_STRING"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row keep-together="always">
                      <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                        <fo:block>Hardware Interface:</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding-top="4px" padding-bottom="4px" padding-left="5px" font-size="10px">
                        <fo:block>
                          <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='PIN_DESCRIPTION']/@ANNOTATION_STRING"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                    <fo:table-row keep-together="always">
                      <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                        <fo:block>Software Interface:</fo:block>
                      </fo:table-cell>
                      <fo:table-cell padding-top="4px" padding-bottom="4px" padding-left="5px" font-size="10px">
                        <fo:block>
                          <xsl:value-of select="CMX_ANNOTATION_STRING_LIST/CMX_ANNOTATION_STRING[@STRING_TYPE='VARIABLE_DESCRIPTION']/@ANNOTATION_STRING"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>

                  </fo:table-body>
                </fo:table>

                <xsl:choose>
                  <xsl:when test="count(CMX_PROPERTY_LIST/CMX_PROPERTY) = 0">
                    <fo:table table-layout="fixed" border="0px" border-style="hidden">
                      <fo:table-column column-width="540px"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-style="italic" font-size="10px">
                            <fo:block>(No Configurable Properties Defined)</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>
                  </xsl:when>
                  <xsl:otherwise>
                    <fo:table table-layout="fixed" border="0px" border-style="hidden">
                      <fo:table-column column-width="540px"/>
                      <fo:table-body>
                        <fo:table-row keep-together="always">
                          <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                            <fo:block>Configurable Properties:</fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>

                    <fo:table padding-top="0px" table-layout="fixed" width="535px" border-collapse="separate"
                             border-spacing="0px" border="0px"
                             border-style="hidden" space-after="10px">

                      <fo:table-column column-width="5px"/>
                      <fo:table-column column-width="195px"/>
                      <fo:table-column column-width="105px"/>
                      <fo:table-column column-width="230px"/>

                      <!-- Define the table header -->
                      <fo:table-header>

                        <fo:table-row keep-together="always">
                          <fo:table-cell/>
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                               font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                               font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block>Name</fo:block>
                          </fo:table-cell>
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                               font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                               font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block>Value</fo:block>
                          </fo:table-cell>
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                               font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                               font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block>Comment</fo:block>
                          </fo:table-cell>
                        </fo:table-row>

                      </fo:table-header>

                      <fo:table-body>
                        <xsl:for-each select="CMX_PROPERTY_LIST/CMX_PROPERTY">

                          <fo:table-row keep-together="always">
                            <fo:table-cell/>
                            <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                                 font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                                 text-align="center" border="0.5px" border-style="solid"
                                 border-color="white">
                              <fo:block wrap-option="wrap">
                                <xsl:value-of select="@NAME"/>
                              </fo:block>
                            </fo:table-cell>

                            <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                                 font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                                 text-align="center" border="0.5px" border-style="solid"
                                 border-color="white">
                              <fo:block wrap-option="wrap">
                                <xsl:value-of select="@CURRENT_VALUE"/>
                              </fo:block>
                            </fo:table-cell>

                            <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                                 font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                                 text-align="center" border="0.5px" border-style="solid"
                                 border-color="white">
                              <fo:block wrap-option="wrap">
                                <xsl:value-of select="@COMMENT"/>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>

                        </xsl:for-each>

                      </fo:table-body>

                    </fo:table>

                  </xsl:otherwise>
                </xsl:choose>

                <xsl:if test="count(CMX_HIDDEN_PROPERTY_LIST/CMX_PROPERTY) != 0">

                  <fo:table table-layout="fixed" border="0px" border-style="hidden">
                    <fo:table-column column-width="540px"/>
                    <fo:table-body>
                      <fo:table-row keep-together="always">
                        <fo:table-cell wrap-option="no-wrap" padding-top="4px" padding-bottom="4px" padding-left="5px" font-weight="bold" font-size="10px">
                          <fo:block>Non-Configurable Properties:</fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </fo:table-body>
                  </fo:table>

                  <fo:table padding-top="0px" table-layout="fixed" width="535px" border-collapse="separate"
                           border-spacing="0px" border="0px"
                           border-style="hidden" space-after="10px">

                    <fo:table-column column-width="5px"/>
                    <fo:table-column column-width="130px"/>
                    <fo:table-column column-width="135px"/>
                    <fo:table-column column-width="265px"/>

                    <!-- Define the table header -->
                    <fo:table-header>

                      <fo:table-row keep-together="always">
                        <fo:table-cell/>
                        <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                             border-color="white">
                          <fo:block>Name</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                             border-color="white">
                          <fo:block>Value</fo:block>
                        </fo:table-cell>
                        <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                             border-color="white">
                          <fo:block>Comment</fo:block>
                        </fo:table-cell>
                      </fo:table-row>

                    </fo:table-header>

                    <fo:table-body>
                      <xsl:for-each select="CMX_HIDDEN_PROPERTY_LIST/CMX_PROPERTY">

                        <fo:table-row keep-together="always">
                          <fo:table-cell/>
                          <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                               font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                               text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block wrap-option="wrap">
                              <xsl:value-of select="@NAME"/>
                            </fo:block>
                          </fo:table-cell>

                          <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                               font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                               text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block wrap-option="wrap">
                              <xsl:value-of select="@CURRENT_VALUE"/>
                            </fo:block>
                          </fo:table-cell>

                          <!--<fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                                          font-size="11px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"-->
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"  
                               font-size="8px" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                               text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block wrap-option="wrap">
                              <xsl:value-of select="@COMMENT"/>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>

                      </xsl:for-each>

                    </fo:table-body>

                  </fo:table>

                </xsl:if>

              </fo:block>

            </xsl:for-each>

          </xsl:if>

          <!-- Transfer Functions -->
          <xsl:if test="count(//CMX_TRANSFER_FUNCTION_SUMMARY_LIST/CMX_TRANSFER_FUNCTION_SUMMARY) > 0" >
            <!-- Output the report title -->
            <fo:block role="deviceDescription" font-size="17px" font-weight="bold"
                font-family="Helvetica" break-before="page">
              Transfer Functions:  <xsl:value-of select="//CMX_PROJECT/@NAME" />
            </fo:block>

            <xsl:apply-templates select="//CMX_TRANSFER_FUNCTION_SUMMARY_LIST"/>

          </xsl:if>

        </fo:flow>
      </fo:page-sequence>

    </fo:root>

  </xsl:template>

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='LoopDelay']">
    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>
    <xsl:variable name="CmxInputInstanceName">
      <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_TFINPUT_LIST/CMX_TFINPUT/@NAME"/>
    </xsl:variable>

    <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
               font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
               font-weight="bold" padding-left="16px">
            <fo:block>
              Transfer Function:  <xsl:value-of select="$CmxTransferInstanceName"/> Loop Delay
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="270px"/>
      <fo:table-column column-width="270px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>INPUT NAME</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>INPUT VALUE</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
             font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
             text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>
              <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxInputInstanceName]/@NAME"/>
            </fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
             font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
             text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>
              <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxInputInstanceName]/@DEFAULT_VALUE"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>

    </fo:table>
  </xsl:template>

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='Expression']">
    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>

    <xsl:variable name="PriorityOrStatusEncoder">
      <xsl:choose>
        <xsl:when test="count(//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]//CMX_EXPRESSION_LIST/CMX_EXPRESSION[@EXPRESSION_TYPE='ELSEIF']) > 0">
          <xsl:text disable-output-escaping="yes">Priority Encoder</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text disable-output-escaping="yes">Status Encoder</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
               font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
               font-weight="bold" padding-left="16px">
            <fo:block>
              Transfer Function:  <xsl:value-of select="$CmxTransferInstanceName"/><xsl:text> </xsl:text><xsl:value-of select="$PriorityOrStatusEncoder"/>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>EXPRESSION</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="55px"/>
      <fo:table-column column-width="320px"/>
      <fo:table-column column-width="50px"/>
      <fo:table-column column-width="115px"/>

      <fo:table-body>
        <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]//CMX_EXPRESSION_LIST/CMX_EXPRESSION">
          <xsl:sort data-type="number" order="ascending" select="./@ORDINAL"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@EXPRESSION_TYPE"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block wrap-option="wrap">
                <xsl:value-of select="./@LEFT_EXPRESSION"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>THEN</fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block wrap-option="wrap">
                <xsl:value-of select="./@RIGHT_EXPRESSION"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>
      </fo:table-body>
    </fo:table>

  </xsl:template>

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='SetPointRegion']">

    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>
    <xsl:variable name="CmxInputInstanceName">
      <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_TFINPUT_LIST/CMX_TFINPUT/@NAME"/>
    </xsl:variable>
    <xsl:variable name="CmxWidgetScale">
      <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/@WIDGSCALE"/>
    </xsl:variable>

    <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
               font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
               font-weight="bold" padding-left="16px">
            <fo:block>
              Transfer Function:  <xsl:value-of select="$CmxTransferInstanceName"/> SetPointRegion
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="180px"/>
      <fo:table-column column-width="180px"/>
      <fo:table-column column-width="180px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>ORDINAL</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>THRESHHOLD</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>HYSTERESIS</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_FUNCTION_LIST/CMX_FUNCTION/CMX_FUNCTION_PARAMETER_INSTANCE/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='SetPointArray']/CMX_ARRAY_ELEMENT_LIST/CMX_ARRAY_ELEMENT">
          <xsl:sort data-type="number" order="ascending" select="./@ARRAY_ORDINAL"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./CMX_STRUC/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='Ordinal']/@VALUE"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:variable name="CmxThreshold">
                  <xsl:value-of select="./CMX_STRUC/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='Threshold']/@VALUE"/>
                </xsl:variable>
                <xsl:value-of select="$CmxThreshold div $CmxWidgetScale"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:variable name="CmxHysteresis">
                  <xsl:value-of select="./CMX_STRUC/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='Hysteresis']/@VALUE"/>
                </xsl:variable>
                <xsl:value-of select="$CmxHysteresis div $CmxWidgetScale"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>

      </fo:table-body>

    </fo:table>

  </xsl:template>

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='StateMachine']">

    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>

    <xsl:variable name="CmxContainerName">
      <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/@CONTAINER_NAME"/>
    </xsl:variable>

    <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
               font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
               font-weight="bold" padding-left="16px">
            <fo:block>
              Transfer Function:  <xsl:value-of select="$CmxContainerName"/> State Machine
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="300px"/>
      <fo:table-column column-width="240px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>STATE NAME</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>DEFAULT</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <xsl:for-each select="//CMX_VARIABLE_CONTAINER_LIST/CMX_VARIABLE_CONTAINER[@INSTANCE_NAME=$CmxContainerName]/CMX_STATE_LIST/CMX_STATE">
          <xsl:sort data-type="number" order="ascending" select="./@ORDER"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@NAME"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@DEFAULT"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>

      </fo:table-body>

    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#e5e5e5" padding-top="15px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block> </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="120px"/>
      <fo:table-column column-width="80px"/>
      <fo:table-column column-width="60px"/>
      <fo:table-column column-width="280px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>TRANSITION NAME</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>FROM STATE</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>TO STATE</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>EXPRESSION</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <xsl:for-each select="//CMX_VARIABLE_CONTAINER_LIST/CMX_VARIABLE_CONTAINER[@INSTANCE_NAME=$CmxContainerName]/CMX_STATE_TRANSITION_LIST/CMX_STATE_TRANSITION">
          <xsl:sort data-type="number" order="ascending" select="./@ORDER"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@NAME"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@FROM_STATE"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@TO_STATE"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block wrap-option="wrap">
                <xsl:value-of select="./@EXPRESSION"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>

      </fo:table-body>

    </fo:table>

  </xsl:template>

  <xsl:template match="CMX_TRANSFER_FUNCTION_SUMMARY[@TRANSFER_FUNCTION_TYPE='TableLookup']">

    <xsl:variable name="CmxTransferInstanceName">
      <xsl:value-of select="@INSTANCE_NAME" />
    </xsl:variable>

    <fo:table padding-top="22px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" 
               font-size="11px" background-color="#6A6A6A" padding-top="4px" padding-bottom="4px"
               font-weight="bold" padding-left="16px">
            <fo:block>
              Transfer Function:  <xsl:value-of select="$CmxTransferInstanceName"/> Table Lookup
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="330px"/>
      <fo:table-column column-width="210px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>OUTPUT NAME</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>EXPRESSION</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_VALUE_LIST/CMX_VALUE">
          <xsl:sort data-type="number" order="ascending" select="./@ORDER"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select="./@NAME"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block wrap-option="wrap">
                <xsl:value-of select="./@EXPRESSION_TEXT"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>

      </fo:table-body>

    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="540px"/>

      <xsl:variable name="maxOrdinal">
        <xsl:value-of select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_FUNCTION_LIST/CMX_FUNCTION/CMX_FUNCTION_PARAMETER_INSTANCE/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='InputList']/CMX_ARRAY_ELEMENT_LIST/CMX_ARRAY_ELEMENT/@ARRAY_ORDINAL" />
      </xsl:variable>

      <fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
             font-size="8pt" font-style="italic" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
             text-align="center" border="0.5px" border-style="solid" border-color="white">
            <fo:block>
              Input Value =
              <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_FUNCTION_LIST/CMX_FUNCTION/CMX_FUNCTION_PARAMETER_INSTANCE/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='InputList']/CMX_ARRAY_ELEMENT_LIST/CMX_ARRAY_ELEMENT">
                <xsl:sort data-type="number" order="ascending" select="./@ARRAY_ORDINAL"/>

                <xsl:value-of select=".//CMX_STRUC_MEMBER[@MEMBER_NAME='instanceID']/@VALUE"/> *
                <xsl:value-of select=".//CMX_STRUC_MEMBER[@MEMBER_NAME='multiplier']/@VALUE"/>
                <xsl:if test="position()!=last()"> + </xsl:if>
              </xsl:for-each>
            </fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-body>
    </fo:table>

    <fo:table padding-top="0px" table-layout="fixed" width="540px" border-collapse="separate"
        border-spacing="0px" border="0px" border-style="hidden">

      <fo:table-column column-width="240px"/>
      <fo:table-column column-width="300px"/>

      <!-- Define the table header -->
      <fo:table-header>

        <fo:table-row>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>INPUT VALUE</fo:block>
          </fo:table-cell>
          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
             font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
             font-weight="bold" text-align="center" border="0.5px" border-style="solid"
             border-color="white">
            <fo:block>ASSIGNED OUTPUT</fo:block>
          </fo:table-cell>
        </fo:table-row>

      </fo:table-header>

      <fo:table-body>

        <xsl:for-each select="//CMX_VARIABLE_LIST/CMX_VARIABLE[@NAME=$CmxTransferInstanceName]/CMX_TRANSFERFUNCTION_LIST/CMX_TRANSFERFUNCTION/CMX_FUNCTION_LIST/CMX_FUNCTION/CMX_FUNCTION_PARAMETER_INSTANCE/CMX_STRUC_MEMBER_LIST/CMX_STRUC_MEMBER[@MEMBER_NAME='LookupTable']/CMX_ARRAY_ELEMENT_LIST/CMX_ARRAY_ELEMENT">
          <xsl:sort data-type="number" order="ascending" select="./@ARRAY_ORDINAL"/>

          <fo:table-row>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select=".//@ARRAY_ORDINAL"/>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
               text-align="center" border="0.5px" border-style="solid"
               border-color="white">
              <fo:block>
                <xsl:value-of select=".//@ARRAY_ELEMENT_VALUE"/>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

        </xsl:for-each>

      </fo:table-body>

    </fo:table>

  </xsl:template>

</xsl:stylesheet>
