<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output method="xml"
            version="1.0"
            encoding="UTF-8"
            indent="yes"/>

<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>
<xsl:variable name="g_maxLabelWidth">185</xsl:variable>

<xsl:template match="/">
            
	<fo:root writing-mode="lr-tb" hyphenate="false" text-align="start" role="html:html" id="main" 
			 xmlns:fo="http://www.w3.org/1999/XSL/Format" 
			 xmlns:html="http://www.w3.org/1999/xhtml">
	
		<!-- Define the layouts for the report -->
		<fo:layout-master-set>
		
			<fo:simple-page-master page-width="11in" page-height="8.5in" master-name="all-pages">
				<fo:region-body margin-top="0.5in" margin-right="0.5in" margin-bottom="0.5in" margin-left="0.5in"/>
				<fo:region-before region-name="page-header" extent="1in" display-align="before"/>
				<fo:region-after region-name="page-footer" extent="0.5in" display-align="after"/>
			</fo:simple-page-master>
			
		</fo:layout-master-set>

		<!-- The 1st page sequence (for page 1) shows the pinout and schematic. -->
		<fo:page-sequence master-reference="all-pages">

			<!-- Define the footer -->
			<fo:static-content flow-name="page-footer">
				<fo:block font-size="small" text-align="center" space-after.conditionality="retain" space-after="0.5in">- <fo:page-number/> -</fo:block>
			</fo:static-content>
			
			<fo:flow flow-name="xsl-region-body">
				<fo:block role="html:body">
		
					<fo:table table-layout="fixed" border-collapse="separate" border-spacing="0px" border="0px" border-style="hidden" role="html:table">
					
						<fo:table-column column-width="432px" role="html:col"/>

						<fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">

							<fo:table-row role="html:tr">

								<fo:table-cell border="0.25px" padding="0px" role="html:td" id="outerCell">

									<fo:block>

										<!-- Display Part Summary -->
										<fo:table table-layout="fixed" width="720px" border-collapse="separate" border-spacing="0px" border="0.25px" border-style="outset" role="html:table">
										
											<fo:table-column column-width="100px" role="html:col"/>
											<fo:table-column column-width="620px" role="html:col"/>

											<fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
												<fo:table-row role="html:tr">
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block>Last Built:</fo:block>
													</fo:table-cell>
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block><xsl:value-of select="//CMX_PROJECT/@BUILD_DATE" /></fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row role="html:tr">
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block>Project Path:</fo:block>
													</fo:table-cell>
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block><xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH" /></fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row role="html:tr">
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block>Project Name:</fo:block>
													</fo:table-cell>
													<fo:table-cell border="0.25px" padding="0px" border-style="inset" role="html:td">
														<fo:block><xsl:value-of select="//CMX_PROJECT/@NAME" /></fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
		
										<!-- Display pinout and schematic -->
										<fo:table table-layout="fixed" width="720px" height="450px" border-collapse="separate" border-spacing="5px" border="0.25px" border-style="outset" role="html:table" id="svg">
											
											<fo:table-column column-width="520px" role="html:col"/>
											<fo:table-column column-width="200px" role="html:col"/>
											
											<fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
												<fo:table-row role="html:tr" text-align="center">
													<fo:table-cell border="0px" padding="0px" border-style="inset" role="html:td">
														<fo:block>
															<fo:external-graphic content-width="6.2in" content-height="6.2in" width="6.2in" height="6.2in" src="url('../Pinout.svg')"/>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell border="0px" padding="0px" border-style="inset" role="html:td">
														<fo:block font-size="10pt" space-before="10px" space-after="10px">
                                             <xsl:value-of select="//CMX_PROJECT/@NAME" />
                                          </fo:block>
                                          <fo:block>
                                             <fo:external-graphic>
                                                <xsl:attribute name="role">
                                                   <xsl:text>html:img</xsl:text>
                                                </xsl:attribute>
                                                <xsl:attribute name="id">
                                                   <xsl:text>Schematic</xsl:text>
                                                </xsl:attribute>
                                                <xsl:attribute name="src">
                                                   <xsl:text>url('../ProjectSchematic_</xsl:text>
                                                   <xsl:value-of select="//BASEPROJ_FILE_PATH/@BASEPROJ_PSOC_PART" />
                                                   <xsl:text>.gif')</xsl:text>
                                                </xsl:attribute>
                                             </fo:external-graphic>
                                             <!-- 
                                             <fo:external-graphic src="url(concat(concat('../ProjectSchematic_', 'CY8C27443B'), '.gif'))"  role="html:img" id="Schematic"/>  
                                             -->
														</fo:block>
														<fo:block font-size="10pt" space-before="10px">
                                             Recommended Support
                                          </fo:block>
														<fo:block font-size="10pt">
                                             Components
                                          </fo:block>
													</fo:table-cell>
												</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</fo:flow>
		</fo:page-sequence>

		<!-- This page sequence shows the detailed schematic with labels. -->
		<fo:page-sequence master-reference="all-pages">

			<fo:static-content flow-name="page-footer">
				<fo:block font-size="small" text-align="center" space-after.conditionality="retain" space-after="0.5in">- <fo:page-number/> -</fo:block>
			</fo:static-content>

			<fo:flow flow-name="xsl-region-body">
			
				<fo:block break-before="page"></fo:block>

				<fo:block-container position="absolute" top="0in" left="0in" width="10in" height="7.5in">
					<fo:block font-size="14pt" color="blue" text-align="center" font-weight="bold">Driver Schematics</fo:block>
				</fo:block-container>
										
				<!-- Display schematic images -->
				<xsl:call-template name="DrawSchematic"/>

			</fo:flow>
	    </fo:page-sequence>

	</fo:root>            

</xsl:template>

<!-- This template generates XSL-FO that displays the detailed schematic with labels. -->
<xsl:template name="DrawSchematic" >
		<fo:block-container position="absolute" top="0.25in" left="0in" width="10in" height="7.5in">
				<fo:block>

						<fo:table table-layout="fixed" width="700px" border-collapse="collapse" border-spacing="0px" border="0px" border-style="hidden" role="html:table">
							<fo:table-column column-width="233px"/>
							<fo:table-column column-width="233px"/>
							<fo:table-column column-width="233px"/>
												
								<fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">

									<!-- Loop over each of the detailed schematic elements. -->
									<xsl:for-each select="//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='DRIVER_SCHEM_BOM']">

									<!-- If we're starting a new row -->
									<xsl:if test="((position() mod 3) = 1)">
													
										<!-- Close out the old row (if there was a previous row) -->
										<xsl:if test="(position() != 1)">
											<xsl:text disable-output-escaping="yes">&lt;/fo:table-row&gt;</xsl:text>
										</xsl:if>
	
										<!-- If we should close out the table + start a new one (2 rows/table) do that --> 			
										<xsl:if test="((position() mod 6) = 1)">
											<xsl:text disable-output-escaping="yes">&lt;/fo:table-body&gt;&lt;/fo:table&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;/fo:block&gt;&lt;/fo:block-container&gt;</xsl:text>

											<!-- If we are done with this table and this isn't the 1st element then add the labels on top of the images. -->
											<xsl:if test="(position() != 1)">
												<xsl:call-template name="DrawLabels">
													<xsl:with-param name="begPos" select="position() - 6"/>
												</xsl:call-template>

												<fo:block break-before="page"></fo:block>
											</xsl:if>

											<!-- Start a new table. -->
											<xsl:text disable-output-escaping="yes">&lt;fo:block-container position="absolute" top="0.25in" left="0in" width="10in" height="7.5in"&gt;&lt;fo:block&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;fo:table table-layout="fixed" width="700px" border-collapse="collapse" border-spacing="0px" border=".25px" border-style="outset" role="html:table"&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
											<xsl:text disable-output-escaping="yes">&lt;fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start"&gt;</xsl:text>
										</xsl:if>
										
										<!-- Open the new row -->			
										<xsl:text disable-output-escaping="yes">&lt;fo:table-row role="html:tr"&gt;</xsl:text>
									</xsl:if>
	
									<!-- Display the schematic images -->
									<fo:table-cell border="0px" width="233px" height="233px" padding-left="7px" padding-right="7px" padding-top="15px" border-style="hidden" role="html:td" text-align="left" display-align="before">

										<fo:external-graphic width="233px" height="233px" role="html:img">
											<xsl:attribute name="src">
												<xsl:variable name="PATH" select="@SCHEMATIC_PATH"/>
												<xsl:variable name="LCPATH" select="translate($PATH,$ucletters,$lcletters)"/>
												<xsl:text>url('</xsl:text>
												<xsl:value-of select="concat('../', translate($LCPATH,'\','/'))" />
												<xsl:text>')</xsl:text>
											</xsl:attribute>
										</fo:external-graphic>

									</fo:table-cell>
												
									</xsl:for-each>
												
								   <xsl:text disable-output-escaping="yes">&lt;/fo:table-row&gt;</xsl:text>
												
								</fo:table-body>
						</fo:table>

				</fo:block>
		</fo:block-container>

		<!-- Overlay labels on top of the last schematic images. -->
		<xsl:variable name="bom_cnt" select="count(//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='DRIVER_SCHEM_BOM'])"/>
		<xsl:if test="$bom_cnt > 0">
         <xsl:variable name="strtPos">
            <xsl:choose>
               <xsl:when test="$bom_cnt mod 6 = 0">
                  <xsl:value-of select="$bom_cnt - 5"/>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:value-of select="$bom_cnt - ($bom_cnt mod 6) + 1"/>
               </xsl:otherwise>
            </xsl:choose>
         </xsl:variable>
			<xsl:call-template name="DrawLabels">
				<xsl:with-param name="begPos">
					<!-- Pass the starting position to the labelling routine. Note that there could be less than 6 label elements for the last table. -->
					<xsl:value-of select="$strtPos"/>
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

</xsl:template>

<!-- This template generates XSL-FO that overlays labels on top of the detailed schematic images. -->
<xsl:template name="DrawLabels" >
	<!-- This parameter indicates the starting position to use for labelling. -->
	<xsl:param name="begPos" select="1"/>
	
		<!-- Loop over each of the detailed schematic elements. -->
		<xsl:for-each select="//CMX_SCHEM_BOM_REPORT_LIST/CMX_SCHEM_BOM_REPORT[@SCHEM_BOM_TYPE='DRIVER_SCHEM_BOM']">

			<!-- Check if the current position is within the bounds specified by the caller. -->
			<xsl:if test="(position() >= $begPos) and (position() &lt; ($begPos + 6))">
									
				<!-- If we're starting a new row -->
				<xsl:if test="((position() mod 3) = 1)">
														
					<!-- Close out the old row (if there was a previous row) -->
					<xsl:if test="(position() != $begPos)">
						<xsl:text disable-output-escaping="yes">&lt;/fo:table-row&gt;</xsl:text>
					</xsl:if>
		
					<!-- If we should close out the table + start a new one (2 rows/table) do that --> 			
					<xsl:if test="((position() mod 6) = 1)">

						<!-- Close out the current table as long as this isn't the 1st table. -->
						<xsl:if test="position() != $begPos">
							<xsl:text disable-output-escaping="yes">&lt;/fo:table-body&gt;&lt;/fo:table&gt;</xsl:text>
							<xsl:text disable-output-escaping="yes">&lt;/fo:block&gt;&lt;/fo:block-container&gt;</xsl:text>
						</xsl:if>
						
						<!-- Start a new table. -->
						<xsl:text disable-output-escaping="yes">&lt;fo:block-container position="absolute" top="0.25in" left="0in" width="10in" height="7.5in"&gt;&lt;fo:block&gt;</xsl:text>
						<xsl:text disable-output-escaping="yes">&lt;fo:table table-layout="fixed" width="700px" border-collapse="collapse" border-spacing="0px" border=".25px" border-style="outset" role="html:table"&gt;</xsl:text>
						<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
						<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
						<xsl:text disable-output-escaping="yes">&lt;fo:table-column column-width="233px" role="html:col"/&gt;</xsl:text>
						<xsl:text disable-output-escaping="yes">&lt;fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start"&gt;</xsl:text>
					</xsl:if>
											
					<!-- Open the new row -->			
					<xsl:text disable-output-escaping="yes">&lt;fo:table-row role="html:tr"&gt;</xsl:text>
				</xsl:if>
		
				<!-- Initialize some working variables for the current schematic element. -->
				<xsl:variable name="INST_NAME" select="@INSTANCE_NAME" />
				<xsl:variable name="OUTPUT_TEXT">
					<xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_OUTPUT']) > 0" >
						<xsl:value-of select="$INST_NAME" />
					</xsl:if>
				</xsl:variable>
				<xsl:variable name="INPUT_TEXT">
					<xsl:if test="count(//CMX_IO_DRIVER[@DRIVER_INSTANCE=$INST_NAME and @DRIVER_TYPE='CMX_INPUT']) > 0" >
						<xsl:value-of select="$INST_NAME" />
					</xsl:if>
				</xsl:variable>

				<!-- Output the labels for the current schematic element. -->
				<fo:table-cell border="0px" width="233px" height="233px" padding-top="3px" padding-left="7px" padding-right="14px" padding-bottom="12px" border-style="hidden" role="html:td" text-align="left" display-align="before">
					<fo:table table-layout="fixed" border-collapse="collapse" border-spacing="0px" border="0px" border-style="hidden" role="html:table">
						<fo:table-column column-width="226px"/>
						<fo:table-body>
							<fo:table-row>
								<fo:table-cell border="0px" border-style="hidden" role="html:td" text-align="left" display-align="before">

                           <!-- Determine if using the old style of labelling or the absolution positioning of labels. -->
                           <xsl:choose>
                              <xsl:when test="string-length(@NAME_LOCATION_SPEC) = 0">
              
									      <!-- Generate XSL-FO for the schematic element name. -->
									      <fo:block text-align="center" role="html:center" font-size="10pt">
										      <xsl:value-of select="$INST_NAME" />
									      </fo:block>

									      <!-- If available, generate XSL-FO for the schematic element output text. -->
									      <fo:block space-before="96px" text-align="left" font-size="10pt">
										      <xsl:if test="string-length($OUTPUT_TEXT) != 0">
											      <xsl:value-of select="$OUTPUT_TEXT" />
										      </xsl:if>
									      </fo:block>

									      <!-- If available, generate XSL-FO for the schematic element input text. -->
									      <fo:block text-align="right" font-size="10pt">
										      <xsl:if test="string-length($INPUT_TEXT) != 0">
											      <xsl:value-of select="concat($INPUT_TEXT, '&#160;&#160;')" />
										      </xsl:if>
									      </fo:block>
                              </xsl:when>
                              <xsl:otherwise>
                                 <!-- Handle absolute placement of schematic labels.  Placement details are located 
                                      in an external XML file loaded below by the document() function. -->

                                 <!-- The labells defined in the specified XML file all provide X,Y coordinates that are
                                      relative to the current image. This is fine for the FOP and this implementation as well accept
                                      for the fact that each image is embedded in a fixed size table cell that is 233 x 233
                                      pixels. As such, the X,Y coordinates need to be scaled based on the image size vs. the
                                      actual scaled size. For some reason an additional scale factor is necessary in the
                                      horizontal direction. One other thing to note is that the FOP outputs each subsequent
                                      label within the table cell relative to the previous label. As such, it is necessary to
                                      compute the difference between the previous Y and the current Y coordinates to compute a
                                      relative offset from the previous label. To accomplish this, it was necessary to use
                                      recursive calls to a template since XSL doesn't allow variable reassignment. -->
                                 
									      <!-- Generate XSL-FO for the schematic element name. -->
									      <fo:block text-align="center" role="html:center" font-size="10pt">
										      <xsl:value-of select="$INST_NAME" />
									      </fo:block>
  
									      <!-- Setup some variables. -->
                                 <xsl:variable name="imgWidth">
                                    <xsl:choose>
                                       <xsl:when test="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/@IMAGE_WIDTH">
                                          <xsl:value-of select="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/@IMAGE_WIDTH"/>
                                       </xsl:when>
                                       <xsl:otherwise>
                                          <xsl:value-of select="289"/>
                                       </xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:variable>
                                 <xsl:variable name="imgHeight">
                                    <xsl:choose>
                                       <xsl:when test="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/@IMAGE_HEIGHT">
                                          <xsl:value-of select="document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/@IMAGE_HEIGHT"/>
                                       </xsl:when>
                                       <xsl:otherwise>
                                          <xsl:value-of select="289"/>
                                       </xsl:otherwise>
                                    </xsl:choose>
                                 </xsl:variable>
                                 <xsl:variable name="numLabels">
                                    <xsl:value-of select="count(document(@NAME_LOCATION_SPEC)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION)"/>
                                 </xsl:variable>

                                 <!-- Call a template to position the current absolute label. -->
			                        <xsl:call-template name="positionLabel">
				                        <xsl:with-param name="maxCount">
					                        <xsl:value-of select="$numLabels"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="prevY">
					                        <xsl:value-of select="-8"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="curIdx">
					                        <xsl:value-of select="1"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="locSpec">
					                        <xsl:value-of select="@NAME_LOCATION_SPEC"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="instName">
					                        <xsl:value-of select="$INST_NAME"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="iWidth">
					                        <xsl:value-of select="$imgWidth"/>
				                        </xsl:with-param>
				                        <xsl:with-param name="iHeight">
					                        <xsl:value-of select="$imgHeight"/>
				                        </xsl:with-param>
			                        </xsl:call-template>

                              </xsl:otherwise>
                           </xsl:choose>
								</fo:table-cell>
							</fo:table-row>
						</fo:table-body>
					</fo:table>
				</fo:table-cell>
														
			</xsl:if>
		</xsl:for-each>
												
		<!-- Terminate the table, block and block container. -->
	   <xsl:text disable-output-escaping="yes">&lt;/fo:table-row&gt;</xsl:text>
		<xsl:text disable-output-escaping="yes">&lt;/fo:table-body&gt;&lt;/fo:table&gt;</xsl:text>
		<xsl:text disable-output-escaping="yes">&lt;/fo:block&gt;&lt;/fo:block-container&gt;</xsl:text>

</xsl:template>

<!-- This template positions a single absolute label and then recursively calls itself for the next label. -->
<xsl:template name="positionLabel">
	<xsl:param name="maxCount" select="0"/>
	<xsl:param name="prevY" select="0"/>
	<xsl:param name="curIdx" select="0"/>
	<xsl:param name="locSpec" select="0"/>
	<xsl:param name="instName" select="0"/>
	<xsl:param name="iWidth" select="0"/>
	<xsl:param name="iHeight" select="0"/>

   <!-- Check if we have already generated all the labels yet and if so don't output a new one and don't call this template again. -->
   <xsl:if test="$maxCount >= $curIdx">

      <!-- Setup some variables. -->
      <xsl:variable name="justify">
         <xsl:value-of select="document($locSpec)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION[number($curIdx)]/@JUSTIFY"/>
      </xsl:variable>
      <xsl:variable name="append_str">
         <xsl:value-of select="document($locSpec)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION[number($curIdx)]/@APPEND_STRING"/>
      </xsl:variable>
      <xsl:variable name="locX">
         <xsl:value-of select="document($locSpec)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION[number($curIdx)]/@LOCATION_X"/>
      </xsl:variable>
      <xsl:variable name="locY">
         <xsl:value-of select="document($locSpec)//CMX_SCHEMATIC_NAME_LOCATION_LIST/CMX_SCHEMATIC_NAME_LOCATION[number($curIdx)]/@LOCATION_Y"/>
      </xsl:variable>

      <!-- Compute the relative X, Y coordinates for the label. -->
      <xsl:variable name="locationX">
         <xsl:choose>
            <xsl:when test="$justify='RIGHT'">
               <xsl:value-of select="round((212 div $iWidth) * (number($locX) - (string-length(concat($instName,$append_str)) * 6)))"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise>
               <xsl:value-of select="round((212 div $iWidth) * number($locX))"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="locationY">
         <xsl:value-of select="round((218 div $iHeight) * (number($locY)-number($prevY))) - 8"/>
      </xsl:variable>
  
      <!-- TEST: <xsl:value-of select="concat('curIdx=',$curIdx,',justify=',$justify,',append_str=',$append_str,',locX=',$locX,',locY=',$locY,',locationX=',$locationX,',locationY=',$locationY)"/> -->
  
      <!-- Output the FO block with the label. -->
      <fo:block font-size="8px" text-align="left">
         <xsl:attribute name="text-indent">
            <xsl:value-of select="concat($locationX,'px')"></xsl:value-of>
         </xsl:attribute>
         <xsl:attribute name="space-before">
            <xsl:value-of select="concat($locationY,'px')"></xsl:value-of>
         </xsl:attribute>
      
         <xsl:value-of select="$instName" />
         <xsl:value-of select="$append_str" />
      </fo:block>

      <!-- Call a template to position the next absolute label. -->
      <xsl:call-template name="positionLabel">
         <xsl:with-param name="maxCount">
            <xsl:value-of select="$maxCount"/>
         </xsl:with-param>
         <xsl:with-param name="prevY">
            <xsl:value-of select="$locY"/>
         </xsl:with-param>
         <xsl:with-param name="curIdx">
            <xsl:value-of select="$curIdx + 1"/>
         </xsl:with-param>
         <xsl:with-param name="locSpec">
            <xsl:value-of select="$locSpec"/>
         </xsl:with-param>
         <xsl:with-param name="instName">
            <xsl:value-of select="$instName"/>
         </xsl:with-param>
         <xsl:with-param name="iWidth">
            <xsl:value-of select="$iWidth"/>
         </xsl:with-param>
         <xsl:with-param name="iHeight">
            <xsl:value-of select="$iHeight"/>
         </xsl:with-param>
      </xsl:call-template>
   </xsl:if>

</xsl:template>
</xsl:stylesheet>
