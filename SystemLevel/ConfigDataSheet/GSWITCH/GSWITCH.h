//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_GSWITCH.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for Generic Switch driver 
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef GSWITCH_include
#define GSWITCH_include

#include <M8C.h>
`@GSWITCH_ORDINAL_DEFINE_BLOCK`
#define `@LIB_NAME`_GSWITCH_COUNT `@GSWITCH_COUNT`

////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE ID;                // Instance ID ( All drivers )
    BYTE Instance;          // DIO channel instance
    BYTE InputActiveState;  // 1 active Hi,0 active lo
    BYTE DriveMode;         // Pin IO Drive Mode, PU, PD, etc
    BYTE ChannelID;         // Pin and port, pin [3:0], port [7:4]
}`@LIB_NAME`_GSWITCH_ParameterBlock;

//-------------------------------------------------
// Prototypes of the GSWITCH API.
//-------------------------------------------------
extern void `@LIB_NAME`_GSWITCH_Instantiate(const CMX_GSWITCH_ParameterBlock * pPBlock);
extern BYTE `@LIB_NAME`_GSWITCH_GetValue(const CMX_GSWITCH_ParameterBlock * pPBlock);

#endif
