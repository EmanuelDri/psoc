;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME:   Inversor.asm
;;  Version: 1.10, Updated on 2015/3/4 at 22:27:44
;;  Generated by PSoC Designer 5.4.3191
;;
;;  DESCRIPTION: SwitchCapConfig User Module software implementation file.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "Inversor.inc"
include "m8c.inc"
include "memory.inc"

;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------
export  Inversor_Start
export _Inversor_Start
export  Inversor_SetPower
export _Inversor_SetPower
export  Inversor_Stop
export _Inversor_Stop
export  Inversor_SetGain
export _Inversor_SetGain

AREA UserModules (ROM, REL)

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: Inversor_Start(BYTE bPowerSetting)
;  FUNCTION NAME: Inversor_SetPower(BYTE bPowerSetting)
;
;  DESCRIPTION:
;    Applies power setting to the module's analog PSoC block.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  contains the power setting 0-3
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 Inversor_Start:
_Inversor_Start:
 Inversor_SetPower:
_Inversor_SetPower:
   RAM_PROLOGUE RAM_USE_CLASS_2
   and   A, Inversor_POWERMASK
   push  X
   mov   X, SP
   push  A
   mov   A, reg[Inversor_CR3]
   and   A, ~Inversor_POWERMASK
   or    A, [X]
   mov   reg[Inversor_CR3], A
   pop   A
   pop   X
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: Inversor_Stop(void)
;
;  DESCRIPTION:
;    Removes power from the module's analog PSoC block
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: None
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 Inversor_Stop:
_Inversor_Stop:
   RAM_PROLOGUE RAM_USE_CLASS_1
   and   reg[Inversor_CR3], ~Inversor_POWERMASK
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: Inversor_SetGain(BYTE bGainSetting)
;
;  DESCRIPTION:
;    This function sets the Gain/Atten of the amplifier.  Valid gain settings
;    are defined in the .inc file.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  Contains gain settings.
;
;    Gain values shown are for example. (See .inc file for gain equates)
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 Inversor_SetGain:
_Inversor_SetGain:
   RAM_PROLOGUE RAM_USE_CLASS_2
   and   A, Inversor_GAINMASK                              ; mask A to protect unchanged bits
   mov   X, SP                                             ; define temp store location
   push  A                                                 ; put gain value in temp store
   mov   A, reg[Inversor_CR0]                              ; read reg settings
   and   A, ~Inversor_GAINMASK                             ; clear gain bits in A
   or    A, [X]                                            ; combine gain value with balance of reg.
   mov   reg[Inversor_CR0], A                              ; move complete value back to register
   pop   A
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

; End of File Inversor.asm
