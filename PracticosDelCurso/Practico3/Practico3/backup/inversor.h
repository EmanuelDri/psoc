//*****************************************************************************
//*****************************************************************************
//  FILENAME:  Inversor.h
//  Version: 1.10, Updated on 2015/3/4 at 22:27:44
//  Generated by PSoC Designer 5.4.3191
//
//  DESCRIPTION:  SwitchCapConfig User Module C Language interface file.
//-----------------------------------------------------------------------------
//  Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#ifndef Inversor_INCLUDE
#define Inversor_INCLUDE

#include <M8C.h>

//-------------------------------------------------
// Constants for Inversor API's.
//-------------------------------------------------
#define Inversor_OFF         0
#define Inversor_LOWPOWER    1
#define Inversor_MEDPOWER    2
#define Inversor_HIGHPOWER   3

#define Inversor_Gain_0_03125   0x81
#define Inversor_Gain_0_0625    0x82
#define Inversor_Gain_0_09375   0x83
#define Inversor_Gain_0_125     0x84
#define Inversor_Gain_0_15625   0x85
#define Inversor_Gain_0_1875    0x86
#define Inversor_Gain_0_21875   0x87
#define Inversor_Gain_0_25      0x88
#define Inversor_Gain_0_28125   0x89
#define Inversor_Gain_0_3125    0x8a
#define Inversor_Gain_0_34375   0x8b
#define Inversor_Gain_0_375     0x8c
#define Inversor_Gain_0_40625   0x8d
#define Inversor_Gain_0_4375    0x8e
#define Inversor_Gain_0_46875   0x8f
#define Inversor_Gain_0_5       0x90
#define Inversor_Gain_0_53125   0x91
#define Inversor_Gain_0_5625    0x92
#define Inversor_Gain_0_59375   0x93
#define Inversor_Gain_0_625     0x94
#define Inversor_Gain_0_65625   0x95
#define Inversor_Gain_0_6875    0x96
#define Inversor_Gain_0_71875   0x97
#define Inversor_Gain_0_75      0x98
#define Inversor_Gain_0_78125   0x99
#define Inversor_Gain_0_8125    0x9a
#define Inversor_Gain_0_84375   0x9b
#define Inversor_Gain_0_875     0x9c
#define Inversor_Gain_0_90625   0x9d
#define Inversor_Gain_0_9375    0x9e
#define Inversor_Gain_0_96875   0x9f
#define Inversor_Gain_1         0x10
#define Inversor_Gain_1_0625    0x11
#define Inversor_Gain_1_125     0x12
#define Inversor_Gain_1_1875    0x13
#define Inversor_Gain_1_25      0x14
#define Inversor_Gain_1_3125    0x15
#define Inversor_Gain_1_375     0x16
#define Inversor_Gain_1_4375    0x17
#define Inversor_Gain_1_5       0x18
#define Inversor_Gain_1_5625    0x19
#define Inversor_Gain_1_625     0x1a
#define Inversor_Gain_1_6875    0x1b
#define Inversor_Gain_1_75      0x1c
#define Inversor_Gain_1_8125    0x1d
#define Inversor_Gain_1_875     0x1e
#define Inversor_Gain_1_9375    0x1f

//--------------------------------------
// API Function Declarations
//--------------------------------------

#pragma fastcall16 Inversor_Start
#pragma fastcall16 Inversor_SetPower
#pragma fastcall16 Inversor_Stop
#pragma fastcall16 Inversor_SetGain

//-------------------------------------------------
// Prototypes of the Inversor API.
//-------------------------------------------------

extern void Inversor_Start(BYTE bPowerSetting);
extern void Inversor_SetPower(BYTE bPowerSetting);
extern void Inversor_Stop(void);
extern void Inversor_SetGain(BYTE bGainSetting);

//-------------------------------------------------
// Register Addresses for Inversor
//-------------------------------------------------
#pragma ioport  Inversor_CR0:   0x088
BYTE            Inversor_CR0;
#pragma ioport  Inversor_CR1:   0x089
BYTE            Inversor_CR1;
#pragma ioport  Inversor_CR2:   0x08a
BYTE            Inversor_CR2;
#pragma ioport  Inversor_CR3:   0x08b
BYTE            Inversor_CR3;

#endif
// end of file Inversor.h
