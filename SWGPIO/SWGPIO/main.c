//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules

#pragma interrupt_handler elboton

int contador;

void main(void)
{
	 M8C_EnableGInt ; // Uncomment this line to enable Global Interrupts
	// Insert your main routine code here.
	PRT1DM2=0x00;
	PRT1DM1=0xfe;
	PRT1DM0=0x01;
	PRT1GS=0x01;
	M8C_EnableIntMask(INT_MSK0,INT_MSK0_GPIO);
	PWM8_1_Start();
	LCD_1_Start();
	LCD_1_Position(0,0);
	LCD_1_PrCString("int no tratada");
	contador=0;
}

void elboton(void){
//		if(PRT1DM0==0x01){
//			PRT1DM1=0xef;
//			PRT0DM0=0x10;
//		}
//		else {
//			PRT1DM1=0xfe;
//			PRT1DM0=0x01;
//		}
	PRT1DM0^=0x11;
	PRT1DM1^=0x11;
	PRT1GS^=0x11;
		LCD_1_Position(0,0);
		LCD_1_PrCString("la int tratada");
		LCD_1_Position(1,0);
		LCD_1_PrCString("DM1");
		LCD_1_Position(1,4);
		LCD_1_PrHexByte(PRT1DM1);
		LCD_1_Position(0,7);
		LCD_1_PrCString("DM0");
		LCD_1_Position(0,9);
		LCD_1_PrHexByte(PRT1DM0);
	LCD_1_Position(0,0);
		LCD_1_PrHexByte(contador);
	contador++;
	
	
}