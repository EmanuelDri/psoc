;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: SystemConst.inc
;;  @Version@
;;  
;;
;;  DESCRIPTION: Definitions of system constants.
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************


;; System defines these values
FlashBlockSize:          equ   0x40     ; Flash block size ( 64 bytes for Diamond)
FlashSize:               equ   0x4000   ; Size of flash in PSoC

SystemCalBlockCnt:       equ   ((FlashSize - 0x3fc0)/FlashBlockSize)        ; Blocks used for calibration
SystemConstBlockCnt:     equ   ((0x3fc0 - 0x3f80)/FlashBlockSize)        ; Blocks used for System constants

Enable_Flash_Interface:  equ   0             ; Disable -- 0,  Enable -- 1, Enable No Timeout -- 2

;; Calculate First block that contains constants for I2C flash interface
TotalBlocksUsed:         equ   (SystemCalBlockCnt+SystemConstBlockCnt)
TotalFlashBlockCnt:      equ   (FlashSize/FlashBlockSize) 
FirstConstBlock:         equ   (TotalFlashBlockCnt - TotalBlocksUsed)
