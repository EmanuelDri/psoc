//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LCDGRAPH.h
//   Version: 1.3, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the LCDGRAPH Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_LCDGRAPH_include
#define CMX_LCDGRAPH_include

#include <M8C.h>
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
	BYTE bNumberRows; 	//number of rows
	int iMinRange;  	// minimum range
	int iMaxRange; 		//maximum range
}CMX_LCDGRAPH_ParameterBlock;

// Function Prototypes
void CMX_LCDGRAPH_SetValue(const CMX_LCDGRAPH_ParameterBlock * thisBLK, int iValue);
void CMX_LCDGRAPH_Instantiate(const CMX_LCDGRAPH_ParameterBlock * thisBLK);


#endif
