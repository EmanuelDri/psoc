//*****************************************************************************
//*****************************************************************************
//  FILENAME:  ADCMux8_LLD.h
//  @Version@
//  
//
//  DESCRIPTION:  Header file for the low level 8 input muxed ADC converter.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef ADCMUX8_HEADER
#define ADCMux8_HEADER

#pragma fastcall16 AdcScanReset
#pragma fastcall16 AdcScan
#pragma fastcall16 iGetAinChanCounts

// Functions written in asm in file ADCMux8_LLD.asm
extern void    AdcScanReset(void);
extern void    AdcScan(void);
extern int     iGetAinChanCounts(BYTE bIndex);

// Function written in C in file ADCMux8a_LLD.c
extern int     iGetChanMVolts(BYTE bChan);

//extern float * gGetChanVolts(BYTE bChan, float * fResult);

extern int     aiADC_Results[];
extern const BYTE MVOLTSChannelPins[];

// ADC Parameters

// (2^16)cnts/2.6volts = 25206 cnts/volt
#define COUNTS_PER_VOLT  25206

#define CMX_ADC_CHAN_RESOLUTION        12
#define CMX_ADC_CHAN_MAX_VOLTS         2.600
#define CMX_ADC_CHAN_MIN_VOLTS         0.0
#define CMX_ADC_CHAN_COUNTS16_PER_VOLT 25206
#define CMX_ADC_CHAN_MAX_CHANNELS      8     


#endif
