//
// FunctionParamDecl.h
//

#ifndef FUNCTION_PARAMDECL_H
#define FUNCTION_PARAMDECL_H

#include <m8c.h>
#include "CMXSystemFunction.h"

// function parameter block ID's


// system variable value defines
// pse_Elecede output state ID defines
#define ID_pse_Elecede_OFF 0
#define ID_pse_Elecede_Scroll 1
#define ID_pse_Elecede_Pause 2
#define ID_pse_Elecede_UNASSIGNED 255


// pse_Elecede output state value defines
#define ID_pse_Elecede_OFF_VALUE 0
#define ID_pse_Elecede_Scroll_VALUE 1
#define ID_pse_Elecede_Pause_VALUE 2
#define ID_pse_Elecede_UNASSIGNED_VALUE 0


// system variable value defines for expressions
// pse_Elecede output state ID defines for expressions
#define pse_Elecede__OFF ID_pse_Elecede_OFF_VALUE
#define pse_Elecede__Scroll ID_pse_Elecede_Scroll_VALUE
#define pse_Elecede__Pause ID_pse_Elecede_Pause_VALUE
#define pse_Elecede__UNASSIGNED ID_pse_Elecede_UNASSIGNED_VALUE


// function parameter block definitions


#endif
