//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

const CMX_LCDSCROLLINGTEXT_ParameterBlock pse_Elecede = {LCDSCROLLINGTEXT_pse_Elecede_ORDINAL,0,"Hola Mundo",100>>4};

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_Elecede, // ID = 1
		// ID_MAX = 2
};
