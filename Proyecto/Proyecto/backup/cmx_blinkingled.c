//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_BlinkingLed.c
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This is driver for BLINKINGLED.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "CMX_BlinkingLed.h"
#include "cmx.h"
#include "projectproperties.h"
#include "SystemTimer.h"

#define BLINKINGLED_OFF     0x00
#define BLINKINGLED_ON		0x01
#define BLINKINGLED_BLINK	0x02


BYTE BlinkingLedState[CMX_BLINKINGLED_COUNT];  // Allocate state byte for each instance
BYTE BlinkingLedCount[CMX_BLINKINGLED_COUNT];  // Allocate count byte for each instance
 
//-----------------------------------------------------------------------------
//  FUNCTION NAME: BLINKINGLED_Instantiate(pBlock)
//
//    DESCRIPTION: 
//      Initialize this instance of the BlinkingLED
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    pPBlock  == Pointer to parameter block.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
////////////////////////////////////////////////////////////////////////////////

void CMX_BLINKINGLED_Instantiate(const CMX_BLINKINGLED_ParameterBlock * pPBlock)
{
    BYTE Blink_pin;
	BYTE Blink_port;
	BYTE PPChan;
	BYTE bInstance;

    bInstance = pPBlock->BLINKINGLED_INSTANCE;                   // Get Instance number 
	PPChan = DIOChannelPins[(pPBlock->BLINKINGLED_ChannelID)];   // Get pin and port info
	Blink_pin = pinlu[(BYTE)(PPChan & 0x0F)];                    // Create pin mask
	Blink_port = portlu[(BYTE)((PPChan & 0xF0) >>4)];            // Extract port address
	GlobalSelectDisconnect(Blink_pin,Blink_port);                // Disconnect Global
	BlinkingLedCount[bInstance] = SystemTimer_bGetTickCntr();    // Initiate blinking counter
	BlinkingLedState[bInstance] = BLINKINGLED_OFF; 				 // Initiate LED state variable
	DiscreteDriveMode(Blink_pin, Blink_port, pPBlock->BLINKINGLED_DriveMode); // Set mode to Strong
}
//-----------------------------------------------------------------------------
//  FUNCTION NAME: BLINKINGLED_SetValue(pPBlock,  bMode)
//
//  DESCRIPTION: 
//    Sets LED mode between Off, On, and Blink.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    pPBlock == Pointer to Parameter block
//    bMode   == New Mode 
//               0 = Off
//               1 = On
//               2 = Blink
//
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
////////////////////////////////////////////////////////////////////////////////

void CMX_BLINKINGLED_SetValue(const CMX_BLINKINGLED_ParameterBlock * pPBlock, BYTE bMode)
{
    BYTE bInstance;
	BYTE bTickCntr;
	
    bInstance = pPBlock->BLINKINGLED_INSTANCE;
    bTickCntr = SystemTimer_bGetTickCntr();
        
    if (bMode >= BLINKINGLED_BLINK) 
	{    // If Value is 2 or great, put LED in blink mode.
        if ( (BYTE)(bTickCntr - BlinkingLedCount[bInstance]) >= pPBlock->BLINKINGLED_BlinkRate) 
	    {
            BlinkingLedCount[bInstance] = bTickCntr;
            BlinkingLedState[bInstance] ^= BLINKINGLED_ON;
        }
        bMode = BlinkingLedState[bInstance];
    }
	else 
	{
	    BlinkingLedCount[bInstance] = bTickCntr;
		BlinkingLedState[bInstance] = BLINKINGLED_OFF;  // The LED  starts in the Off state in the blink mode
	}
	
    bInstance = DIOChannelPins[(pPBlock->BLINKINGLED_ChannelID)]; // Store the PinPort to the bInstance 
	if ((pPBlock->BLINKINGLED_DRIVESENSE ^ bMode ) == 0) 
	{   // Set LED to ON or OFF depending on mode.
        bTickCntr = BLINKINGLED_ON;
    } 
    else 
	{
        bTickCntr = BLINKINGLED_OFF;
    }
    DISetPin(bInstance, bTickCntr);
}
