;;*****************************************************************************
;;*****************************************************************************
;;  adcMux8.asm
;;  @Version@
;;  
;;
;;  DESCRIPTION: Low level 8 channel mux for any incremental ADC.  This 
;;               code provides storage for 8 integer results and defines
;;               the simple state machine to collect the data.
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************
include "m8c.inc"
include "memory.inc"
include "Driverdecl.inc"
include "ADC.inc"

export  AdcScanReset
export _AdcScanReset

export  AdcScan 
export _AdcScan 

export  iGetAinChanCounts
export _iGetAinChanCounts

export  aiADC_Results
export _aiADC_Results

export  bADC_Index    
export _bADC_Index    

export  REF_TABLE
export _REF_TABLE

export  MVOLTSChannelPins
export _MVOLTSChannelPins

AREA InterruptRAM (RAM, REL, CON)

_bTemp:
 bTemp:               BLK   2

_bADC_Index:  
 bADC_Index:          BLK   1                ; Index of next scan

_aiADC_Results:                             ; ADC result RAM allocation
 aiADC_Results:       BLK   (2*CMX_MVOLTS_IN_CHAN_COUNT) ; 2 Bytes per channel
 

AREA UserModules (ROM, REL)

.literal
_MVOLTSChannelPins:
 MVOLTSChannelPins:
    db 00h // MVOLTS_00 connects to Port_0_0

 REF_TABLE:
_REF_TABLE:
	db 0x6 // MVOLTS_00 reference range Vss to 2.6V

GAIN_TABLE:
_GAIN_TABLE:
    db 248 // PGA_G1_00

FILTER_TABLE:
_FILTER_TABLE:
    db 00 // Disable

.endliteral

;-----------------------------------------------------------------------------
;  FUNCTION NAME: void AdcGetReset( void )
;
;  DESCRIPTION:
;    Reset scanning index to zero, and set up input mux for next reading.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     none
;
;  RETURNS:  
;     none
;
;  SIDE EFFECTS:
;    REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;-----------------------------------------------------------------------------
 AdcScanReset:
_AdcScanReset:
   RAM_PROLOGUE RAM_USE_CLASS_4
   RAM_SETPAGE_CUR >bADC_Index
   mov    A,0x00
   mov    [bADC_Index],A              ; Reset index

   // Set up gain for first channel
   index GAIN_TABLE             ; Get mux value of first channel
      lcall ADCBUF_SetGain
      
   // Set up mux for first channel
   index  MVOLTSChannelPins             ; Get mux value of first channel
      lcall AMUX8_InputSelect
  
   // Set up the Reference mux for the first reading
   mov   A,>REF_TABLE                ; Place the REF_TABLE MSB in A
   mov   X,<REF_TABLE                ; Place the REF_TABLE LSB in X
   romx                              ; Get index value from reference table
   asl   A                           ; Shift mode 3 bits to left, to fit register alightment
   asl   A
   asl   A
   and   A,ARF_CR_REF                ; Mask off all non-valid bits
   mov   X,SP
   push  A
   mov   A,reg[ARF_CR]
   and   A,~ARF_CR_REF     ; Mask out reference setting
   or    A,[X]               ; Set new reference setting
   mov   reg[ARF_CR],A
   pop   A
   RAM_EPILOGUE RAM_USE_CLASS_4
   ret

;-----------------------------------------------------------------------------
;  FUNCTION NAME: int iGetAinChanCounts(BYTE chan)
;
;  DESCRIPTION:
;    Returns an integer value from the ADC scanning system
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     A => Index of value to return
;
;  RETURNS:  
;     A:X  => Integer value returned
;             A = MSB
;             X = LSB
;
;  SIDE EFFECTS:
;    REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;-----------------------------------------------------------------------------
 iGetAinChanCounts:
_iGetAinChanCounts:
   RAM_PROLOGUE RAM_USE_CLASS_3
   RAM_SETPAGE_IDX >aiADC_Results
   and   A,0x07                      ; Mask off invalid values
   asl   A                           ; Mult by 2, since values are ints.
   mov   X,A                         ; Set up for indexed read
   M8C_DisableGInt                   ; Disable global interrupts while retrieving 
                                     ; result to ensure that the MSB and LSB come 
                                     ; from the same result.
   mov   A,[X+aiADC_Results]         ; Get MSB of integer value
   inc   X                           ; Advance to LSB
   mov   X,[X+aiADC_Results]         ; Get LSB of integer value
   M8C_EnableGInt
   swap  A,X                         ; Put MSB in A and LSB in X
   RAM_EPILOGUE RAM_USE_CLASS_3
   ret

;-----------------------------------------------------------------------------
;  FUNCTION NAME: void AdcScan(int iResult)
;
;  DESCRIPTION:
;    Returns an integer value from the ADC scanning system
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;     Latest ADC result
;     A:X  => Integer value returned
;             A = MSB
;             X = LSB
;
;  RETURNS:   none
;
;  SIDE EFFECTS:
;    REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;-----------------------------------------------------------------------------
; A => MSB
; X => LSB
 AdcScan: 
_AdcScan: 
   RAM_PROLOGUE RAM_USE_CLASS_3
   RAM_PROLOGUE RAM_USE_CLASS_4
   RAM_SETPAGE_CUR >bADC_Index
   RAM_SETPAGE_IDX >aiADC_Results
   push  X  ; Push LSB
   push  A  ; Push MSB

   mov   A,[bADC_Index]              ; Get current index
   asl   A                           ; mult by to for correct index  (2 byte ints)
   mov   X,A                         ; Setup index register for result array.
   asr   A	
   index FILTER_TABLE
   jz .no_filter                      ; jump if no filtering required
   
.filter:
   mov A,[X+aiADC_Results]        ; previous MSB
   push A                         ; store previous MSB
   mov [bTemp+HighByte],A         ; prestore MSB
   inc X                          ; point to LSB
   mov A,[X+aiADC_Results]        ; previous LSB
   mov [bTemp+LowByte],A          ; prestore LSB   
   
   asl [bTemp+LowByte]            ; mult LSB by 2 
   rlc [bTemp+HighByte]           ; mult MSB by 2
   
   add [bTemp+LowByte],A          ; add to LSB 
   pop A                          ; restore previous MSB
   adc [bTemp+HighByte],A         ; add to MSB
   
   pop A                          ; New MSB
   add [bTemp+HighByte],A         ; add new MSB
   pop A                          ; New LSB 
   add [bTemp+LowByte],A          ; add new LSB
   adc [bTemp+HighByte],0x00      ; add carry flag
   
   asr [bTemp+HighByte]			  ; dividing sum
   rrc [bTemp+LowByte]            ; by 4 to obtain
   asr [bTemp+HighByte]           ; 3/4 + 1/4
   rrc [bTemp+LowByte]            ; filter

   mov A,[bTemp+LowByte]
   mov [X+aiADC_Results],A        ; write LSB to array
   dec X                          ; point to MSB
   mov A,[bTemp+HighByte]
   mov [X+aiADC_Results],A        ; write MSB to array
      
   jmp .next_scan
   
.no_filter:
   pop   A                           ; Pop MSB of result
   mov   [X+aiADC_Results],A         ; Write it to array
   inc   X                           ; Advance pointer to LSB   
   pop   A                           ; Pop LSB of result
   mov   [X+aiADC_Results],A         ; Write it to array
   
.next_scan:

   inc   [bADC_Index]                ; Advance to next value
   cmp   [bADC_Index],(CMX_MVOLTS_IN_CHAN_COUNT) ; Check to see if we have if index should be reset.
   jc    .SetRefMux
   mov   [bADC_Index],0x00           ; Reset index back to zero

   // Set next reference setting
.SetRefMux:
   mov   X,>REF_TABLE                ; Get MSB of REF_TABLE        ( Swap Later )
   mov   A,<REF_TABLE                ; Put LSB of REF_TABLE in A   ( Swap later )
   add   A,[bADC_Index]              ; Add the ADC index value
   swap  A,X                         ; Move MSB to A, and LSB to X
   adc   A,0                         ; Add the carry from the add of the REF_TABLE LSB
                                     ; and ADC_Index.
   romx                              ; Get index value from reference table
   asl   A                           ; Shift mode 3 bits to left, to fit register alightment
   asl   A
   asl   A
   and   A,ARF_CR_REF                ; Mask off all non-valid bits
IF (SYSTEM_LARGE_MEMORY_MODEL)
   mov   reg[IDX_PP],SYSTEM_STACK_PAGE  ; Make sure index uses stack
ENDIF
   mov   X,SP
   push  A
   mov   A,reg[ARF_CR]
   and   A,~ARF_CR_REF              ; Mask out reference setting
   or    A,[X]                      ; Set new reference setting
   mov   reg[ARF_CR],A
   pop   A

SetGain:
   mov   A,[bADC_Index]             ; Get current channel ID
   index GAIN_TABLE          ; Get port/pin value of channel
   RAM_CHANGE_PAGE_MODE( FLAG_PGMODE_11b )
      lcall ADCBUF_SetGain
   RAM_RESTORE_NATIVE_PAGING	; Restore native paging
   
SetMux8:
   mov   A,[bADC_Index]             ; Get current index
   index MVOLTSChannelPins          ; Get port/pin value of channel
   RAM_CHANGE_PAGE_MODE( FLAG_PGMODE_11b )
      lcall AMUX8_InputSelect
   RAM_RESTORE_NATIVE_PAGING	; Restore native paging
AdcScanEnd:   
   RAM_EPILOGUE RAM_USE_CLASS_4
   RAM_EPILOGUE RAM_USE_CLASS_3
   ret

