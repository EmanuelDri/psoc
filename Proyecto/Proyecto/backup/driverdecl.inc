//----------------------------------------------------------------------------
// DriverDecl.inc
//----------------------------------------------------------------------------

include "m8c.inc"
include "memory.inc"

; default case, will be redefined below if PWM channels are used
IF(TOOLCHAIN & HITECH) 
	; in HI-TECH, symbols can't be re-equated, so set must be used.
	CMX_PWM_CHAN_COUNT: set 0
ELSE
	CMX_PWM_CHAN_COUNT: equ 0 
ENDIF

; Driver ID's
ID_pse_Elecede:	equ	1

; Channel ID's
ID_LCD_00:		equ 0
ID_LCD_SHARED_01:		equ 1
ID_SHADOWREGS_0:		equ 0
ID_SHADOWREGS_1:		equ 1
ID_SHADOWREGS_2:		equ 2

; Channel Type maximum count
CMX_LCD_CHAN_MAX:	equ	1
CMX_LCD_SHARED_CHAN_MAX:	equ	1
CMX_SHADOW_MAX:	equ	3

; Channel Type instantiation count
CMX_LCD_CHAN_COUNT:	equ	1
CMX_LCD_SHARED_CHAN_COUNT:	equ	0
CMX_SHADOW_COUNT:	equ	0
