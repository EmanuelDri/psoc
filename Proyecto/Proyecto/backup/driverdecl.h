//----------------------------------------------------------------------------
// DriverDecl.h
//----------------------------------------------------------------------------

#include <m8c.h>        // part specific constants and macros
#include "PSoCAPI.h"    // PSoC API definitions for all User Modules
#include "CMX_LCDSCROLLINGTEXT.h"

// Driver ID's
#define ID_pse_Elecede 1

// Channel ID's
#define ID_LCD_00 0
#define ID_LCD_SHARED_01 1
#define ID_SHADOWREGS_0 0
#define ID_SHADOWREGS_1 1
#define ID_SHADOWREGS_2 2

// Channel Type maximum count
#define CMX_LCD_CHAN_MAX 1
#define CMX_LCD_SHARED_CHAN_MAX 1
#define CMX_SHADOW_MAX 3

// Channel Type instantiation count
#define CMX_LCD_CHAN_COUNT 1
#define CMX_LCD_SHARED_CHAN_COUNT 0
#define CMX_SHADOW_COUNT 0

// Driver Parameter Block externs
extern CMX_LCDSCROLLINGTEXT_ParameterBlock const pse_Elecede;
