//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_LCDSCROLLINGTEXT.h
//   Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the LCDSCROLLINGTEXT Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_LCDSCROLLINGTEXT_include
#define CMX_LCDSCROLLINGTEXT_include

#include <M8C.h>
#define LCDSCROLLINGTEXT_pse_Elecede_ORDINAL  0
#define CMX_LCDSCROLLINGTEXT_COUNT 1
// parameter block
typedef struct
{
	BYTE bInstance;
	BYTE bNumberRows; 				// Number of rows
	const char *pScrollingText;  	// Text for scrolling
	BYTE bStep_time;				// Step tyme 
}CMX_LCDSCROLLINGTEXT_ParameterBlock;

// Function Prototypes
void CMX_LCDSCROLLINGTEXT_SetValue(const CMX_LCDSCROLLINGTEXT_ParameterBlock * thisBLK,BYTE Value);
void CMX_LCDSCROLLINGTEXT_Instantiate(const CMX_LCDSCROLLINGTEXT_ParameterBlock * thisBLK);

#endif
