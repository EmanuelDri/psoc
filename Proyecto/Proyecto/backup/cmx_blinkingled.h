//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_BlinkingLed.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the BlinkingLed Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef CMX_BlinkingLed_include
#define CMX_BlinkingLed_include
#include "cmx_dio_chan.h" 
#include <M8C.h>

#define BLINKINGLED_pse_LED_ORDINAL  0
#define CMX_BLINKINGLED_COUNT 1

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE BLINKINGLED_ID;			// Instance ID
    BYTE BLINKINGLED_BlinkRate;		// Blinking Rate
	BYTE BLINKINGLED_DRIVESENSE;	// Active state of pin		
    BYTE BLINKINGLED_INITIALSTATE;	// Initial pin state
	BYTE BLINKINGLED_DriveMode;		//PSoC drive mode 0 - 7	
    BYTE BLINKINGLED_INSTANCE;
    BYTE BLINKINGLED_ChannelID;
}CMX_BLINKINGLED_ParameterBlock;

//*********************
// Function Prototypes
//*********************
void CMX_BLINKINGLED_SetValue(const CMX_BLINKINGLED_ParameterBlock * pPBlock, BYTE bMode);
void CMX_BLINKINGLED_Instantiate(const CMX_BLINKINGLED_ParameterBlock * pPBlock);

#endif
