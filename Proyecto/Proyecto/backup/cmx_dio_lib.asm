;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME: DIO_LLD.asm
;;   Version: 1.0.0
;;
;;  DESCRIPTION:
;;    Digital Input Output algorithms.
;;
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
;;*****************************************************************************
;;  HISTORY:
;;
;;*****************************************************************************
include "m8c.inc"
include "memory.inc"
include "cmx_dio_lib.inc"
;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------
export  _GlobalSelectConnect
export   GlobalSelectConnect

export  _GlobalSelectDisconnect
export   GlobalSelectDisconnect

export  _DiscreteDriveMode
export   DiscreteDriveMode

export  _SetDriveModePortPin
export   SetDriveModePortPin

export  _SetPin
export   SetPin

export  _DISetPin
export   DISetPin

export  _DiscreteReadPin
export   DiscreteReadPin

export  _pinlu;
export   pinlu

export  _portlu
export   portlu

;-----------------------------------------------
;  Function Prototypes
;-----------------------------------------------

;-----------------------------------------------
;  Constant Definitions
;-----------------------------------------------
DRIVE_MODE_ZERO_OFFSET:     equ 0x00   ; BANK 1
DRIVE_MODE_ONE_OFFSET:      equ 0x01
GLOBAL_SELECT_OFFSET:       equ 0x02
DRIVE_MODE_TWO_OFFSET:      equ 0x03
DATA_REGISTER_OFFSET:       equ 0x00   ; BANK 0 


area text(ROM,REL)
.literal
_pinlu:
 pinlu:  db 01h, 02h, 04h, 08h, 10h, 20h, 40h, 80h
_portlu:
 portlu: db 00h, 04h, 08h, 0Ch, 10h, 14h, 18h, 1Ch
.endliteral

;-----------------------------------------------
; Variable Allocation
;-----------------------------------------------
AREA InterruptRAM (RAM, REL, CON)

area    text(ROM,REL)

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: GlobalSelectConnect
;
;  DESCRIPTION:
;    Sets the appropriate bit of the PRTxGS register to connect the pin to the 
;    global bus.
;
;-----------------------------------------------------------------------------
;   ARGUMENTS: 
;     A => pin mask  (ie  0 => 0x01, 1 => 0x02, 2 => 0x04, etc)
;     X => Address of actual port.  
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Sets Global Select register for pin
;------------------------------------------------------------------------------
; Stack offset constants
    GSpin:          equ  -2

_GlobalSelectConnect:
 GlobalSelectConnect:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  A                                ; store inverted the pin_mask to stack
    push  X                                ; store address of actual port to stack
    mov   A,reg[X+GLOBAL_SELECT_OFFSET]    ; mov GS pin into A
    mov   X,SP                             ; 
    or    A,[X + GSpin]                    ; and with inverted mask
    pop   X                                ; restore address of actual port from stack   
    mov   reg[X+GLOBAL_SELECT_OFFSET],A    ; write back to GS pin      
    pop   A
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: GlobalSelectDisconnect
;
;  DESCRIPTION:
;    Clears the appropriate bit of the PRTxGS register to disconnect the pin 
;    from the global bus.
;-----------------------------------------------------------------------------
;  ARGUMENTS: 
;    A => pin mask  (ie  0 => 0x01, 1 => 0x02, 2 => 0x04, etc)
;    X => Address of actual port.  
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Clears Global Select register for pin
;------------------------------------------------------------------------------
; Stack offset constants
    GSpin:          equ -2
    
_GlobalSelectDisconnect:
 GlobalSelectDisconnect:
    RAM_PROLOGUE RAM_USE_CLASS_2
    cpl   A                                ; invert pin mask
    push  A                                ; store inverted the pin_mask to stack
    push  X                                ; store address of actual port to stack
    mov   A,reg[X+GLOBAL_SELECT_OFFSET]    ; mov GS pin into A
    mov   X,SP                             ; 
    and   A,[X + GSpin]                    ; and with inverted mask
    pop   X                                ; restore address of actual port from stack   
    mov   reg[X+GLOBAL_SELECT_OFFSET],A    ; write back to GS pin      
    pop   A
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION	


.SECTION	
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SetDriveMode
;
;  DESCRIPTION: Sets the drive mode of a port pin to the setting passed in X
;-----------------------------------------------------------------------------
;  ARGUMENTS: port_pin passed in A, drive_mode passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: Three registers control the drive mode:
;  PRTxDM0, PRTxDM1, and PRTxDM2. These registers are set by the setting passed in.
;  This function uses 5 bytes on the stack: four as locals, one push/pop.
;------------------------------------------------------------------------------

_SetDriveModePortPin:
 SetDriveModePortPin:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  X                     ; Store DM into stack
    mov   X,A                   ; Store port_pin to X
    and   A,0x70                ; A has port*16  
    asr   A                     ; A has port*8  
    asr   A                     ; A has port*4 which is the register address
    push  A                     ; store port register address into stack
    mov   A,X                   ; restore port_pin to A
    and   A,0x07                ; isolate the pin
    index pinlu                 ; takes pin mask
    push  A                     ; Store pin_mask into stack
    call  DiscreteDriveMode
    add   SP,-3
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION	


.SECTION 
;-----------------------------------------------------------------------------
;  FUNCTION NAME: DiscreteDriveMode
;
;  DESCRIPTION:
;    Sets the drive mode for the pin
;-----------------------------------------------------------------------------
;  ARGUMENTS:  ( FASTCALL16 format)
;    [SP-3] => pin (ie  0 => 0x01, 1 => 0x02, 2 => 0x04, etc)
;    [SP-4] => Actual port address
;    [SP-5] => drive mode
; 
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: 
;------------------------------------------------------------------------------
; Stack offset constants
    DriveMode:      equ -6
    DMport:         equ -5  
    DMpin:          equ -4       
    set_value_dm:   equ -4
    clear_value_dm: equ -1

_DiscreteDriveMode:
 DiscreteDriveMode:
    RAM_PROLOGUE RAM_USE_CLASS_2

    push  A                               ; SP is at +1
    mov   X,SP                            ; get frame pointer + 1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Get the bit mask
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   A,[X+DMpin]                     ; A has pin
    mov   [X+set_value_dm],A              ; get the bit mask
    cpl   A                               ; prepare to clear bits with 'and'
    mov   [X+clear_value_dm],A            ; write bit mask for clearing

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM0, Bank1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
    M8C_SetBank1
    mov   X,[X+DMport]
    mov   A,reg[X+DRIVE_MODE_ZERO_OFFSET] ; get PRTxDM0 current value
    mov   X,SP                            ; get frame pointer + 1
    tst   [X+DriveMode],0x01              ; test bit0 of DMode
    jz    .clear_dm0
    or    A,[X+set_value_dm]              ; set masked bit of PRTxDM0
    jmp   .done_set_dm0

.clear_dm0:
    and   A,[X+clear_value_dm]            ; clear masked bit 0f PRTxDM0

.done_set_dm0:
    mov   X,[X+DMport]                    ; get the register address back 
    mov   reg[X+DRIVE_MODE_ZERO_OFFSET],A ; move new value into PRTxDM0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM1, already in Bank1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    mov   A,reg[X+DRIVE_MODE_ONE_OFFSET]  ; get PRTxDM1 current value
    mov   X,SP                            ; get the frame pointer + 1
    tst   [X+DriveMode], 0x02             ; test bit1 of DMode
    jz    .clear_dm1
    or    A,[X+set_value_dm]              ; set masked bit of PRTxDM1
    jmp   .done_set_dm1

.clear_dm1:
    and   A, [X+clear_value_dm]           ; clear masked bit of PRTxDM1

.done_set_dm1:
    mov   X,[X+DMport]                    ; get the register address back 
    mov   reg[X+DRIVE_MODE_ONE_OFFSET],A  ; move new value into PRTxDM1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; set or clear in PRTxDM2, Bank0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    M8C_SetBank0
    mov   A, reg[X+DRIVE_MODE_TWO_OFFSET] ; get PRTxDM2 current value
    mov   X,SP                            ; get the frame pointer + 1
    tst   [X+DriveMode], 0x04             ; test bit2 of DMode
    jz    .clear_dm2
    or    A,[X+set_value_dm]              ; set masked bit of PRTxDM2
    jmp   .done_set_dm2

.clear_dm2:
    and   A,[X+clear_value_dm]            ; clear masked bit of PRTxDM2

.done_set_dm2:
    mov    X,[X+DMport]                   ; get the register address back 	
    mov   reg[X+DRIVE_MODE_TWO_OFFSET],A  ; move new value into PRTxDM2
    
    add   SP,-1                           ; clean up stack
    RAM_EPILOGUE RAM_USE_CLASS_2
    ret
.ENDSECTION	



.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: DISetPin
;
;  DESCRIPTION:
;    This function is a wrapper for SetPin that disables IRQs
;    before and re-enables IRQs after function is called.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: pin passed in A, port passed in X by fastcall16
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: Data_SHADE RAM image of port written 
;
;  THEORY of OPERATION or PROCEDURE: Sets pin hi
;
;------------------------------------------------------------------------------
_DISetPin:
 DISetPin:
    M8C_DisableGInt                 ;  Disable Global Irqs
    call  SetPin
    M8C_EnableGInt                  ;  Enable Global Irqs
    ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SetPin()
;
;  DESCRIPTION:
;    This function drives a given pin to a high or low state.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: 
;     A  =>  Pin and Port info  pin = A[3:0],  port = A[7:4]
;     X  =>  0x00 => Drive pin Low
;            0x01 => Drive pin high,  
;            0xFF => Invert current pin value,  
;
;  RETURNS:
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: Data_SHADE RAM image of port written 
;
;  THEORY of OPERATION or PROCEDURE: 
;
;
;  IMPORTANT:
;    It is assumed that port shadow registers are sequential.
;------------------------------------------------------------------------------

; Stack offset constants
   SP_value:       equ  -3
   
_SetPin:
 SetPin:
   RAM_PROLOGUE RAM_USE_CLASS_4
   RAM_PROLOGUE RAM_USE_CLASS_2
   RAM_SETPAGE_IDX  >Port_0_Data_SHADE        ; Set the IDX page pointer to the SHADE Ram

    push  X                         ; Store new Pin Value to X
    mov   X,A                       ; Store port_pin to X
    and   A,0x70                    ; A has port*16  
    asr   A                         ; A has port*8  
    asr   A                         ; A has port*4 which is the register address
    push  A                         ; store port register address into stack
    asr   A                         ; A has port*2  
    asr   A                         ; A has port number 
    swap  A,X                       ; port number => X, port_pin => A
    and   A,0x07                    ; isolate the pin
    index pinlu                     ; pin mask => A
    
    push  X                         ; store port number to stack 
    mov   X,SP                      ; get frame pointer + 1
    cmp   [X+SP_value],0x01
    pop   X                         ; restore port number to X
    RAM_X_POINTS_TO_INDEXPAGE       ; Use IDX_PP for indexing      
    jc    OP_Reset_pin
    jz    OP_Set_pin

OP_Inv_pin:                          ; invert pin operation  
    xor   [X+Port_0_Data_SHADE],A
    jmp   Op_Done

OP_Set_pin:                          ; set pin operation
    or    [X+Port_0_Data_SHADE],A
    jmp   Op_Done 

OP_Reset_pin:                        ; reset pin operation
    cpl   A 
    and   [X+Port_0_Data_SHADE],A

Op_Done:
    mov   A,[X+Port_0_Data_SHADE]    ; copy new port value to A 
    pop   X                          ; restore the port address to X
    mov   reg[X],A                   ; Write the new value back to the port     
    pop   A                          ; clean up stack
   RAM_EPILOGUE RAM_USE_CLASS_2
   RAM_EPILOGUE RAM_USE_CLASS_4
   ret
.ENDSECTION


.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: DiscreteReadPin
;
;  DESCRIPTION:
;    Returns a 1 if the particular pin is high, a 0 if it is low.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: ( fastcall16 format )
;    A => pin mask  (ie  0 => 0x01, 1 => 0x02, 2 => 0x04, etc)
;    X => Address of actual port. 
;
;  RETURNS: 
;    State of pin
;
;  SIDE EFFECTS: REGISTERS ARE VOLATILE: THE A AND X REGISTERS MAY BE MODIFIED!
;
;  THEORY of OPERATION or PROCEDURE: 
;
;------------------------------------------------------------------------------
; Stack offset constants
    GSpin_mask:          equ -1
_DiscreteReadPin:
 DiscreteReadPin:
    RAM_PROLOGUE RAM_USE_CLASS_2
    push  A                                     ; save pin_maks into the stack
    mov   A,reg[X+DATA_REGISTER_OFFSET]         ; restore X pointer to DR and load pin contents    
    mov   X,SP                                  ; get frame pointer + 1 
    and   A,[X + GSpin_mask]
    jz    EndChk
    mov   A,0x01                                 ; return 1 if set, return 0 in other case
 EndChk:
    add   SP,-1
    RAM_EPILOGUE RAM_USE_CLASS_2 
    ret
; End of File DIO_LLD.asm
.ENDSECTION	
