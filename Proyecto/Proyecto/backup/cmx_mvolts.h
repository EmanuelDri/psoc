//*****************************************************************************
//*****************************************************************************
//  FILENAME:  CMX_mVolts.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the mVolts Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

// Volts input Parameter block
//
#include <m8c.h>

#ifndef CMX_MVOLTS_DRV_HEADER
#define CMX_MVOLTS_DRV_HEADER

// mVolts input Parameter block

typedef struct
{
   BYTE ID;      // Instance ID.  This ID value will be set upon code generation
                 // and will refer to the index in the array of pointers to 
                 // parameter blocks.

   BYTE InPort;  //  This is actually the index of the input channel to the ADC
                 //  scan array. The scan array in independent of the port or pin
                 //  that the input is on.

   int  ScaleNumerator;    // Volts scaling numerator
   int  ScaleDenominator;  // Volts scaling denominator

}CMX_mVolts_ParameterBlock;

// Function prototypes
void CMX_mVolts_Instantiate( const CMX_mVolts_ParameterBlock * thisBLK );
int  CMX_mVolts_GetValue( const CMX_mVolts_ParameterBlock * thisBLK );


#endif
