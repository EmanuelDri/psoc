//----------------------------------------------------------------------------
// C main line
//----------------------------------------------------------------------------

#include <m8c.h>          // part specific constants and macros
#include "PSoCAPI.h"      // PSoC API definitions for all User Modules

#include "driverdecl.h"
#include "CMXSystem.h"
#include "CMXSystemExtern.h"
#include "TransferFunction.h"

#include "cmx.h"
#include "ProjectProperties.h"
#include "Custom.h"

// Channel includes
// LCD_SHARED_01 Include
#include "cmx_lcd_chan.h"

void main( void )
{
    // Initialize Project
    M8C_EnableGInt;                            // Turn on interrupts
    SystemTimer_Start();
    SystemTimer_SetInterval(SystemTimer_64_HZ);
    SystemTimer_EnableInt();

    // Initialize Channels
    // LCD_SHARED_01 Initialization
    LCDStartSharedChan();
    I2C_CFG &= 0xFC;                           // Disable I2C in case it's not used.
    
    // Initialize Variables
	SystemVars.ReadOnlyVars.pse_Elecede = 1;

    // Driver instantiations
CMX_LCDSCROLLINGTEXT_Instantiate(&pse_Elecede);

    // Custom initization code.
    CustomInit();
    // End Initialize Project

	while(1)
	{
        // Sync loop sample rate
#if ( SAMPLE_DIVIDER  )
        SystemTimer_SyncWait(SAMPLE_DIVIDER, SystemTimer_WAIT_RELOAD);
#endif

		// update input variables


        // Custom Post Input function
        CustomPostInputUpdate();

		// run transfer function and update output variables
		TransferFunction();

        // CustomPreOutputUpdate();
        CustomPreOutputUpdate();

		// set outputs
		CMX_LCDSCROLLINGTEXT_SetValue(&pse_Elecede, (BYTE)SystemVars.ReadOnlyVars.pse_Elecede);

	}
}
