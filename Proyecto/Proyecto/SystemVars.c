//
// SystemVars.c
//
//   Updates system variables
#include "CMXSystem.h"
#include "CMXSystemFunction.h"
#include "SystemConst.h"
#include "FunctionParamDecl.h"
#include "SystemVars.h"

// system variable structure definition
SYSTEM_VARS_STRUC SystemVars;

void pse_Output4_TransferFunction(void)
{
	int outputIndex = SystemVars.ReadOnlyVars.pse_Output4;

	if (1)
	{
		outputIndex = 2;
	}

	SystemVars.ReadOnlyVars.pse_Output4 = outputIndex;


}

