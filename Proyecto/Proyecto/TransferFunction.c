//
//  TransferFunction.c
//

#include <m8c.h>
#include "DriverDecl.h"
#include "CMXSystem.h"
#include "CMXSystemFunction.h"
#include "CMXSystemExtern.h"
#include "FunctionParamDecl.h"

extern void pse_Output4_TransferFunction(void);

void TransferFunction( void)
{
	pse_Output4_TransferFunction();
}
