//----------------------------------------------------------------------------
// DriverDecl.c
//----------------------------------------------------------------------------

#include "DriverDecl.h"
#include "CMXSystemExtern.h"

const CMX_LCDGRAPH_ParameterBlock pse_Output4 = {0,0, 0};

void * const DriverParams[] =
{
	(void *)0,	// ID = 0
	(void *)&pse_Output4, // ID = 1
		// ID_MAX = 2
};
