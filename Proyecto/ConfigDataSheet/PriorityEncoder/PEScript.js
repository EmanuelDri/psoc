//
// Priority Encode change management scripts
//
//
var g_ResultXMLDoc;
var g_VarXMLDoc;
var g_ChangeXMLDoc;
var g_nImpact = 0;			// 0 = No impact, 1 = no teardown, 2 = no teardown null expression 3 = teardown, 4 = teardown w/output state loss
var g_nUpstreamImpact = 0;
var CMXVariable;
var oldCMXVariable;
var g_sNullExpression = "NULL_EXPRESSION";

//
// Input Qualification
//
//   assumes that g_sVarDOM loaded with input candidate DOM
//
//	 returns 1 = input qualified, 0 = input not qualified
//
function InputQual()
{
	return 1;
}

//
// Change Evaluation
//
//   assumes that g_sVarDOM loaded with test variable DOM and g_sChangeDOM loaded with changing variable DOM
//
//   returns g_nImpact
//
function ChangeEval()
{
	// assume no impact
	g_nImpact = 0;

	// set up DOM
	g_ChangeXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_ChangeXMLDoc.validateOnParse	= false;
	g_ChangeXMLDoc.loadXML(g_sChangeDOM);
	g_VarXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_VarXMLDoc.validateOnParse	= false;
	g_VarXMLDoc.loadXML(g_sVarDOM);

	// get the top variable DOM
	CMXVariable = g_ChangeXMLDoc.selectSingleNode("//CMX_NEW_VARIABLE/CMX_VARIABLE");
	oldCMXVariable = g_ChangeXMLDoc.selectSingleNode("//CMX_CURRENT_VARIABLE/CMX_VARIABLE");
	if ((CMXVariable != null) && (oldCMXVariable != null))
	{
		var sVarName = oldCMXVariable.getAttribute("NAME");
		// get the attributes
		var sType = CMXVariable.getAttribute("VALUE_TYPE");
		// get the old attributes
		var sOldType = oldCMXVariable.getAttribute("VALUE_TYPE");
		if ((sOldType == "DISCRETE") || (sOldType == "CMX_DISCRETE"))
		{
			if ((sType == "DISCRETE") || (sType == "CMX_DISCRETE"))
			{
				var newValues = new Array();
				var newValueNodes = CMXVariable.selectNodes("./CMX_VALUE_LIST/CMX_VALUE");
				var nNewLength;
				if (newValueNodes != null)
				{
					// collect the values
					nNewLength = newValueNodes.length;
					for (var ii = 0;ii < nLength;ii++)
					{
						var ValueNode = newValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sName = ValueNode.getAttribute("NAME");
						newValues[nOrder] = sName;
					}
				}
				// get the old value names and check
				var checkValues = new Array();
				var oldValues = new Array();
				var oldValueNodes = oldCMXVariable.selectNodes("./CMX_VALUE_LIST/CMX_VALUE");
				if (oldValueNodes != null)
				{
					// collect the values
					var kk = 0;
					var nLength = oldValueNodes.length;
					for (var ii = 0;ii < nLength;ii++)
					{
						var ValueNode = oldValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sName = ValueNode.getAttribute("NAME");
						oldValues[nOrder] = sName;
						var jj;
						// look for identical names in the old values
						var bUsed = IsValueNameUsed( sVarName, sName);
						// if they are used, then make sure the new values include the old value
						if (bUsed == 1)
						{
							var bFound = 0;
							for (jj in newValues)
							{
								if (newValues[jj] == sName)
								{
									bFound = 1;
								}
							}
							if ((bFound == 0) && (nNewLength != nLength))
							{
								g_nImpact = 2;
							}
						}
					}
				}
			}
			else
			{
				// if the new type is continuous, then make sure no old values are used in expressions
				// get the old values
				var oldValueNodes = oldCMXVariable.selectNodes("./CMX_VALUE_LIST/CMX_VALUE");
				if (oldValueNodes != null)
				{
					// collect the values
					var nLength = oldValueNodes.length;
					for (var ii = 0;(ii < nLength) && (g_nImpact ==0);ii++)
					{
						var ValueNode = oldValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sValueName = ValueNode.getAttribute("NAME");
						var bUsed = IsValueNameUsed( sVarName, sValueName);
						if (bUsed == 1)
						{
							// if it was used, then we need to tear down
							g_nImpact = 2;
						}
					}
				}
			}
		}
		else
		{
			// if the old type was continuous, then we don't care about the change
			g_nImpact = 0;
		}

	}
	else
	{
		g_nImpact = 3;
	}

	return g_nImpact;
}

function ChangeCommit(sInstanceName)
{
	// null expressions using missing value names
	g_nImpact = 0;
	// set up DOM
	g_ChangeXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_ChangeXMLDoc.validateOnParse	= false;
	g_ChangeXMLDoc.loadXML(g_sChangeDOM);
	g_VarXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_VarXMLDoc.validateOnParse	= false;
	g_VarXMLDoc.loadXML(g_sVarDOM);

	// get the top variable DOM
	CMXVariable = g_ChangeXMLDoc.selectSingleNode("//CMX_NEW_VARIABLE/CMX_VARIABLE");
	oldCMXVariable = g_ChangeXMLDoc.selectSingleNode("//CMX_CURRENT_VARIABLE/CMX_VARIABLE");
	if ((CMXVariable != null) && (oldCMXVariable != null))
	{
		var sVarName = oldCMXVariable.getAttribute("NAME");
		// get the attributes
		var sType = CMXVariable.getAttribute("VALUE_TYPE");
		// get the old attributes
		var sOldType = oldCMXVariable.getAttribute("VALUE_TYPE");
		if ((sOldType == "DISCRETE") || (sOldType == "CMX_DISCRETE"))
		{
			if ((sType == "DISCRETE") || (sType == "CMX_DISCRETE"))
			{
				var newValues = new Array();
				var newValueNodes = CMXVariable.selectNodes("./CMX_VALUE_LIST/CMX_VALUE");
				var nNewLength;
				if (newValueNodes != null)
				{
					// collect the values
					nNewLength = newValueNodes.length;
					for (var ii = 0;ii < nLength;ii++)
					{
						var ValueNode = newValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sName = ValueNode.getAttribute("NAME");
						newValues[nOrder] = sName;
					}
				}
				// get the old value names and check
				var checkValues = new Array();
				var oldValues = new Array();
				var oldValueNodes = oldCMXVariable.selectNodes("./CMX_VALUE_LIST/CMX_VALUE");
				if (oldValueNodes != null)
				{
					// collect the values
					var kk = 0;
					var nLength = oldValueNodes.length;
					for (var ii = 0;ii < nLength;ii++)
					{
						var ValueNode = oldValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sName = ValueNode.getAttribute("NAME");
						oldValues[nOrder] = sName;
						var jj;
						// look for identical names in the old values
						var bUsed = IsValueNameUsed( sVarName, sName);
						// if they are used, then make sure the new values include the old value
						if (bUsed == 1)
						{
							var bFound = 0;
							for (jj in newValues)
							{
								if (newValues[jj] == sName)
								{
									bFound = 1;
								}
							}
							if ((bFound == 0) && (nNewLength != nLength))
							{
								ResetExpressionUsing( sVarName, sName);
								g_nImpact = 2;
							}
						}
					}
				}
			}
			else
			{
				// if the new type is continuous, then make sure no old values are used in expressions
				// get the old values
				var oldValueNodes = oldCMXVariable.selectNodes("//CMX_VALUE_LIST/CMX_VALUE");
				if (oldValueNodes != null)
				{
					// collect the values
					var nLength = oldValueNodes.length;
					for (var ii = 0;(ii < nLength) && (g_nImpact ==0);ii++)
					{
						var ValueNode = oldValueNodes[ii];
						var nOrder = ValueNode.getAttribute("ORDER");
						var sValueName = ValueNode.getAttribute("NAME");
						ResetExpressionUsing( sVarName, sValueName);
					}
				}
			}
		}
		else
		{
			// if the old type was continuous, then we don't care about the change
			g_nImpact = 0;
		}

	}

	return g_nImpact;
}

//
// evaluates the variable for delete instance name
//
function DeleteEval( sInstanceName)
{
	g_nImpact = 0;	// assume no teardown

	g_VarXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_VarXMLDoc.validateOnParse	= false;
	g_VarXMLDoc.loadXML(g_sVarDOM);

	var bIsVarUsed = IsValueNameUsed( sInstanceName, "");
	if (bIsVarUsed == 1)
	{
		g_nImpact = 2;	// no teardown null expression
	}

	return g_nImpact;
}

//
// evaluates the variable for delete instance name
//
function DeleteCommit( sInstanceName)
{
	g_nImpact = 0;	// assume no teardown

	g_VarXMLDoc = new ActiveXObject("Microsoft.XMLDOM");
	g_VarXMLDoc.validateOnParse	= false;
	g_VarXMLDoc.loadXML(g_sVarDOM);

	var bIsVarUsed = ResetExpressionUsing( sInstanceName, "");
	if (bIsVarUsed == 1)
	{
		g_nImpact = 2;	// no teardown null expression
	}

	return g_nImpact;
}

//
// retrieves the variable DOM, when loaded
//
function GetVarDOM()
{
	var sReturnDOM;

	if (g_VarXMLDoc != null)
	{
		sReturnDOM = g_VarXMLDoc.xml;
	}

	return sReturnDOM;
}

//
// returns up stream impact after ChangeEval() run
//
function GetUpStreamImpact()
{
	return g_nUpstreamImpact;
}

//
// returns impact based on dependent input impact
//
function GetImpact(downStreamImpact)
{
	var nRetVal = 0;

	if (downStreamImpact >= 3)
	{
		nRetVal = 2;	// null summary
	}

	return nRetVal;
}

function GetResultDOM()
{
	return g_sResultDOM;
}

function IsValueNameUsed(sVarName, sValueName)
{
	// assume not used
	var nRetVal = 0;	

	if (g_VarXMLDoc != null)
	{
		var sFullName;
		if (sValueName != "")
		{
			sFullName = sVarName + "__" + sValueName;
		}
		else
		{
			sFullName = sVarName;
		}
		// get all the expression nodes
		var expressionNodes = g_VarXMLDoc.selectNodes("//CMX_EXPRESSION_LIST/CMX_EXPRESSION");
		var nLength = expressionNodes.length;
		// scan the expressions to see if the vlaues are used
		for (var ii=0;(ii < nLength) && (nRetVal == 0);ii++)
		{
			var expressionNode = expressionNodes[ii];
			if (expressionNode != null)
			{
				// test the left expression
				var sExpression = expressionNode.getAttribute("LEFT_EXPRESSION");
				var sFound;
				if (sExpression != null)
				{
					sFound = sExpression.match( sFullName);
					if (sFound != null)
					{
						// found at least one
						nRetVal = 1;
					}
				}
				// test the right expression
				sExpression = expressionNode.getAttribute("RIGHT_EXPRESSION");
				if (sExpression != null)
				{
					sFound = sExpression.match( sFullName);
					if (sFound != null)
					{
						// found at least one
						nRetVal = 1;
					}
				}
			}
		}
	}

	return nRetVal;
}

function ResetExpressionUsing(sVarName, sValueName)
{
	// assume not used
	var nRetVal = 0;	

	if (g_VarXMLDoc != null)
	{
		var sFullName;
		if (sValueName != "")
		{
			sFullName = sVarName + "__" + sValueName;
		}
		else
		{
			sFullName = sVarName;
		}
		// get all the expression nodes
		var expressionNodes = g_VarXMLDoc.selectNodes("//CMX_EXPRESSION_LIST/CMX_EXPRESSION");
		var nLength = expressionNodes.length;
		// scan the expressions to see if the vlaues are used
		for (var ii=0;ii < nLength;ii++)
		{
			var expressionNode = expressionNodes[ii];
			if (expressionNode != null)
			{
				// test the left expression
				var sExpression = expressionNode.getAttribute("LEFT_EXPRESSION");
				var sFound;
				if (sExpression != null)
				{
					sFound = sExpression.match( sFullName);
					if (sFound != null)
					{
						// found at least one
						expressionNode.setAttribute("LEFT_EXPRESSION", g_sNullExpression);
						nRetVal = 1;
					}
				}
				// test the right expression
				sExpression = expressionNode.getAttribute("RIGHT_EXPRESSION");
				if (sExpression != null)
				{
					sFound = sExpression.match( sFullName);
					if (sFound != null)
					{
						// found at least one
						expressionNode.setAttribute("RIGHT_EXPRESSION", g_sNullExpression);
						nRetVal = 1;
					}
				}
			}
		}
	}

	return nRetVal;
}