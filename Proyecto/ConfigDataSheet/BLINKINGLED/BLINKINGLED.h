//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_BlinkingLed.h
//  Version: 1.2, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the BlinkingLed Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_BlinkingLed_include
#define `@LIB_NAME`_BlinkingLed_include
#include "cmx_dio_chan.h" 
#include <M8C.h>

`@BLINKINGLED_ORDINAL_DEFINE_BLOCK`
#define `@LIB_NAME`_BLINKINGLED_COUNT `@BLINKINGLED_COUNT`

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
    BYTE BLINKINGLED_ID;			// Instance ID
    BYTE BLINKINGLED_BlinkRate;		// Blinking Rate
	BYTE BLINKINGLED_DRIVESENSE;	// Active state of pin		
    BYTE BLINKINGLED_INITIALSTATE;	// Initial pin state
	BYTE BLINKINGLED_DriveMode;		//PSoC drive mode 0 - 7	
    BYTE BLINKINGLED_INSTANCE;
    BYTE BLINKINGLED_ChannelID;
}`@LIB_NAME`_BLINKINGLED_ParameterBlock;

//*********************
// Function Prototypes
//*********************
void `@LIB_NAME`_BLINKINGLED_SetValue(const CMX_BLINKINGLED_ParameterBlock * pPBlock, BYTE bMode);
void `@LIB_NAME`_BLINKINGLED_Instantiate(const CMX_BLINKINGLED_ParameterBlock * pPBlock);

#endif
