;
; BLINKINGLEDFrags.tpl
;
; BLINKINGLED driver fragment definition file
;
;

;@CMX_FRAG_BLINKINGLED_PARAMBLOCK_EXTERN
extern `@DRIVER_LIB_NAME`_BLINKINGLED_ParameterBlock const `@DRIVER_INSTANCE`;
;@CMX_SEGMENT_END

;@CMX_FRAG_BLINKINGLED_DECLARATION
const `@DRIVER_LIB_NAME`_BLINKINGLED_ParameterBlock `@DRIVER_INSTANCE` = { ID_`@DRIVER_INSTANCE`, `@DRIVER_BlinkRate`, `@DRIVER_Current Mode`, `@DRIVER_InitialState`, `@DRIVER_DriveMode`, BLINKINGLED_`@DRIVER_INSTANCE`_ORDINAL, ID_`@CHANNEL_NAME`};
;@CMX_SEGMENT_END

;@CMX_FRAG_BLINKINGLED_INCLUDE
#include "`@DRIVER_LIB_NAME`_BLINKINGLED.h"
;@CMX_SEGMENT_END

;@CMX_FRAG_BLINKINGLED_INSTANTIATE
`@DRIVER_LIB_NAME`_BLINKINGLED_Instantiate(&`@DRIVER_INSTANCE`);
`@DRIVER_LIB_NAME`_BLINKINGLED_SetValue(&`@DRIVER_INSTANCE`, (BYTE)`@DRIVER_OUTPUT_VALUE`);
;@CMX_SEGMENT_END

;@CMX_FRAG_BLINKINGLED_SET_VALUE
`@DRIVER_LIB_NAME`_BLINKINGLED_SetValue(&`@DRIVER_INSTANCE`, (BYTE)`@DRIVER_OUTPUT_VALUE`);
;@CMX_SEGMENT_END

;@CMX_FRAG_BLINKINGLED_ORDINAL_DEFINE
#define BLINKINGLED_`@DRIVER_INSTANCE`_ORDINAL `@DRIVER_INTRAORDINAL`
;@CMX_SEGMENT_END


