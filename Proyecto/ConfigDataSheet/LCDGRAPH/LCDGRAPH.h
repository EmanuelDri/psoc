//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LCDGRAPH.h
//   Version: 1.3, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  Header file for the LCDGRAPH Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_LCDGRAPH_include
#define `@LIB_NAME`_LCDGRAPH_include

#include <M8C.h>
////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////
// parameter block
typedef struct
{
	BYTE bNumberRows; 	//number of rows
	int iMinRange;  	// minimum range
	int iMaxRange; 		//maximum range
}`@LIB_NAME`_LCDGRAPH_ParameterBlock;

// Function Prototypes
void `@LIB_NAME`_LCDGRAPH_SetValue(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK, int iValue);
void `@LIB_NAME`_LCDGRAPH_Instantiate(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK);


#endif
