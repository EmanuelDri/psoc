//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LCDGRAPH.c
//   Version: 1.3, Updated on 2009/10/29 at 15:6:51
//  
//
//  DESCRIPTION:  This file contains the C code for LCDGRAPH Driver.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "`@LIB_NAME`_LCDGRAPH.h"
#include "cmx.h"
#include "SystemTimer.h"
// Channel type header file
#include "CMX_LCD_CHAN.h"

//define maximum positions in one row (16 columns * 5 positions)
#define MAX_POSITION     80

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDGRAPH_Instantiate(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK)
//
//  DESCRIPTION: 
//	LCDGRAPH Initialization
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//	thisBLK  => Pointer to device parameter block
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//  	Initialization for LCDGRAPH
////////////////////////////////////////////////////////////////////////////////
void `@LIB_NAME`_LCDGRAPH_Instantiate(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK)
{
	//initialize the LCD to display the specific type of horizontal bar grath
	LCD_InitBG(LCD_SOLID_BG); 
}
//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDGRAPH_SetValue(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK, int iValue)
//
//  DESCRIPTION: LCDGRAPH Display  Horisontal Bargrath
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK       => Pointer to parameter block
//    iValue          => integer displaying value
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//	This driver displays on LCD the horizontal bargraph on the selected  row.
//	Minimum and maximum range is chosen as properties by user.
//	MinRange means that  LCD is not fill, MaxRange means that
//	LCD is full.
//	The area fill  the LCD is scaled by using the next equation
// 
//	 ValuePosition-80  iValue-MinRange
// 	-------------  =  ---------------------
//     	80-0         	  MaxRange-MinRange
//
// 	With this equation find a ValuePosition
////////////////////////////////////////////////////////////////////////////////
void `@LIB_NAME`_LCDGRAPH_SetValue(const `@LIB_NAME`_LCDGRAPH_ParameterBlock * thisBLK, int iValue)
{
	long lValuePosition;		// Value Position 
	int iMaxRange,iMinRange;	// Max and Min Range
	BYTE bCurrentPosition;  	//
	
	iMaxRange = thisBLK->iMaxRange; //set maximum range
	iMinRange = thisBLK->iMinRange; //set minimum range
	//if current value is more than minimum range or
	// maximum range is more than minimum range
 	if ((iValue <= iMinRange) || (iMaxRange <= iMinRange))
 	{
 		bCurrentPosition=0;
 	}	
 	else 
 		//if current value is more than maximum range
 		if (iValue >= iMaxRange)
	 	{
	 		bCurrentPosition = MAX_POSITION;	
	 	}
	 	//calculate lValuePosition and bCurrentPosition 
	 	//accordingly the following equations
	 	else
 		{
 			lValuePosition = (long)iValue - iMinRange;
			lValuePosition *= MAX_POSITION;
			bCurrentPosition = lValuePosition/((long)iMaxRange - iMinRange);
 		}
 	
	//draw horizontal bargraph on next row
    LCD_DrawBG(thisBLK->bNumberRows,0,16,bCurrentPosition); 
 
}
