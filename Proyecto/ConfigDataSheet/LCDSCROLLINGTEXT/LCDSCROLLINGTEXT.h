//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LCDSCROLLINGTEXT.h
//   Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the LCDSCROLLINGTEXT Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#ifndef `@LIB_NAME`_LCDSCROLLINGTEXT_include
#define `@LIB_NAME`_LCDSCROLLINGTEXT_include

#include <M8C.h>
`@LCDSCROLLINGTEXT_ORDINAL_DEFINE_BLOCK`
#define `@LIB_NAME`_LCDSCROLLINGTEXT_COUNT `@LCDSCROLLINGTEXT_COUNT`
// parameter block
typedef struct
{
	BYTE bInstance;
	BYTE bNumberRows; 				// Number of rows
	const char *pScrollingText;  	// Text for scrolling
	BYTE bStep_time;				// Step tyme 
}`@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock;

// Function Prototypes
void `@LIB_NAME`_LCDSCROLLINGTEXT_SetValue(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK,BYTE Value);
void `@LIB_NAME`_LCDSCROLLINGTEXT_Instantiate(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK);

#endif
