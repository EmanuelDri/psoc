//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_LCDSCROLLINGTEXT.c
//   Version: 1.0, Updated on 2009/10/29 at 15:6:51
//
//  DESCRIPTION:  This file contains the C code for LCDSCROLLINGTEXT Driver.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "`@LIB_NAME`_LCDSCROLLINGTEXT.h"
#include "CMX.h"
// Channel type header file
#include "CMX_LCD_CHAN.h"
#include "systemtimer.h"

#define LCDSCROLLINGTEXT_EMPTY_STRING "                "
#define LCDSCROLLINGTEXT_OFF 	      0
#define LCDSCROLLINGTEXT_PAUSE 	      2
#define LCDSCROLLINGTEXT_MAXLENGTH 	  32

BYTE `@LIB_NAME`_LCDSCROLL_Offset[`@LIB_NAME`_LCDSCROLLINGTEXT_COUNT];	
BYTE `@LIB_NAME`_LCDSCROLL_DelayCount[`@LIB_NAME`_LCDSCROLLINGTEXT_COUNT];	

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDSCROLLINGTEXT_Instantiate(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK)
//
//  DESCRIPTION: LCDSCROLLINGTEXT Initialization
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to device parameter block
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
// 		Initialize Offset
////////////////////////////////////////////////////////////////////////////////

void `@LIB_NAME`_LCDSCROLLINGTEXT_Instantiate(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK)
{
	BYTE bInstance;	 								// Current  instance 
	bInstance = thisBLK->bInstance;					// Get Instance of this LCDSCROLL driver.
	`@LIB_NAME`_LCDSCROLL_Offset[bInstance] = 0;	// Initialize Offset  of this LCDSCROLL driver
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: LCDSCROLLINGTEXT_SetValue(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK, BYTE Value)
//
//  DESCRIPTION: LCDSCROLLINGTEXT  Display scrolling Text
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK	=> Pointer to parameter block
//    Value		=> State Value ( 0 - OFF , 1 - Scroll, 2 - Pause )
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE:  
//		This driver displays on LCD  scrolling Text string 
//		The scrolling text is controlled by an 8-bit value: 0 = Off ,  1 = Scroll , 2 = Pause.
////////////////////////////////////////////////////////////////////////////////

void `@LIB_NAME`_LCDSCROLLINGTEXT_SetValue(const `@LIB_NAME`_LCDSCROLLINGTEXT_ParameterBlock * thisBLK, BYTE Value)
{

	BYTE i;
	BYTE bInstance;								// Driver Instance
	BYTE arBuffStr[16];							// Internal Text String buffer
	BYTE bCurrentChar; 							// Current Char
	BYTE bReset = 0;  							// Reset Flag			
	BYTE bPtr2_temp;  							// Temporal pointer							
	BYTE bEndStr = LCDSCROLLINGTEXT_MAXLENGTH;	// Size of User String 
	BYTE bPtr1,bPtr2;							// Poinrer for LCD and User String accordingly
	
	bInstance = thisBLK->bInstance;				// Get Instance of this LCDSCROLL driver
	LCD_Position(thisBLK->bNumberRows,0);		// Set position (selected row, column 0)
	 	
	if(Value == LCDSCROLLINGTEXT_OFF)			// If scroll OFF , display an Empty string
	{					
		LCD_PrCString(LCDSCROLLINGTEXT_EMPTY_STRING); 
		// Initialize Offset  of this LCDSCROLL driver
		`@LIB_NAME`_LCDSCROLL_Offset[bInstance] = 0;			
	}
	else	
	{	
		// Set accordingly internal pointers 
		if(`@LIB_NAME`_LCDSCROLL_Offset[bInstance] < 16 )
		{
			bPtr1 = (16 - `@LIB_NAME`_LCDSCROLL_Offset[bInstance]);
			bPtr2 = 0;
		}
		else
		{
			bPtr1 = 0;
			bPtr2 = (`@LIB_NAME`_LCDSCROLL_Offset[bInstance] - 16 );	
		}
		// Pause ,  display  same Text
		if((bPtr2 != 1) || (Value != LCDSCROLLINGTEXT_PAUSE))	
		{	
			// Check to see if enough time has passed to show new character
			if ((BYTE)(SystemTimer_bGetTickCntr() - `@LIB_NAME`_LCDSCROLL_DelayCount[bInstance]) > thisBLK->bStep_time)
			{						
				// Reset the delay counter     
				`@LIB_NAME`_LCDSCROLL_DelayCount[bInstance] = SystemTimer_bGetTickCntr();
				
				bPtr2_temp=bPtr2;
				for(i=0; i<16; i++)					// Main Cycle 
				{
					// Fill with  space character internal text string buffer accordingly 
					if((i < bPtr1)|| (bPtr2_temp >= bEndStr) || (i + bPtr2 > LCDSCROLLINGTEXT_MAXLENGTH))
						arBuffStr[i] = ' ';
					else 
					{	// Get current Char from User String
						bCurrentChar = thisBLK->pScrollingText[bPtr2_temp++];

						if(bCurrentChar == 0)		// If  current Char equally end of String 
						{
							bEndStr = bPtr2_temp;	// Set end of String	
							if(i == 1)						
								bReset = 1;			// Set Reset Flag
							bCurrentChar = ' ';							
						}
							
						if(bCurrentChar == '_')		// Change underscore character
							bCurrentChar = ' ';		// with Space character
						// Save Current Char in Internal Buffer 	
						arBuffStr[i] = bCurrentChar;	
					}	
				}
					
				LCD_PrString(arBuffStr);					// Print Current String 	
					
				`@LIB_NAME`_LCDSCROLL_Offset[bInstance]++;	// Increment Offset
				// If reset Flag set 	
				if((bReset == 1) || (bPtr2 == LCDSCROLLINGTEXT_MAXLENGTH))			
				{										
					//  Initialize Offset  of this LCDSCROLL driver			
					`@LIB_NAME`_LCDSCROLL_Offset[bInstance] = 0;
				}
			}	
		}	
	}			
}

