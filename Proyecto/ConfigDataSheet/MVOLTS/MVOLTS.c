//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_mVolts.c
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  This Driver calls the low level driver and returns the
//                voltage that corresponds to a given channel.
//
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

#include "m8c.h"
#include "`@LIB_NAME`_mVolts.h"
#include "cmx.h"
#include "calibration.h"

// Channel type header file
#include "CMX_MVOLTS_IN_CHAN.h"

// The format of the function should always be the same,
// no matter what low level driver is use.
extern int     iGetChanMVolts(BYTE bChan);


//-----------------------------------------------------------------------------
//  FUNCTION NAME: mVolts_Instantiate(const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    This function doesn't do anything at this time, but is placed here
//    for forward compatibility.
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//-----------------------------------------------------------------------------
void `@LIB_NAME`_mVolts_Instantiate(const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK)
{
   // Blank function
   
}

//-----------------------------------------------------------------------------
//  FUNCTION NAME: mVolts_GetValue(const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK)
//
//  DESCRIPTION:  
//    Blah Blah Blah       
//
//-----------------------------------------------------------------------------
//
//  ARGUMENTS: 
//    thisBLK  => Pointer to ParameterBlock for this instance.
//
//  RETURNS: None
//
//  SIDE EFFECTS: 
//
//  THEORY of OPERATION or PROCEDURE: 
//
//  The voltage output is scaled by using the ScaleNumerator and ScaleDenominator
//  in the parameter block.
//
//                                ScaleNumerator
//  OutputReading = inputVolts * ----------------
//                               ScaleDenominator
//
//
//-----------------------------------------------------------------------------
int  `@LIB_NAME`_mVolts_GetValue(const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK)
{
   BYTE bChan;
   long  lmVolts;

   bChan = thisBLK->InPort;                  // Get channel from PB
   lmVolts = (long)iGetChanMVolts(bChan);

   // Scale the voltage to the proper range
   lmVolts = lmVolts * thisBLK->ScaleNumerator;
   lmVolts = lmVolts / thisBLK->ScaleDenominator;

   lmVolts += imVolts_Chan_Offset[bChan];   // Add user calibration offset
   return((int)lmVolts);                    // Convert to mVolts and return
}

