function MVOLTS_SetValue(sVal, sIO)
{
	var sWidget	= myHashTableIOToWidget[sIO];
	var sID		= sWidget + "_Slider1";
	
	try
	{
		var oldGuy	= g_SvgDoc.getElementById(sID);		
		if(oldGuy !=null)
		{
			var val = parseInt(sVal);

			//convert val to pos
			//
			var hi		= myHashtableMidgetIDToHI[sWidget];
			var lo		= myHashtableMidgetIDToLO[sWidget];

			var ratio	= 1 - (val - lo)/(hi-lo);
			var pos		= ratio * 100;
						
			oldGuy.setAttribute("y1", pos);
			oldGuy.setAttribute("y2", pos);
			
			var s = "[" + pos + "]";
		//	changeText("StatusTextW1", "From Sim " + s);		
						
			{
				var sIDMover = sID +  "Mover"
				var oldGuy2	= g_SvgDoc.getElementById(sIDMover);
			
				if(oldGuy2)
				{
					oldGuy2.setAttribute("y", pos);
					oldGuy2.setAttribute("height", 100-pos);
					var sLabel = "Current zzz ";
					var a = sID.split("_");
					logIt(a[0], a[0] + "_XYText", sLabel, pos);
				}
			}
		}
		else
		{
			
		}	
	}
	catch(kuku)
	{
	}
}



function MVOLTS_addTicks(oldguy)
{
	var sRectData =	" ";

	for(var i=1; i < 10; i++)
	{
		sRectData +=	"M 0 "	+ (i*10) + " l 2 0";
	}
	var bod = g_SvgDoc.createElement("path");
	bod.setAttribute("d", sRectData);
	bod.setAttribute("stroke", "green");
	bod.setAttribute("stroke-width", 1.0);
	
	oldguy.appendChild(bod);
}



function MVOLTS_setHiLo(sTherm, lo, hi)
{
	
	//external interface
	
	WidgetSetMinMax(sTherm, lo, hi);
		
	var nPos	= sTherm.lastIndexOf("Therm");
	var sUnit	= ""; //"rpm";
			
	changeText(sTherm+"_HiText", hi + sUnit);
	changeText(sTherm+"_LowText", lo + sUnit);
	
}







function MVOLTS_WidgetFunc(sInstanceName, bigKaHoonaID, sWidget, min, max,  xTransfrom, yTransfrom, height)
{




	var bigKaHoona	= g_SvgDoc.getElementById(bigKaHoonaID);		
		
	var widgetGroup = g_SvgDoc.createElement("g");
	
	widgetGroup.setAttribute("style", "fill: blue; stroke: #000000; stroke-linecap: round; stroke-linejoin: round; "); 
	widgetGroup.setAttribute("transform", "translate(" + xTransfrom + ", " + yTransfrom + ")");  
	widgetGroup.id = sWidget;  

	widgetGroup.addEventListener( "onmouseup",	endColorDrag,	true);	 
	
	//this group is just for scaling
	var widgetGroup2 = g_SvgDoc.createElement("g");
	widgetGroup2.setAttribute("transform", "scale(.5)");
	
	
	//var sStyle	= style="font-size:12;	fill:orange; stroke: none;	text-anchor:middle"
	//var nodeText = AddGhostText(widgetGroup2, sWidget , 5, -16, sStyle);

	widgetGroup.appendChild(widgetGroup2 );
	bigKaHoona.appendChild(widgetGroup );
	
	var sStyle	= style="font-size:12;	fill:red; stroke: none;	text-anchor:end"
	var nodeText = AddGhostText(widgetGroup2, "", -15, 100, sStyle);
	nodeText.id = sWidget + "_XYText";  
	
	
	var sStyle	= "font-size:12;	fill:green; stroke: none;	text-anchor:end"
	var nodeText = AddGhostText(widgetGroup2, "99", -12, 0, sStyle);
	nodeText.id = sWidget + "_HiText";  
	var nodeText = AddGhostText(widgetGroup2, "0", -12, 100, sStyle);
	nodeText.id = sWidget + "_LowText";  

	MVOLTS_setHiLo(sWidget, min, max);

	
	{
		var myRect;
		myRect = g_SvgDoc.createElement("rect");
									
		myRect.setAttribute("x",		0);
		myRect.setAttribute("y",		0);
		myRect.setAttribute("height",	100);
		myRect.setAttribute("width",	10);

		var newGCurveStyle = myRect.getStyle();
		
		var sColor = "lightsteelblue";

		newGCurveStyle.setProperty("fill", sColor );
					
		widgetGroup2.appendChild(myRect);
	}

	
	{
		var myRect;
		myRect = g_SvgDoc.createElement("rect");
									
		myRect.setAttribute("x",		0);
		myRect.setAttribute("y",		90);
		myRect.setAttribute("height",	10);
		myRect.setAttribute("width",	10);
						
		var sXID = sWidget + "_Slider1Mover";
		myRect.setAttribute("id", sXID);
				
		widgetGroup2.appendChild(myRect);
	}

	
	{
		var myLine;
		myLine = g_SvgDoc.createElement("line");
		myLine.setAttribute("id", sWidget + "_Slider1");

									
		myLine.setAttribute("x1", -5);
		myLine.setAttribute("y1", 90);
		myLine.setAttribute("x2", 15); 
		myLine.setAttribute("y2", 90);

		var sStyle="stroke: magenta; stroke-width: 6.75; stroke-linejoin: bevel;  stroke-linecap: square;" 
		myLine.setAttribute("style", sStyle);
	
		myLine.addEventListener( "mousedown",	startColorDrag, false );		 
		myLine.addEventListener( "mouseup",		endColorDrag, false );		 
		myLine.addEventListener( "mousemove",	doColorDrag, false );		 

		widgetGroup2.appendChild(myLine);
	}
	
	MVOLTS_addTicks(widgetGroup2);

}



