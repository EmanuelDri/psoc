//*****************************************************************************
//*****************************************************************************
//  FILENAME:  `@LIB_NAME`_mVolts.h
//  Version: 1.0, Updated on 2009/10/29 at 15:6:51
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Header file for the mVolts Driver
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************

// Volts input Parameter block
//
#include <m8c.h>

#ifndef `@LIB_NAME`_MVOLTS_DRV_HEADER
#define `@LIB_NAME`_MVOLTS_DRV_HEADER

// mVolts input Parameter block

typedef struct
{
   BYTE ID;      // Instance ID.  This ID value will be set upon code generation
                 // and will refer to the index in the array of pointers to 
                 // parameter blocks.

   BYTE InPort;  //  This is actually the index of the input channel to the ADC
                 //  scan array. The scan array in independent of the port or pin
                 //  that the input is on.

   int  ScaleNumerator;    // Volts scaling numerator
   int  ScaleDenominator;  // Volts scaling denominator

}`@LIB_NAME`_mVolts_ParameterBlock;

// Function prototypes
void `@LIB_NAME`_mVolts_Instantiate( const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK );
int  `@LIB_NAME`_mVolts_GetValue( const `@LIB_NAME`_mVolts_ParameterBlock * thisBLK );


#endif
