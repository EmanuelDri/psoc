/* transform <input xml file> <output xml file> */
  
   /* This array specifies the viewbox for each known
    * pinout. */
   var xformTable = new Array(33);
   xformTable["DEFAULT"]     = "0 0 400 400";
   xformTable["CY8C21020BP"]     = "50 50 350 350";
   xformTable["CY8C21030BP"]     = "50 50 380 380";
   xformTable["CY8C21123"]       = "-140 -180 700 700";
   xformTable["CY8C21223"]       = "-140 -140 700 700";
   xformTable["CY8C21234"]       = "-140 -160 700 700";
   xformTable["CY8C21323"]       = "-140 -140 700 700";
   xformTable["CY8C21323MLF"]    = "-120 -120 680 680";
   xformTable["CY8C21334"]       = "-140 -110 700 700";
   xformTable["CY8C21434MLF"]    = "-90 -120 680 680";
   xformTable["CY8C21534"]       = "-140 -110 700 700";
   xformTable["CY8C21634MLF"]    = "-140 -170 750 750";
   xformTable["CY8C24000BP"]     = "30 60 350 350";
   xformTable["CY8C24090BP"]     = "50 50 500 500";
   xformTable["CY8C24123B"]      = "-140 -170 650 650";
   xformTable["CY8C24223B"]      = "-140 -140 700 700";
   xformTable["CY8C24423B"]      = "-140 -110 700 700";
   xformTable["CY8C24423BMLF"]   = "-90 -90 680 680";
   xformTable["CY8C24794"]       = "-120 -100 850 850";
   xformTable["CY8C27000BP"]     = "30 60 350 350";
   xformTable["CY8C27143B"]      = "-130 -150 750 750";
   xformTable["CY8C27243B"]      = "-190 -150 750 750";
   xformTable["CY8C27443B"]      = "-140 -100 650 650";
   xformTable["CY8C27543B"]      = "-120 -50 800 800";
   xformTable["CY8C27643B"]      = "-140 60 650 650";
   xformTable["CY8C27643BMLF"]   = "-80 -40 780 780";
   xformTable["CY8C29000BP"]     = "30 60 350 350";
   xformTable["CY8C29466"]       = "-150 -150 800 800";
   xformTable["CY8C29566"]       = "-110 -60 800 800";
   xformTable["CY8C29666"]       = "-150 60 650 650";
   xformTable["CY8C29666MLF"]    = "-90 -40 750 750";
   xformTable["CY8C29866"]       = "-150 -180 1200 1200";
   



   var argv = WScript.Arguments;
   var fso  = WScript.CreateObject("Scripting.FileSystemObject");

   /* Load the specified SVG file. */
   var xmlFile = fso.OpenTextfile(argv(0),1);
   var sXmlDoc = xmlFile.ReadAll();
   xmlFile.Close();

   /* Determine the part # from the specified schematic XML file. */
   var xmlDoc = new ActiveXObject("Msxml2.DOMDocument");
   xmlDoc.async = false;
   xmlDoc.preserveWhiteSpace = true;
	xmlDoc.load(argv(2));
   var node = xmlDoc.selectSingleNode("//CMX_PROJECT/BASEPROJ_FILE_PATH/@BASEPROJ_PSOC_PART");
   var part = node.text;

   /* Set the viewbox to one of the known settings or the default. */
   var viewBx = xformTable["DEFAULT"];
   if (part != null && part != "" && typeof(xformTable[part]) != 'undefined')
   {
      viewBx = xformTable[part];
   }

   WScript.Echo("BASEPROJ_PSOC_PART = " + part);
   WScript.Echo("XFORM = " + viewBx);

   /* Replace the viewbox setting in the SVG file with a new one. */
   sXmlDoc = sXmlDoc.replace(/xmlns:xmlns="http:\/\/www\.w3\.org\/2000\/svg-20000303-stylable"/,
      'xmlns:svg="http://www.w3.org/2000/svg"');
   sXmlDoc = sXmlDoc.replace(/<svg .*>/, '<svg xmlns:svg="http://www.w3.org/2000/svg" font-family="Helvetica" viewBox="' + viewBx + '" width="6.8in" height="6in" preserveAspectRatio="xMidYMid" zoomAndPan="magnify">');

   /* Save the file to the specified output file. */
   var tf = fso.CreateTextFile(argv(1), true, false);
   tf.Write(sXmlDoc);
   tf.Close();
