<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="user">

<msxsl:script language="JScript" implements-prefix="user">
   <![CDATA[
            function ReplaceFxn(str, pattern, replacement)
				{
					return str.replace(pattern, replacement);
				}
            
            function ReplaceLineBreak(str)
				{
               var re = /\\n/g;
               var ret = str.replace(re, '</fo:block><fo:block>');

               re = /<fo:block><\/fo:block>/g;
               return ret.replace(re, '<fo:block padding-before="10px"></fo:block>');
				}
   ]]>
</msxsl:script>
                

<xsl:output method="xml"
            version="1.0"
            encoding="UTF-8"
            indent="yes"/>

<xsl:template match="/">
            
	<fo:root writing-mode="lr-tb" hyphenate="false" text-align="start" role="html:html" id="main" 
			 xmlns:fo="http://www.w3.org/1999/XSL/Format" 
			 xmlns:html="http://www.w3.org/1999/xhtml">
	
		<!-- Define the layouts for the report -->
		<fo:layout-master-set>
		
			<fo:simple-page-master page-width="8.5in" page-height="11in" master-name="all-pages">
				<fo:region-body margin-top="1.25in" margin-right="0.5in" margin-bottom="1in" margin-left="0.5in"/>
				<fo:region-before region-name="page-header" extent="2in" display-align="before"/>
				<fo:region-after region-name="page-footer" extent="0.5in" display-align="after"/>
			</fo:simple-page-master>
			
		</fo:layout-master-set>

		<!-- The 1st page sequence shows the pinout. -->
		<fo:page-sequence master-reference="all-pages">

			<!-- Define the header -->
			<fo:static-content flow-name="page-header">
            <!-- Output the brand -->
			   <fo:block role="brand" span="all" color="#ffffff" font-weight="bold"
                font-family="Helvetica" font-size="22px" background-color="#09367A"
                padding-top="13px" padding-bottom="13px" padding-left="10px"
                margin-left="0.5in" margin-right="0.5in" border-style="solid"
                border-width="1px" border-color="#09367A" space-before="0.25in">
                PSoC Designer
           <fo:inline role="trademark" vertical-align="super" font-size="6pt">TM</fo:inline>
            </fo:block>

            <!-- Output the copyright -->
            <fo:block role="copyright" text-align="right" font-size="7pt"
                padding-top="5px" margin-right="0.5in">
               Cypress Semiconductor Corporation Copyright 2006-2008
            </fo:block>
			</fo:static-content>

			<!-- Define the footer -->
			<fo:static-content flow-name="page-footer">
				<fo:block font-size="small" text-align="center" space-after.conditionality="retain" space-after="0.5in">- <fo:page-number/> -</fo:block>
			</fo:static-content>
			
			<fo:flow flow-name="xsl-region-body">
         
            <!-- Output the report title -->
            <fo:block role="reportTitle" font-size="22px" font-weight="bold"
                font-family="Helvetica">
               BOM
            </fo:block>

            <!-- Output the report build status -->
				<fo:table role="statusTable" padding-top="22px" table-layout="fixed" width="500px" border-collapse="separate"
                border-spacing="0px" border="0px" border-style="hidden">
										
					<fo:table-column column-width="85px"/>
					<fo:table-column column-width="415px"/>

					<fo:table-body start-indent="0pt" end-indent="0pt" text-indent="0pt" last-line-end-indent="0pt" text-align="start">
						<fo:table-row>
							<fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
								<fo:block>Last Built:</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0px" font-size="9pt">
								<fo:block><xsl:value-of select="//CMX_PROJECT/@BUILD_DATE" /></fo:block>
							</fo:table-cell>
					   </fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
								<fo:block>Project Path:</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0px" font-size="9pt">
								<fo:block><xsl:value-of select="//CMX_PROJECT/@PROJECT_PATH" /></fo:block>
							</fo:table-cell>
						</fo:table-row>
						<fo:table-row>
							<fo:table-cell padding="0px" font-size="9pt" font-weight="bold">
								<fo:block>Project Name:</fo:block>
							</fo:table-cell>
							<fo:table-cell padding="0px" font-size="9pt">
								<fo:block><xsl:value-of select="//CMX_PROJECT/@NAME" /></fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
            
            <!-- Table: BOM table -->
            <xsl:if test="count(//CMX_BOM_LIST/CMX_BOM) > 0" >
              <xsl:variable name="columnNames" select="//CMX_BOM_COLUMN/@NAME[not(. = preceding::CMX_BOM_COLUMN/@NAME)]" />

				   <fo:table padding-top="10px" table-layout="fixed" width="540px" border-collapse="separate"
                   border-spacing="0px" border="0px" border-style="hidden">

					   <fo:table-column column-width="140px"/>
					   <fo:table-column column-width="90px"/>
					   <fo:table-column column-width="80px"/>
					   <fo:table-column column-width="140px"/>
					   <fo:table-column column-width="40px"/>
             <fo:table-column column-width="50px"/>

             <!-- Print Column Headings  -->
             <fo:table-header>                    
                 <fo:table-row>
                    <xsl:for-each select="//CMX_BOM_COLUMN/@NAME[not(. = preceding::CMX_BOM_COLUMN/@NAME)] ">
                       <xsl:if test="name() != 'PIN_SELECT' and name() != 'PIN_DRIVE' and name() != 'PIN_INTERRUPT'">
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="white" font-family="Courier"
                              font-size="11px" background-color="#999999" padding-top="4px" padding-bottom="4px"
                              font-weight="bold" text-align="center" border="0.5px" border-style="solid"
                              border-color="white">
                             <fo:block><xsl:value-of select="."/></fo:block>
                          </fo:table-cell>
                       </xsl:if>
                    </xsl:for-each>
                 </fo:table-row>
             </fo:table-header>
            <fo:table-body>
               <xsl:for-each select="//CMX_BOM_LIST/CMX_BOM">
                  <fo:table-row>
                    <xsl:variable name="bomNode" select="." />
                    <xsl:variable name="deviceValues" select="./CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN" />
                    <xsl:for-each select="$columnNames">
                      <xsl:variable name="columnName" select="." />
                      <xsl:choose>
                        <xsl:when test="$columnName='Label'">
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                               text-align="center" border="0.5px" border-style="solid"
                               border-color="white" font-weight="bold">
                            <fo:block wrap-option="wrap">
                              <xsl:choose>
                                <xsl:when test="$bomNode/@INSTANCE_NAME='BASE_PROJECT'">
                                  <xsl:value-of select="//CMX_PROJECT/@NAME"/>_<xsl:value-of select="$bomNode/CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME='Label']/@CONTENTS"/>
                                </xsl:when>
                                <xsl:otherwise>
                                  <xsl:value-of select="$bomNode/@INSTANCE_NAME"/>_<xsl:value-of select="$bomNode/CMX_BOM_COLUMN_LIST/CMX_BOM_COLUMN[@NAME='Label']/@CONTENTS"/>
                                </xsl:otherwise>
                              </xsl:choose>
                            </fo:block>
                          </fo:table-cell>
                        </xsl:when>
                        <xsl:otherwise>
                          <fo:table-cell padding="0px" wrap-option="no-wrap" color="black" font-family="Courier"
                               font-size="8pt" background-color="#e5e5e5" padding-top="4px" padding-bottom="4px"
                               text-align="center" border="0.5px" border-style="solid"
                               border-color="white">
                            <fo:block wrap-option="wrap">
                              <xsl:attribute name="class">cell</xsl:attribute>
                              <xsl:value-of select="$deviceValues[@NAME = $columnName]/@CONTENTS"/>
                            </fo:block>
                          </fo:table-cell>
                        </xsl:otherwise>
                      </xsl:choose>
                     </xsl:for-each>
                    </fo:table-row>
                 </xsl:for-each>
              </fo:table-body>
            </fo:table>
          </xsl:if>
        </fo:flow>
      </fo:page-sequence>
	</fo:root>            

</xsl:template>

</xsl:stylesheet>
