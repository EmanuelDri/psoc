
;@CMX_FRAG_CMX_LCD_SHARED_CHAN_INIT
    // `@CHANNEL_TYPE` Initialization
    LCDStartSharedChan();
;@CMX_SEGMENT_END

;@CMX_FRAG_CMX_LCD_SHARED_CHAN_MAIN_INCLUDE
// `@CHANNEL_TYPE` Include
#include "cmx_lcd_chan.h"
;@CMX_SEGMENT_END
