<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 
  
<!-- Match the root node -->
<xsl:template match="/">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Select the nodes to apply the templates -->
<xsl:template match="@*|node()">
<xsl:copy>
	<xsl:apply-templates select="@*|node()"/>
</xsl:copy>
</xsl:template>



<!-- LCD Channel instance expansion -->
<xsl:template match="CMX_LCD_SHARED_CHANNEL_INSTANCE">
<xsl:element name="CMX_CHANNEL_INSTANCE">
<xsl:attribute name="INSTANCE_NAME"><xsl:value-of select="@INSTANCE_NAME"/></xsl:attribute>
<xsl:attribute name="IO_CHANNEL_TYPE_ORDINAL"><xsl:value-of select="@ORDINAL"/></xsl:attribute>
	<xsl:element name="CMX_USER_MODULE_SPEC_LIST">
	<xsl:element name="CMX_USER_MODULE_SPEC">
	<xsl:attribute name="UM_INSTANCE_NAME"><xsl:value-of select="@UM_INSTANCE_NAME"/></xsl:attribute>
	<xsl:attribute name="UM_NAME">LCD</xsl:attribute>

		<xsl:element name="PARAMETER_LIST">

		<xsl:element name="PARAMETER">
		<xsl:attribute name="NAME">LCDPort</xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:value-of select="@LCDPORT"/></xsl:attribute>
		</xsl:element>
		
		<xsl:element name="PARAMETER">
		<xsl:attribute name="NAME">BarGraph</xsl:attribute>
		<xsl:attribute name="VALUE"><xsl:value-of select="@BARGRAPH"/></xsl:attribute>
		</xsl:element>
		
		</xsl:element>
	</xsl:element>
	</xsl:element>

	
	<xsl:element name="CMX_CHANNEL_RESOURCE_LIST">	
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin11</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_0</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin12</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_1</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin13</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_2</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin14</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_3</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin6</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_4</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin4</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_5</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>
	
	<xsl:element name="CMX_CHANNEL_RESOURCE">
		<xsl:attribute name="NAME">LCDPin5</xsl:attribute>
		<xsl:attribute name="CMX_RESOURCE_NAME"><xsl:value-of select="@LCDPORT"/>_6</xsl:attribute>
		<xsl:attribute name="ASSIGNABLE">FALSE</xsl:attribute>
		<xsl:attribute name="PSOC_RESOURCE_TYPE">PIN</xsl:attribute>
	</xsl:element>   
	
	</xsl:element>   
		
	<xsl:element name="CMX_SUBSTITUTION_LIST"> 
	</xsl:element>

</xsl:element>

</xsl:template>

</xsl:stylesheet>

