//*****************************************************************************
//*****************************************************************************
//  FILENAME:  cmx_lcd_chan.c
//  @Version@
//  `@PSOC_VERSION`
//
//  DESCRIPTION:  Source file for the LCD channel.
//-----------------------------------------------------------------------------
//      Copyright (c) Cypress Semiconductor 2009. All Rights Reserved.
//*****************************************************************************
//*****************************************************************************
#include "lcd.h"
#include "stdlib.h"

//start function for all instance
void LCDStartSharedChan(void)
{
	LCD_Start(); //initializes LCD
}

//provide print integer value in decimal code
void LCDPrDecInt(int iValue)
{	
	//storage array converted value
	//5 cells for max or min integer> 1 cell for sign and last for null-terminated character 
	char cNumberString[7];
   	int i;
	
	//converts an integer value to a string
	itoa(cNumberString,iValue,10);
	//search the null-terminated character
	for(i=0;i<6;i++)
		//if  the null-terminated character is not located in last position
		if (cNumberString[i]==0x0) break;	
	//put the character ' ' in cells, so as to clear last cells
	while (i<6)
	{
		cNumberString[i]=' ';
		i++;
	}
	//put the null-terminated character in last position
	cNumberString[6]=0x0;
	//display string
	LCD_PrString(cNumberString);
}