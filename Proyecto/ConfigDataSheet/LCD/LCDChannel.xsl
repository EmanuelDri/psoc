<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<!-- Match the root node -->
<xsl:template match="/">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Select the nodes to apply the templates -->
<xsl:template match="@*|node()">
<xsl:copy>
	<xsl:apply-templates select="@*|node()"/>
</xsl:copy>
</xsl:template>

<!-- ONEWIRE Channel instance expansion -->
<xsl:template match="CMX_LCD_CHANNEL_INSTANCE">
<xsl:element name="CMX_CHANNEL_INSTANCE">
<xsl:attribute name="INSTANCE_NAME"><xsl:value-of select="@INSTANCE_NAME"/></xsl:attribute>
<xsl:attribute name="IO_CHANNEL_TYPE_ORDINAL"><xsl:value-of select="@ORDINAL"/></xsl:attribute>
	<xsl:element name="CMX_USER_MODULE_SPEC_LIST">
	</xsl:element>
	<xsl:element name="CMX_CHANNEL_RESOURCE_LIST">
	</xsl:element>
	<xsl:element name="CMX_SUBSTITUTION_LIST">
	</xsl:element>
	<xsl:element name="CMX_CHANNEL_RESOURCE_LIST">
	</xsl:element>
</xsl:element>
</xsl:template>

</xsl:stylesheet>