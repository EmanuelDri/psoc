//
//  SimTransferFunction.js
//

// variable declarations
var g_Output4;

// variable ID's
var g_ID_Output4 = 0;

// function parameter ID's


// function parameter defines


// function output states
// Output4 output state ID 
var	g_ID_Output4_UNASSIGNED = 255;


// variable output state declarations
// Output4 output state declarations
var	g_Output4__UNASSIGNED;


// function parameter declarations


var g_FunctionParams = new Array();

// current value array
var g_CurrentValues = new Array();

function TransferFunction()
{
	// load values from globals
	var ioInfo;
	ioInfo = GetIOInfoFromInstanceName("Output4");
	if (ioInfo != null)
	{
		g_Output4 = parseInt(ioInfo.sValue);
	}

	// update current value array
	g_CurrentValues[g_ID_Output4] = g_Output4;

	// call transfer functions
	Output4_TransferFunction();

	// set values in globals
	SetIOInfoValueFromInstanceName("Output4", g_Output4);

}

function Output4_TransferFunction()
{
		if (1)
	{
		g_Output4 = 2;
	}


	g_CurrentValues[g_ID_Output4] = g_Output4; 

}


function InitTransferFunctionGlobals()
{


	// variable output state initializations
	// Output4 output state initializations
	g_Output4__UNASSIGNED = 0;


	// variable initialization
	// initialize Output4 globals
    ioInfo = GetIOInfoFromInstanceName("Output4");
    if (ioInfo != null)
    {
		if ((ioInfo.sValue != null) && (ioInfo.sValue != ""))
		{
			g_Output4 = parseInt(ioInfo.sValue);
		}
		else
		{
			g_Output4 = -32768;
		}
    }
	else
	{
		g_Output4 = -32768;
	}

}

InitTransferFunctionGlobals();




function UpdateSimValues()
{
	// set values in globals
	SetIOInfoValueFromInstanceName("Output4", g_Output4);
}

// update the input widgets
function UpdateInputWidgets()
{
//	alert("updating input widgets");
	for(var j  in g_IOInfos)
	{
        if (g_IOInfos[j].sType == "IN" || (g_IOInfos[j].sType == "VARIABLE" && g_IOInfos[j].sDriver == "Variable_IN"))
        {
			var sInstanceName = g_IOInfos[j].sInstanceName;
			var sID	= IDFromInstanceName(sInstanceName)
            var sValue	= g_IOInfos[j].sValue;
		    var sDriver	= g_IOInfos[j].sDriver;
//			alert("updating " + sInstanceName + " = " + sValue);
            sValue = RawToDisplayValue( sID, sValue);
		    WidgetSetValue(sDriver, sID, sValue);
		}
	}
}

function IOState( strID, strValue)
{
    this.strID = strID;
    this.strValue = strValue;
}
