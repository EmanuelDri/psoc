;;*****************************************************************************
;;*****************************************************************************
;;  FILENAME:   SwitchCapConfig_1.asm
;;  Version: 1.10, Updated on 2015/3/4 at 22:27:44
;;  Generated by PSoC Designer 5.4.3191
;;
;;  DESCRIPTION: SwitchCapConfig User Module software implementation file.
;;-----------------------------------------------------------------------------
;;  Copyright (c) Cypress Semiconductor 2015. All Rights Reserved.
;;*****************************************************************************
;;*****************************************************************************

include "SwitchCapConfig_1.inc"
include "m8c.inc"
include "memory.inc"

;-----------------------------------------------
;  Global Symbols
;-----------------------------------------------
export  SwitchCapConfig_1_Start
export _SwitchCapConfig_1_Start
export  SwitchCapConfig_1_SetPower
export _SwitchCapConfig_1_SetPower
export  SwitchCapConfig_1_Stop
export _SwitchCapConfig_1_Stop
export  SwitchCapConfig_1_SetReference
export _SwitchCapConfig_1_SetReference
export  SwitchCapConfig_1_EnableInt
export _SwitchCapConfig_1_EnableInt
export  SwitchCapConfig_1_ClearInt
export _SwitchCapConfig_1_ClearInt
export  SwitchCapConfig_1_DisableInt
export _SwitchCapConfig_1_DisableInt

AREA UserModules (ROM, REL)

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_Start(BYTE bPowerSetting)
;  FUNCTION NAME: SwitchCapConfig_1_SetPower(BYTE bPowerSetting)
;
;  DESCRIPTION:
;    Applies power setting to the module's analog PSoC block.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  contains the power setting 0-3
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 SwitchCapConfig_1_Start:
_SwitchCapConfig_1_Start:
 SwitchCapConfig_1_SetPower:
_SwitchCapConfig_1_SetPower:
   RAM_PROLOGUE RAM_USE_CLASS_2
   and   A, SwitchCapConfig_1_POWERMASK
   push  X
   mov   X, SP
   push  A
   mov   A, reg[SwitchCapConfig_1_CR3]
   and   A, ~SwitchCapConfig_1_POWERMASK
   or    A, [X]
   mov   reg[SwitchCapConfig_1_CR3], A
   pop   A
   pop   X
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_Stop(void)
;
;  DESCRIPTION:
;    Removes power from the module's analog PSoC block
;-----------------------------------------------------------------------------
;
;  ARGUMENTS: None
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
 SwitchCapConfig_1_Stop:
_SwitchCapConfig_1_Stop:
   RAM_PROLOGUE RAM_USE_CLASS_1
   and   reg[SwitchCapConfig_1_CR3], ~SwitchCapConfig_1_POWERMASK
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_SetReference(BYTE bReference)
;
;  DESCRIPTION:
;    This function sets the reference of the comparator. Valid reference settings
;    are defined in the .inc file.
;
;-----------------------------------------------------------------------------
;
;  ARGUMENTS:
;    A  Contains reference settings.
;
;    Reference values shown are for example. (See .inc file for gain equates)
;
;  RETURNS:  NA
;
;  SIDE EFFECTS:
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.
;
;-----------------------------------------------------------------------------
; Stack offset constants
ARefMux: equ 1
ASign:   equ 0

 SwitchCapConfig_1_SetReference:
_SwitchCapConfig_1_SetReference:
   RAM_PROLOGUE RAM_USE_CLASS_2
   mov   X, SP                                             ; define temp store location
   push  A                                                 ; store argument value
   and   A, SwitchCapConfig_1_AREFMUXMASK                  ; mask ARefMux bitfield
   push  A                                                 ; store ARefMux value
   and   [X + ASign], SwitchCapConfig_1_ASIGNMASK          ; mask ASign bitfield
   mov   A, reg[SwitchCapConfig_1_CR3]                     ; read reg settings
   and   A, ~SwitchCapConfig_1_AREFMUXMASK                 ; clear ARefMux bitfield
   or    A, [X + ARefMux]                                  ; combine ARefMux value with balance of reg.
   mov   reg[SwitchCapConfig_1_CR3], A                     ; move complete value back to register
   mov   A, reg[SwitchCapConfig_1_CR0]                     ; read reg settings
   and   A, ~SwitchCapConfig_1_ASIGNMASK                   ; clear ASign bitfield
   or    A, [X + ASign]                                    ; combine ASign value with balance of reg.
   mov   reg[SwitchCapConfig_1_CR0], A                     ; move complete value back to register
   add   SP, -2                                            ; restore stack pointer
   RAM_EPILOGUE RAM_USE_CLASS_2
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_EnableInt(void)
;
;  DESCRIPTION:
;  Enables the Analog Column Interrupt
;-----------------------------------------------------------------------------
;  ARGUMENTS:    None.
;  RETURNS:      Nothing.
;  SIDE EFFECTS: 
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.

 SwitchCapConfig_1_EnableInt:
_SwitchCapConfig_1_EnableInt:
   RAM_PROLOGUE RAM_USE_CLASS_1
   M8C_EnableIntMask SwitchCapConfig_1_INT_REG, SwitchCapConfig_1_INT_MASK  ;enable ACol interrupt
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_DisableInt(void)
;
;  DESCRIPTION:
;  Disables the Analog Column Interrupt
;-----------------------------------------------------------------------------
;  ARGUMENTS:    None.
;  RETURNS:      Nothing.
;  SIDE EFFECTS: 
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.

 SwitchCapConfig_1_DisableInt:
_SwitchCapConfig_1_DisableInt:
   RAM_PROLOGUE RAM_USE_CLASS_1
   M8C_DisableIntMask SwitchCapConfig_1_INT_REG, SwitchCapConfig_1_INT_MASK  ;disable ACol interrupt
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

.SECTION
;-----------------------------------------------------------------------------
;  FUNCTION NAME: SwitchCapConfig_1_ClearInt(void)
;
;  DESCRIPTION:
;  Cleares the pending Analog Column Interrupt
;-----------------------------------------------------------------------------
;  ARGUMENTS:    None.
;  RETURNS:      Nothing.
;  SIDE EFFECTS: 
;    The A and X registers may be modified by this or future implementations
;    of this function.  The same is true for all RAM page pointer registers in
;    the Large Memory Model.  When necessary, it is the calling function's
;    responsibility to perserve their values across calls to fastcall16 
;    functions.

 SwitchCapConfig_1_ClearInt:
_SwitchCapConfig_1_ClearInt:
   RAM_PROLOGUE RAM_USE_CLASS_1
   M8C_ClearIntFlag SwitchCapConfig_1_INT_CLR_REG, SwitchCapConfig_1_INT_MASK  ;clearing ACol interrupt
   RAM_EPILOGUE RAM_USE_CLASS_1
   ret
.ENDSECTION

; End of File SwitchCapConfig_1.asm
